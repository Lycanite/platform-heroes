#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (LevelBase))]
public class LevelEditor : Editor {

	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		LevelBase level = (LevelBase)this.target;
		if (GUILayout.Button ("Finish Level"))
			level.Stats.FinishLevel ();
		if (GUILayout.Button ("Fail Level"))
			level.Stats.FailLevel ();
	}
}
#endif
