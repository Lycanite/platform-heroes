using UnityEngine;
using System.Collections.Generic;

public class LevelBase : MonoBehaviour {
	public static LevelBase Instance;

	// Level Info:
	public string ZoneName = "zone"; // Should be set to the zone that this level is in.
	public int LevelNumber = 1; // Should be set to the zone number of this level.
	
	// Music:
	public AudioClip LevelMusic; // The main music to play this level.
	public AudioClip ChaseMusic; // The music to play this level when in the Chase state.
	public AudioClip BossMusic; // The music to play this level when in the Boss state. If null, the default boss music from the ZoneManager is used instead.

	// Components:
	public LevelStats Stats; // The LevelStats component which stores the current state of the level.
	public LevelInfo LevelInfo; // The LevelInfo used by this level.
	public InterfaceHUDLevel LevelHUD; // The main HUD UI that dispalys the time, player stats, etc.

	// Type and State:
	public enum LevelType {
		Adventure,
		Arena
	};
	public LevelType CurrentLevelType = LevelType.Adventure; // The current type of level such as adventure or arena.
	
	// Players:
	public PlayerStats[] Players; // An array of all spawned player stats.
	public List<PlayerStats> LocalPlayers; // A list of all spawned player stats that are local to this machine (no remote online players).
	public int PlayerInfoVersion = 0;
	public bool AllPlayersDead = true; // Set to true when all players are dead.

	// Common Prefabs:
	public GameObject StatsPrefab; // The LevelStats prefab to use for storing the current state of this level (time, results, etc).
	public GameObject LevelHUDPrefab; // The prefab to use when generating the main level HUD.
	public GameObject PlayerStatsPrefab; // The PlayerStats prefab to spawn for each player in the level.
	public GameObject CameraTargetPrefab; // The prefab to use when creating the camera target.
	public GameObject CreatureSpawnerPrefab; // The default prefab to use when replacing creatures with spawners on startup.
	public GameObject ScorePickupPrefab; // The prefab to use when spawning score pickups (orbs).
	public GameObject SingleScorePickupPrefab; // The prefab to use when spawning non-multiplier score pickups (blood orbs).
	public GameObject CreatureDisplayPrefab; // The prefab to use when creating health displays for non-player creatures.
	public GameObject DamageCounterPrefab; // The prefab to use when displaying damage.

	// Main Objects:
	public CameraBase Camera; // The level's main camera.
	public GameObject CameraTargetObject; // The current target object that the level's main camera should follow.
	public InterfaceMain Interface; // The level interface.
	public SpawnPointBase StartingSpawnPoint; // The main starting point for adventure levels, etc.
	public List<SpawnPointBase> RandomSpawnPoints; // A list of random spawn oints for arena levels, etc.


	// ========== Get Instance ==========
	public static LevelBase Get () {
		if (Instance == null)
			Instance = GameObject.FindWithTag ("Level").GetComponent<LevelBase> ();
		return Instance;
	}
	
	
	// ========== Awake ==========
	public virtual void Awake () {
		if (Instance == null)
			Instance = this;

		// Camera Target:
		if (this.CameraTargetObject == null && Game.Get ().CurrentGameType == Game.GameType.Level)
			this.CameraTargetObject = GameObject.Instantiate (this.CameraTargetPrefab);

		if (Game.Get ().CurrentGameType == Game.GameType.Level)
			GameObject.Instantiate (this.LevelHUDPrefab);
	}


	// ========== Start ==========
	public virtual void Start () {
		ZoneInfo zone = ZoneManager.Get ().GetZone (this.ZoneName);
		if (zone != null)
			this.LevelInfo = zone.GetLevel (this.LevelNumber);
	}


	// ========== Update ==========
	public virtual void Update () {
		// Server Side:
		if (NetworkManager.Get ().ServerSide ()) {
			// Instantiate Stats:
			if (this.Stats == null) {
				GameObject.Instantiate (this.StatsPrefab);
				return;
			}

			// Instantiate Players:
			ClientManager clientManager = ClientManager.Get ();
			if (this.PlayerInfoVersion != clientManager.Version) {
				List<PlayerInfo> playerInfos = clientManager.GetAllPlayerInfos ();
				this.PlayerInfoVersion = clientManager.Version;
				//Dictionary<int, PlayerInfo>.ValueCollection localPlayerInfos = clientManager.GetLocalPlayerManager ().PlayerInfos.Values;

				// Empty Current Player Stats:
				if (this.Players != null && this.Players.Length > 0) {
					foreach (PlayerStats playerStats in this.Players) {
						if (playerStats != null)
							playerStats.Remove ();
					}
				}

				this.Players = new PlayerStats[playerInfos.Count];
				this.LocalPlayers = new List<PlayerStats> ();
				int i = 0;
				foreach (PlayerInfo playerInfo in playerInfos) {
					// Instantiate Player Stats:
					GameObject newPlayer = GameObject.Instantiate (this.PlayerStatsPrefab);
					newPlayer.transform.SetParent (this.transform, false);
					PlayerStats playerStats = newPlayer.GetComponent<PlayerStats> ();
					playerStats.Info = playerInfo;
					playerInfo.CurrentPlayerStats = playerStats;

					// Add To Player Arrays:
					this.Players [i] = playerStats;
					if (playerInfo.IsLocal ())
						this.LocalPlayers.Add (playerStats);

					i++;
				}
			}

			// Level Update:
			if (Game.Get ().CurrentGameType == Game.GameType.Level) {
				if (!this.Stats.LevelComplete)
					this.Stats.LevelUpdate ();
				else
					this.FinishUpdate ();
			}
		}

		// Client Side:
		else {
			// Empty Current Player Stats:
			if (this.Players != null && this.Players.Length > 0) {
				foreach (PlayerStats playerStats in this.Players) {
					if (playerStats != null)
						playerStats.Remove ();
				}
				this.Players = null;
			}
		}
	}


	// ========== Finish Update ==========
	public virtual void FinishUpdate () {
		this.Interface.OpenMenu ("complete");
	}


	// ========== Spawn Points ==========
	/** Returns a starting spawn point for the provided player, null can also be passed. **/
	public virtual SpawnPointBase GetSpawnPoint (PlayerStats player) {
		if (player == null)
			return this.StartingSpawnPoint;
		return this.StartingSpawnPoint;
	}

	/** Updates the provided Spawn point, this is used for arena matches where the sapwn points are randomized. **/
	public virtual SpawnPointBase UpdateSpawnPoint (SpawnPointBase spawnPoint) {
		if (this.CurrentLevelType == LevelType.Arena && this.RandomSpawnPoints.Count > 1) {
			int random = UnityEngine.Random.Range (0, this.RandomSpawnPoints.Count);
			return this.RandomSpawnPoints[random];
		}
		if (spawnPoint == null)
			return this.StartingSpawnPoint;
		return spawnPoint;
	}


	// ========== Check All Players Dead ==========
	/** Returns true if all players in the level are dead. **/
	public bool CheckAllPlayersDead () {
		this.AllPlayersDead = false;
		foreach (PlayerStats playerStat in this.Players) {
			if (playerStat.PlayerCreature != null && !playerStat.PlayerCreature.Dead) {
				return false;
			}
		}
		this.AllPlayersDead = true;
		return this.AllPlayersDead;
	}


	// ========== On Player Spawn ==========
	/** Called every time a player spawns in the level, used to update if all players are dead. **/
	public virtual void OnPlayerSpawn () {
		this.CheckAllPlayersDead ();
	}


	// ========== On Player Death ==========
	/** Called every time a player dies in the level, used to update if all players are dead and store the total death count. **/
	public virtual void OnPlayerDeath () {
		this.Stats.Deaths++;
		if (this.CheckAllPlayersDead ())
			this.OnLastPlayerDead ();
	}


	// ========== On Last Player Dead ==========
	/** Called when the last remaining player dies. **/
	public virtual void OnLastPlayerDead() {
		this.Stats.SetLevelState (LevelStats.LevelState.Normal);
	}


	// ========== Get Music ==========
	/** Returns the music that should be playing or null if LevelBase should not be in control of music. **/
	public virtual AudioClip GetMusic () {
		if (this.Stats == null)
			return this.LevelMusic;
		if (this.Stats.CurrentLevelState == LevelStats.LevelState.Boss) {
			if (this.BossMusic != null)
				return this.BossMusic;
			return ZoneManager.Get ().DefaultBossMusic;
		}
		if (this.Stats.CurrentLevelState == LevelStats.LevelState.Chase && this.ChaseMusic != null)
			return this.ChaseMusic;
		return this.LevelMusic;
	}
}
