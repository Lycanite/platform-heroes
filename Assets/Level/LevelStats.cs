using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class LevelStats : NetworkBehaviour {
	// Level State:
	public enum LevelState {
		Normal,
		Chase,
		Boss
	};
	[SyncVar] public LevelState CurrentLevelState = LevelState.Normal; // The current state that this level is in. Different states can affect how the level works.

	// Level Stats:
	[SyncVar] public float CurrentTime; // The current time that this level instance has been active for.
	[SyncVar] public float TimeLimit; // The time limit of this level (not used in every mode).
	public string DisplayTime = "00:00"; // The display time to show on the HUD.
	[SyncVar] public bool LevelComplete = false; // Set to true when the level is complete.
	public List<PlayerStats> Winners = new List<PlayerStats> (); // A list of all players who have won the current level (should be all players in Adventure Mode).
	public List<PlayerStats> SecretExits = new List<PlayerStats> (); // A list of players that finished the level via the secret exit.
	[SyncVar] public int Deaths = 0; // The total number of player deaths in this level instance.
	public LevelResults Results; // The results object generated on victory.

	// Local Stats:
	public bool CrestCollected = false; // True if the crest was collected by a local player (not online remote players).
	public bool KeyCollected = false; // True if the key was collected by a local player (not online remote players).

	// Objects:
	public LevelBase Level; // The LevelBase that this stats is attached to.
	public List<CreatureBase> BossCreatures; // A list of all boss creatures in this level. (Boss creatures should add themselves to this list).


	// ========== Start ==========
	public void Start () {
		this.Level = LevelBase.Get ();
		if (this.Level.Stats != null) {
			this.Level.Stats.Remove ();
		}
		this.Level.Stats = this;
		this.transform.SetParent (this.Level.transform, false);

		// Server:
		if (NetworkManager.Get ().ServerSide ()) {
			if (this.Level.CurrentLevelType == LevelBase.LevelType.Arena)
				this.TimeLimit = (float)(RulesManager.Get ().TimeLimit * 60);
			NetworkServer.Spawn (this.gameObject);
		}
	}


	// ========== Level Update ==========
	public void LevelUpdate () {
		if (NetworkManager.Get ().ServerSide ()) {

			// Adventure Complete Checks:
			if (this.Level.CurrentLevelType == LevelBase.LevelType.Adventure) {
				bool noLives = true;
				bool allPlayersExit = true;
				foreach (PlayerStats player in this.Level.Players) {
					if (player != null) {
						if (player.Lives > 0)
							noLives = false;
						if (!player.LevelExited)
							allPlayersExit = false;
					}
					if (!noLives && !allPlayersExit)
						break;
				}

				// No Lives:
				if (noLives) {
					this.FailLevel ();
				}

				// All Exit:
				if (allPlayersExit) {
					this.FinishLevel ();
					return;
				}
			}

			// Arena Victory Checks:
			else {

				// Deathmatch:
				if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Deathmatch) {
					// Kills:
					int targetKills = RulesManager.Get ().Kills;
					if (targetKills > 0) {
						foreach (PlayerStats playerStats in this.Level.Players) {
							if (playerStats.Kills >= targetKills) {
								this.Victory (playerStats);
								this.GenerateResults ();
							}
						}
					}

					// Time:
					if (this.TimeLimit > 0 && this.TimeLimit - this.CurrentTime <= 0) {
						int record = 0;
						List<PlayerStats> recordHolders = new List<PlayerStats> ();
						foreach (PlayerStats playerStats in this.Level.Players) {
							if (playerStats.Kills > record) {
								record = playerStats.Kills;
								recordHolders.Clear ();
								recordHolders.Add (playerStats);
							}
							else if (playerStats.Kills == record)
								recordHolders.Add (playerStats);
						}
						this.Victory (recordHolders);
						this.GenerateResults ();
					}
				}

				// Elimination:
				if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Elmination) {
					// Remaining Players:
					int remainingCount = 0;
					List<int> remainingTeams = new List<int> ();
					List<PlayerStats> remainingPlayers = new List<PlayerStats> ();
					foreach (PlayerStats playerStats in this.Level.Players) {
						if (playerStats.Lives > 0) {
							if (playerStats.Info.Team != 0 && !remainingTeams.Contains (playerStats.Info.Team)) {
								remainingCount++;
								remainingTeams.Add (playerStats.Info.Team);
								remainingPlayers.Add (playerStats);
							}
							else if (playerStats.Info.Team == 0) {
								remainingCount++;
								remainingPlayers.Add (playerStats);
							}
						}
					}
					if (remainingCount <= 1) {
						this.Victory (remainingPlayers);
						this.GenerateResults ();
					}

					// Time:
					if (this.TimeLimit > 0 && this.TimeLimit - this.CurrentTime <= 0) {
						int record = 0;
						List<PlayerStats> recordHolders = new List<PlayerStats> ();
						foreach (PlayerStats playerStats in this.Level.Players) {
							if (playerStats.Lives > record) {
								record = playerStats.Lives;
								recordHolders.Clear ();
								recordHolders.Add (playerStats);
							}
							else if (playerStats.Lives == record)
								recordHolders.Add (playerStats);
						}
						this.Victory (recordHolders);
						this.GenerateResults ();
					}
				}

				// Points:
				if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Points) {
					// Score:
					int targetScore = RulesManager.Get ().Score;
					if (targetScore > 0) {
						foreach (PlayerStats playerStats in this.Level.Players) {
							if (playerStats.Score >= targetScore) {
								this.Victory (playerStats);
								this.GenerateResults ();
							}
						}
					}

					// Time:
					if (this.TimeLimit > 0 && this.TimeLimit - this.CurrentTime <= 0) {
						int record = 0;
						List<PlayerStats> recordHolders = new List<PlayerStats> ();
						foreach (PlayerStats playerStats in this.Level.Players) {
							if (playerStats.Score > record) {
								record = playerStats.Score;
								recordHolders.Clear ();
								recordHolders.Add (playerStats);
							}
							else if (playerStats.Score == record)
								recordHolders.Add (playerStats);
						}
						this.Victory (recordHolders);
						this.GenerateResults ();
					}
				}
			}
		}

		// Update Time:
		this.CurrentTime += Time.deltaTime;
		float displayTime = this.CurrentTime;
		if (this.Level.CurrentLevelType == LevelBase.LevelType.Arena && this.TimeLimit > 0)
			displayTime = Mathf.RoundToInt (this.TimeLimit - this.CurrentTime);
		TimeSpan ts = TimeSpan.FromSeconds (Math.Ceiling (displayTime));
		this.DisplayTime = (ts.Hours > 0 ? ts.Hours.ToString ("00") + ":" : "") + ts.Minutes.ToString ("00") + ":" + ts.Seconds.ToString ("00");

		// Boss Creatures:
		if (this.CurrentLevelState == LevelState.Boss && this.BossCreatures != null) {
			foreach (CreatureBase bossCreature in this.BossCreatures.ToArray ()) {
				if (bossCreature.Dead || bossCreature == null) {
					this.BossCreatures.Remove (bossCreature);
				}
			}
		}
	}


	// ========== Remove ==========
	public void Remove () {
		GameObject.Destroy (this.gameObject);
	}


	// ========== Victory ==========
	public void Victory (PlayerStats playerStats) {
		this.Winners.Add (playerStats);
		this.LevelComplete = true;
	}

	public void Victory (List<PlayerStats> playerStats) {
		this.Winners = playerStats;
		this.LevelComplete = true;
	}


	// ========== Finish Level ==========
	/* Adds all players to the winners list and triggers a victory and results save. Used to finish adventure mode levels. */
	public void FinishLevel () {
		this.Victory (new List<PlayerStats> (this.Level.Players));
		this.GenerateResults ();
		this.SaveResults ();
	}


	// ========== Fail Level ==========
	/* Fails the level not triggering a victory and not saving any results. Used to fail adventure mode levels. */
	public void FailLevel () {
		this.LevelComplete = true;
		this.GenerateResults ();
	}


	// ========== Generate Results ==========
	/** Stores all level data into a LevelResults object. **/
	public void GenerateResults () {
		this.Results = new LevelResults (this.Level.LevelInfo);
		this.Results.FromLevelStats (this);
	}


	// ========== Save Results ==========
	/** Sends the results object to the local PlayerProgress. **/
	public void SaveResults () {
		if (this.Level.CurrentLevelType != LevelBase.LevelType.Adventure)
			return;
		this.Results.Save ();
	}


	// ========== Set Level State ==========
	/* Sets the current state that the level should be in. This can be used to enter boss fight modes or chase modes, etc. */
	public virtual void SetLevelState (LevelState targetState) {
		if (!NetworkManager.Get ().ServerSide ())
			return;
		this.CurrentLevelState = targetState;
	}


	// ========== Add Boss ==========
	/** Adds a creature to the level boss creatures list. **/
	public virtual void AddBoss (CreatureBase bossCreature) {
		if (this.BossCreatures.Contains (bossCreature))
			return;
		this.BossCreatures.Add (bossCreature);
	}
}
