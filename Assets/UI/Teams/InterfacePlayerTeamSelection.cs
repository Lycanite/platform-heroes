﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class InterfacePlayerTeamSelection : MonoBehaviour {
	
	public PlayerInfo PlayerInfo;
	public Text PlayerName;
	public InputField TeamInput;


	//========== Start ==========
	public void Start () {
		this.PlayerName.text = PlayerInfo.PlayerName + ":";
		this.TeamInput.onValueChanged.AddListener (delegate {
			string team = this.TeamInput.text;
			if (team == "")
				team = "0";
			this.PlayerInfo.SetPlayerTeam (Int32.Parse (team));
		});
	}
}
