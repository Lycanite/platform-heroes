﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class InterfaceTeams : MonoBehaviour {
	
	public List<GameObject> PlayerTeamSelections = new List<GameObject> ();
	public GameObject PlayerTeamSelectionPrefab;


	//========== Update ==========
	public void Update () {
		List<PlayerInfo> playerInfos = ClientManager.Get ().GetAllPlayerInfos ();

		// Update To Active Players:
		if (this.PlayerTeamSelections.Count != playerInfos.Count) {

			// Clean Old Selections On Player Change:
			foreach (GameObject playerTeamSelection in this.PlayerTeamSelections) {
				GameObject.Destroy (playerTeamSelection);
			}

			// Create Team Selection for Each player:
			this.PlayerTeamSelections = new List<GameObject> ();
			foreach (PlayerInfo playerInfo in playerInfos) {
				GameObject playerTeamSelectionObject = GameObject.Instantiate (this.PlayerTeamSelectionPrefab);
				playerTeamSelectionObject.transform.SetParent (this.transform, false);
				InterfacePlayerTeamSelection playerTeamSelection = playerTeamSelectionObject.GetComponent<InterfacePlayerTeamSelection> ();
				if (playerTeamSelection == null)
					continue;
				playerTeamSelection.PlayerInfo = playerInfo;
				PlayerTeamSelections.Add (playerTeamSelectionObject);
			}
		}
	}
}
