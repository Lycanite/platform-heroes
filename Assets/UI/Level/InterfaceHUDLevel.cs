using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceHUDLevel : MonoBehaviour {

	// Objects:
	public LevelBase Level;
	public GameObject PlayersGroup;
	public GameObject BossGroup;
	public Dictionary<CreatureBase, InterfaceHUDBoss> BossHUDs = new Dictionary<CreatureBase, InterfaceHUDBoss> ();

	// UI Objects:
	public Text TimeText;
	public Image Crest;
	public Image Key;

	// Prefabs:
	public GameObject BossHUDPrefab;


	//========== Start ==========
	public void Start () {
		this.Level = LevelBase.Get ();
		this.Level.LevelHUD = this;
	}


	//========== Update ==========
	public void Update () {
		if (Level.Stats == null)
			return;

		this.TimeText.text = Level.Stats.DisplayTime;
		this.Crest.gameObject.SetActive (Level.Stats.CrestCollected);
		this.Key.gameObject.SetActive (Level.Stats.KeyCollected);

		// Boss Creatures:
		if (this.Level.Stats.CurrentLevelState == LevelStats.LevelState.Boss && this.Level.Stats.BossCreatures != null) {
			foreach (CreatureBase bossCreature in this.Level.Stats.BossCreatures) {
				if (bossCreature == null || bossCreature.Dead || this.BossHUDs.ContainsKey (bossCreature))
					continue;
				GameObject bossHUDObject = GameObject.Instantiate (this.BossHUDPrefab);
				InterfaceHUDBoss bossHUD = bossHUDObject.GetComponent<InterfaceHUDBoss> ();
				bossHUD.BossCreature = bossCreature;
				this.BossHUDs.Add (bossCreature, bossHUD);
			}
		}
	}
}
