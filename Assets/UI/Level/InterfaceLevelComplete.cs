using System.Collections.Generic;
using UnityEngine;

public class InterfaceLevelComplete : MonoBehaviour {

	// Prefabs:
	public GameObject CrestPrefab;
	public GameObject KeyPrefab;
	public GameObject SecretPrefab;
	public GameObject AcedPrefab;
	public GameObject ScorePrefab;
	public GameObject TimePrefab;
	public GameObject SummaryPrefab;

	// Slides:
	public float SlideDuration = 5; // The time (seconds) that each slide is shown for. Stops on the last display.
	protected float SlideTimeActive = 0; // The current time (seconds) that the current slide has been shown for.
	protected int SlideIndex = 0; // The current slide index that is active.
	protected LevelResults Results; // The level results to display.
	protected List<GameObject> Slides = new List<GameObject> (); // A list of all GameObjects to show for each slide.
	protected GameObject CurrentSlide; // The current slide object that is being shown.
	

	//========== Update ==========
	public void Update () {
		// No Slides:
		if (this.Slides == null || this.Slides.Count <= 0)
			return;

		// Next Slide:
		if (this.SlideIndex < this.Slides.Count - 1 && this.SlideTimeActive >= this.SlideDuration) {
			GameObject.Destroy (this.CurrentSlide);
			this.CurrentSlide = null;
			this.SlideTimeActive = 0;
			this.SlideIndex++;
		}

		// Current Slide:
		if (this.CurrentSlide == null) {
			this.CurrentSlide = GameObject.Instantiate (Slides[this.SlideIndex]);
			Vector3 initScale = this.CurrentSlide.transform.localScale;
			this.CurrentSlide.transform.SetParent (this.transform);
			this.CurrentSlide.transform.localPosition = Vector3.zero;
			this.CurrentSlide.transform.localScale = initScale;
		}

		// Count Up:
		this.SlideTimeActive += Time.deltaTime;
	}


	//========== Reset ==========
	public void Reset () {
		this.Results = LevelBase.Get ().Stats.Results;
		this.Slides = new List<GameObject> ();

		// Collectables and Secret Exit:
		if (this.Results.CrestCollected)
			this.Slides.Add (this.CrestPrefab);
		if (this.Results.KeyCollected)
			this.Slides.Add (this.KeyPrefab);
		if (this.Results.SecretExit)
			this.Slides.Add (this.KeyPrefab);

		// Normal Exit Only:
		if (!this.Results.SecretExit) {
			if (this.Results.Aced)
				this.Slides.Add (this.AcedPrefab);
			this.Slides.Add (this.ScorePrefab);
			this.Slides.Add (this.TimePrefab);
		}

		// Summary:
		this.Slides.Add (this.SummaryPrefab);

		// Clear Current Slide:
		if (this.CurrentSlide != null) {
			GameObject.Destroy (this.CurrentSlide);
			this.CurrentSlide = null;
		}
		this.SlideTimeActive = 0;
		this.SlideIndex = 0;
	}
}
