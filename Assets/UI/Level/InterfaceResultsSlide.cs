using UnityEngine;
using UnityEngine.UI;

public class InterfaceResultsSlide : MonoBehaviour {

	// Type:
	public enum ResultsSlideType {
		Standard,
		Crest,
		Score,
		Time
	}
	public ResultsSlideType SlideType;

	// Components:
	protected LevelStats LevelStats;
	public Text Title;
	public Image Icon;
	protected InterfaceCounter Counter;

	// Audio:
	public AudioClip StartSound;


	//========== Start ==========
	public void Start () {
		this.LevelStats = LevelBase.Get ().Stats;
		if (this.Title == null)
			this.Title = this.GetComponent<Text> ();
		if (this.Icon == null)
			this.Icon = this.GetComponent<Image> ();

		// Crest:
		if (this.SlideType == ResultsSlideType.Crest) {
			this.StartSound = this.LevelStats.Level.LevelInfo.ZoneInfo.CrestSound;
		}

		// Score:
		if (this.SlideType == ResultsSlideType.Score) {
			this.Counter = this.Title.gameObject.AddComponent<InterfaceCounter> ();
			this.Counter.NumberType = InterfaceCounter.CounterType.Score;
			this.Counter.CountTarget = this.LevelStats.Results.Score;
			this.Counter.TextPrefix = this.Title.text;
		}

		// Time:
		if (this.SlideType == ResultsSlideType.Time) {
			this.Counter = this.Title.gameObject.AddComponent<InterfaceCounter> ();
			this.Counter.NumberType = InterfaceCounter.CounterType.Time;
			this.Counter.CountTarget = this.LevelStats.Results.Time;
			this.Counter.TextPrefix = this.Title.text;
			this.Icon.color = this.LevelStats.Level.LevelInfo.GetTimeRankColor (this.LevelStats.Results.Time, Color.grey);
		}

		// Start Sound:
		if (this.StartSound != null)
			this.LevelStats.Level.Camera.PlaySound (this.StartSound);
	}


	//========== Update ==========
	public void Update () {
		// Score:
		if (this.SlideType == ResultsSlideType.Score) {
			this.Icon.color = this.LevelStats.Level.LevelInfo.GetScoreRankColor (Mathf.RoundToInt (this.Counter.CountCurrent), Color.grey);
		}
	}
}
