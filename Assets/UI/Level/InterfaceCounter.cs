using System;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceCounter : MonoBehaviour {

	// Type:
	public enum CounterType {
		Score,
		Time
	}
	public CounterType NumberType;

	// Animation:
	public double CountTarget; // The target number to count up to.
	public float CountCurrent; // The current number counted up to.
	public float Duration = 3; // The time (seconds) it takes to count up to the target number.
	protected float TimeActive = 0; // The current time (seconds) that this counter has been active.

	// Components:
	public Text Text; // The text component to display the count on.
	public string TextPrefix = "";
	public string TextSuffix = "";

	//========== Start ==========
	public void Start () {
		if (this.Text == null)
			this.Text = this.GetComponent<Text> ();
	}


	//========== Update ==========
	public void Update () {
		float progress = this.TimeActive / this.Duration;
		this.CountCurrent = (float)this.CountTarget * progress;

		// Score:
		string displayCount = Mathf.RoundToInt (this.CountCurrent).ToString ();

		// Time:
		if (this.NumberType == CounterType.Time) {
			TimeSpan ts = TimeSpan.FromSeconds (Math.Ceiling (this.CountCurrent));
			displayCount  = (ts.Hours > 0 ? ts.Hours.ToString ("00") + ":" : "") + ts.Minutes.ToString ("00") + ":" + ts.Seconds.ToString ("00");
		}

		this.Text.text = this.TextPrefix + displayCount + this.TextSuffix;

		this.TimeActive = Mathf.Min (this.Duration, this.TimeActive + Time.deltaTime);
	}
}
