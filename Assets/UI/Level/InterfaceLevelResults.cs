﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class InterfaceLevelResults : MonoBehaviour {
	public bool ResultsRendered = false;

	// Objects:
	public LevelBase Level;
	public GameObject LevelComplete;
	public GameObject LevelFailed;
	public GameObject MatchResults;
	public Text MatchTitle;


	//========== Start ==========
	public void Start () {
		this.Level = LevelBase.Get ();
	}


	//========== Update ==========
	public void Update () {
		if (this.Level.Stats == null || !this.Level.Stats.LevelComplete) {
			this.ResultsRendered = false;
			return;
		}
		if (this.ResultsRendered)
			return;

		// Adventure Level:
		if (this.Level.CurrentLevelType == LevelBase.LevelType.Adventure) {
			if (this.Level.Stats.Results != null) {
				this.LevelComplete.SetActive (true);
				InterfaceLevelComplete levelComplete = this.LevelComplete.GetComponent<InterfaceLevelComplete> ();
				if (levelComplete != null)
					levelComplete.Reset ();
			}
			else {
				this.LevelFailed.SetActive (true);
			}
		}

		// Arena Match:
		else {
			if (this.Level.Stats.Winners.Count == 0)
				this.MatchTitle.text = "Draw!";
			else if (this.Level.Stats.Winners.Count == 1 && this.Level.Stats.Winners [0].Info.Team == 0)
				this.MatchTitle.text = this.Level.Stats.Winners [0].Info.PlayerName + " Wins!";
			else {
				List<int> winningTeams = new List<int> ();
				foreach (PlayerStats playerStats in this.Level.Stats.Winners) {
					if (!winningTeams.Contains (playerStats.Info.Team))
						winningTeams.Add (playerStats.Info.Team);
				}
				if (winningTeams.Count == 1)
					this.MatchTitle.text = "Team " + winningTeams[0] + " Wins!";
				else
					this.MatchTitle.text = "Draw!";
			}
			this.MatchResults.SetActive (true);
		}

		this.ResultsRendered = true;
	}
}
