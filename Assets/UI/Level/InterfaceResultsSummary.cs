using System;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceResultsSummary : MonoBehaviour {
	public LevelResults Results;
	public RulesManager Rules;
	
	public GameObject ResultsPrefab;
	public GameObject TimeTitle;
	public GameObject TimeResult;
	public GameObject[] ScoreTitles;
	public GameObject[] ScoreResults;
	public GameObject TotalScoreTitle;
	public GameObject TotalScoreResult;


	//========== Start ==========
	public void Start () {
		this.Results = LevelBase.Get ().Stats.Results;
		this.Rules = RulesManager.Get ();
	}


	//========== Update ==========
	public void Update () {
		if (this.ResultsPrefab == null)
			return;

		// Time:
		if (this.TimeTitle == null) {
			this.TimeTitle = GameObject.Instantiate (ResultsPrefab);
			this.TimeTitle.transform.SetParent (this.transform, false);
			this.TimeTitle.GetComponent<Text> ().text = "<b>Time:</b>";
		}
		if (this.TimeResult == null) {
			this.TimeResult = GameObject.Instantiate (ResultsPrefab);
			this.TimeResult.transform.SetParent (this.transform, false);
		}
		this.TimeResult.GetComponent<Text> ().text = this.FormatTime ((float)Math.Ceiling (this.Results.Time));

		// Create Score Titles and Results:
		PlayerStats[] players = LevelBase.Get ().Players;
		if (this.ScoreTitles.Length != players.Length) {
			this.ScoreTitles = new GameObject[players.Length];
			this.ScoreResults = new GameObject[players.Length];
		}

		// Match Type:
		string scoreType = "Score";
		if (LevelBase.Get ().CurrentLevelType == LevelBase.LevelType.Arena) {
			if (this.Rules.CurrentMatchType == RulesManager.MatchType.Deathmatch)
				scoreType = "Kills";
			else if (this.Rules.CurrentMatchType == RulesManager.MatchType.Elmination)
				scoreType = "Lives";
		}

		// Render For Each Player:
		int playerID = 0;
		foreach (PlayerStats player in players) {
			
			if (this.ScoreTitles[playerID] == null) {
				this.ScoreTitles[playerID] = GameObject.Instantiate (ResultsPrefab);
				this.ScoreTitles[playerID].transform.SetParent (this.transform, false);
				this.ScoreTitles[playerID].GetComponent<Text> ().text = "<b>" + player.Info.PlayerName + " " + scoreType + ":</b>";
			}
			if (this.ScoreResults[playerID] == null) {
				this.ScoreResults[playerID] = GameObject.Instantiate (ResultsPrefab);
				this.ScoreResults[playerID].transform.SetParent (this.transform, false);
			}

			string playerScore = "";
			if (scoreType == "Score") {
				playerScore = player.Score.ToString ();
				if (player.Deaths <= 0)
					playerScore += " (x2 ACE!)";
			}
			else if (scoreType == "Kills") {
				playerScore = player.Kills.ToString ();
			}
			else if (scoreType == "Lives") {
				playerScore = player.Lives.ToString ();
			}

			this.ScoreResults[playerID].GetComponent<Text> ().text = playerScore;
			playerID++;
		}

		// Total Score:
		if (this.TotalScoreTitle == null) {
			this.TotalScoreTitle = GameObject.Instantiate (this.ResultsPrefab);
			this.TotalScoreTitle.transform.SetParent (this.transform, false);
			this.TotalScoreTitle.GetComponent<Text> ().text = "<b>Total Score:</b>";
		}
		if (this.TotalScoreResult == null) {
			this.TotalScoreResult = GameObject.Instantiate (this.ResultsPrefab);
			this.TotalScoreResult.transform.SetParent (this.transform, false);
		}
		this.TotalScoreResult.GetComponent<Text> ().text = this.Results.Score.ToString ();
	}


	// ========== Format Time ==========
	/* Returns the time (seconds) as a timestamp string, will omit the hours if they are at 0. */
	public string FormatTime (float time) {
		TimeSpan ts = TimeSpan.FromSeconds (time);
		return (ts.Hours > 0 ? ts.Hours.ToString ("00") + ":" : "") + ts.Minutes.ToString ("00") + ":" + ts.Seconds.ToString ("00");
	}
}
