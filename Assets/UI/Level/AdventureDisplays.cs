using System.Collections.Generic;
using UnityEngine;

public class AdventureDisplays : MonoBehaviour {

	// Objects:
	public AdventureManager AdventureManager;
	public GameObject CrestDisplayParent;
	public GameObject KeyDisplayParent;

	// Key:
	public Mesh KeyMesh;
	public Material KeyMaterial;

	// Displays:
	protected List<GameObject> CrestDisplays = new List<GameObject> (); // A list of the current Crest display objects.
	protected GameObject KeyDisplay; // The current Key display object.
	protected string CurrentZone; // The name of the current zone being displayed.


	// ========== Update ==========
	public void Update () {
		// Hide:
		if (!this.AdventureManager.SelectionActive) {
			this.ClearDisplays ();
			this.CurrentZone = "";
			return;
		}

		// Zone Check:
		ZoneInfo zone = this.AdventureManager.GetSelectedZone ().GetZone ();
		if (this.CurrentZone != zone.Name) {
			this.ClearDisplays ();
			foreach (LevelInfo level in zone.Levels.Values) {
				if (level.GetProgress ().Crest)
					this.CrestDisplays.Add (this.GenerateDisplay (level.CrestMesh, level.CrestMaterial, this.CrestDisplayParent.transform));
			}
			if (zone.GetProgress ().Key)
				this.KeyDisplay = this.GenerateDisplay (this.KeyMesh, this.KeyMaterial, this.KeyDisplayParent.transform);
			this.CurrentZone = zone.Name;
		}
	}


	// ========== Generate Display ==========
	/* Generates a new display object. */
	public GameObject GenerateDisplay (Mesh mesh, Material material, Transform parent) {
		GameObject display = new GameObject ();
		display.transform.parent = parent;
		display.transform.localPosition = Vector3.zero;
		display.transform.localEulerAngles = Vector3.zero;
		display.transform.localScale = Vector3.one;
		display.layer = parent.gameObject.layer;
		MeshFilter filter = display.AddComponent<MeshFilter> ();
		filter.mesh = mesh;
		MeshRenderer renderer = display.AddComponent<MeshRenderer> ();
		renderer.material = material;
		return display;
	}


	// ========== Clear Displays ==========
	/* Removes all display objects. */
	public void ClearDisplays () {
		if (this.CrestDisplays.Count > 0) {
			foreach (GameObject display in this.CrestDisplays) {
				GameObject.Destroy (display);
			}
			this.CrestDisplays.Clear ();
		}
		if (this.KeyDisplay != null) {
			GameObject.Destroy (this.KeyDisplay);
			this.KeyDisplay = null;
		}
	}
}
