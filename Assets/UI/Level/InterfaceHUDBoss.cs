using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceHUDBoss : MonoBehaviour {

	// Objects:
	protected LevelBase Level;
	public CreatureBase BossCreature;

	// UI Objects:
	public Image Portrait;
	public Text NameText;
	public Image PhaseMax;
	public Image Phase;
	public Image HealthMax;
	public Image Health;


	//========== Start ==========
	public void Start () {
		this.Level = LevelBase.Get ();
		this.transform.SetParent (this.Level.LevelHUD.BossGroup.transform);
		this.NameText.text = new CultureInfo ("en-US").TextInfo.ToTitleCase (this.BossCreature.CreatureName);
		this.Portrait.sprite = this.BossCreature.Icon;
	}


	//========== Update ==========
	void Update () {
		if (this.BossCreature == null || this.BossCreature.Dead || this.Level.Stats.CurrentLevelState != LevelStats.LevelState.Boss) {
			GameObject.Destroy (this.gameObject);
			return;
		}

		// Phase Health:
		this.PhaseMax.rectTransform.sizeDelta = new Vector2 (this.HealthMax.rectTransform.sizeDelta.y * this.BossCreature.PhaseCount, this.PhaseMax.rectTransform.sizeDelta.y);
		this.Phase.rectTransform.sizeDelta = new Vector2 (this.Health.rectTransform.sizeDelta.y * (this.BossCreature.PhaseCount - this.BossCreature.Phase), this.Phase.rectTransform.sizeDelta.y);

		// Health Bar:
		float phasedHealth = (float)this.BossCreature.HealthMax.ValueCurrent / Mathf.Max (this.BossCreature.PhaseCount, 1);
		float healthNormal = (float)(this.BossCreature.Health % (phasedHealth + 0.01f)) / phasedHealth;
		this.Health.rectTransform.sizeDelta = new Vector2 (this.HealthMax.rectTransform.sizeDelta.x * healthNormal, this.Health.rectTransform.sizeDelta.y);
	}
}
