using UnityEngine;
using UnityEngine.UI;

public class InterfaceHUDPlayer : MonoBehaviour {

	// Objects:
	public LevelBase Level;
	public PlayerStats playerStats;

	// UI Objects:
	public Image Frame;
	public Image Portrait;
	public Text NameText;
	public Image Lives;
	public Text LivesText;
	public Image Score;
	public Text ScoreText;
	public Image Kills;
	public Text KillsText;
	public Image HealthMax;
	public Image Health;
	public Image PowerMax;
	public Image Power;

	// Sizes:
	private int MenuSize = 35;
	private int LevelSize = 100;


	//========== Start ==========
	public void Start () {
		this.Level = LevelBase.Get ();
		this.transform.SetParent (this.Level.LevelHUD.PlayersGroup.transform);

		// Display Setup:
		if (Game.Get ().CurrentGameType == Game.GameType.Level) {
			this.Frame.rectTransform.sizeDelta = new Vector2 (this.Frame.rectTransform.sizeDelta.x, LevelSize);
			this.Kills.gameObject.SetActive (false);
			this.KillsText.gameObject.SetActive (false);
			if (this.Level.CurrentLevelType == LevelBase.LevelType.Arena) {
				if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Deathmatch) {
					this.Kills.gameObject.SetActive (true);
					this.KillsText.gameObject.SetActive (true);
					this.Lives.gameObject.SetActive (false);
					this.LivesText.gameObject.SetActive (false);
				}
				if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Points) {
					this.Lives.gameObject.SetActive (false);
					this.LivesText.gameObject.SetActive (false);
				}
			}
		}
		else {
			this.Frame.rectTransform.sizeDelta = new Vector2 (this.Frame.rectTransform.sizeDelta.x, MenuSize);
			this.Lives.gameObject.SetActive (false);
			this.LivesText.gameObject.SetActive (false);
			this.Score.gameObject.SetActive (false);
			this.ScoreText.gameObject.SetActive (false);
			this.Kills.gameObject.SetActive (false);
			this.KillsText.gameObject.SetActive (false);
		}

		this.OnPlayerUpdate ();
	}


	//========== Update ==========
	public void Update () {
		this.LivesText.text = this.playerStats.Lives.ToString ();
		this.ScoreText.text = this.playerStats.Score.ToString ();
		this.KillsText.text = this.playerStats.Kills.ToString ();
		
		int healthMax = 0;
		int health = 0;
		int powerMax = 0;
		int power = 0;
		if (this.playerStats.PlayerCreature != null) {
			healthMax = this.playerStats.PlayerCreature.HealthMax.ValueCurrent;
			health = this.playerStats.PlayerCreature.Health;
			if (this.playerStats.PlayerCreature.Power != null) {
				PowerBase powerBase = this.playerStats.PlayerCreature.Power.GetChargePower ();
				if (powerBase != null) {
					powerMax = this.playerStats.PlayerCreature.Power.GetPowerEnergyMax (powerBase);
					power = this.playerStats.PlayerCreature.Power.GetPowerEnergy (powerBase);
					this.Power.overrideSprite = powerBase.Icon;
					this.Power.color = powerBase.Color;
					this.PowerMax.overrideSprite = powerBase.Icon;
				}
			}
		}
		
		this.HealthMax.rectTransform.sizeDelta = new Vector2 (this.HealthMax.rectTransform.sizeDelta.y * healthMax, this.HealthMax.rectTransform.sizeDelta.y);
		this.Health.rectTransform.sizeDelta = new Vector2 (this.Health.rectTransform.sizeDelta.y * health, this.Health.rectTransform.sizeDelta.y);
		this.PowerMax.rectTransform.sizeDelta = new Vector2 (this.PowerMax.rectTransform.sizeDelta.y * powerMax, this.PowerMax.rectTransform.sizeDelta.y);
		this.Power.rectTransform.sizeDelta = new Vector2 (this.Power.rectTransform.sizeDelta.y * power, this.Power.rectTransform.sizeDelta.y);
	}


	//========== Player Update ==========
	public void OnPlayerUpdate () {
		CreatureBase characterPrefabCreature = this.playerStats.Info.GetCharacterPrefab ().GetComponent<CreatureBase> ();
		this.Portrait.overrideSprite = characterPrefabCreature.Icon;
		this.NameText.text = this.playerStats.Info.PlayerName;
	}
}
