﻿using UnityEngine;
using UnityEngine.UI;

public class InterfaceWindow : MonoBehaviour {

	// Objects:
	public int CurrentViewID = -1;
	public GameObject CurrentView;
	public GameObject[] Views;


	//========== Start ==========
	public void Start () {
		this.CurrentViewID = -1;
		if (this.Views != null) {
			this.SetView (0);
		}
	}


	//========== Set Title ==========
	public void SetView (int viewID) {
		if (viewID == this.CurrentViewID || this.Views == null || viewID >= this.Views.Length)
			return;
		this.CurrentViewID = viewID;
		if (this.CurrentView != null)
			this.CurrentView.SetActive (false);
		this.CurrentView = this.Views[viewID];
		this.CurrentView.SetActive (true);
	}
}
