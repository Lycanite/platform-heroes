using System;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceLevelSelection : MonoBehaviour {

	// Objects:
	public AdventureManager AdventureManager;

	// Text:
	public Text LevelTitle;
	public Text ZoneTitle;
	public Text ZoneNumber;
	public Text BestScore;
	public Text TargetScore;
	public Text BestTime;
	public Text TargetTime;

	// Images:
	public Image Trophy;
	public Image Hourglass;
	public Image Aced;
	public Image Crest;
	public Image Key;

	// Buttons:
	public Button PreviousZoneButton;
	public Button PreviousLevelButton;
	public Button NextZoneButton;
	public Button NextLevelButton;

	public Text PreviousZoneButtonText;
	public Text PreviousLevelButtonText;
	public Text NextZoneButtonText;
	public Text NextLevelButtonText;


	// ========== Update ==========
	void Update () {
		ZoneInfo zoneInfo = this.AdventureManager.GetSelectedZone ().GetZone ();
		LevelInfo levelInfo = this.AdventureManager.GetSelectedLevel ().GetLevel ();

		ZoneProgress zoneProgress = zoneInfo.GetProgress ();
		LevelProgress levelProgress = levelInfo.GetProgress ();

		this.UpdateTitles (zoneInfo, levelInfo);
		this.UpdateNavigation (zoneInfo, levelInfo, zoneProgress, levelProgress);
		this.UpdateStats (zoneInfo, levelInfo, zoneProgress, levelProgress);
		this.UpdateIcons (zoneInfo, levelInfo, zoneProgress, levelProgress);
	}


	// ========== Update Titles ==========
	/* Updates the selected zone and levels titles. */
	void UpdateTitles (ZoneInfo zoneInfo, LevelInfo levelInfo) {
		this.LevelTitle.text = levelInfo.Name;
		this.LevelTitle.color = zoneInfo.Color;
		this.ZoneTitle.text = zoneInfo.Name;
		this.ZoneTitle.color = zoneInfo.Color;
		this.ZoneNumber.text = (this.AdventureManager.SeletedLevel + 1).ToString ("00");
		this.ZoneNumber.color = zoneInfo.Color;
	}


	// ========== Update Navigation ==========
	/* Updates the navigation buttons. */
	void UpdateNavigation (ZoneInfo zoneInfo, LevelInfo levelInfo, ZoneProgress zoneProgress, LevelProgress levelProgress) {
		// Update Zone Buttons:
		ZoneInfo nextZone = this.AdventureManager.HasNextZone () ? this.AdventureManager.GetNextZone ().GetZone () : null;
		ZoneInfo prevZone = this.AdventureManager.HasPreviousZone () ? this.AdventureManager.GetPreviousZone ().GetZone () : null;

		if (nextZone != null && nextZone.GetProgress ().Unlocked) {
			this.NextZoneButton.interactable = true;
			this.NextZoneButton.gameObject.SetActive (true);
			this.NextZoneButtonText.text = nextZone.Name + " Zone >";
		}
		else {
			this.NextZoneButton.interactable = false;
			this.NextZoneButton.gameObject.SetActive (false);
			this.NextZoneButtonText.text = "-->";
		}

		if (prevZone != null) {
			this.PreviousZoneButton.interactable = true;
			this.PreviousZoneButton.gameObject.SetActive (true);
			this.PreviousZoneButtonText.text = "< " + prevZone.Name + " Zone";
		}
		else {
			this.PreviousZoneButton.interactable = false;
			this.PreviousZoneButton.gameObject.SetActive (false);
			this.PreviousZoneButtonText.text = "<--";
		}

		// Update Level Buttons:
		LevelInfo nextLevel = this.AdventureManager.HasNextLevel () ? this.AdventureManager.GetNextLevel ().GetLevel () : null;
		LevelInfo prevLevel = this.AdventureManager.HasPreviousLevel () ? this.AdventureManager.GetPreviousLevel ().GetLevel () : null;

		if (nextLevel != null && nextLevel.GetProgress ().Unlocked) {
			this.NextLevelButton.interactable = true;
			this.NextLevelButton.gameObject.SetActive (true);
			this.NextLevelButtonText.text = nextLevel.Name + " >";
		}
		else {
			this.NextLevelButton.interactable = false;
			this.NextLevelButton.gameObject.SetActive (false);
			this.NextLevelButtonText.text = "-->";
		}

		if (prevLevel != null) {
			this.PreviousLevelButton.interactable = true;
			this.PreviousLevelButton.gameObject.SetActive (true);
			this.PreviousLevelButtonText.text = "< " + prevLevel.Name;
		}
		else {
			this.PreviousLevelButton.interactable = false;
			this.PreviousLevelButton.gameObject.SetActive (false);
			this.PreviousLevelButtonText.text = "<--";
		}
	}


	// ========== Update Stats ==========
	/* Updates the selected zone and level stats. */
	void UpdateStats (ZoneInfo zoneInfo, LevelInfo levelInfo, ZoneProgress zoneProgress, LevelProgress levelProgress) {
		// Incomplete Level:
		if (!levelProgress.Completed) {
			this.BestScore.text = "No Score";
			this.TargetScore.text = "Bronze Score: " + (levelInfo.ScoreGoals.Length > 0 ? levelInfo.ScoreGoals[0].ToString () : "Not Set");
			this.Trophy.color = Color.black;
			this.BestTime.text = "No Time Record";
			this.TargetTime.text = "Bronze Time: " + this.FormatTime (levelInfo.TimeGoals.Length > 0 ? levelInfo.TimeGoals[0] : 0);
			this.Hourglass.color = Color.black;
		}

		// Complete Level:
		else {
			string[] ranks = LevelResults.Ranks;
			Color[] colors = new Color[] { Color.black, Global.Get ().Bronze, Global.Get ().Silver, Global.Get ().Gold, Global.Get ().Platinum };

			// Score:
			int scoreBest = levelProgress.ScoreBest;
			this.BestScore.text = "Best Score: " + scoreBest.ToString ();
			for (int i = 0; i <= levelInfo.ScoreGoals.Length; i++) {
				if (i >= levelInfo.ScoreGoals.Length) {
					this.TargetScore.text = "";
					this.Trophy.color = colors[i];
					break;
				}
				if (scoreBest < levelInfo.ScoreGoals[i]) {
					this.TargetScore.text = ranks[i] + " Score: " + levelInfo.ScoreGoals[i].ToString ();
					this.Trophy.color = colors[i];
					break;
				}
			}

			// Time:
			float timeBest = (float)Math.Ceiling (levelProgress.TimeBest);
			if (timeBest <= 0) {
				this.BestTime.text = "No Time Record";
				this.TargetTime.text = "Bronze Time: " + this.FormatTime (levelInfo.TimeGoals.Length > 0 ? levelInfo.TimeGoals[0] : 0);
				this.Hourglass.color = Color.black;
			}
			else {
				this.BestTime.text = "Best Time: " + this.FormatTime (timeBest);
				for (int i = 0; i <= levelInfo.TimeGoals.Length; i++) {
					if (i >= levelInfo.ScoreGoals.Length) {
						this.TargetTime.text = "";
						this.Hourglass.color = colors[i];
						break;
					}
					if (timeBest > levelInfo.TimeGoals[i] || timeBest <= 0) {
						this.TargetTime.text = ranks[i] + " Time: " + this.FormatTime (levelInfo.TimeGoals[i]);
						this.Hourglass.color = colors[i];
						break;
					}
				}
			}
		}
	}


	// ========== Update Icons ==========
	/* Updates the selected zone and level icons. */
	void UpdateIcons (ZoneInfo zoneInfo, LevelInfo levelInfo, ZoneProgress zoneProgress, LevelProgress levelProgress) {
		this.Aced.color = levelProgress.Aced ? Color.white : Color.black;
		this.Crest.color = levelProgress.Crest ? Color.white : Color.black;
		if (levelInfo.HasKey) {
			this.Key.gameObject.SetActive (true);
			this.Key.color = zoneProgress.Key ? Color.white : Color.black;
		}
		else {
			this.Key.gameObject.SetActive (false);
		}
	}


	// ========== Format Time ==========
	/* Returns the time (seconds) as a timestamp string, will omit the hours if they are at 0. */
	public string FormatTime (float time) {
		TimeSpan ts = TimeSpan.FromSeconds (time);
		return (ts.Hours > 0 ? ts.Hours.ToString ("00") + ":" : "") + ts.Minutes.ToString ("00") + ":" + ts.Seconds.ToString ("00");
	}
}
