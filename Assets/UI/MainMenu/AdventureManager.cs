using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AdventureManager : NetworkBehaviour {

	// Zone Entry:
	[System.Serializable]
	public class LevelZone {
		public string Name = "zone";
		public List<LevelEntry> Levels = new List<LevelEntry> ();

		public ZoneInfo GetZone () {
			return ZoneManager.Get ().GetZone (this.Name);
		}
	}

	// Level Entry:
	[System.Serializable]
	public class LevelEntry {
		public int LevelNumber = 1;
		[System.NonSerialized] protected LevelZone Zone;
		public GameObject CameraTarget;
		
		public void SetZone (LevelZone zone) {
			this.Zone = zone;
		}

		public LevelInfo GetLevel () {
			return ZoneManager.Get ().GetZone (this.Zone.Name).GetLevel (this.LevelNumber);
		}
	}

	// Selection:
	public List<LevelZone> LevelZones = new List<LevelZone> ();
	[SyncVar] public int SeletedZone = 0;
	[SyncVar] public int SeletedLevel = 0;
	public bool SelectionActive = false; // Set to true when on the level selection menu.

	// Components:
	public ZoneManager ZoneManager;


	// ========== Start ==========
	public void Start () {
		this.ZoneManager = ZoneManager.Get ();

		// Automatically Set The Zone for Each Level:
		foreach (LevelZone zone in this.LevelZones) {
			foreach (LevelEntry level in zone.Levels) {
				level.SetZone (zone);
			}
		}
	}


	// ========== Update ==========
	public void Update () {
		// Adventure:
		if (LevelBase.Get ().Interface.CurrentMenuName == "adventure" && LevelZones.Count > 0) {
			if (LevelBase.Get ().Camera.State != CameraBase.CameraState.Target)
				LevelBase.Get ().Camera.SetState (CameraBase.CameraState.Target);
			LevelBase.Get ().Camera.FollowTarget = this.LevelZones[this.SeletedZone].Levels[this.SeletedLevel].CameraTarget;
			this.SelectionActive = true;
		}

		// Main:
		else {
			LevelBase.Get ().Camera.SetState (CameraBase.CameraState.Menu);
			this.SelectionActive = false;
		}
	}


	// ========== Next Zone ==========
	public void NextZone () {
		if (this.HasNextZone ()) {
			this.SeletedZone++;
			this.SeletedLevel = 0;
		}
	}

	public bool HasNextZone () {
		return this.SeletedZone < this.LevelZones.Count - 1;
	}

	public LevelZone GetNextZone () {
		if (!this.HasNextZone ())
			return null;
		return this.LevelZones[this.SeletedZone + 1];
	}


	// ========== Previous Zone ==========
	public void PreviousZone () {
		if (this.HasPreviousZone ()) {
			this.SeletedZone--;
			this.SeletedLevel = this.GetSelectedZone ().Levels.Count - 1;
		}
	}

	public bool HasPreviousZone () {
		return this.SeletedZone > 0;
	}

	public LevelZone GetPreviousZone () {
		if (!this.HasPreviousZone ())
			return null;
		return this.LevelZones[this.SeletedZone - 1];
	}


	// ========== Get Selected Zone ==========
	public LevelZone GetSelectedZone () {
		return this.LevelZones[this.SeletedZone];
	}


	// ========== Next Level ==========
	public void NextLevel () {
		if (this.HasNextLevel ())
			this.SeletedLevel++;
	}

	public bool HasNextLevel () {
		return this.SeletedLevel < this.LevelZones[this.SeletedZone].Levels.Count - 1;
	}

	public LevelEntry GetNextLevel () {
		if (!this.HasNextLevel ())
			return null;
		return this.GetSelectedZone ().Levels[this.SeletedLevel + 1];
	}


	// ========== Previous Level ==========
	public void PreviousLevel () {
		if (this.HasPreviousLevel ())
			this.SeletedLevel--;
	}

	public bool HasPreviousLevel () {
		return this.SeletedLevel > 0;
	}

	public LevelEntry GetPreviousLevel () {
		if (!this.HasPreviousLevel ())
			return null;
		return this.GetSelectedZone ().Levels[this.SeletedLevel - 1];
	}


	// ========== Get Selected Level ==========
	public LevelEntry GetSelectedLevel () {
		return this.GetSelectedZone ().Levels[this.SeletedLevel];
	}


	// ========== Play Selected Level ==========
	public void PlaySelectedLevel () {
		if (LevelBase.Get ().Interface.CurrentMenuName == "adventure") {
			Game.Get ().LoadLevel (this.GetSelectedLevel ().GetLevel ());
		}
	}
}
