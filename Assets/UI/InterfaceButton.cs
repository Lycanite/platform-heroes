﻿using UnityEngine;
using UnityEngine.UI;

public class InterfaceButton : MonoBehaviour {

	// Objects:
	public GameObject TitleObject;


	//========== Set Title ==========
	public void SetTitle (string title) {
		if (this.TitleObject == null)
			this.TitleObject = this.gameObject;
		Text titleText = TitleObject.GetComponent<Text> ();
		if (titleText == null)
			titleText = TitleObject.gameObject.AddComponent<Text> ();
		titleText.text = title;
	}


	//========== Add Command ==========
	public void AddCommand (string command) {
		Button button = this.GetComponent<Button> ();
		button.onClick.AddListener (delegate {
			LevelBase.Get ().Interface.Command (command);
		});
	}

	public void AddCloseCommand (GameObject closeObject) {
		Button button = this.GetComponent<Button> ();
		button.onClick.AddListener (delegate {
			LevelBase.Get ().Interface.DisableMenuObject (closeObject);
		});
	}
}
