﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking.Match;

public class InterfaceServerButton : MonoBehaviour {
	public int HostID;
	public Text Text;


	// ========== Start ==========
	public void Start () {
		Button button = this.GetComponent<Button> ();
		button.onClick.AddListener (delegate {
			NetworkManager.Get ().SelectMatch (this.HostID);
		});

		MatchInfoSnapshot match = NetworkManager.Get ().matches[this.HostID];
		if (match == null) {
			this.Text.text = "Unable to get match details.";
			return;
		}
		string buttonText = match.name;
		buttonText += " - Players " + match.currentSize + " / " + match.maxSize;
		if (match.isPrivate)
			buttonText += " [Private]";
		this.Text.text = buttonText;
	}
}
