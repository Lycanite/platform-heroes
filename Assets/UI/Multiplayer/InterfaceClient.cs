﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class InterfaceClient : MonoBehaviour {
	private NetworkManager NetworkManager;

	// Inputs:
	public Text Title;
	public Text LeaveButtonText;


	// ========== Start ==========
	public void Start () {
		this.NetworkManager = NetworkManager.Get ();
	}


	// ========== Update ==========
	public void Update () {
		if (this.NetworkManager.CurrentJoinState == NetworkManager.JoinState.Connecting) {
			this.Title.text = "Connecting To Host...";
			this.LeaveButtonText.text = "Cancel";
		}
		else if (this.NetworkManager.CurrentJoinState == NetworkManager.JoinState.Connected) {
			this.Title.text = "Connected To Host";
			this.LeaveButtonText.text = "Leave Game";
		}
		else if (this.NetworkManager.CurrentJoinState == NetworkManager.JoinState.Disconnecting) {
			this.Title.text = "Disconnecting From Host";
			this.LeaveButtonText.text = "Leave Game";
		}
		else {
			this.Title.text = "Disconnected From Host";
			this.LeaveButtonText.text = "Leave Game";
		}
	}
}
