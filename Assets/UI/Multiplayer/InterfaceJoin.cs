﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class InterfaceJoin : MonoBehaviour {
	private NetworkManager NetworkManager;

	// Inputs:
	public InputField ServerAddressInput;
	public InputField ServerPortInput;
	public InputField PasswordInput;


	// ========== Start ==========
	public void Start () {
		this.NetworkManager = NetworkManager.Get ();

		if (this.ServerAddressInput.text == "")
			this.ServerAddressInput.text = this.NetworkManager.GetAddress ();
		this.ServerAddressInput.onValueChanged.AddListener (delegate {
			this.OnServerAddressChange ();
		});

		if (this.ServerPortInput.text == "")
			this.ServerPortInput.text = this.NetworkManager.GetPort ().ToString ();
		this.ServerPortInput.onValueChanged.AddListener (delegate {
			this.OnServerPortChange ();
		});

		if (this.PasswordInput.text == "")
			this.PasswordInput.text = this.NetworkManager.GetPassword ().ToString ();
		this.PasswordInput.onValueChanged.AddListener (delegate {
			this.OnPasswordChange ();
		});
	}


	// ========== On Host Address Change ==========
	public void OnServerAddressChange () {
		string serverAddress = this.ServerAddressInput.text;
		if (serverAddress == "")
			serverAddress = "localhost";
		this.NetworkManager.SetAddress (serverAddress);
	}


	// ========== On Server Port Change ==========
	public void OnServerPortChange () {
		string port = this.ServerPortInput.text;
		if (port == "")
			port = "8";
		this.NetworkManager.SetPort (Math.Max (0, Int32.Parse (port)));
	}


	// ========== On Password ==========
	public void OnPasswordChange () {
		string password = this.PasswordInput.text;
		this.NetworkManager.SetPassword(password);
	}


	// ========== On Server Initialized ==========
	public void OnServerInitialized () {
		LevelBase.Get ().Interface.Command ("closemenu");
	}
}
