﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class InterfaceMultiplayer : MonoBehaviour {
	private NetworkManager NetworkManager;
	public InterfaceWindow Window = null;
	public int HostViewID = 2;
	public int ClientViewID = 4;

	public bool RefreshingHostList = true;
	public GameObject ServerList;
	public GameObject[] ServerButtons;
	public GameObject ServerButtonPrefab;


	// ========== Start ==========
	public void Start () {
		this.NetworkManager = NetworkManager.Get ();
		this.RefreshHostList ();
	}


	// ========== Update ==========
	public void Update () {
		if (this.Window != null) {
			if (!this.NetworkManager.Local ()) {
				if (!this.NetworkManager.Local () && this.NetworkManager.ServerSide ())
					this.Window.SetView (this.HostViewID);
				else if (!this.NetworkManager.Local () && this.NetworkManager.ClientSide ())
					this.Window.SetView (this.ClientViewID);
			}
			else if (this.Window.CurrentViewID == this.HostViewID || this.Window.CurrentViewID == this.ClientViewID)
				this.Window.SetView (0);
		}

		if (this.RefreshingHostList && !this.NetworkManager.PollingMatchList) {
			// Empty Current List:
			this.EmptyHostList ();

			// Create New Server list:
			if (this.NetworkManager.matches != null) {
				this.ServerButtons = new GameObject[this.NetworkManager.matches.Count];
				for (int i = 0; i < this.NetworkManager.matches.Count; i++) {
					GameObject serverButtonObject = GameObject.Instantiate (this.ServerButtonPrefab);
					serverButtonObject.transform.SetParent (this.ServerList.transform, false);
					InterfaceServerButton serverButton = serverButtonObject.GetComponent<InterfaceServerButton> ();
					serverButton.HostID = i;
					this.ServerButtons [i] = serverButtonObject;
				}
			}

			this.RefreshingHostList = false;
		}
	}


	// ========== Refresh Server List ==========
	public void RefreshHostList () {
		this.NetworkManager.RefreshMatchList ();
		this.EmptyHostList ();
		this.RefreshingHostList = true;
	}


	// ========== Empty Host List ==========
	public void EmptyHostList () {
		if (this.ServerButtons == null)
			return;
		foreach (GameObject serverButton in this.ServerButtons) {
			GameObject.Destroy (serverButton);
		}
	}
}
