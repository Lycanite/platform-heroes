﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class InterfaceHost : MonoBehaviour {
	private NetworkManager NetworkManager;

	// Inputs:
	public InputField ServerNameInput;
	public InputField ServerPortInput;
	public InputField MaxPlayersInput;
	public InputField PasswordInput;
	public Toggle PrivateToggle;


	// ========== Start ==========
	public void Start () {
		this.NetworkManager = NetworkManager.Get ();

		if (this.ServerNameInput.text == "")
			this.ServerNameInput.text = this.NetworkManager.GetMatchName ();
		this.ServerNameInput.onValueChanged.AddListener (delegate {
			this.OnServerNameChange ();
		});

		if (this.ServerPortInput.text == "")
			this.ServerPortInput.text = this.NetworkManager.RemotePort.ToString ();
		this.ServerPortInput.onValueChanged.AddListener (delegate {
			this.OnServerPortChange ();
		});

		if (this.MaxPlayersInput.text == "")
			this.MaxPlayersInput.text = this.NetworkManager.GetMaxConnections ().ToString ();
		this.MaxPlayersInput.onValueChanged.AddListener (delegate {
			this.OnMaxPlayersChange ();
		});

		if (this.PasswordInput.text == "")
			this.PasswordInput.text = this.NetworkManager.GetPassword ().ToString ();
		this.PasswordInput.onValueChanged.AddListener (delegate {
			this.OnPasswordChange ();
		});

		this.PrivateToggle.isOn = this.NetworkManager.Hidden;
		this.PrivateToggle.onValueChanged.AddListener (delegate {
			this.OnPrivateChange ();
		});
	}


	// ========== On Host Name Change ==========
	public void OnServerNameChange () {
		string serverName = this.ServerNameInput.text;
		if (serverName == "")
			serverName = "Network Game";
		this.NetworkManager.SetMatchName (serverName);
	}


	// ========== On Server Port Change ==========
	public void OnServerPortChange () {
		string port = this.ServerPortInput.text;
		if (port == "")
			port = "8";
		this.NetworkManager.SetPort (Math.Max (0, Int32.Parse (port)));
	}


	// ========== On Max Players Change ==========
	public void OnMaxPlayersChange () {
		string maxPlayers = this.MaxPlayersInput.text;
		if (maxPlayers == "")
			maxPlayers = "8";
		this.NetworkManager.SetMaxConnections (Math.Max (1, Int32.Parse (maxPlayers)));
	}


	// ========== On Password Change ==========
	public void OnPasswordChange () {
		string password = this.PasswordInput.text;
		this.NetworkManager.SetPassword(password);
	}


	// ========== On Private Change ==========
	public void OnPrivateChange () {
		this.NetworkManager.Hidden = this.PrivateToggle.isOn;
	}


	// ========== On Server Initialized ==========
	public void OnServerInitialized () {
		LevelBase.Get ().Interface.Command ("closemenu");
	}
}
