﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class InterfaceRules : MonoBehaviour {
	private RulesManager RulesManager;

	// Inputs:
	public InputField TimeInput;
	public InputField LivesInput;
	public InputField KillsInput;
	public InputField ScoreInput;
	public Toggle EnemiesToggle;


	// ========== Start ==========
	public void Start () {
		this.RulesManager = RulesManager.Get ();

		if (this.TimeInput.text == "")
			this.TimeInput.text = this.RulesManager.TimeLimit.ToString ();
		this.TimeInput.onValueChanged.AddListener (delegate {
			this.OnTimeChange ();
		});

		if (this.LivesInput.text == "")
			this.LivesInput.text = this.RulesManager.Lives.ToString ();
		this.LivesInput.onValueChanged.AddListener (delegate {
			this.OnLivesChange ();
		});

		if (this.KillsInput.text == "")
			this.KillsInput.text = this.RulesManager.Kills.ToString ();
		this.KillsInput.onValueChanged.AddListener (delegate {
			this.OnKillsChange ();
		});

		if (this.ScoreInput.text == "")
			this.ScoreInput.text = this.RulesManager.Score.ToString ();
		this.ScoreInput.onValueChanged.AddListener (delegate {
			this.OnScoreChange ();
		});

		this.EnemiesToggle.isOn = this.RulesManager.Enemies;
		this.EnemiesToggle.onValueChanged.AddListener (delegate {
			this.OnEnemiesChange ();
		});
	}


	// ========== On Time Change ==========
	public void OnTimeChange () {
		string timeLimit = this.TimeInput.text;
		if (timeLimit == "")
			timeLimit = "5";
		this.RulesManager.TimeLimit = Double.Parse (timeLimit);
	}


	// ========== On Lives Change ==========
	public void OnLivesChange () {
		string lives = this.LivesInput.text;
		if (lives == "")
			lives = "10";
		this.RulesManager.Lives = Int32.Parse (lives);
	}


	// ========== On Kills Change ==========
	public void OnKillsChange () {
		string kills = this.KillsInput.text;
		if (kills == "")
			kills = "10";
		this.RulesManager.Kills = Int32.Parse (kills);
	}


	// ========== On Score Change ==========
	public void OnScoreChange () {
		string score = this.ScoreInput.text;
		if (score == "")
			score = "5000";
		this.RulesManager.Score = Int32.Parse (score);
	}


	// ========== On Enemies Change ==========
	public void OnEnemiesChange () {
		this.RulesManager.Enemies = this.EnemiesToggle.isOn;
	}
}
