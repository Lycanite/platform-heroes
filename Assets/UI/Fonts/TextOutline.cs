using UnityEngine;

public class TextOutline : MonoBehaviour {
	// Style:
	public float pixelSize = 1;
	public Color outlineColor = Color.black;
	public bool resolutionDependant = false;
	public int doubleResolution = 1024;

	// Text Mesh:
	private TextMesh textMesh;
	private MeshRenderer meshRenderer;

	// ========== Start ==========
	void Start () {
		textMesh = this.GetComponent<TextMesh> ();
		meshRenderer = this.GetComponent<MeshRenderer> ();

		for (int i = 0; i < 8; i++) {
			GameObject outline = new GameObject ("TextOutline", typeof (TextMesh));
			outline.transform.parent = transform;
			outline.transform.localScale = new Vector3 (1, 1, 1);
			outline.layer = this.gameObject.layer;

			MeshRenderer otherMeshRenderer = outline.GetComponent<MeshRenderer> ();
			otherMeshRenderer.material = new Material (meshRenderer.material);
			otherMeshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			otherMeshRenderer.receiveShadows = false;
			otherMeshRenderer.sortingLayerID = meshRenderer.sortingLayerID;
			otherMeshRenderer.sortingLayerName = meshRenderer.sortingLayerName;
		}
	}


	// ========== Late Update ==========
	void LateUpdate () {
		Vector3 screenPoint = Camera.main.WorldToScreenPoint (transform.position);

		if (textMesh.color.a >= 1)
			outlineColor.a = textMesh.color.a;
		else
			outlineColor.a = textMesh.color.a / transform.childCount;

		// Copy Attributes
		for (int i = 0; i < transform.childCount; i++) {

			TextMesh other = transform.GetChild (i).GetComponent<TextMesh> ();
			other.color = outlineColor;
			other.text = textMesh.text;
			other.alignment = textMesh.alignment;
			other.anchor = textMesh.anchor;
			other.characterSize = textMesh.characterSize;
			other.font = textMesh.font;
			other.fontSize = textMesh.fontSize;
			other.fontStyle = textMesh.fontStyle;
			other.richText = textMesh.richText;
			other.tabSize = textMesh.tabSize;
			other.lineSpacing = textMesh.lineSpacing;
			other.offsetZ = textMesh.offsetZ;

			bool doublePixel = resolutionDependant && (Screen.width > doubleResolution || Screen.height > doubleResolution);
			Vector3 pixelOffset = GetOffset (i) * (doublePixel ? 2.0f * pixelSize : pixelSize);
			Vector3 worldPoint = Camera.main.ScreenToWorldPoint (screenPoint + pixelOffset);
			other.transform.position = worldPoint;

			MeshRenderer otherMeshRenderer = transform.GetChild (i).GetComponent<MeshRenderer> ();
			otherMeshRenderer.sortingLayerID = meshRenderer.sortingLayerID;
			otherMeshRenderer.sortingLayerName = meshRenderer.sortingLayerName;
		}
	}


	// ========== Get Offset ==========
	Vector3 GetOffset (int i) {
		int zOffset = 1;
		switch (i % 8) {
			case 0: return new Vector3 (0, 1, zOffset);
			case 1: return new Vector3 (1, 1, zOffset);
			case 2: return new Vector3 (1, 0, zOffset);
			case 3: return new Vector3 (1, -1, zOffset);
			case 4: return new Vector3 (0, -1, zOffset);
			case 5: return new Vector3 (-1, -1, zOffset);
			case 6: return new Vector3 (-1, 0, zOffset);
			case 7: return new Vector3 (-1, 1, zOffset);
			default: return new Vector3 (0, 0, zOffset);
		}
	}
}
