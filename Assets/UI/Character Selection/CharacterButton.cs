﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CharacterButton : MonoBehaviour {
	
	public GameObject CharacterPrefab;
	public CreatureBase CharacterPrefabCreature;
	public PlayerInfo PlayerInfo;

	public ToggleGroup ToggleGroup;
	public Toggle Toggle;
	public Image Portrait;
	public Color SelectedColor;


	//========== Start ==========
	public void Start () {
		this.CharacterPrefabCreature = this.CharacterPrefab.GetComponent<CreatureBase> ();
		this.Portrait.overrideSprite = this.CharacterPrefabCreature.Icon;
		this.Toggle.group = this.ToggleGroup;
		this.ToggleGroup.RegisterToggle (this.Toggle);
		if (this.PlayerInfo.Character == this.CharacterPrefabCreature.CreatureName)
			this.Toggle.isOn = true;
		this.Toggle.onValueChanged.AddListener (delegate {
			if (this.Toggle.isOn)
				LevelBase.Get ().Interface.Command ("characterselect " + this.PlayerInfo.ID.ToString () + " " + this.CharacterPrefabCreature.CreatureName);
		});
	}
}
