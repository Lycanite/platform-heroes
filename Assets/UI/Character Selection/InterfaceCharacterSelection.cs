﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class InterfaceCharacterSelection : MonoBehaviour {
	
	public List<GameObject> PlayerCharacterSelections = new List<GameObject> ();
	public GameObject PlayerCharacterSelectionPrefab;


	//========== Update ==========
	public void Update () {
		Dictionary<int, PlayerInfo>.ValueCollection playerInfos = ClientManager.Get ().GetLocalPlayerManager ().PlayerInfos.Values;

		// Update To Active Local Players:
		if (this.PlayerCharacterSelections.Count != playerInfos.Count) {

			// Clean Old Selection On Player Change:
			foreach (GameObject playerCharacterSelection in this.PlayerCharacterSelections) {
				GameObject.Destroy (playerCharacterSelection);
			}

			// Creature Character Selection for Each player:
			this.PlayerCharacterSelections = new List<GameObject> ();
			foreach (PlayerInfo playerInfo in playerInfos) {
				GameObject playerCharacterSelectionObject = GameObject.Instantiate (this.PlayerCharacterSelectionPrefab);
				playerCharacterSelectionObject.transform.SetParent (this.transform, false);
				InterfacePlayerCharacterSelection playerCharacterSelection = playerCharacterSelectionObject.GetComponent<InterfacePlayerCharacterSelection> ();
				if (playerCharacterSelection == null)
					continue;
				playerCharacterSelection.PlayerInfo = playerInfo;
				PlayerCharacterSelections.Add (playerCharacterSelectionObject);
			}
		}
	}
}
