﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class InterfacePlayerCharacterSelection : MonoBehaviour {
	
	public PlayerInfo PlayerInfo;
	public GameObject CharacterButtonPrefab;
	public Text PlayerName;


	//========== Start ==========
	public void Start () {
		this.PlayerName.text = PlayerInfo.PlayerName + ":";
		foreach (GameObject characterPrefab in Global.Get ().Characters.Values) {
			GameObject characterButtonObject = GameObject.Instantiate (this.CharacterButtonPrefab);
			characterButtonObject.transform.SetParent (this.transform, false);
			CharacterButton characterButton = characterButtonObject.GetComponent <CharacterButton> ();
			characterButton.CharacterPrefab = characterPrefab;
			characterButton.PlayerInfo = this.PlayerInfo;
			characterButton.ToggleGroup = this.GetComponent<ToggleGroup> ();
		}
	}


	//========== Update ==========
	public void Update () {
		
	}
}
