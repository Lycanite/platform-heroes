﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;

public class InterfaceMain : MonoBehaviour {
	private NetworkManager NetworkManager;

	public enum MenuState {
		Main,
		Menu,
		Paused,
		Game
	}
	public MenuState CurrentState = MenuState.Game;
	public MenuState PrevState = MenuState.Game;

	public GameObject CurrentMenu;
	public string CurrentMenuName;

	// UI Menus and Overlays:
	public GameObject GameOverlay;
	public GameObject MainMenu;
	public GameObject PauseMenu;
	public GameObject CompleteMenu;
	public GameObject SettingsMenu;
	public GameObject CharacterSelectionMenu;
	public GameObject MultiplayerMenu;
	public GameObject AdventureMenu;
	public GameObject ArenaMenu;
	public GameObject ChallengeMenu;

	// Popups:
	public GameObject PopupPrefab;
	public Dictionary<string, GameObject> ActivePopups = new Dictionary<string, GameObject> ();


	//========== Awake ==========
	public void Awake() {
		this.CloseMenu ();
	}


	//========== Start ==========
	public void Start() {
		LevelBase.Get ().Interface = this;
		this.NetworkManager = NetworkManager.Get ();
	}


	//========== String Equals ==========
	public static bool StringEquals(string a, string b) {
		return string.Equals (a, b, System.StringComparison.InvariantCultureIgnoreCase);
	}


	//========== Command ==========
	public void Command(string command) {
		string[] commandArgs = command.Split (new char[] {' '});

		// Set Menu State:
		if (StringEquals (commandArgs[0], "setmenustate")) {
			this.SetMenuState (commandArgs[1]);
			return;
		}

		// Open Menu:
		if (StringEquals (commandArgs[0], "openmenu")) {
			this.OpenMenu (commandArgs[1]);
			return;
		}

		// Close Menu:
		if (StringEquals (commandArgs[0], "closemenu")) {
			if (commandArgs.Length > 1) {
				this.CloseMenu (commandArgs [1]);
				return;
			}
			this.CloseMenu ();
			return;
		}

		// Quit Game:
		if (StringEquals (commandArgs[0], "quit")) {
			if (commandArgs.Length > 1 && StringEquals (commandArgs [1], "confirmed")) {
				Application.Quit ();
				#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
				#endif
				return;
			}
			string info = "Are you sure you want to quit Platform Heroes?! It is pretty awesome, even though it's pretty early in development! XD I jest! Also if this is a rage quit then you're doing it wrong, use Alt+F4 like a boss instead!";
			Dictionary<string, string> options = new Dictionary<string, string> ();
			options.Add ("-Yes-", "quit confirmed");
			this.OpenPopup ("quit", "Quit To Desktop?!", info, options, true, "-No-");
			return;
		}

		// Load Level:
		if (StringEquals (commandArgs[0], "levelselect")) {
			string level = commandArgs [1];
			if (level == "next" && LevelBase.Get ().LevelInfo != null) {
				Game.Get ().LoadLevel (LevelBase.Get ().LevelInfo.GetNextLevel ());
				return;
			}
			Game.Get ().LoadLevel (level);
			return;
		}

		// Restart Level:
		if (StringEquals (commandArgs[0], "levelrestart")) {
			if (commandArgs.Length > 1 && StringEquals (commandArgs [1], "confirmed")) {
				Game.Get ().LoadLevel (SceneManager.GetActiveScene ().name);
				return;
			}
			string info = "Do you want to restart this level?";
			Dictionary<string, string> options = new Dictionary<string, string> ();
			options.Add ("-Yes-", "levelrestart confirmed");
			this.OpenPopup ("quit", "Rstart Level?", info, options, true, "-No-");
			return;
		}

		// Quit Level:
		if (StringEquals (commandArgs[0], "levelquit")) {
			if (commandArgs.Length > 1 && StringEquals (commandArgs [1], "confirmed")) {
				Game.Get ().LoadLevel ("menu");
				return;
			}
			string info = "Do you want to quit this level and return to the main menu? If you have joined a multiplayer host, you will also disconnect.";
			Dictionary<string, string> options = new Dictionary<string, string> ();
			options.Add ("-Yes-", "levelquit confirmed");
			this.OpenPopup ("quit", "Quit To Menu?!", info, options, true, "-No-");
			return;
		}

		// Select Character:
		if (StringEquals (commandArgs[0], "characterselect")) {
			ClientManager.Get ().SelectCharacter (commandArgs[1], commandArgs[2]); // Player ID, Character Name.
			return;
		}

		// Add Gamepad:
		if (StringEquals (commandArgs[0], "addgamepad")) {
			if (commandArgs.Length == 1) {
				string info = "Please enter a gamepad ID. Ex: addgamepad 3. You can also add a player ID to change their gamepad ID. Ex: addgamepad 3 4 (to assign gamepad 3 to player 4).";
				Dictionary<string, string> options = new Dictionary<string, string> ();
				this.OpenPopup ("addgamepad", "Missing Command Arguments!", info, options, true, "-Ok-");
				return;
			}

			if (commandArgs.Length == 2) {
				Dictionary<string, string> options = new Dictionary<string, string> ();
				string info = "A new Gamepad (ID: " + commandArgs[1] + ") has been detected!";
				if (!ClientManager.Get ().Player1HasGamepad ()) {
					info += " Player 1 does not yet have a gamepad assigned, do you want to use this gamepad for Player 1 (in addition to Mouse + Keyboard) or do you want to add a new player?";
					options.Add ("-Use For Player 1-", "addgamepad " + commandArgs [1] + " 0");
				}
				options.Add ("-Add Player-", "addplayer " + commandArgs[1]);
				options.Add ("-Ignore Gamepad-", "ignoregamepad " + commandArgs[1]);
				this.OpenPopup ("addgamepad", "New Gamepad Added:", info, options, true, "-Cancel-");
				return;
			}

			ClientManager.Get ().SetPlayerGamepadID (Int32.Parse (commandArgs [2]), Int32.Parse (commandArgs [1])); // Arg2 Player ID then Arg1 Gamepad ID.
			return;
		}

		// Add Player:
		if (StringEquals (commandArgs [0], "addplayer")) {
			if (commandArgs.Length == 1) {
				string info = "A new player must have a gamepad ID assigned to them (except for player 1 as they can also use the mouse and keyboard). Ex: addplayer 2 (will add a new player with Gamepad ID 2 assigned).";
				Dictionary<string, string> options = new Dictionary<string, string> ();
				this.OpenPopup ("addgamepad", "Can't Add Player Without Gamepad!", info, options, true, "-Ok-");
				return;
			}
			ClientManager.Get ().AddPlayer (Int32.Parse (commandArgs [1]));
			return;
		}

		// Ignore Gamepad:
		if (StringEquals (commandArgs[0], "ignoregamepad")) {
			if (commandArgs.Length == 1) {
				string info = "Please state the Gamepad ID to ignore.";
				Dictionary<string, string> options = new Dictionary<string, string> ();
				this.OpenPopup ("addgamepad", "Can't Ignore Gamepad", info, options, true, "-Ok-");
				return;
			}
			ClientManager.Get ().IgnoreGamepadID (Int32.Parse (commandArgs[1]));
			return;
		}

		// Multiplayer:
		if (StringEquals (commandArgs[0], "multiplayer")) {
			if (StringEquals (commandArgs[1], "host")) {
				this.NetworkManager.Host ();
				return;
			}
			if (StringEquals (commandArgs[1], "join")) {
				if (commandArgs.Length > 2 && StringEquals (commandArgs[2], "direct")) {
					this.NetworkManager.Join (true);
					return;
				}
				this.NetworkManager.Join ();
				return;
			}
			if (StringEquals (commandArgs[1], "stop")) {
				this.NetworkManager.Stop ();
				return;
			}
			return;
		}

		// Rules:
		if (StringEquals (commandArgs[0], "setrule")) {
			if (commandArgs.Length < 2)
				return;
			if (StringEquals (commandArgs[1], "matchtype")) {
				if (commandArgs.Length < 3)
					return;
				RulesManager.Get ().SetMatchType (commandArgs[2]);
				return;
			}
		}
	}


	//========== Set Menu State ==========
	public void SetMenuState(string state) {
		MenuState targetState = this.CurrentState;

		// Assess Target State:
		if (StringEquals (state, "main")) {
			targetState = MenuState.Main;
			this.SetMenuObject (this.MainMenu);
		}
		else if (StringEquals (state, "menu"))
			targetState = MenuState.Menu;
		else if (StringEquals (state, "paused"))
			targetState = MenuState.Paused;
		else if (StringEquals (state, "game"))
			targetState = MenuState.Game;

		// Change State:
		if (targetState == this.CurrentState)
			return;
		this.PrevState = this.CurrentState;
		this.CurrentState = targetState;

		// Pause/Resume Game:
		if (this.CurrentState == MenuState.Paused || (this.CurrentState == MenuState.Menu && Game.Get ().CurrentGameType == Game.GameType.Level)) {
			// TODO Pause
		} else {
			// TODO Resume
		}
	}


	//========== Open Menu ==========
	public void OpenMenu(string menuName) {
		if (StringEquals (menuName, "none") || StringEquals (menuName, "main") || StringEquals (menuName, "game") || StringEquals (menuName, "resume")) {
			this.CloseMenu ();
		}
		else if (StringEquals (menuName, "pause") && this.PauseMenu != null) {
			this.SetMenuState ("paused");
			this.SetMenuObject (this.PauseMenu, menuName);
		}
		else if (StringEquals (menuName, "complete")) {
			this.SetMenuState ("paused");
			this.SetMenuObject (this.CompleteMenu, menuName);
		}
		else if (StringEquals (menuName, "settings")) {
			this.SetMenuState ("menu");
			this.SetMenuObject (this.SettingsMenu, menuName);
		}
		else if (StringEquals (menuName, "characterselection")) {
			this.SetMenuState ("menu");
			this.SetMenuObject (this.CharacterSelectionMenu, menuName);
		}
		else if (StringEquals (menuName, "multiplayer")) {
			this.SetMenuState ("menu");
			this.SetMenuObject (this.MultiplayerMenu, menuName);
		}
		else if (StringEquals (menuName, "adventure")) {
			this.SetMenuState ("menu");
			this.SetMenuObject (this.AdventureMenu, menuName);
		}
		else if (StringEquals (menuName, "arena")) {
			this.SetMenuState ("menu");
			this.SetMenuObject (this.ArenaMenu, menuName);
		}
		else if (StringEquals (menuName, "challenge")) {
			this.SetMenuState ("menu");
			this.SetMenuObject (this.ChallengeMenu, menuName);
		}
	}


	//========== Close Menu ==========
	public void CloseMenu(string menuname = "") {
		if (menuname != "" && menuname != this.CurrentMenuName)
			return;
		if (this.CurrentMenu != null) {
			this.DisableMenuObject (this.CurrentMenu);
			this.CurrentMenu = null;
			this.CurrentMenuName = "";
		}
		if (Game.Get ().CurrentGameType == Game.GameType.Menu)
			this.SetMenuState ("main");
		else
			this.SetMenuState ("game");
	}


	//========== Set Menu Object ==========
	public void SetMenuObject(GameObject menuObject, string menuName = "") {
		if (this.CurrentMenu == menuObject)
			return;
		this.DisableMenuObject (this.CurrentMenu);
		this.CurrentMenu = menuObject;
		this.EnableMenuObject (this.CurrentMenu);
		this.CurrentMenuName = menuName;
	}


	//========== Open Popup ==========
	/** Open Popup - Opens a new popup window, takes a title and info (strings) along with a Dictionary of string as button titles associated with commands. If close is true, a close button will be added using closeTitle as the title. **/
	public InterfacePopup OpenPopup(string popupID, string title, string info, Dictionary<string, string> options, bool close = false, string closeTitle = "Cancel") {
		if (this.ActivePopups.ContainsKey (popupID)) {
			this.DisableMenuObject (this.ActivePopups[popupID]);
		}
		GameObject popupObject = this.EnableMenuObject (this.PopupPrefab);
		if (popupObject == null)
			return null;
		InterfacePopup popup = popupObject.GetComponent<InterfacePopup> ();
		if (popup == null)
			return popup;

		popup.ID = popupID;
		popup.SetTitle (title);
		popup.SetInfo (info);
		popup.AddOptions (options);
		if (close)
			popup.AddCloseButton (closeTitle);
		this.ActivePopups.Add (popupID, popupObject);

		return popup;
	}


	//========== Enable Menu Object ==========
	public GameObject EnableMenuObject(GameObject menuObject) {
		InterfacePopup popup = menuObject.GetComponent<InterfacePopup> ();
		if (popup == null) {
			menuObject.SetActive (true);
			return menuObject;
		}

		GameObject popupObject = GameObject.Instantiate (menuObject);
		popupObject.transform.SetParent (this.transform, false);
		return popupObject;
	}


	//========== Disable Menu Object ==========
	public void DisableMenuObject(GameObject menuObject) {
		if (menuObject == null)
			return;
		menuObject.SetActive (false);
		InterfacePopup popup = menuObject.GetComponent<InterfacePopup> ();
		if (popup != null) {
			this.ActivePopups.Remove (popup.ID);
			GameObject.Destroy (menuObject);
		}
	}
}
