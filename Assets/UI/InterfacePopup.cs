﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class InterfacePopup : MonoBehaviour {
	public string ID = "";

	// Objects:
	public GameObject WindowObject;
	public GameObject TitleObject;
	public GameObject InfoObject;
	public GameObject OptionsObject;

	// Prefabs:
	public GameObject ButtonPrefab;

	// Buttons:
	public InterfaceButton CloseButton;


	//========== Set Title ==========
	public void SetTitle (string title) {
		Text titleText = TitleObject.GetComponent<Text> ();
		if (titleText == null)
			titleText = TitleObject.gameObject.AddComponent<Text> ();
		titleText.text = title;
	}


	//========== Set Info ==========
	public void SetInfo (string info) {
		Text infoText = InfoObject.GetComponent<Text> ();
		if (infoText == null)
			infoText = InfoObject.gameObject.AddComponent<Text> ();
		infoText.text = info;
	}


	//========== Add Options ==========
	public void AddOptions (Dictionary<string, string> options) {
		if (options != null) {
			foreach (KeyValuePair<string, string> option in options) {
				this.AddButton (option.Key, option.Value);
			}
		}
	}

	public void AddButton (string title, string command) {
		if (this.OptionsObject == null)
			this.OptionsObject = this.gameObject;

		GameObject buttonObject = GameObject.Instantiate (this.ButtonPrefab);
		buttonObject.transform.SetParent (this.OptionsObject.transform, false);

		InterfaceButton button = buttonObject.GetComponent<InterfaceButton> ();
		button.SetTitle (title);
		button.AddCommand (command);
		button.AddCloseCommand (this.gameObject);
	}

	public void AddCloseButton (string title) {
		if (this.OptionsObject == null)
			this.OptionsObject = this.gameObject;

		GameObject buttonObject = GameObject.Instantiate (this.ButtonPrefab);
		buttonObject.transform.SetParent (this.OptionsObject.transform, false);

		InterfaceButton button = buttonObject.GetComponent<InterfaceButton> ();
		button.SetTitle (title);
		button.AddCloseCommand (this.gameObject);
	}

	public void AddCloseCommand (string command) {
		if (this.CloseButton == null)
			return;
		this.CloseButton.AddCommand (command);
	}
}
