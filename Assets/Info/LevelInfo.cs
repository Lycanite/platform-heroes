using UnityEngine;

[System.Serializable]
public class LevelInfo {
	[System.NonSerialized] public ZoneInfo ZoneInfo;

	// Info:
	public string Name = "Level Name";
	public int Index = 1;
	public int[] ScoreGoals = new int[4]; // 0 Bronze, 1 Silver, 2 Gold, 3 Platinum
	public float[] TimeGoals = new float[4]; // 0 Bronze, 1 Silver, 2 Gold, 3 Platinum
	public bool HasKey = false;
	public bool HasDoor = false;

	// Progress Defaults:
	public bool UnlockedByDefault = false;

	// Crest Display:
	public Mesh CrestMesh;
	public Material CrestMaterial;


	// ========== Init ==========
	/* Initializes this Level Info. */
	public void Init (ZoneInfo zoneInfo) {
		this.ZoneInfo = zoneInfo;
	}


	// ========== Get Progress ==========
	public LevelProgress GetProgress () {
		return ClientManager.Get ().GetLocalPlayerManager ().PlayerProgress.GetZoneProgress (this.ZoneInfo.Name.ToLower ()).LevelProgress[this.Index];
	}


	// ========== Get Next Level ==========
	public LevelInfo GetNextLevel () {
		LevelInfo nextLevel = this.ZoneInfo.GetLevel (this.Index + 1);
		if (nextLevel == null || !nextLevel.GetProgress ().Unlocked)
			return null;
		return nextLevel;
	}


	// ========== Get Scene Name ==========
	public string GetSceneName () {
		return this.ZoneInfo.Name + this.Index.ToString ("00");
	}


	// ========== Get Time Rank Color ==========
	public Color GetTimeRankColor (float time, Color noRankColor) {
		Color[] colors = new Color[] { noRankColor, Global.Get ().Bronze, Global.Get ().Silver, Global.Get ().Gold, Global.Get ().Platinum };

		for (int i = 0; i <= this.TimeGoals.Length; i++) {
			if (i >= this.ScoreGoals.Length) {
				return colors[i];
			}
			if (time > this.TimeGoals[i] || time <= 0) {
				return colors[i];
			}
		}

		return noRankColor;
	}


	// ========== Get Score Rank Color ==========
	public Color GetScoreRankColor (int score, Color noRankColor) {
		Color[] colors = new Color[] { noRankColor, Global.Get ().Bronze, Global.Get ().Silver, Global.Get ().Gold, Global.Get ().Platinum };

		for (int i = 0; i <= this.ScoreGoals.Length; i++) {
			if (i >= this.ScoreGoals.Length) {
				return colors[i];
			}
			if (score < this.ScoreGoals[i]) {
				return colors[i];
			}
		}

		return noRankColor;
	}
}
