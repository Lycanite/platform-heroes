using UnityEngine;

[System.Serializable]
public class SkinInfo {
	public string Name = "";
	public GameObject Prefab;

	public bool UnockedByDefault = false;

	// ========== Method ==========
	public void Method () {

	}
}
