using System.Collections.Generic;
using UnityEngine;

public class ZoneManager : MonoBehaviour {
	protected static ZoneManager Instance;

	// Zones:
	public List<ZoneInfo> ZoneEntries = new List<ZoneInfo> ();
	public Dictionary<string, ZoneInfo> Zones = new Dictionary<string, ZoneInfo> ();

	// Music:
	public AudioClip DefaultBossMusic;


	// ========== Get ==========
	public static ZoneManager Get () {
		return Instance;
	}


	// ========== Awake ==========
	public void Awake () {
		if (Instance == null)
			Instance = this;

		// Add and Init Zones:
		foreach (ZoneInfo zoneInfo in this.ZoneEntries) {
			this.Zones.Add (zoneInfo.Name.ToLower (), zoneInfo);
			zoneInfo.Init ();
		}
	}


	// ========== Get Zone ==========
	public ZoneInfo GetZone (string name) {
		if (!this.Zones.ContainsKey (name))
			return null;
		return this.Zones[name];
	}
}
