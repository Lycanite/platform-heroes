using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ZoneInfo {
	// Info:
	public string Name = "ZoneName";
	public string ScenePrefix = "zonename";
	public Color Color;

	// Defaults:
	public bool UnlockedByDefault = false;

	// Sounds:
	public AudioClip CrestSound;

	// Levels:
	public Dictionary<int, LevelInfo> Levels = new Dictionary<int, LevelInfo> ();
	public List<LevelInfo> LevelEntries = new List<LevelInfo> ();

	// ========== Init ==========
	/* Initializes this Zone Info. */
	public void Init () {
		foreach (LevelInfo levelInfo in this.LevelEntries) {
			this.Levels.Add (levelInfo.Index, levelInfo);
			levelInfo.Init (this);
		}
	}


	// ========== Get Level ==========
	public LevelInfo GetLevel (int index) {
		if (!this.Levels.ContainsKey (index))
			return null;
		return this.Levels[index];
	}


	// ========== Get Progress ==========
	public ZoneProgress GetProgress () {
		return ClientManager.Get ().GetLocalPlayerManager ().PlayerProgress.GetZoneProgress (this.Name.ToLower ());
	}
}
