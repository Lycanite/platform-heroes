using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterInfo {
	public string Name = "Character";
	public GameObject Prefab;
	public Dictionary<string, SkinInfo> Skins = new Dictionary<string, SkinInfo> ();

	public bool UnlockedByDefault = false;

	// ========== Get Skin ==========
	public SkinInfo GetSkin (string name) {
		if (!this.Skins.ContainsKey (name))
			return null;
		return this.Skins[name];
	}
}
