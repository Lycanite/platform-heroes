using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : MonoBehaviour {
	protected static CharacterManager Instance;

	public Dictionary<string, CharacterInfo> Characters = new Dictionary<string, CharacterInfo> ();

	// ========== Get ==========
	public static CharacterManager Get () {
		return Instance;
	}


	// ========== Awake ==========
	public void Awake () {
		if (Instance == null)
			Instance = this;
	}


	// ========== Get Character ==========
	public CharacterInfo GetCharacter (string name) {
		if (!this.Characters.ContainsKey (name))
			return null;
		return this.Characters[name];
	}
}
