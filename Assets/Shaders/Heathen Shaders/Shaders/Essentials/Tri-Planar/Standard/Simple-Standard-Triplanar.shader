Shader "Heathen/Essentials/Tri-Planar/Standard"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}
		
		_Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5
		[Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
		_MetallicGlossMap("Metallic", 2D) = "white" {}

		_BumpScale("Scale", Float) = 1.0
		_BumpMap("Normal Map", 2D) = "bump" {}

		_Parallax ("Height Scale", Range (0.005, 0.08)) = 0.02
		_ParallaxMap ("Height Map", 2D) = "black" {}

		_OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
		_OcclusionMap("Occlusion", 2D) = "white" {}

		_EmissionColor("Color", Color) = (0,0,0)
		_EmissionMap("Emission", 2D) = "white" {}
		
		_DetailMask("Detail Mask", 2D) = "white" {}

		_DetailAlbedoMap("Detail Albedo x2", 2D) = "grey" {}
		_DetailNormalMapScale("Scale", Float) = 1.0
		_DetailNormalMap("Normal Map", 2D) = "bump" {}

		[Enum(UV0,0,UV1,1)] _UVSec ("UV Set for secondary textures", Float) = 0

		// UI-only data
		[HideInInspector] _EmissionScaleUI("Scale", Float) = 0.0
		[HideInInspector] _EmissionColorUI("Color", Color) = (1,1,1)
		[HideInInspector] _MetalMode ("__metalMode", Float) = 0.0

		// Blending state
		[HideInInspector] _Mode ("__mode", Float) = 0.0
		[HideInInspector] _SrcBlend ("__src", Float) = 1.0
		[HideInInspector] _DstBlend ("__dst", Float) = 0.0
		[HideInInspector] _ZWrite ("__zw", Float) = 1.0
	}

	SubShader {
	Tags { "RenderType"="Opaque" }
		LOD 300
CGPROGRAM
#pragma surface surf Standard vertex:vert 
		#pragma target 4.0
		// needs more than 8 texcoords
		#pragma exclude_renderers gles
		#include "UnityPBSLighting.cginc"
	
	half4 _MainTex_ST;
	half4 _BumpMap_ST;
	fixed4 _Color;
	fixed4 _EmissionColor;
	sampler2D _MainTex;
	sampler2D _BumpMap;
	sampler2D _MetallicGlossMap;
	sampler2D _OcclusionMap;
	sampler2D _EmissionMap;
	float _BumpScale;
	float _Glossiness;
	float _OcclusionStrength;
	float _Metallic;
	float _MetalMode;

	struct Input {
			half4 xyv;
			half4 zvb;
			half4 xyb;
			half3 norm;
		};

	void vert (inout appdata_full v, out Input o) {
			o.xyv.xy = TRANSFORM_TEX(v.vertex.zy, _MainTex);
			o.xyv.zw = TRANSFORM_TEX(v.vertex.zx, _MainTex);
			o.zvb.xy = TRANSFORM_TEX(v.vertex.xy, _MainTex);
			o.xyb.xy = TRANSFORM_TEX(v.vertex.zy, _BumpMap);
			o.xyb.zw = TRANSFORM_TEX(v.vertex.zx, _BumpMap);
			o.zvb.zw = TRANSFORM_TEX(v.vertex.xy, _BumpMap);
			
			o.norm = (abs(v.normal));
			o.norm = (o.norm - 0.2) * 7;  
			o.norm = max(o.norm, 0);
			o.norm /= (o.norm.x + o.norm.y + o.norm.z ).xxx;   
		}

	void surf (Input IN, inout SurfaceOutputStandard o) {

			half3 norm = IN.norm;
			 
			half4 output = 
			tex2D(_MainTex, IN.xyv.xy)*norm.xxxx+
			tex2D(_MainTex, IN.xyv.zw)*norm.yyyy+
			tex2D(_MainTex, IN.zvb.xy)*norm.zzzz;
			
			o.Albedo = output.rgb  * _Color.rgb;
			o.Alpha = 1;//output.a;
			
			fixed3 tempNorm = UnpackNormal(tex2D(_BumpMap, IN.xyv.xy))*norm.xxx+
					UnpackNormal(tex2D(_BumpMap, IN.xyv.zw))*norm.yyy+
					UnpackNormal(tex2D(_BumpMap, IN.zvb.xy))*norm.zzz;
					
			tempNorm.z = tempNorm.z / (_BumpScale);
			
			o.Normal = normalize(tempNorm);
					
			fixed4 metal = 
			tex2D(_MetallicGlossMap, IN.xyv.xy)*norm.xxxx+
			tex2D(_MetallicGlossMap, IN.xyv.zw)*norm.yyyy+
			tex2D(_MetallicGlossMap, IN.zvb.xy)*norm.zzzz;
								
			if(_MetalMode < 0.5f)
			{
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
			}
			else
			{
				o.Metallic = metal.r;
				o.Smoothness = metal.a;
			}
			//_OcclusionMap
			//_OcclusionStrength
			o.Occlusion =
			(tex2D(_OcclusionMap, IN.xyv.xy)*norm.xxxx+
			tex2D(_OcclusionMap, IN.xyv.zw)*norm.yyyy+
			tex2D(_OcclusionMap, IN.zvb.xy)*norm.zzzz) * _OcclusionStrength;
			
			o.Emission = 
			(tex2D(_EmissionMap, IN.xyv.xy)*norm.xxxx+
			tex2D(_EmissionMap, IN.xyv.zw)*norm.yyyy+
			tex2D(_EmissionMap, IN.zvb.xy)*norm.zzzz) * _EmissionColor;
		}
ENDCG  
}

Fallback "Diffuse"
CustomEditor "HeathenStandardShaderGUI"
}
