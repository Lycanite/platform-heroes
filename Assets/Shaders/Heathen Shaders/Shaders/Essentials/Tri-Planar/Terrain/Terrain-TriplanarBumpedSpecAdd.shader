﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Heathen/Essentials/Tri-Planar/Terrain/Bumped Specular AddPass" {
Properties {
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125

	// set by terrain engine
	_Control ("Control (RGBA)", 2D) = "red" {}
	_Splat3 ("Layer 3 (A)", 2D) = "white" {}
	_Splat2 ("Layer 2 (B)", 2D) = "white" {}
	_Splat1 ("Layer 1 (G)", 2D) = "white" {}
	_Splat0 ("Layer 0 (R)", 2D) = "white" {}
	_Normal3 ("Normal 3 (A)", 2D) = "bump" {}
	_Normal2 ("Normal 2 (B)", 2D) = "bump" {}
	_Normal1 ("Normal 1 (G)", 2D) = "bump" {}
	_Normal0 ("Normal 0 (R)", 2D) = "bump" {}
}
	
SubShader {
	Tags {
		"SplatCount" = "4"
		"Queue" = "Geometry-99"
		"IgnoreProjector"="True"
		"RenderType" = "Opaque"
	}
CGPROGRAM
#pragma surface surf BlinnPhong vertex:vert decal:add
#pragma target 3.0

struct Input 
{
	float2 uv_Control : TEXCOORD0;
	//float2 uv_Splat0 : TEXCOORD1;
	//float2 uv_Splat1 : TEXCOORD2;
	//float2 uv_Splat2 : TEXCOORD3;
	//float2 uv_Splat3 : TEXCOORD4;
	float3	worldPos;
	float3	wNormal;
	float3	viewDir;
};

void vert (inout appdata_full v, out Input o) 
{
	UNITY_INITIALIZE_OUTPUT(Input,o);
	v.tangent.xyz = cross(v.normal, float3(0,0,1));
	v.tangent.w = -1;
	
	o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	o.wNormal = mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz;
	o.viewDir = normalize(ObjSpaceViewDir(v.vertex));
}

sampler2D _Control;
sampler2D _Splat0,_Splat1,_Splat2,_Splat3;
half4 _Splat0_ST, _Splat1_ST, _Splat2_ST, _Splat3_ST;
sampler2D _Normal0,_Normal1,_Normal2,_Normal3;
half _Shininess;

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 splat_control = tex2D (_Control, IN.uv_Control);
	float3 worldNormal = normalize(IN.wNormal);
	float3 projNormal = saturate(pow(worldNormal * 1.4, 4));
	
	float2 uvX, uvY, uvZ;
			
	//Set the base	 
	uvX = IN.worldPos.zy * 15/_Splat0_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat0_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat0_ST.xy;
			
	half4 layer1 =	
	tex2D(_Splat0, uvX)*projNormal.xxxx+
	tex2D(_Splat0, uvY)*projNormal.yyyy+
	tex2D(_Splat0, uvZ)*projNormal.zzzz;
	half4 layer1Normal =
	tex2D(_Normal0, uvX)*projNormal.xxxx+
	tex2D(_Normal0, uvY)*projNormal.yyyy+
	tex2D(_Normal0, uvZ)*projNormal.zzzz;
	
	uvX = IN.worldPos.zy * 15/_Splat1_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat1_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat1_ST.xy;
			
	half4 layer2 =	
	tex2D(_Splat1, uvX)*projNormal.xxxx+
	tex2D(_Splat1, uvY)*projNormal.yyyy+
	tex2D(_Splat1, uvZ)*projNormal.zzzz;
	half4 layer2Normal =
	tex2D(_Normal1, uvX)*projNormal.xxxx+
	tex2D(_Normal1, uvY)*projNormal.yyyy+
	tex2D(_Normal1, uvZ)*projNormal.zzzz;
	
	uvX = IN.worldPos.zy * 15/_Splat2_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat2_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat2_ST.xy;
			
	half4 layer3 =	
	tex2D(_Splat2, uvX)*projNormal.xxxx+
	tex2D(_Splat2, uvY)*projNormal.yyyy+
	tex2D(_Splat2, uvZ)*projNormal.zzzz;
	half4 layer3Normal =
	tex2D(_Normal2, uvX)*projNormal.xxxx+
	tex2D(_Normal2, uvY)*projNormal.yyyy+
	tex2D(_Normal2, uvZ)*projNormal.zzzz;
	
	uvX = IN.worldPos.zy * 15/_Splat3_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat3_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat3_ST.xy;
			
	half4 layer4 =	
	tex2D(_Splat3, uvX)*projNormal.xxxx+
	tex2D(_Splat3, uvY)*projNormal.yyyy+
	tex2D(_Splat3, uvZ)*projNormal.zzzz;
	half4 layer4Normal =
	tex2D(_Normal3, uvX)*projNormal.xxxx+
	tex2D(_Normal3, uvY)*projNormal.yyyy+
	tex2D(_Normal3, uvZ)*projNormal.zzzz;
			
	fixed4 col;
	col  = splat_control.r * layer1;
	col += splat_control.g * layer2;
	col += splat_control.b * layer3;
	col += splat_control.a * layer4;
	o.Albedo = col.rgb;

	fixed4 nrm;
	nrm  = splat_control.r * layer1Normal;
	nrm += splat_control.g * layer2Normal;
	nrm += splat_control.b * layer3Normal;
	nrm += splat_control.a * layer4Normal;
	nrm = normalize(nrm);
	// Sum of our four splat weights might not sum up to 1, in
	// case of more than 4 total splat maps. Need to lerp towards
	// "flat normal" in that case.
	fixed splatSum = dot(splat_control, fixed4(1,1,1,1));
	fixed4 flatNormal = fixed4(0.5,0.5,1,0.5); // this is "flat normal" in both DXT5nm and xyz*2-1 cases
	nrm = lerp(flatNormal, nrm, splatSum);
	o.Normal = UnpackNormal(nrm);

	o.Gloss = col.a * splatSum;
	o.Specular = _Shininess;

	o.Alpha = 0.0;
}
ENDCG  
}

Fallback "Hidden/TerrainEngine/Splatmap/Lightmap-AddPass"
}
