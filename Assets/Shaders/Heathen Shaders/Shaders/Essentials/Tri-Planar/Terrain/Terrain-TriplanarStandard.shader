﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Heathen/Essentials/Tri-Planar/Terrain/Standard" {
Properties {
	// set by terrain engine
	 _Control ("Control (RGBA)", 2D) = "red" {}
	 _Splat3 ("Layer 3 (A)", 2D) = "white" {}
	 _Splat2 ("Layer 2 (B)", 2D) = "white" {}
	 _Splat1 ("Layer 1 (G)", 2D) = "white" {}
	 _Splat0 ("Layer 0 (R)", 2D) = "white" {}
	 _Normal3 ("Normal 3 (A)", 2D) = "bump" {}
	 _Normal2 ("Normal 2 (B)", 2D) = "bump" {}
	 _Normal1 ("Normal 1 (G)", 2D) = "bump" {}
	 _Normal0 ("Normal 0 (R)", 2D) = "bump" {}
	[Gamma] _Metallic0 ("Metallic 0", Range(0.0, 1.0)) = 0.0	
	[Gamma] _Metallic1 ("Metallic 1", Range(0.0, 1.0)) = 0.0	
	[Gamma] _Metallic2 ("Metallic 2", Range(0.0, 1.0)) = 0.0	
	[Gamma] _Metallic3 ("Metallic 3", Range(0.0, 1.0)) = 0.0
	_Smoothness ("Smoothness", Range(0.0, 1.0)) = 0.0
	// used in fallback on old cards & base map
	[HideInInspector] _MainTex ("BaseMap (RGB)", 2D) = "white" {}
	[HideInInspector] _Color ("Main Color", Color) = (1,1,1,1)
}
	
SubShader {
	Tags {
		"SplatCount" = "4"
		"Queue" = "Geometry-100"
		"RenderType" = "Opaque"
	}
CGPROGRAM
#pragma surface surf Standard vertex:vert finalcolor:myfinal exclude_path:prepass exclude_path:deferred
		#pragma multi_compile_fog
		#pragma target 3.0
		// needs more than 8 texcoords
		#pragma exclude_renderers gles
		#include "UnityPBSLighting.cginc"

		#pragma multi_compile __ _TERRAIN_NORMAL_MAP
		#pragma multi_compile __ _TERRAIN_OVERRIDE_SMOOTHNESS

#ifdef _TERRAIN_OVERRIDE_SMOOTHNESS
	half _Smoothness;
#endif

struct Input 
{
	float3	worldPos;
	float3	wNormal;
	float2 uv_Splat0 : TEXCOORD0;
	float2 uv_Splat1 : TEXCOORD1;
	float2 uv_Splat2 : TEXCOORD2;
	float2 uv_Splat3 : TEXCOORD3;
	float2 tc_Control : TEXCOORD4;	// Not prefixing '_Contorl' with 'uv' allows a tighter packing of interpolators, which is necessary to support directional lightmap.
	UNITY_FOG_COORDS(5)
};

half _Metallic0;
half _Metallic1;
half _Metallic2;
half _Metallic3;

sampler2D _Control;
sampler2D _Splat0,_Splat1,_Splat2,_Splat3;
half4 _Control_ST, _Splat0_ST, _Splat1_ST, _Splat2_ST, _Splat3_ST;
sampler2D _Normal0,_Normal1,_Normal2,_Normal3;
half _Shininess;

void SplatmapApplyWeight(inout fixed4 color, fixed weight)
{
	color.rgb *= weight;
	color.a = 1.0f;
}

void SplatmapApplyFog(inout fixed4 color, Input IN)
{
	#ifdef TERRAIN_SPLAT_ADDPASS
		UNITY_APPLY_FOG_COLOR(IN.fogCoord, color, fixed4(0,0,0,0));
	#else
		UNITY_APPLY_FOG(IN.fogCoord, color);
	#endif
}
//end common emulation

void myfinal(Input IN, SurfaceOutputStandard o, inout fixed4 color)
{
	SplatmapApplyWeight(color, o.Alpha);
	SplatmapApplyFog(color, IN);
}

void vert (inout appdata_full v, out Input o) 
{
	UNITY_INITIALIZE_OUTPUT(Input, o);
	o.tc_Control = TRANSFORM_TEX(v.texcoord, _Control);	// Need to manually transform uv here, as we choose not to use 'uv' prefix for this texcoord.
	float4 pos = UnityObjectToClipPos (v.vertex);
	UNITY_TRANSFER_FOG(data, pos);
	
#ifdef _TERRAIN_NORMAL_MAP
	v.tangent.xyz = cross(v.normal, float3(0,0,1));
	v.tangent.w = -1;
#endif
	
	o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
	o.wNormal = mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz;
}

void surf (Input IN, inout SurfaceOutputStandard o) {
	fixed4 splat_control = tex2D (_Control, IN.tc_Control);
	half weight;
	weight = dot(splat_control, half4(1,1,1,1));

	#ifndef UNITY_PASS_DEFERRED
		splat_control /= (weight + 1e-3f); // avoid NaNs in splat_control
	#endif

	#if !defined(SHADER_API_MOBILE) && defined(TERRAIN_SPLAT_ADDPASS)
		clip(weight - 0.0039 /*1/255*/);
	#endif
	
	float3 worldNormal = normalize(IN.wNormal);
	float3 projNormal = saturate(pow(worldNormal * 1.4, 4));
	
	float2 uvX, uvY, uvZ;
	half3 lightX;
	half3 lightY;
	half3 lightZ;
			
	//Set the base	 
	uvX = IN.worldPos.zy * 15/_Splat0_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat0_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat0_ST.xy;
			
	fixed4 layer1 =	
	tex2D(_Splat0, uvX)*projNormal.xxxx+
	tex2D(_Splat0, uvY)*projNormal.yyyy+
	tex2D(_Splat0, uvZ)*projNormal.zzzz;
	half4 layer1Normal =
	tex2D(_Normal0, uvX)*projNormal.xxxx+
	tex2D(_Normal0, uvY)*projNormal.yyyy+
	tex2D(_Normal0, uvZ)*projNormal.zzzz;
	
	uvX = IN.worldPos.zy * 15/_Splat1_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat1_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat1_ST.xy;
			
	fixed4 layer2 =	
	tex2D(_Splat1, uvX)*projNormal.xxxx+
	tex2D(_Splat1, uvY)*projNormal.yyyy+
	tex2D(_Splat1, uvZ)*projNormal.zzzz;
	half4 layer2Normal =
	tex2D(_Normal1, uvX)*projNormal.xxxx+
	tex2D(_Normal1, uvY)*projNormal.yyyy+
	tex2D(_Normal1, uvZ)*projNormal.zzzz;
	
	uvX = IN.worldPos.zy * 15/_Splat2_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat2_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat2_ST.xy;
			
	fixed4 layer3 =	
	tex2D(_Splat2, uvX)*projNormal.xxxx+
	tex2D(_Splat2, uvY)*projNormal.yyyy+
	tex2D(_Splat2, uvZ)*projNormal.zzzz;
	half4 layer3Normal =
	tex2D(_Normal2, uvX)*projNormal.xxxx+
	tex2D(_Normal2, uvY)*projNormal.yyyy+
	tex2D(_Normal2, uvZ)*projNormal.zzzz;
	
	uvX = IN.worldPos.zy * 15/_Splat3_ST.xy;
	uvY = IN.worldPos.xz * 15/_Splat3_ST.xy;
	uvZ = IN.worldPos.xy * 15/_Splat3_ST.xy;
			
	fixed4 layer4 =	
	tex2D(_Splat3, uvX)*projNormal.xxxx+
	tex2D(_Splat3, uvY)*projNormal.yyyy+
	tex2D(_Splat3, uvZ)*projNormal.zzzz;
	half4 layer4Normal =
	tex2D(_Normal3, uvX)*projNormal.xxxx+
	tex2D(_Normal3, uvY)*projNormal.yyyy+
	tex2D(_Normal3, uvZ)*projNormal.zzzz;
			
	fixed4 col = 0.0f;
	col  = splat_control.r * layer1;
	col += splat_control.g * layer2;
	col += splat_control.b * layer3;
	col += splat_control.a * layer4;
	o.Albedo = col.rgb;

	fixed4 nrm = 0.0f;
	nrm  = splat_control.r * layer1Normal;
	nrm += splat_control.g * layer2Normal;
	nrm += splat_control.b * layer3Normal;
	nrm += splat_control.a * layer4Normal;
	//nrm = normalize(nrm);
	// Sum of our four splat weights might not sum up to 1, in
	// case of more than 4 total splat maps. Need to lerp towards
	// "flat normal" in that case.
	fixed splatSum = dot(splat_control, fixed4(1,1,1,1));
	fixed4 flatNormal = fixed4(0.5,0.5,1,0.5); // this is "flat normal" in both DXT5nm and xyz*2-1 cases
	nrm = lerp(flatNormal, nrm, splatSum);
	o.Normal = UnpackNormal(normalize(nrm));
	
	o.Alpha = weight;
	
	#ifdef _TERRAIN_OVERRIDE_SMOOTHNESS
		o.Smoothness = _Smoothness;
	#else
		o.Smoothness = 0;//col.a;//*0.125f;
	#endif
	o.Metallic = dot(splat_control, half4(_Metallic0, _Metallic1, _Metallic2, _Metallic3));
}
ENDCG  
}

Dependency "AddPassShader" = "Heathen/Essentials/Tri-Planar/Terrain/Standard AddPass"
Dependency "BaseMapShader" = "Hidden/TerrainEngine/Splatmap/Standard-Base"
Dependency "Details0"      = "Hidden/TerrainEngine/Details/Vertexlit"
Dependency "Details1"      = "Hidden/TerrainEngine/Details/WavingDoublePass"
Dependency "Details2"      = "Hidden/TerrainEngine/Details/BillboardWavingDoublePass"
Dependency "Tree0"         = "Hidden/TerrainEngine/BillboardTree"

Fallback "Diffuse"
}
