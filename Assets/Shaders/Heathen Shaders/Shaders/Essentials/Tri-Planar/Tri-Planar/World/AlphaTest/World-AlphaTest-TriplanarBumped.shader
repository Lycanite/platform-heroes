// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

//Renders in world space such that faces looking down render the "Bottom" faces looking up render "Top" and Side renders on faces looking X and Z +/-
Shader "Heathen/Essentials/Tri-Planar/World/Cutout/Bumped" {
	Properties {
  		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_SideTexture ("Side", 2D) = "white" {}
		_SideBumpMap ("Side Normals", 2D) = "bump" {}
		_TopTexture ("Top", 2D) = "white" {}
		_TopBumpMap ("Top Normals", 2D) = "bump" {}
		_BottomTexture ("Bottom", 2D) = "white" {}
		_BottomBumpMap ("Bottom Normals", 2D) = "bump" {}
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="AlphaTest"}
		Cull off
		CGPROGRAM
			#pragma surface surf Lambert vertex:vert alphatest:_Cutoff
			#pragma exclude_renderers flash
			#pragma target 3.0
			
			sampler2D _SideTexture, _TopTexture, _BottomTexture;
			half4 _SideTexture_ST, _TopTexture_ST, _BottomTexture_ST;
			sampler2D _SideBumpMap, _TopBumpMap, _BottomBumpMap;
										
			struct Input {
				float3	worldPos;
				float3	wNormal;
				float3	viewDir;
				half4 sUV;		
				INTERNAL_DATA
			};
	
			void vert(inout appdata_full v, out Input o) {
				UNITY_INITIALIZE_OUTPUT(Input,o);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.wNormal = mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz;
				o.viewDir = normalize(ObjSpaceViewDir(v.vertex));	
				
				o.sUV.xy = TRANSFORM_TEX(v.vertex.zy, _SideTexture); //Side
				o.sUV.zw = TRANSFORM_TEX(v.vertex.zx, _TopTexture); //Top
			}
	
			void surf (Input IN, inout SurfaceOutput o) {
				float3 worldNormal = normalize(IN.wNormal);
				float3 projNormal = saturate(pow(worldNormal * 1.4, 4));
				
				float2 uv;
				
				float3 h;
				
				half4 albedoX;
				half4 albedoY;
				half4 albedoZ;
				half3 lightX;
				half3 lightY;
				half3 lightZ;
	
				// X 
				uv = IN.worldPos.zy*_SideTexture_ST.xy;
				albedoX = tex2D(_SideTexture, uv)*projNormal.xxxx;
				lightX = UnpackNormal(tex2D(_SideBumpMap, uv))*projNormal.xxx;
	
				// Y
				uv = IN.worldPos.xz*_TopTexture_ST.xy;//+IN.sUV.xz;					
				albedoY = tex2D(_TopTexture, uv)*projNormal.yyyy;
				lightY = UnpackNormal(tex2D(_TopBumpMap, uv))*projNormal.yyy;
	
				// Z
				uv = IN.worldPos.xy*_SideTexture_ST.xy;//+IN.sUV.xy;
				uv.x *= -1;
				albedoZ = tex2D(_SideTexture, uv)*projNormal.zzzz;
				lightZ = UnpackNormal(tex2D(_SideBumpMap, uv))*projNormal.zzz;
				
				//Apply
				half4 output = albedoZ;
				half2 uvBottom = IN.worldPos.xz*_BottomTexture_ST.xy;
				half4 bottomTex = tex2D(_BottomTexture, uvBottom);
				half4 bottomNormal = tex2D(_BottomBumpMap, uvBottom); 
				
				if(worldNormal.y > 0){
				  output = lerp(output, albedoY, projNormal.y); 
				} 
				else{
				  output = lerp(output, bottomTex, projNormal.y);
				  lightY = UnpackNormal(bottomNormal)*projNormal.yyy;
				}
				/*					
				if(worldNormal.y > 0){
					output = lerp(output, albedoY, projNormal.y);
				}
				else{
					uv = IN.worldPos.xz*_BottomTexture_ST.xy;
					output = lerp(output, tex2D(_BottomTexture, uv), projNormal.y);
					lightY = UnpackNormal(tex2D(_BottomBumpMap, uv))*projNormal.yyy; 
				}
				*/		
				output = lerp(output, albedoX, projNormal.x);
				o.Albedo = output.rgb;
				o.Alpha = output.a;
				o.Normal = (lightX+lightY+lightZ);
			}
	
		ENDCG
	}
	Fallback "Heathen/Essentials/Tri-Planar/World/Cutout/Diffuse"
}
