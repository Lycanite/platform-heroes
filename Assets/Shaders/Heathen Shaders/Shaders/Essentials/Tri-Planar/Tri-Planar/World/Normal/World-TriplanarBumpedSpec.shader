// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

//Renders in world space such that faces looking down render the "Bottom" faces looking up render "Top" and Side renders on faces looking X and Z +/-
Shader "Heathen/Essentials/Tri-Planar/World/Normal/Bumped Specular" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
  		_Shininess ("Shininess", Range (0.01, 1)) = 0.078125
		_SideTexture ("Side", 2D) = "white" {}
		_SideBumpMap ("Side Normals", 2D) = "bump" {}
		_TopTexture ("Top", 2D) = "white" {}
		_TopBumpMap ("Top Normals", 2D) = "bump" {}
		_BottomTexture ("Bottom", 2D) = "white" {}
		_BottomBumpMap ("Bottom Normals", 2D) = "bump" {}
	}
	SubShader {
		Tags {"Queue" = "Geometry-100" "RenderType" = "Opaque"}
		
		CGPROGRAM
			#pragma surface surf BlinnPhong vertex:vert
			#pragma exclude_renderers flash
			#pragma target 3.0
			
			sampler2D _SideTexture, _TopTexture, _BottomTexture;
			half4 _SideTexture_ST, _TopTexture_ST, _BottomTexture_ST;
			sampler2D _SideBumpMap, _TopBumpMap, _BottomBumpMap;
			half4 _Color;
			half _Shininess;
										
			struct Input {
				float3	worldPos;
				float3	wNormal;
				float3	viewDir;
				half4 sUV;		
				INTERNAL_DATA
			};
	
			void vert(inout appdata_full v, out Input o) {
				UNITY_INITIALIZE_OUTPUT(Input,o);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.wNormal = mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz;
				o.viewDir = normalize(ObjSpaceViewDir(v.vertex));	
				
				o.sUV.xy = TRANSFORM_TEX(v.vertex.zy, _SideTexture); //Side
				o.sUV.zw = TRANSFORM_TEX(v.vertex.zx, _TopTexture); //Top
			}
	
			void surf (Input IN, inout SurfaceOutput o) {
				float3 worldNormal = normalize(IN.wNormal);
				float3 projNormal = saturate(pow(worldNormal * 1.4, 4));
				
				float2 uv;
				
				float3 h;
				
				half4 albedoX;
				half4 albedoY;
				half4 albedoZ;
				half3 lightX;
				half3 lightY;
				half3 lightZ;
	
				// X 
				uv = IN.worldPos.zy*_SideTexture_ST.xy;
				albedoX = tex2D(_SideTexture, uv)*projNormal.xxxx;
				lightX = UnpackNormal(tex2D(_SideBumpMap, uv))*projNormal.xxx;
	
				// Y
				uv = IN.worldPos.xz*_TopTexture_ST.xy;//+IN.sUV.xz;					
				albedoY = tex2D(_TopTexture, uv)*projNormal.yyyy;
				lightY = UnpackNormal(tex2D(_TopBumpMap, uv))*projNormal.yyy;
	
				// Z
				uv = IN.worldPos.xy*_SideTexture_ST.xy;//+IN.sUV.xy;
				uv.x *= -1;
				albedoZ = tex2D(_SideTexture, uv)*projNormal.zzzz;
				lightZ = UnpackNormal(tex2D(_SideBumpMap, uv))*projNormal.zzz;
				
				//Apply
				half4 output = albedoZ;
									
				half2 uvBottom = IN.worldPos.xz*_BottomTexture_ST.xy;
				half4 bottomTex = tex2D(_BottomTexture, uvBottom);
				half4 bottomNormal = tex2D(_BottomBumpMap, uvBottom); 
				
				if(worldNormal.y > 0){
				  output = lerp(output, albedoY, projNormal.y); 
				} 
				else{
				  output = lerp(output, bottomTex, projNormal.y);
				  lightY = UnpackNormal(bottomNormal)*projNormal.yyy;
				}
						
				output = lerp(output, albedoX, projNormal.x);
				o.Albedo = output.rgb * _Color.rgb;
				o.Alpha = output.a * _Color.a;
				o.Gloss = output.a/256;
				o.Specular = _Shininess/2;
				o.Normal = (lightX+lightY+lightZ);
			}
	
		ENDCG
	}
	Fallback "Heathen/Essentials/Tri-Planar/World/Normal/Diffuse"
}
