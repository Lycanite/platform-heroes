// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

//Renders in world space such that faces looking down render the "Bottom" faces looking up render "Top" and Side renders on faces looking X and Z +/-
Shader "Heathen/Essentials/Tri-Planar/World/Cutout/Diffuse" {
	Properties {
  		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
		_SideTexture ("Side", 2D) = "white" {}
		_TopTexture ("Top", 2D) = "white" {}
		_BottomTexture ("Bottom", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="AlphaTest"}
		Cull off
		CGPROGRAM
			#pragma surface surf Lambert vertex:vert alphatest:_Cutoff
			#pragma target 3.0
	
			sampler2D _Control;
									
			struct Input {
				float3	worldPos;
				float3	worldNormal;
				float3	viewDir;
			};
	
			void vert(inout appdata_full v, out Input o) {
				UNITY_INITIALIZE_OUTPUT(Input,o);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.worldNormal = mul(unity_ObjectToWorld, float4(v.normal, 0.0)).xyz;
				o.viewDir = normalize(ObjSpaceViewDir(v.vertex));		
			}

			sampler2D _SideTexture, _TopTexture, _BottomTexture;
			half4 _SideTexture_ST, _TopTexture_ST, _BottomTexture_ST;
	
			void surf (Input IN, inout SurfaceOutput o) {
	
				float3 worldNormal = normalize(IN.worldNormal);
				float3 projNormal = saturate(pow(worldNormal * 1.4, 4));
				
				float2 uv;
				
				float3 h;
				
				half4 albedoX;
				half4 albedoY;
				half4 albedoZ;
				half3 lightX;
				half3 lightY;
				half3 lightZ;
	
				// X
				uv = IN.worldPos.zy;
				albedoX = tex2D(_SideTexture, uv);
				// Y
				uv = IN.worldPos.xz;		
				albedoY = tex2D(_TopTexture, uv);
				// Z
				uv = IN.worldPos.xy;
				uv.x *= -1;
				albedoZ = tex2D(_SideTexture, uv);
				
				// Apply to output
				half4 output = albedoZ;		
				half2 uvBottom = IN.worldPos.xz*_BottomTexture_ST.xy;					
				half4 bottomTex = tex2D(_BottomTexture, uvBottom);
				
				if(worldNormal.y > 0){
				  output = lerp(output, albedoY, projNormal.y); 
				} 
				else{
				  output = lerp(output, bottomTex, projNormal.y);
				}
				/*					
				if(worldNormal.y > 0){
					output = lerp(output, albedoY, projNormal.y);
				}
				else{
					output = lerp(output, tex2D(_BottomTexture, uv), projNormal.y);
				}
				*/			
				output = lerp(output, albedoX, projNormal.x);
				o.Albedo = output.rgb;
				o.Alpha = output.a;
			}
	
		ENDCG
	}
	Fallback "Heathen/Essentials/Tri-Planar/World/Normal/Diffuse"
}
