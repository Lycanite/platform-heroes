using UnityEngine;
using System.Collections.Generic;

public class PowerManager : MonoBehaviour {
	protected static PowerManager Instance;

	/** PowerID - An enum of all power IDs. **/
	public enum PowerID {
		Base,
		Blast,
		Strike,
		Bomb,
		Shield,
		Speed,
		Flight
	}
	
	/** PowerType - An enum of all power IDs. **/
	public enum PowerType {
		Charge,
		Energy,
		Duration
	}

	/** Powers - A list of all available powers. **/
	public Dictionary<PowerID, PowerBase> Powers = new Dictionary<PowerID, PowerBase> ();
	[System.Serializable]
	public class PowerEntry {
		public PowerID ID;
		public PowerType PowerType;
		public string Name; // Default name of power.
		public int AnimationID = 0; // ID used for specific animations.
		public GameObject EffectObject; // Special effects object spawned when power is granted.
		public Sprite Icon; // The image used for GUIs.
		public Color Color; // The color to use for the icons in the GUI.
	}
	public List<PowerEntry> PowerEntries = new List<PowerEntry> ();

	// ========== Get ==========
	public static PowerManager Get () {
		return Instance;
	}


	// ========== Awake ==========
	public virtual void Awake() {
		if (Instance == null)
			Instance = this;
		
		// Add Powers:
		foreach (PowerEntry powerEntry in this.PowerEntries) {
			if (powerEntry.PowerType == PowerType.Charge)
				this.Powers [powerEntry.ID] = PowerAttack.CreateInstance (powerEntry.ID, powerEntry.Name);
			else if (powerEntry.PowerType == PowerType.Energy)
				this.Powers [powerEntry.ID] = PowerEnergy.CreateInstance (powerEntry.ID, powerEntry.Name);
			else if (powerEntry.PowerType == PowerType.Duration)
				this.Powers [powerEntry.ID] = PowerDuration.CreateInstance (powerEntry.ID, powerEntry.Name);

			this.Powers [powerEntry.ID].AnimationID = powerEntry.AnimationID;
			this.Powers [powerEntry.ID].EffectObject = powerEntry.EffectObject;
			this.Powers [powerEntry.ID].Icon = powerEntry.Icon;
			this.Powers [powerEntry.ID].Color = powerEntry.Color;
		}
	}


	// ========== Get Power ==========
	public virtual PowerBase GetPower(PowerID powerID) {
		if (!this.Powers.ContainsKey (powerID))
			return null;
		return this.Powers[powerID];
	}
}
