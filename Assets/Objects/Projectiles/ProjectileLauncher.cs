using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProjectileLauncher : MonoBehaviour {

	public GameObject Owner;
	public Vector3 DirectionScale = new Vector3 (1, 1, 1);
	protected bool Dead = false;

	[System.Serializable]
	public class ProjectileLaunchDefinition {
		public GameObject Prefab;
		public Vector3 DirectionScale = new Vector3 (1, 1, 1);
	}
	public List<ProjectileLaunchDefinition> Projectiles = new List<ProjectileLaunchDefinition> ();
	public bool RandomList = false; // If false all projectiles will be launched at the same time each launch, if true, one projectile will be randomly selected from the list instead.

	// Repeating:
	public int LaunchAmount = 1; // How many times to launch projectiles.
	public float LaunchInterval = 0; // The time (seconds) between launches.
	protected float LaunchTime = 0; // The current time (seconds) until te next launch.

	// ========== Update ==========
	public void Update () {
		if (this.LaunchAmount <= 0) {
			if (!this.Dead) {
				this.Dead = true;
				GameObject.Destroy (this);
			}
			return;
		}

		this.LaunchTime -= Time.deltaTime;
		if (this.LaunchTime <= 0) {
			this.LaunchTime = this.LaunchInterval;
			this.LaunchAmount--;

			// Launch Projectiles:
			if (!this.RandomList) {
				foreach (ProjectileLaunchDefinition projectileLaunch in this.Projectiles) {
					this.LaunchProjectile (projectileLaunch);
				}
			}
			else {
				int index = 0;
				if (Projectiles.Count > 1)
					index = UnityEngine.Random.Range (0, Projectiles.Count - 1);
				this.LaunchProjectile (this.Projectiles[index]);
			}
		}
	}


	// ========== Launch Projectile ==========
	public void LaunchProjectile (ProjectileLaunchDefinition projectileLaunch) {
		GameObject projectileObject = GameObject.Instantiate (projectileLaunch.Prefab);
		projectileObject.transform.position = this.transform.position;

		// Projectile:
		ProjectileBase projectile = projectileObject.GetComponent<ProjectileBase> ();
		if (projectile != null) {
			projectile.Owner = this.Owner;
			Vector3 directionScale = this.DirectionScale;
			directionScale.Scale (projectileLaunch.DirectionScale);
			projectile.Direction.Scale (directionScale);
			if (NetworkManager.Get ().ServerSide ()) {
				NetworkServer.Spawn (projectileObject);
			}
			return;
		}

		// Projectile Launcher:
		ProjectileLauncher projectileLauncher = projectileObject.GetComponent<ProjectileLauncher> ();
		if (projectileLauncher != null) {
			projectileLauncher.Owner = this.Owner;
			Vector3 directionScale = this.DirectionScale;
			directionScale.Scale (projectileLaunch.DirectionScale);
			projectileLauncher.DirectionScale.Scale (directionScale);
			return;
		}
	}
}
