using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections.Generic;

public class ProjectileBase : NetworkBehaviour, AnimationBase.IAnimationListener {
	private int DestroyedTime = 0;

	// Objects:
	public GameObject Owner; // The GameObject that created this projectile.
	public GameObject Target; // If not null, direction is constantly set towards the target.
	public bool FollowOwner = false; // If true, this projectile will set its owner as the follow target when spawning.
	
	// Stats:
	public float Life = 10; // The initial amount of time (seconds) until this projectile expires.
	public float LifeTime = 10; // The current remaining amount of time (seconds) until this projectile expires.
	public Vector3 Origin;

	// Movement:
	[SyncVar] public float Speed = 16; // The speed of this projectile.
	[SyncVar] public Vector3 Direction = new Vector3 (1, 0, 0); // The direction this projectile will move in.
	[SyncVar] public Vector3 AltDirection = new Vector3 (0, 0, 0); // The second direction to move to. The x direction is reversed if the base direction is negative. If 0 the axis is ignored.
	[SyncVar] public float AltDirectionTime = 0; // The time (seconds) that it takes to transition through the alt direcion curves to the alt direction. If 0 the projectile will not use the alt direction.
	[SyncVar] public bool LoopAltDirection = false; // If true, when the alt direction is at 100% the cycle will repeat, this may look weird without seamless curves. Can be used for sine wave projectiles for example.
	public AnimationCurve AltDirectionCurveX;
	public AnimationCurve AltDirectionCurveY;
	public AnimationCurve AltDirectionCurveZ;
	[SyncVar] protected float GroundedTime = 0; // The time (seconds) that downward movement is disabled.
	
	// Damage and Impact:
	public DamageSource.DamageType DamageType = DamageSource.DamageType.Physical;
	public float Strength = 1; // Base damage if there is no owner and scaled damage if there is.
	public GameObject SplashEffect; // The effect to play after impact.
	public float SplashRadius = 0; // The radius of the splash damage sphere collider.
	public float SplashStrength = 0.5f; // The damage multiplier for splash damage based on base damage.
	public bool Pierce = false; // If true, this projectile wont explode on impact.
	public bool BlockProjectiles = false; // If true, this projectile will trigger the impact of other projectiles.
	public bool DestroyedByTerrain = true; // If true, terrain and solid objects such as platforms or obstacles will destroy this projectile if the collision is below the center position.

	// Animation:
	public bool FaceDirection = true;

	// ========== Growth ==========
	[SyncVar] public Vector3 Growth = new Vector3 (0, 0, 0); // How much this projectile should grow (or shrink) by over each second.
	[SyncVar] public Vector3 GrowthMax = new Vector3 (); // The maximum size to grow to, if an axis is 0 then the growth is unlimited.
	[SyncVar] protected Vector3 InitScale;

	// ========== Fragment Projectiles ==========
	/* Fragment Prefab - The prefab to be used if creating fragment projectiles. */
	public GameObject FragmentPrefab = null;
	/* Fragment Count - How many fragment projectiles to create on impact. */
	public int FragmentCount = 0;
	/* Boomerang - If true, all fragment projectiles will target this projectile's owner, this can be used to create boomerangs, etc. */
	public bool FragmentBoomerang;
	/* If true, this is a boomerang projectile returning to it's owner and will reset the owner's attack cooldown on return. */
	public bool IsBoomerang = false;

	// ========== Repeat Projectiles ==========
	/* Repeat Prefab - The prefab to use when firing a repeat projectile. The prefab will have it's repeat count set to this projectile's -1 and will then be responsible for the next repeat. */
	public GameObject RepeatPrefab;
	/* Repeat Count - After firing, this projectile will fire again from either it's origin or it's owner. For the specified amount of times. */
	public int RepeatCount = 0;
	/* Repeat Delay - The time in seconds between projectile repeats. */
	public float RepeatDelay = 0.25f;
	/* Repeat Time - Counts down to 0 then fires a repeat projectile. */
	private float RepeatTime = 0.25f;


	// ========== Start ==========
	public void Start () {
		this.Origin = this.transform.position;
		this.LifeTime = this.Life;
		this.RepeatTime = this.RepeatDelay;
		this.InitScale = this.transform.localScale;

		if (this.FollowOwner)
			this.Target = this.Owner;
	}


    // ========== Update ==========
    public void Update() {
		// Destroy Countdown:
		if (this.DestroyedTime > 0) {
			if (this.DestroyedTime > 1) {
				this.DestroyedTime--;
				if (this.DestroyedTime == 1)
					this.DoDestroy ();
			}
			return;
		}

		float speed = this.Speed;

		if (NetworkManager.Get ().ServerSide ()) {
			// Repeat Fire:
			if (this.RepeatCount > 0 && this.RepeatPrefab != null) {
				this.RepeatTime -= Time.deltaTime;
				if (this.RepeatTime <= 0) {
					GameObject repeatObject = GameObject.Instantiate (this.RepeatPrefab);
					ProjectileBase repeatProjectile = repeatObject.GetComponent<ProjectileBase> ();
					if (repeatProjectile != null) {
						repeatProjectile.Owner = this.Owner;
						repeatProjectile.Target = this.Target;
						repeatProjectile.RepeatCount = this.RepeatCount - 1;
					}
					this.RepeatCount = 0;
					if (this.Owner != null) {
						float ownerYOffset = 0;
						CreatureBase ownerCreature = this.Owner.GetComponent<CreatureBase> ();
						if (ownerCreature != null)
							ownerYOffset = ownerCreature.ProjectileOffsetY;
						repeatObject.transform.position = new Vector3 (
							this.Owner.transform.position.x,
							this.Owner.transform.position.y + ownerYOffset,
							this.Owner.transform.position.z);
					}
					else
						repeatObject.transform.position = this.Origin;
					NetworkServer.Spawn (repeatObject);
				}
			}

			// Life Time:
			this.LifeTime -= Time.deltaTime;
			if (this.LifeTime <= 0)
				this.Explode ();

			// Follow Target If Exists:
			if (this.Target != null) {
				Vector3 targetPos = this.Target.transform.position;
				if (this.Target.GetComponent<CreatureBase> () != null)
					targetPos.y += this.Target.GetComponent<CreatureBase> ().ProjectileOffsetY;
				this.Direction = Vector3.Normalize (targetPos - this.transform.position);
				float distanceToTarget = Vector3.Distance (this.transform.position, this.Target.transform.position);
				if (distanceToTarget <= 3) {
					this.Impact (this.Target, Vector3.zero);
					speed = 0;
					this.transform.position = Target.transform.position;
				}
			}
		}
		
		// Store Direction and Time:
		Vector3 currentDirection = this.Direction;
		if (this.GroundedTime > 0) {
			this.GroundedTime -= Time.deltaTime;
			currentDirection.y = Math.Max (currentDirection.y, 0);
		}
		float timeAlive = this.Life - this.LifeTime;

		// Alternating Direction:
		if (this.AltDirectionTime > 0) {
			if (this.LoopAltDirection || timeAlive <= this.AltDirectionTime) {
				float altInfluence = (timeAlive % this.AltDirectionTime) / this.AltDirectionTime;

				float altX = this.AltDirection.x;
				if (this.Direction.x < 0)
					altX = -altX;
				float curvedAltInfluence = this.AltDirectionCurveX.Evaluate (altInfluence);
				currentDirection.x = (currentDirection.x - (currentDirection.x * curvedAltInfluence)) + (this.AltDirection.x * curvedAltInfluence);

				curvedAltInfluence = this.AltDirectionCurveY.Evaluate (altInfluence);
				currentDirection.y = (currentDirection.y - (currentDirection.y * curvedAltInfluence)) + (this.AltDirection.y * curvedAltInfluence);

				curvedAltInfluence = this.AltDirectionCurveZ.Evaluate (altInfluence);
				currentDirection.z = (currentDirection.z - (currentDirection.z * curvedAltInfluence)) + (this.AltDirection.z * curvedAltInfluence);
			}
			else {
				if (currentDirection.x >= 0)
					currentDirection = this.AltDirection;
				else
					currentDirection = new Vector3 (-this.AltDirection.x, this.AltDirection.y, this.AltDirection.z);
			}
		}

		// Apply Movement:
		this.transform.position += currentDirection * speed * Time.deltaTime * 2;

		// Face Direction:
		if (this.FaceDirection) {
			this.transform.rotation = Quaternion.identity;
			float angle = 90 + -Mathf.Atan2 (currentDirection.x, currentDirection.y) * Mathf.Rad2Deg;
			this.transform.Rotate (Vector3.forward, angle, Space.World);
		}

		// Growth:
		Vector3 growth = this.Growth * timeAlive;
		if (this.GrowthMax.x > 0 && growth.x > this.GrowthMax.x)
			growth.x = this.GrowthMax.x;
		if (this.GrowthMax.x < 0 && growth.x < this.GrowthMax.x)
			growth.x = this.GrowthMax.x;
		if (this.GrowthMax.y > 0 && growth.y > this.GrowthMax.y)
			growth.y = this.GrowthMax.y;
		if (this.GrowthMax.y < 0 && growth.y < this.GrowthMax.y)
			growth.y = this.GrowthMax.y;
		if (this.GrowthMax.z > 0 && growth.z > this.GrowthMax.z)
			growth.z = this.GrowthMax.z;
		if (this.GrowthMax.z < 0 && growth.z < this.GrowthMax.z)
			growth.z = this.GrowthMax.z;
		this.transform.localScale = this.InitScale + (growth);
	}
	
	
	// ========== Collision ==========
	public void OnCollisionEnter (Collision collision) {
		if ((this.Life - this.LifeTime) < 0.5f)
			return;
		this.Impact (collision.gameObject, collision.contacts[0].point);
	}


	// ========== Trigger ==========
	public void OnTriggerEnter(Collider collider) {
		this.Impact (collider.gameObject, Vector3.zero);
	}


	// ========== Impact ==========
	public void Impact (GameObject target, Vector3 impactPosition) {
		if (NetworkManager.Get ().ClientSide () || this.DestroyedTime > 0 || this.JustSpawned ())
			return;
		bool terrainDestruction = true;

		// Ignored Physics Layers:
		if (target.layer == 8 || target.layer == 10 || target.layer == 11)
			return; // Projectile Physics, Pickups, Ragdolls

		// Owner Check:
		if (target == this.Owner) {
			if (this.IsBoomerang) {
				if (target.GetComponent<CreatureBase> () != null) {
                    target.GetComponent<CreatureBase> ().AttackCooldown = 0; // TODO: This wont work for none base attacks.
				}
				GameObject.Destroy (this.gameObject);
			}
			return;
		}

		// Projectile Impact:
		ProjectileBase targetProjectile = target.GetComponent<ProjectileBase> ();
		if (targetProjectile != null) {
			terrainDestruction = false;
			// TODO: Have certain damage types cancel each other out.
			if (this.BlockProjectiles)
				targetProjectile.Impact (this.gameObject, impactPosition);
			if (!targetProjectile.BlockProjectiles)
				return;
		}

		// Creature Impact:
		CreatureBase targetCreature = target.GetComponent<CreatureBase> ();
		if (targetCreature != null) {
			terrainDestruction = false;
			this.DamageCreature (targetCreature);
		}

		// Terrain Impact:
		if (terrainDestruction) {
			Collider targetCollider = target.GetComponent<Collider> ();
			if (targetCollider != null) {
				if (targetCollider.isTrigger)
					return;
				if (targetCollider.tag == "Creature" || targetCollider.tag == "Player")
					return;
			}

			// Terrain Immunity and Push Up:
			if (!this.DestroyedByTerrain) {
				this.transform.position += new Vector3 (0, this.Speed * Time.deltaTime * 2, 0);
				this.GroundedTime = 1;
				return;
			}
		}

		if (!this.Pierce && target.GetComponent<PickupBase> () == null)
			this.Explode ();
	}
	
	
	// ========== Explode ==========
	public void Explode () {
		// Splash Damage:
		if (this.SplashRadius > 0) {
			Collider[] splashCollision = Physics.OverlapSphere (this.transform.position, this.SplashRadius);
			foreach (Collider splashCollider in splashCollision) {
				CreatureBase targetCreature = splashCollider.GetComponent<CreatureBase> ();
				if (targetCreature != null) {
					this.DamageCreature (targetCreature, this.SplashStrength);
				}

			}
		}

		// Fragments:
		if (this.FragmentPrefab != null && this.FragmentCount > 0 && !this.IsBoomerang) {
			for (int i = 0; i < this.FragmentCount; i++) {
				GameObject fragmentObject = GameObject.Instantiate (this.FragmentPrefab);
				fragmentObject.transform.position = this.transform.position;
				ProjectileBase fragmentProjectile = fragmentObject.GetComponent<ProjectileBase> ();

				if (fragmentProjectile != null) {
					fragmentProjectile.Owner = this.Owner;

					// Prevent Infinite Fragments of Fragments!
					if (fragmentProjectile.FragmentPrefab == this.FragmentPrefab)
						fragmentProjectile.FragmentPrefab = null;

					// Fragment Boomerang:
					if (this.FragmentBoomerang) {
						fragmentProjectile.Target = this.Owner;
						fragmentProjectile.Life = this.Life * 4;
						fragmentProjectile.Pierce = true;
						fragmentProjectile.IsBoomerang = true;
					}

					// Random Fragment Direction:
					fragmentProjectile.Direction = new Vector3 (
						1 - (UnityEngine.Random.value * 2),
						UnityEngine.Random.value,
						0);
					fragmentProjectile.AltDirection.x = fragmentProjectile.Direction.x;
				}
				if (NetworkManager.Get ().ServerSide ())
					NetworkServer.Spawn (fragmentObject);
			}
		}

		// Effect:
		if (this.SplashEffect != null) {
			this.PlaySplashEffect ();
			if (NetworkManager.Get ().ServerSide ())
				this.RpcPlaySplashEffect ();
		}

		// Destroy:
		this.Destroy ();
	}
	
	public void PlaySplashEffect () {
		GameObject gameObject = GameObject.Instantiate (this.SplashEffect);
		gameObject.transform.position = this.transform.position;
	}

	[ClientRpc]
	public void RpcPlaySplashEffect () {
		this.PlaySplashEffect ();
	}


	// ========== Collision Stay ==========
	public void OnCollisionStay (Collision collision) {
		this.UpdateCollision (collision.gameObject);
	}


	// ========== Trigger Stay ==========
	public void OnTriggerStay (Collider collider) {
		this.UpdateCollision (collider.gameObject);
	}


	// ========== Update Collision ==========
	public void UpdateCollision (GameObject target) {
		if (target == null)
			return;

		// Ignored Physics Layers:
		if (target.layer == 8 || target.layer == 10 || target.layer == 11)
			return; // Projectile Physics, Pickups, Ragdolls

		// Projectiles:
		ProjectileBase targetProjectile = target.GetComponent<ProjectileBase> ();
		if (targetProjectile != null)
			return;

		// Creatures:
		CreatureBase targetCreature = target.GetComponent<CreatureBase> ();
		if (targetCreature != null) {
			if (NetworkManager.Get ().ClientSide () || this.DestroyedTime > 0 || this.JustSpawned ())
				return;
			this.DamageCreature (targetCreature);
			return;
		}

		// Terrain Immunity and Push Up:
		if (!this.DestroyedByTerrain) {
			this.transform.position += new Vector3 (0, this.Speed * Time.deltaTime * 2, 0);
			this.GroundedTime = 1;
			return;
		}
	}


	// ========== Just Spawned ==========
	/** Returns true if this projectile is less than 0.25 seconds old. **/
	public bool JustSpawned () {
		return this.Life - this.LifeTime < (Time.deltaTime / 2);
	}


	// ========== Damage Creature ==========
	/** Damages the target creature if possible. **/
	public void DamageCreature (CreatureBase targetCreature, float scale = 1) {
		if (targetCreature.Dead)
			return;
		GameObject damageOrigin = this.gameObject;
		if (this.Owner != null)
			damageOrigin = this.Owner;
		int damageAmount = Mathf.RoundToInt (this.Strength * scale);
		targetCreature.Damage (new DamageSource (damageAmount, this.gameObject, damageOrigin, this.DamageType));
	}


	// ========== Destroy ==========
	public void Destroy () {
		this.DestroyedTime = 5;
	}

	public void DoDestroy () {
		GameObject.Destroy (this.gameObject);
	}


	// ========== On Animation Move ==========
	/* Called by animation components when they move the transform. */
	public void OnAnimationMove (Vector3 movement) {

	}


	// ========== On Animation Rotate ==========
	/* Called by animation components when they rotate the transform. */
	public void OnAnimationRotate (Quaternion rotation) {

	}


	// ========== On Animation Scale ==========
	/* Called by animation components when they scale the transform. */
	public void OnAnimationScale (Vector3 movement) {

	}


	// ========== Get Animation Facing ==========
	/* Returns true if facing x+ (right) and false if facing x- (left). Used to flip some animations. */
	public bool GetAnimationFacing () {
		return this.FaceDirection;
	}
}
