﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class CameraTarget : MonoBehaviour {
	public float Distance = 25;
	public float DistanceBase = 25;
	public float DistanceMax = 50;


	// ========== Start ==========
	public void Start () {
		LevelBase.Get ().Camera.SetFollowTarget (this.gameObject);
	}


	// ========== Update ==========
	public void Update () {
		this.Distance = this.DistanceBase;
		List<PlayerStats> players = LevelBase.Get ().LocalPlayers;
		
		// Single Player:
		if (players.Count == 1) {
			GameObject playerObject = players [0].PlayerObject;
			if (playerObject != null) {
				CreatureBase playerCreature = playerObject.GetComponent<CreatureBase> ();
				if (playerCreature != null && !playerCreature.Dead)
					this.transform.position = playerObject.transform.position;
			}
			return;
		}

		// Multi Player:
		if (players.Count > 1) {
			Vector3 min = this.transform.position;
			Vector3 max = this.transform.position;
			bool firstPlayer = true;

			foreach (PlayerStats player in players) {
				GameObject playerObject = player.PlayerObject;
				if (playerObject == null)
					continue;
				CreatureBase playerCreature = playerObject.GetComponent<CreatureBase> ();
				if (playerCreature == null || playerCreature.Dead)
					continue;

				// Set First Player:
				if (firstPlayer) {
					firstPlayer = false;
					min = playerObject.transform.position;
					max = playerObject.transform.position;
					continue;
				}

				// GetType Min:
				if (playerObject.transform.position.x < min.x)
					min.x = playerObject.transform.position.x;
				if (playerObject.transform.position.y < min.y)
					min.y = playerObject.transform.position.y;
				if (playerObject.transform.position.z < min.z)
					min.z = playerObject.transform.position.z;

				// Get Max:
				if (playerObject.transform.position.x > max.x)
					max.x = playerObject.transform.position.x;
				if (playerObject.transform.position.y > max.y)
					max.y = playerObject.transform.position.y;
				if (playerObject.transform.position.z > max.z)
					max.z = playerObject.transform.position.z;
			}

			// All player dead:
			if (firstPlayer)
				return;

			// Center Among Players:
			Vector3 centerPos = new Vector3 (
				min.x + ((max.x - min.x) / 2),
				min.y + ((max.y - min.y) / 2),
				min.z + ((max.z - min.z) / 2)
			);
			this.transform.position = centerPos;

			// Update Distance:
			float newDist = this.DistanceBase;
			float distX = max.x - min.x;
			float distY = max.y - min.y;
			float distZ = max.z - min.z;
			if (distX > newDist)
				newDist = distX;
			if (distY > newDist)
				newDist = distY;
			if (distZ + 25 > newDist)
				newDist = distZ + 25;
			this.Distance = Math.Min (newDist, this.DistanceMax);
		}
	}
}
