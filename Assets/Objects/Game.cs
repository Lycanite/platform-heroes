using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {
	public static Game Instance;
	[SerializeField] private bool Paused = false;
	public enum GameType {
		Menu,
		Hub,
		Level
	};
	public GameType CurrentGameType = GameType.Level;

	// ========== Rules ==========
	public bool RuleSharedPlayerSpawn = true;


	// ========== Get Instance ==========
	public static Game Get () {
		if (Instance == null)
			Instance = GameObject.FindWithTag ("Game").GetComponent<Game> ();
		return Instance;
	}


	// ========== Load Level ==========
	public void LoadLevel (string name) {
		if (!NetworkManager.Get ().ServerSide ())
			return;
		NetworkManager.Get ().ServerChangeScene (name);
	}

	public void LoadLevel (LevelInfo levelInfo) {
		if (!NetworkManager.Get ().ServerSide ())
			return;
		if (levelInfo == null || !levelInfo.GetProgress ().Unlocked)
			return;
		NetworkManager.Get ().ServerChangeScene (levelInfo.GetSceneName ());
	}


	// ========== Pause ==========
	public bool IsPaused () {
		return this.Paused;
	}

	public void SetPaused (bool paused) {
		if (this.CurrentGameType != GameType.Level)
			paused = false;
		if (paused && !this.Paused)
			LevelBase.Get ().Interface.Command ("openmenu pause");
		else if (!paused && this.Paused)
			LevelBase.Get ().Interface.Command ("closemenu pause");
		this.Paused = paused;
	}

	public void TogglePaused () {
		this.SetPaused (!this.Paused);
	}


	// ========== Update ==========
	public void Update () {
		if (this.PauseButton ())
			this.TogglePaused ();
	}


	// ========== Pause Button ==========
	private bool PauseButton () {
		if (Input.GetButtonUp ("Start Keyboard") || Input.GetButtonUp ("Escape Keyboard"))
			return true;
		PlayerManager localPlayerManager = ClientManager.Get ().GetLocalPlayerManager ();
		if (localPlayerManager != null && localPlayerManager.PlayerInfos.Count > 0) {
			PlayerInfo firstPlayer = localPlayerManager.PlayerInfos[0];
			if (firstPlayer.GamePadID > 0) {
				if (Input.GetButtonUp ("Start " + firstPlayer.GamePadID))
					return true;
			}
		}
		return false;
	}
}
