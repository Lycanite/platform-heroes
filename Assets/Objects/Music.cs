﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Music : MonoBehaviour {

	public enum MusicState {
		None,
		World,
		Combat,
		Boss,
		Shop
	};

	public AudioMixerSnapshot WorldSnapshot;
	public AudioMixerSnapshot CombatSnapshot;
	public AudioMixerSnapshot BossSnapshot;
	public AudioMixerSnapshot ShopSnapshot;

	public AudioClip[] Stings;
	public AudioSource StingSource;

	public MusicState CurrentState = MusicState.None;
	public AudioMixerSnapshot CurrentSnapshot;
	public float BPM = 128;
	
	private float _FadeIn;
	private float _FadeOut;
	private float _FadeSpeed;

	
	// ========== Start ==========
	void Start () {
		this._FadeSpeed = 60 / this.BPM;
		this._FadeIn = this._FadeSpeed;
		this._FadeOut = this._FadeSpeed * 32;
		this.SetState (MusicState.World);
	}
	
	
	// ========== Set State ==========
	/** Sets the state of music to play such as World or Combat. **/
	void SetState (MusicState state) {
		if (this.CurrentState == state)
			return;

		this.CurrentSnapshot.TransitionTo (this._FadeOut);

		if (state == MusicState.World) {
			this.WorldSnapshot.TransitionTo (this._FadeIn);
		}
		else if (state == MusicState.Combat) {
			this.WorldSnapshot.TransitionTo (this._FadeIn);
			this.PlaySting ();
		}

		this.CurrentState = state;
	}
	
	
	// ========== Play Sting ==========
	/** Plays a random sting sound, normally called when entering the Combat or Boss states. **/
	void PlaySting () {
		if (this.Stings == null || this.Stings.Length == 0)
			return;
		int randomSting = Random.Range (0, this.Stings.Length);
		this.StingSource.clip = this.Stings[randomSting];
		this.StingSource.Play ();
	}
}
