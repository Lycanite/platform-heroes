using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

[NetworkSettings (channel = 1, sendInterval = 0.25f)]
public class PlatformBase : NetworkBehaviour, AnimationBase.IAnimationListener {

	// ===== Network =====
	public bool LocalOnly = false;

	// ===== Collapsing =====
	/** Collapse - If true, this platform will break apart usually after a specified delay. **/
	[SyncVar] public bool Collapse = false;
	/** Collapse Triggered - If true, this platform will begin to collapse. **/
	[SyncVar] protected bool CollapseTriggered = false;
	/** Collapse Complete - If true, this platform has completed its collapse. **/
	[SyncVar] protected bool CollapseComplete = false;
	/** Collapse Delay - The time in seconds until this platform collapses when something is on it. **/
	[SyncVar] public float CollapseDelay = 2;
	/** Collapse Delay Time - The current amount of remaining time in seconds until this platform collapses. **/
	[SyncVar] protected float CollapseDelayTime = 0;
	/** Resapwn - If true this platform will respawn after collapsing usually after a specified delay. **/
	[SyncVar] public bool Respawn = true;
	/** Respawn Delay - The time in seconds until this platform respawns after collapsing. **/
	[SyncVar] public float RespawnDelay = 6;
	/** Respawn Delay Time - The current amount of remaining time in seconds until this platform respawns. **/
	protected float RespawnDelayTime = 0;
	/** Player Collapse - If true, player characters will trigger the collapse. **/
	[SyncVar] public bool PlayerCollapse = true;
	/** Creature Collapse - If true, creatures (non-players) will trigger the collapse. **/
	[SyncVar] public bool CreatureCollapse = false;
	/** Object Collapse - If true, movable physics objects will trigger the collapse. **/
	[SyncVar] public bool ObjectCollapse = false;
	/** Collapse Effects Object - The GameObject with Particle Effects and other components that is activated when this platofrm collapses. **/
	public GameObject CollapseEffectsObject = null;
	/** Rot Z Init - The initial Z rotation of this platofrm, this is used as a base for Z rotation shaking. **/
	protected float RotXInit = 0;
	/** Rot Z Init - The initial Z rotation of this platofrm, this is used as a base for Z rotation shaking. **/
	protected Vector3 ScaleInit = new Vector3 (1, 1, 1);

	// ===== Sounds =====
	public UnityEngine.Audio.AudioMixerGroup AudioMixerGroup;
	public AudioLibrary AudioLibrary;
	public AudioClip RumbleSound;

	// ===== Objects =====
	public List<GameObject> AttachedObjects = new List<GameObject> ();
	public List<Collider> AdditionalColliders = new List<Collider> ();
	

	// ========== Awake ==========
	public virtual void Awake () {
		// Audio Library:
		this.AudioLibrary = this.gameObject.GetComponent<AudioLibrary> ();
		if (this.AudioLibrary == null)
			this.AudioLibrary = this.gameObject.AddComponent<AudioLibrary> ();
		this.AudioLibrary.AudioMixerGroup = this.AudioMixerGroup;
		this.AudioLibrary.LocalOnly = this.LocalOnly;
	}


	// ========== Start ==========
	public virtual void Start () {
		// Add Sounds:
		this.AudioLibrary.AddSound ("rumble", "main", this.RumbleSound);

		if (this.CollapseEffectsObject == null)
			this.CollapseEffectsObject = this.gameObject;
		
		// Network:
		this.gameObject.AddComponent<NetworkTransform> ();
		if (NetworkManager.Get ().ServerSide ())
			NetworkServer.Spawn (this.gameObject);
	}
	
	
	// ========== Update ==========
	public virtual void Update () {
		this.UpdateAttachedObjects ();
		this.UpdateCollapse ();
	}


	// ========== Collision ==========
	public virtual void OnCollisionEnter (Collision collision) {
		if (!this.AttachedObjects.Contains (collision.gameObject))
			this.AttachedObjects.Add (collision.gameObject);

		if (this.Collapse && !this.CollapseTriggered && this.ObjectCollapse) {
			this.StartCollapse ();
		}
	}
	
	public virtual void OnCollisionExit (Collision collision) {
		if (this.AttachedObjects.Contains (collision.gameObject))
			this.AttachedObjects.Remove (collision.gameObject);
	}

	public virtual void OnGameObjectCollision (GameObject gameObject) {
		if (!this.AttachedObjects.Contains (gameObject))
			this.AttachedObjects.Add (gameObject);

		if (this.Collapse && !this.CollapseTriggered && gameObject.GetComponent<CreatureBase> () != null) {
			if (gameObject.GetComponent<CreaturePlayer>() != null && this.PlayerCollapse) {
				this.StartCollapse ();
			}
			else if (gameObject.GetComponent<CreaturePlayer>() == null && this.CreatureCollapse) {
				this.StartCollapse ();
			}
		}
	}
	
	
	// ========== Collapse ==========
	public virtual void UpdateCollapse () {
		// Respawn:
		if (this.RespawnDelayTime > 0) {
			if (this.RespawnDelayTime <= 0.5f) {
				float respawnNormal = this.RespawnDelayTime / 0.5f;
				if (this.GetComponent<Collider> () != null)
					this.gameObject.GetComponent<Collider> ().enabled = true;
				foreach (Collider collider in this.AdditionalColliders)
					collider.enabled = true;
				if (this.GetComponent<Renderer> () != null)
					this.gameObject.GetComponent<Renderer> ().enabled = true;
				this.transform.localScale =  this.ScaleInit * (1 - respawnNormal);
			}

			this.RespawnDelayTime -= Time.deltaTime;
			if (this.RespawnDelayTime <= 0) {
				this.transform.localScale = this.ScaleInit;
			}
			else
				return;
		}

		// Snapshot Values:
		if (!this.Collapse || !this.CollapseTriggered) {
			this.RotXInit = this.transform.eulerAngles.x;
			this.ScaleInit = this.transform.localScale;
			return;
		}

		// Shake and Countdown:
		if (this.CollapseDelayTime > 0) {
			this.CollapseDelayTime -= Time.deltaTime;
			this.transform.eulerAngles = (new Vector3 (this.RotXInit + (3 * ((0.5f - (float)UnityEngine.Random.value) * 2)), this.transform.eulerAngles.y, this.transform.eulerAngles.z));
			return;
		}

		// Collapse and Start Respawn:
		this.FinishCollapse ();
	}

	public void StartCollapse () {
		this.AudioLibrary.PlaySound ("rumble", true);
		this.CollapseTriggered = true;
		this.CollapseDelayTime = this.CollapseDelay;
	}

	public void FinishCollapse () {
		this.AudioLibrary.StopSound ("main");
		this.CollapseTriggered = false;
		this.RespawnDelayTime = this.RespawnDelay;

		if (this.GetComponent<Collider> () != null)
			this.gameObject.GetComponent<Collider> ().enabled = false;
		foreach (Collider collider in this.AdditionalColliders)
			collider.enabled = false;
		if (this.GetComponent<Renderer> () != null)
			this.gameObject.GetComponent<Renderer> ().enabled = false;

		if (this.CollapseEffectsObject != null) {
			GameObject effectObject = GameObject.Instantiate (this.CollapseEffectsObject);
			effectObject.transform.position = this.transform.position;
			EffectBase effect = effectObject.GetComponent<EffectBase> ();
			if (effect != null)
				effect.Owner = this.gameObject;
		}
	}
	
	
	// ========== Attached Objects ==========
	public virtual void UpdateAttachedObjects () {
		if (this.AttachedObjects == null)
			return;

		foreach (GameObject attachedObject in this.AttachedObjects.ToArray ()) {

			// Remove Null Entries:
			if (attachedObject == null) {
				this.AttachedObjects.Remove (attachedObject);
				continue;
			}

			// Creatures and Players:
			CreatureBase attachedCreature = attachedObject.GetComponent<CreatureBase> ();
			if (attachedCreature != null && !attachedCreature.Grounded && !attachedCreature.Flying) { // Remove ungrounded or flying creatures on update.
				this.AttachedObjects.Remove (attachedObject);
			}
		}
	}


	// ========== On Animation Move ==========
	/* Called by animation components when they move the transform. */
	public void OnAnimationMove (Vector3 movement) {
		foreach (GameObject attachedObject in this.AttachedObjects.ToArray ()) {

			// Creatures and Players:
			CreatureBase attachedCreature = attachedObject.GetComponent<CreatureBase> ();
			if (attachedCreature != null) {
				if (attachedCreature.Grounded && !attachedCreature.Flying)
					attachedObject.transform.position += movement;
			}

			// Everything Else But Platforms:
			else if (attachedObject.GetComponent<PlatformBase> () == null) {
				attachedObject.transform.position += movement;
			}
		}
	}


	// ========== On Animation Rotate ==========
	/* Called by animation components when they rotate the transform. */
	public void OnAnimationRotate (Quaternion rotation) {
		foreach (GameObject attachedObject in this.AttachedObjects.ToArray ()) {

			// Creatures and Players:
			CreatureBase attachedCreature = attachedObject.GetComponent<CreatureBase> ();
			if (attachedCreature != null) {
				//if (attachedCreature.Grounded && !attachedCreature.Flying)
					// TODO Move based on rotation.
			}

			// Everything Else But Platforms:
			else if (attachedObject.GetComponent<PlatformBase> () == null) {
				// TODO Move based on rotation.
			}
		}
	}


	// ========== On Animation Scale ==========
	/* Called by animation components when they scale the transform. */
	public void OnAnimationScale (Vector3 movement) {
		foreach (GameObject attachedObject in this.AttachedObjects.ToArray ()) {

			// Creatures and Players:
			CreatureBase attachedCreature = attachedObject.GetComponent<CreatureBase> ();
			if (attachedCreature != null) {
				//if (attachedCreature.Grounded && !attachedCreature.Flying)
				// TODO Move based on scale.
			}

			// Everything Else But Platforms:
			else if (attachedObject.GetComponent<PlatformBase> () == null) {
				// TODO Move based on scale.
			}
		}
	}


	// ========== Get Animation Facing ==========
	/* Returns true if facing x+ (right) and false if facing x- (left). Used to flip some animations. */
	public bool GetAnimationFacing () {
		return true;
	}
}
