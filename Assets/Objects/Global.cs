using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class Global : MonoBehaviour {
	public static Global Instance;

	// Characters:
	public Dictionary<string, GameObject> Characters = new Dictionary<string, GameObject> ();
	public List<GameObject> CharacterEntries = new List<GameObject> ();
	public List<GameObject> CharacterProjectileEntries = new List<GameObject> ();

	// Creatures:
	public Dictionary<string, GameObject> Creatures = new Dictionary<string, GameObject> ();
	public List<GameObject> CreatureEntries = new List<GameObject> ();

	// Spawners:
	public Dictionary<string, GameObject> Spawners = new Dictionary<string, GameObject> ();
	public List<GameObject> SpawnerEntries = new List<GameObject> ();

	// Projectiles:
	public Dictionary<string, GameObject> Projectiles = new Dictionary<string, GameObject> ();
	public List<GameObject> ProjectileEntries = new List<GameObject> ();

	// Pickups:
	public Dictionary<string, GameObject> Pickups = new Dictionary<string, GameObject> ();
	public List<GameObject> PickupEntries = new List<GameObject> ();

	// Platforms:
	public Dictionary<string, GameObject> Platforms = new Dictionary<string, GameObject> ();
	public List<GameObject> PlatformEntries = new List<GameObject> ();

	// Shapes:
	public Dictionary<string, GameObject> Shapes = new Dictionary<string, GameObject> ();
	public List<GameObject> ShapeEntries = new List<GameObject> ();

	// Colors:
	public Color Bronze;
	public Color Silver;
	public Color Gold;
	public Color Platinum;


	// ========== Get Instance ==========
	public static Global Get () {
		if (Instance == null)
			Instance = GameObject.FindWithTag ("Global").GetComponent<Global> ();
		return Instance;
	}

	// ========== Awake ==========
	public void Awake () {
		// Create Static instance:
		if (Instance != null && Instance != this) {
			Destroy (this.gameObject);
			return;
		}
		Get ();

		// Setup Characters List:
		if (Characters.Count < CharacterEntries.Count) {
			foreach (GameObject characterEntry in this.CharacterEntries) {
				CreatureBase characterCreature = characterEntry.GetComponent<CreatureBase> ();
				if (characterCreature == null)
					continue;
				this.Characters.Add (characterCreature.CreatureName.ToLower (), characterEntry);
				ClientScene.RegisterPrefab (characterEntry);

				// Auto Add Character Projectiles from Power Entries:
				CreatureLogicPower characterPower = characterEntry.GetComponent<CreatureLogicPower> ();
				if (characterPower == null)
					continue;
				foreach (CreatureLogicPower.PowerDefinition powerDefinition in characterPower.PowerSetup) {
					if (powerDefinition.prefab != null && !this.ProjectileEntries.Contains (powerDefinition.prefab)) {
						// Register Projectile Launcher:
						ProjectileLauncher projectileLauncher = powerDefinition.prefab.GetComponent<ProjectileLauncher> ();
						if (projectileLauncher != null) {
							foreach (ProjectileLauncher.ProjectileLaunchDefinition projectileLaunch in projectileLauncher.Projectiles) {
								if (projectileLaunch.Prefab != null && !this.ProjectileEntries.Contains (projectileLaunch.Prefab))
									this.ProjectileEntries.Add (projectileLaunch.Prefab);
							}
						}

						// Register Projectile:
						else
							this.ProjectileEntries.Add (powerDefinition.prefab);
					}
				}
			}
		}

		// Register Objects:
		this.RegisterObjects<CreatureBase> (this.Creatures, this.CreatureEntries);
		this.RegisterObjects<SpawnerBase> (this.Spawners, this.SpawnerEntries);
		this.RegisterObjects<ProjectileBase> (this.Projectiles, this.ProjectileEntries);
		this.RegisterObjects<PickupBase> (this.Pickups, this.PickupEntries);
		this.RegisterObjects<PlatformBase> (this.Platforms, this.PlatformEntries);
		this.RegisterObjects<PlatformBase> (this.Shapes, this.ShapeEntries);

		DontDestroyOnLoad (this.gameObject);
	}


	// ========== Register Objects ==========
	/** Registers Prefabs for network spawning as well as for general use. **/
	public void RegisterObjects <T>(Dictionary<string, GameObject> registry, List<GameObject> entries) {
		if (registry.Count < entries.Count) {
			foreach (GameObject entry in entries) {
				T component = entry.GetComponent<T> ();
				if (component != null && component is IRegisterableObject) {
					registry.Add (((IRegisterableObject)component).GetRegistryName (), entry);
				}
				ClientScene.RegisterPrefab (entry);
			}
		}
	}


	// ========== Get Character ==========
	public GameObject GetCharacter (string name) {
		name = name.ToLower ();
		if (!this.Characters.ContainsKey (name))
			return null;
		return this.Characters[name];
	}
}
