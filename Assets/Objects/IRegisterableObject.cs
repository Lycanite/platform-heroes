﻿public interface IRegisterableObject {
	string GetRegistryName ();
}
