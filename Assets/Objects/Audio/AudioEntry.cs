﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;
using UnityEngine.Networking;

[System.Serializable]
public class AudioEntry {
	public string SoundName;
	public string SourceName;
	public AudioClip[] AudioClips;

	public AudioEntry (string soundName, string sourceName, AudioClip[] audioClips) {
		this.SoundName = soundName;
		this.SourceName = sourceName;
		this.AudioClips = audioClips;
	}

	public AudioClip GetAudioClip () {
		if (this.AudioClips == null || this.AudioClips.Length < 1)
			return null;
		if (this.AudioClips.Length == 1)
			return this.AudioClips [0];
		int random = UnityEngine.Random.Range (0, this.AudioClips.Length);
		return this.AudioClips[random];
	}
}
