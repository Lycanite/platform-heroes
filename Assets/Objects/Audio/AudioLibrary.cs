﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;
using UnityEngine.Networking;

[NetworkSettings (channel = 1, sendInterval = 0.25f)]
public class AudioLibrary : NetworkBehaviour {
	public bool LocalOnly = false;

	public Dictionary<string, AudioEntry> Sounds = new Dictionary<string, AudioEntry> ();
	public Dictionary<string, AudioSource> Sources = new Dictionary<string, AudioSource> ();

	public AudioMixerGroup AudioMixerGroup;
	public bool PlayOnAwake = false;
	public float Volume = 1;
	public float Pitch = 1;
	public float SpatialBlend = 1;
	public float MinDistance = 20;
	public float MaxDistance = 30;


	// ========== Add Sound ==========
	/* Adds an AudioEntry to this library taking a SoundEntry and creates a new sound channel for it if it doesn't exist. */
	public void AddSound (AudioEntry audioEntry) {
		if (this.Sounds.ContainsKey (audioEntry.SoundName))
			this.Sounds.Remove (audioEntry.SoundName);
		this.Sounds.Add (audioEntry.SoundName, audioEntry);
		if (!this.Sources.ContainsKey (audioEntry.SourceName))
			this.AddAudioSource (audioEntry.SourceName);
	}

	/* Adds an AudioEntry to this library using arguments to construct a SoundEntry. */
	public void AddSound (string soundName, string channelName, AudioClip[] audioClips) {
		this.AddSound (new AudioEntry (soundName, channelName, audioClips));
	}

	/* Adds an AudioEntry to this library using arguments to construct a SoundEntry. */
	public void AddSound (string soundName, string channelName, AudioClip audioClip) {
		this.AddSound (new AudioEntry (soundName, channelName, new AudioClip[] {audioClip}));
	}

	// ========== Add Audio Source ==========
	/* Adds a new AudioSource to this library using the provided arguments. */
	public void AddAudioSource (string sourceName, AudioMixerGroup audioMixerGroup, bool playOnAwake, float volume, float pitch, float spatialBlend, float minDistance, float maxDistance) {
		AudioSource audioSource = this.gameObject.AddComponent<AudioSource> ();
		audioSource.outputAudioMixerGroup = audioMixerGroup;
		audioSource.playOnAwake = playOnAwake;
		audioSource.volume = volume;
		audioSource.pitch = pitch;
		audioSource.spatialBlend = spatialBlend;
		audioSource.minDistance = minDistance;
		audioSource.maxDistance = maxDistance;
		this.Sources.Add (sourceName, audioSource);
	}

	/* Adds a new AudioSource to this library using the default arguments, this is used by added sounds that use a AudioSource that doesn't exist too. */
	public void AddAudioSource (string sourceName) {
		this.AddAudioSource (sourceName, this.AudioMixerGroup, this.PlayOnAwake, this.Volume, this.Pitch, this.SpatialBlend, this.MinDistance, this.MaxDistance);
	}


	// ========== Get Sound ==========
	/* Returns an AudioEntry from the given sound name. */
	public AudioEntry GetSound (string soundName) {
		if (this.Sounds.ContainsKey (soundName))
			return this.Sounds[soundName];
		return null;
	}


	// ========== Get Audio Source ==========
	/* Returns an AudioSource from the given source name. */
	public AudioSource GetAudioSource (string sourceName) {
		if (this.Sources.ContainsKey (sourceName))
			return this.Sources[sourceName];
		return null;
	}


	// ========== Play Sound ==========
	/* Plays the provided sound effect (by name), will also send to clients. */
	public void PlaySound (string soundName, bool loop = false) {
		if (NetworkManager.Get ().ServerSide () || this.LocalOnly) {
			if (!this.LocalOnly)
				this.RpcPlaySound (soundName, loop);
			this.DoPlaySound (soundName, loop);
		}
		else if (!this.LocalOnly && this.hasAuthority) {
			this.CmdPlaySound (soundName, loop);
		}
	}

	[ClientRpc]
	protected void RpcPlaySound (string soundName, bool loop) {
		this.DoPlaySound (soundName, loop);
	}

	[Command]
	protected void CmdPlaySound (string soundName, bool loop) {
		this.PlaySound (soundName, loop);
	}

	protected void DoPlaySound (string soundName, bool loop) {
		AudioEntry audioEntry = this.GetSound (soundName);
		if (audioEntry == null)
			return;
		AudioSource audioSource = this.GetAudioSource (audioEntry.SourceName);
		if (audioSource == null)
			return;
		audioSource.clip = audioEntry.GetAudioClip ();
		audioSource.loop = loop;
		audioSource.Play ();
	}


	// ========== Stop Sound ==========
	/* Stops the provided sound source (by name), will also send to clients. */
	public void StopSound (string sourceName) {
		if (NetworkManager.Get ().ServerSide () || this.LocalOnly) {
			if (!this.LocalOnly)
				this.RpcStopSound (sourceName);
			this.DoStopSound (sourceName);
		}
		else if (!this.LocalOnly && this.hasAuthority) {
			this.CmdStopSound (sourceName);
		}
	}

	[ClientRpc]
	protected void RpcStopSound (string sourceName) {
		this.DoStopSound (sourceName);
	}

	[Command]
	protected void CmdStopSound (string sourceName) {
		this.StopSound (sourceName);
	}

	protected void DoStopSound (string sourceName) {
		AudioSource audioSource = this.GetAudioSource (sourceName);
		if (audioSource == null)
			return;
		audioSource.Stop ();
	}
}
