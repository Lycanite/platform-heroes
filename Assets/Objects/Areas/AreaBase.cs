using UnityEngine;
using System.Collections;

public class AreaBase : MonoBehaviour {

	// ========== Start ==========
	public virtual void Start () {

	}
	
	
	// ========== Update ==========
	public virtual void Update () {
		
	}
	
	
	// ========== Collision ==========
	public virtual void OnTriggerEnter (Collider collider) {
		// Standard GameObject collision enter.
	}

	public virtual void OnTriggerStay (Collider collider) {
		// Standard GameObject collision stay.
	}

	public virtual void OnTriggerExit (Collider collider) {
		// Standard GameObject collision exit.
	}
}
