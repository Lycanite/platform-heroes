using UnityEngine;

public class AreaAI : AreaBase {
	public bool Active = true;
	public bool AffectCreatures = true;
	public bool AffectPlayers = false;
	public string CreatureName = "";

	// AI Controls:
	public bool Jump = true;
	public float Wait = 0;

	// AI Conditions:
	public int FacingCondition = 0;

	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		if (!this.Active)
			return;

		// Creatures:
		CreatureBase creature = collider.GetComponent<CreatureBase> ();
		if (creature != null) {

			// Facing Check:
			if (this.FacingCondition != 0) {
				if (creature.Movement.MovementCurrent.x > 0 && this.FacingCondition < 0)
					return;
				if (creature.Movement.MovementCurrent.x < 0 && this.FacingCondition > 0)
					return;
			}

			// Target Checks:
			CreaturePlayer player = creature.GetComponent<CreaturePlayer> ();
			if (!this.AffectPlayers && player != null)
				return;
			if (this.AffectPlayers && !this.AffectCreatures && player == null)
				return;
			if (this.CreatureName != "" && this.CreatureName != creature.CreatureName)
				return;
			
			// Jump:
			if (this.Jump && creature.Grounded) {
				creature.Swimming = false;
				creature.Movement.AIJump ();
			}

			// Wait:
			if (this.Wait > 0)
				creature.AIMovementWait = this.Wait;

			return;
		}
	}
}
