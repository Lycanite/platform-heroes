﻿using UnityEngine;
using System.Collections;

public class AreaFinish : AreaBase {
	public GameObject EffectObject;
	protected AudioSource AudioSource;
	
	// ========== Start ==========
	public override void Start () {
		base.Start ();
		this.AudioSource = this.GetComponent<AudioSource> ();
	}


	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		CreaturePlayer player = collider.GetComponent<CreaturePlayer> ();

		// Players only:
		if (player == null)
			return;

		player.LevelExit (this.gameObject);

		// Sound:
		if (this.AudioSource != null) {
			this.AudioSource.clip = player.AudioLibrary.GetSound ("win").GetAudioClip ();

			if (this.AudioSource.clip != null)
				this.AudioSource.Play ();
		}

		// Effect:
		if (this.EffectObject != null) {
			GameObject effectObject = GameObject.Instantiate (this.EffectObject);
			effectObject.transform.position = player.transform.position;
			if (effectObject.GetComponent<EffectBase> () != null) {
				EffectBase effect = effectObject.GetComponent<EffectBase> ();
				effect.Owner = player.gameObject;
				effect.FollowOwner = true;
			}
		}
	}
}
