﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AreaDepth : AreaBase {
	public Collider Collider;

	/** Horizontal - Depth based on horizontal movement. 0 = No depth change, positive = left to right, negative = right to left. **/
	public int Horizontal = 1;
	/** Vertical - Depth based on vertical movement. 0 = No depth change, positive = bottom to top, negative = top to bottom. **/
	public int Vertical = 0;
	/** Total Weight - Horizintal + Vertical with negatives inverted, used to calculate normals. **/
	private int TotalWeight = 0;

	/** One Way - If true, this will only move creatures towards the exit depth and not back to the enter depth. **/
	public bool OneWay = false;

	/** Enter Depth - The Depth to move the creature from and that it should usually be entering with. **/
	public int EnterDepth = 0;
	/** Exit Depth - The Depth to move the creature to and that it should usually be exiting with. **/
	public int ExitDepth = 10;
	/** Depth Distance - The distance between the Enter Depth and Exit Depth. **/
	private int DepthDistance = 0;

	public float BoundLeft = 0;
	public float BoundWidth = 0;
	public float BoundBottom = 0;
	public float BoundHeight = 0;

	/** Creatures - A list of all creatures in this zone. **/
	public List<CreatureBase> Creatures = new List<CreatureBase> ();

	// ========== Start ==========
	public override void Start () {
		this.Collider = this.GetComponent<Collider> ();
		this.UpdateBounds ();
	}


	// ========== Update Bounds ==========
	public void UpdateBounds () {
		this.BoundLeft = this.Collider.bounds.min.x;
		this.BoundWidth = this.Collider.bounds.size.x;
		this.BoundBottom = this.Collider.bounds.min.y;
		this.BoundHeight = this.Collider.bounds.size.y;

		this.TotalWeight = 0;
		if (this.Horizontal > 0)
			this.TotalWeight += this.Horizontal;
		else
			this.TotalWeight -= this.Horizontal;
		if (this.Vertical > 0)
			this.TotalWeight += this.Vertical;
		else
			this.TotalWeight -= this.Vertical;

		this.DepthDistance = 0;
		if (this.EnterDepth >= this.ExitDepth)
			this.DepthDistance = this.ExitDepth - this.EnterDepth;
		else
			this.DepthDistance = this.EnterDepth - this.ExitDepth;
	}


	// ========== Update ==========
	public override void Update () {

		// Apply Depth To All Collided Creatures:
		foreach (CreatureBase creature in this.Creatures) {
			if (creature != null && !creature.Dead)
				this.ApplyCreatureDepth (creature);
		}

	}


	// ========== Apply Creature Depth ==========
	public void ApplyCreatureDepth (CreatureBase creature, bool round = false) {
		if (this.Horizontal == 0 && this.Vertical == 0) {
			creature.SetDepth (this.ExitDepth);
			return;
		}

		// Horiztonal Depth:
		float horiztonalNormal = 0;
		if (this.Horizontal != 0) {
			horiztonalNormal = creature.transform.position.x - this.BoundLeft;
			if (this.BoundWidth != 0)
				horiztonalNormal /= this.BoundWidth;
			else
				horiztonalNormal = 1;
			if (this.Horizontal > 0)
				horiztonalNormal = (horiztonalNormal * this.Horizontal) / (this.TotalWeight);
			else {
				horiztonalNormal = 1 - horiztonalNormal;
				horiztonalNormal = (horiztonalNormal * -this.Horizontal) / (this.TotalWeight);
			}
			horiztonalNormal = Mathf.Clamp (horiztonalNormal, 0, 1);
		}

		// Vertical Depth:
		float verticalNormal = 0;
		if (this.Vertical != 0) {
			verticalNormal = creature.transform.position.y - this.BoundBottom;
			if (this.BoundHeight != 0)
				verticalNormal /= this.BoundHeight;
			else
				verticalNormal = 1;
			if (this.Vertical > 0)
				verticalNormal *= (verticalNormal * this.Vertical) / (this.TotalWeight);
			else {
				verticalNormal = 1 - verticalNormal;
				verticalNormal *= (verticalNormal * -this.Vertical) / (this.TotalWeight);
			}
			verticalNormal = Mathf.Clamp (verticalNormal, 0, 1);
		}

		// Calculate New Depth:
		float newDepth = creature.Depth;
		if (this.EnterDepth < this.ExitDepth)
			newDepth = this.EnterDepth - (horiztonalNormal + verticalNormal) * (this.DepthDistance);
		else
			newDepth = this.EnterDepth + (horiztonalNormal + verticalNormal) * (this.DepthDistance);

		// Round Depth (On Exit):
		if (round) {
			if (this.OneWay) {
				newDepth = this.ExitDepth;
			}
			else {
				float depthEnterDistance = 0;
				float depthExitDistance = 0;
				if (this.ExitDepth > this.EnterDepth) {
					depthEnterDistance = newDepth - this.EnterDepth;
					depthExitDistance = this.ExitDepth - newDepth;
				} else {
					depthEnterDistance = this.EnterDepth - newDepth;
					depthExitDistance = newDepth - this.ExitDepth;
				}
				if (depthExitDistance <= depthEnterDistance)
					newDepth = this.ExitDepth;
				else
					newDepth = this.EnterDepth;
			}
		}

		// Clamp If One Way:
		if (this.OneWay) {
			if (this.ExitDepth > this.EnterDepth && newDepth < creature.Depth)
				return;
			if (this.ExitDepth < this.EnterDepth && newDepth > creature.Depth)
				return;
		}
		
		// Apply Depth:
		creature.SetDepth (newDepth);
	}


	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		// Creatures:
		CreatureBase creature = collider.GetComponent<CreatureBase> ();
		if (creature != null && !this.Creatures.Contains (creature)) {
			this.Creatures.Add (creature);
			return;
		}
	}

	public override void OnTriggerExit (Collider collider) {
		// Creatures:
		CreatureBase creature = collider.GetComponent<CreatureBase> ();
		if (creature != null && this.Creatures.Contains (creature)) {
			this.Creatures.Remove (creature);
			this.ApplyCreatureDepth (creature, true);
			return;
		}
	}
}
