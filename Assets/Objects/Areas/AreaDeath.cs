﻿using UnityEngine;
using System.Collections;

public class AreaDeath : AreaBase {
	public bool DestroyObjects = true;
	public bool PlayerOnly = false;

	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		if (!this.DestroyObjects || NetworkManager.Get ().ClientSide ())
			return;

		// Creatures:
		if (collider.GetComponent<CreatureBase> () != null) {
			CreatureBase creature = collider.GetComponent<CreatureBase> ();
			// Player only check:
			if (this.PlayerOnly && creature.GetComponent<CreaturePlayer> () == null)
				return;
			
			// Kill the creature that enters this zone:
			creature.Death (false, false);
			return;
		}

		Destroy (collider.gameObject);
	}
}
