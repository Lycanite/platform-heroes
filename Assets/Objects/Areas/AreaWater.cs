﻿using UnityEngine;
using System.Collections;

public class AreaWater : AreaBase {
	public float WaveHeight = 0.5f;

	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		// Camera:
		CameraBase camera = collider.GetComponent<CameraBase> ();
		if (camera != null) {
			camera.AddWaterCollider (this.GetComponent<Collider> ());
			return;
		}

		// Creatures:
		CreatureBase creature = collider.GetComponent<CreatureBase> ();
		if (creature != null) {
			creature.Physics.AddWaterCollider (this.GetComponent<Collider> ());
			return;
		}
	}

	public override void OnTriggerExit (Collider collider) {
		// Camera:
		CameraBase camera = collider.GetComponent<CameraBase> ();
		if (camera != null) {
			camera.RemoveWaterCollider (this.GetComponent<Collider> ());
			return;
		}

		// Creatures:
		CreatureBase creature = collider.GetComponent<CreatureBase> ();
		if (creature != null) {
			creature.Physics.RemoveWaterCollider (this.GetComponent<Collider> ());
			return;
		}
	}
}
