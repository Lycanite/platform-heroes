using UnityEngine;
using System.Collections;

public class AreaDamage : AreaBase {
	public GameObject Owner;
	public bool Active = true;
	public bool PlayerOnly = false;
	public bool DestroyRigidBodies = true;
	public int DamageAmount = 1;
	public DamageSource.DamageType DamageType = DamageSource.DamageType.Physical;
	protected DamageSource DamageSource;

	// Knockback:
	public float KnockbackX = 0;
	public float KnockbackY = 0;
	/* If true, when X Knockback is applied, it is based on the target creature's movement. */
	public bool KnockbackXRelative = true;


	// ========== Start ==========
	public override void Start () {
		base.Start ();
		GameObject damageSource = this.gameObject;
		CreatureBase parentCreature = this.GetComponentInParent<CreatureBase> ();
		if (parentCreature != null)
			damageSource = parentCreature.gameObject;
		if (this.Owner != null)
			damageSource = this.Owner;
		this.DamageSource = new DamageSource (this.DamageAmount, damageSource, damageSource, this.DamageType);
	}

	// ========== Collision ==========
	public override void OnTriggerStay (Collider collider) {
		if (NetworkManager.Get ().ClientSide () || !this.Active)
			return;

		// Self or Owner Check:
		if (collider.gameObject == this.Owner)
			return;
		foreach (AreaDamage childZoneDamage in collider.gameObject.GetComponentsInChildren<AreaDamage> ())
			if (childZoneDamage == this)
				return;

		// Creatures:
		CreatureBase creature = collider.GetComponent<CreatureBase> ();
		if (creature != null) {

			// Player only check:
			if (this.PlayerOnly && creature.GetComponent<CreaturePlayer> () == null)
				return;
			
			// Damage and Knockback the creature that enters this zone:
			if (creature.Damage (this.DamageSource)) {
				float forceX = this.KnockbackX;
				if (this.KnockbackXRelative && creature.Movement.Facing > 0) {
					forceX = -forceX;
				}
				creature.Knockback (0, this.KnockbackY); // Disabled forceX for now.
			}
			return;
		}

		if (this.DestroyRigidBodies && collider.GetComponent<Rigidbody> () != null) {
			if (!collider.GetComponent<Rigidbody> ().isKinematic)
				Destroy (collider.gameObject);
		}
	}
}
