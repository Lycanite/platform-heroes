﻿using UnityEngine;
using System.Collections;

public class AreaCheckPoint : AreaBase {
	public SpawnPointBase SpawnPoint;
	public GameObject EffectObject;
	
	// ========== Start ==========
	public override void Start () {
		base.Start ();
		if (this.SpawnPoint == null)
			this.SpawnPoint = this.GetComponent<SpawnPointBase> ();
		if (this.SpawnPoint == null)
			this.SpawnPoint = this.gameObject.AddComponent<SpawnPointBase> ();
	}


	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		if (NetworkManager.Get ().ClientSide ())
			return;

		CreaturePlayer playerCreature = collider.GetComponent<CreaturePlayer> ();

		// Players only:
		if (playerCreature == null)
			return;

		// Activate Checkpoint If Different:
		PlayerStats player = playerCreature.PlayerStats;
		if (player.SpawnPoint == this.SpawnPoint)
			return;
		player.Checkpoint (this.SpawnPoint);

		// Sound:
		if (this.GetComponent<AudioSource> () != null)
			this.GetComponent<AudioSource> ().Play ();

		// Effect:
		if (this.EffectObject != null) {
			GameObject effectObject = GameObject.Instantiate (this.EffectObject);
			effectObject.transform.position = playerCreature.transform.position;
			if (effectObject.GetComponent<EffectBase> () != null) {
				EffectBase effect = effectObject.GetComponent<EffectBase> ();
				effect.Owner = playerCreature.gameObject;
				effect.FollowOwner = true;
			}
		}
	}
}
