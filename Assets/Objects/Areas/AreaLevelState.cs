﻿using UnityEngine;

public class AreaLevelState : AreaBase {
	public LevelStats.LevelState TargetState = LevelStats.LevelState.Boss;
	protected LevelBase Level;
	
	// ========== Start ==========
	public override void Start () {
		base.Start ();
		this.Level = LevelBase.Get ();
	}


	// ========== Collision ==========
	public override void OnTriggerEnter (Collider collider) {
		CreaturePlayer player = collider.GetComponent<CreaturePlayer> ();

		// Players only:
		if (player == null)
			return;

		if (this.Level.Stats.CurrentLevelState != this.TargetState)
			this.Level.Stats.SetLevelState (this.TargetState);
	}
}
