﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class ClientManager : MonoBehaviour {
	public static ClientManager Instance;
	public GameObject PlayerManagerPrefab;
	[SerializeField] private int LocalConnectionID = 0;

	// Players:
	public Dictionary<int, PlayerManager> PlayerManagers = new Dictionary<int, PlayerManager> ();
	[SerializeField] private List<PlayerInfoLocal> LocalPlayers = new List<PlayerInfoLocal> ();
	public int Version = 0;
	public List<PlayerInfo> AllPlayerInfos = new List<PlayerInfo> ();
	private int AllPlayerInfosVersion = 0;

	// Gamepads:
	public string[] Buttons = {"Jump", "Fire", "Power", "Special", "Start"};
	public int GamepadMax = 4; // Dependant on the Input Manager. 11 Controller limit.
	public List<int> ActiveGamepads;
	public List<int> IgnoredGamepads;
	public bool GamepadPopupOpen = false;


	// ========== Get ==========
	public static ClientManager Get () {
		return Instance;
	}


	// ========== Awake ==========
	public void Awake () {
		if (Instance == null)
			Instance = this;
		else
			GameObject.Destroy (this.gameObject);
	}

	
	// ========== Start ==========
	public void Start () {

	}
	
	
	// ========== Update ==========
	public void Update () {
		// Detect New Controllers:
		if (!this.GamepadPopupOpen && NetworkManager.Get ().Active ()) {
			for (int gamepadIDNum = 1; gamepadIDNum <= this.GamepadMax; gamepadIDNum++) {
				if (this.IsGamepadActive (gamepadIDNum))
					continue;
				if (this.IsGamepadIgnored (gamepadIDNum))
					continue;
				string gamepadID = gamepadIDNum.ToString ();
				foreach (string button in this.Buttons) {
					if (Input.GetButton (button + " " + gamepadID)) {
						LevelBase.Get ().Interface.Command ("addgamepad " + gamepadID);
						continue;
					}
				}
			}
		}
	}


	// ========== Reset ==========
	/** Resets the Client Manager in preperation for a new Client/Host setup. **/
	public void Reset () {
		this.SaveLocalPlayerManager ();
		this.DestroyPlayerManagers ();
		if (NetworkManager.Get ().ServerSide ())
			this.GetLocalPlayerManager (true);
	}


	// ========== Destroy Player Managers ==========
	/** Destroys all Player Managers including the local one and empties the collection. **/
	public void DestroyPlayerManagers () {
		foreach (PlayerManager playerManager in this.PlayerManagers.Values) {
			playerManager.Destroy ();
		}
		PlayerManagers.Clear ();
	}


	// ========== Get Local Player Manager ==========
	/** Returns the Local Player Manager, if it does not exist, it is created if create is true. **/
	public PlayerManager GetLocalPlayerManager (bool create = false) {
		if (this.PlayerManagers.ContainsKey (this.LocalConnectionID))
			return this.PlayerManagers [this.LocalConnectionID];

		if (!create)
			return null;
		PlayerManager playerManager = this.CreatePlayerManager (NetworkManager.Get ().client.connection);
		this.LoadLocalPlayerManager (playerManager);
		return playerManager;
	}


	// ========== Load Local Player Manager ==========
	/** Clears existing PlayerInfos the provided PlayerManager and then adds players from PlayerLocalInfos. **/
	public void LoadLocalPlayerManager (PlayerManager playerManager) {
		playerManager.Empty ();
		foreach (PlayerInfoLocal playerInfoLocal in this.LocalPlayers) {
			playerManager.CreatePlayerInfo (playerInfoLocal.GamePadID, playerInfoLocal);
		}
		playerManager.FirstPlayerCheck ();
	}


	// ========== Save Local Player Manager ==========
	/** Copies all relevant values from the Local Player Manager so that it can be used to save player setups when changing Host/Client. **/
	public void SaveLocalPlayerManager () {
		PlayerManager localPlayerManager = this.GetLocalPlayerManager ();
		if (localPlayerManager == null || localPlayerManager.PlayerInfos.Count == 0)
			return;

		// Destroy Current Storage:
		foreach (PlayerInfoLocal playerInfoLocal in this.LocalPlayers) {
			GameObject.Destroy (playerInfoLocal.gameObject);
		}
		this.LocalPlayers.Clear ();

		// Create New Storage:
		foreach (PlayerInfo playerInfo in localPlayerManager.PlayerInfos.Values) {
			this.LocalPlayers.Add (PlayerInfoLocal.CreateFrom (playerInfo));
		}
	}


	// ========== Add Client ==========
	/** Called server side when a client is set to ready, adds a new PlayerManager if it isn't added already. **/
	public void AddClient (NetworkConnection connection) {
		if (this.PlayerManagers.ContainsKey (connection.connectionId))
			return;
		this.CreatePlayerManager (connection);
	}


	// ========== Remove Client ==========
	/** Called server side when a client leaves the server and remvoes it's entry. **/
	public void RemoveClient (NetworkConnection connection) {
		this.RemovePlayerManager (connection.connectionId);
	}


	// ========== Create Player Manager ==========
	/** Creates and instantiates a new PlayerManager and adds it. **/
	public PlayerManager CreatePlayerManager (NetworkConnection connection) {
		GameObject playerManagerObject = GameObject.Instantiate (this.PlayerManagerPrefab);
		PlayerManager playerManager = playerManagerObject.GetComponent<PlayerManager> ();
		playerManager.Connection = connection;
		playerManager.ConnectionID = connection.connectionId;
		this.AddPlayerManager (playerManager);
		return playerManager;
	}


	// ========== Add Player Manager ==========
	/** Adds a Player Manager. **/
	public void AddPlayerManager (PlayerManager playerManager) {
		this.PlayerManagers.Add (playerManager.ConnectionID, playerManager);
		if (NetworkManager.Get ().ClientSide () && playerManager.IsLocal ()) {
			this.LocalConnectionID = playerManager.ConnectionID;
			this.LoadLocalPlayerManager (playerManager);
		}
	}


	// ========== Remove Player Manager ==========
	/** Removes a Player Manager if it exists. **/
	public void RemovePlayerManager (int connectionID) {
		if (!this.PlayerManagers.ContainsKey (connectionID))
			return;
		this.PlayerManagers[connectionID].Destroy ();
		this.PlayerManagers.Remove (connectionID);
	}


	// ========== Add Player ==========
	/** Adds a new player to the Local Player Manager. **/
	public void AddPlayer (int gamepadID) {
		PlayerManager playerManager = this.GetLocalPlayerManager ();
		if (this.IsGamepadActive (gamepadID))
			gamepadID = -1;
		playerManager.CreatePlayerInfo (gamepadID);
	}


	// ========== Remove Player ==========
	/** Removes a player from the Local Player Manager. **/
	public void RemovePlayer (int playerID) {
		PlayerManager playerManager = this.GetLocalPlayerManager ();
		playerManager.RemovePlayerInfo (playerID);
	}


	// ========== Select Character ==========
	public void SelectCharacter (string playerIDString, string characterName) {
		if (!Global.Get ().Characters.ContainsKey (characterName))
			return;
		int playerID = Int32.Parse (playerIDString);
		PlayerManager playerManager = this.GetLocalPlayerManager ();
		if (playerID >= playerManager.PlayerInfos.Count)
			return;
		playerManager.PlayerInfos[playerID].SetPlayerCharacter (characterName);
	}


	// ========== Set Player Gamepad ID ==========
	public void SetPlayerGamepadID (int playerID, int gamepadID) {
		if (this.IsGamepadActive (gamepadID))
			return;

		PlayerManager playerManager = this.GetLocalPlayerManager ();
		PlayerInfo playerInfo = playerManager.PlayerInfos [playerID];
		if (playerInfo == null)
			return;
		playerInfo.GamePadID = gamepadID;
		this.ReserveGamepadID (gamepadID);
	}


	// ========== Gamepad Checks ==========
	public bool Player1HasGamepad () {
		return this.GetLocalPlayerManager ().PlayerInfos[0].GamePadID > 0;
	}

	public bool IsGamepadActive (int gamepadID) {
		return this.ActiveGamepads.Contains (gamepadID);
	}
	
	public bool IsGamepadIgnored (int gamepadID) {
		return this.IgnoredGamepads.Contains (gamepadID);
	}

	public void ReserveGamepadID (int gamepadID) {
		if (this.ActiveGamepads.Contains (gamepadID) || gamepadID <= 0)
			return;
		this.ActiveGamepads.Add (gamepadID);
	}

	public void ReleaseGamepadID (int gamepadID) {
		if (!this.ActiveGamepads.Contains (gamepadID))
			return;
		this.ActiveGamepads.Remove (gamepadID);
	}

	public void IgnoreGamepadID (int gamepadID) {
		if (this.IgnoredGamepads.Contains (gamepadID))
			return;
		this.IgnoredGamepads.Add (gamepadID);
	}

	public void UnignoreGamepadID (int gamepadID) {
		if (!this.IgnoredGamepads.Contains (gamepadID))
			return;
		this.IgnoredGamepads.Remove (gamepadID);
	}


	// ========== Get All Player Infos ==========
	/** Returns a list of all Player Infos from all Player Managers. **/
	public List<PlayerInfo> GetAllPlayerInfos () {
		if (this.AllPlayerInfosVersion != this.Version) {
			this.AllPlayerInfos.Clear ();
			foreach (PlayerManager playerManager in this.PlayerManagers.Values) {
				foreach (PlayerInfo playerInfo in playerManager.PlayerInfos.Values) {
					this.AllPlayerInfos.Add (playerInfo);
				}
			}
			this.AllPlayerInfosVersion = this.Version;
		}
		return this.AllPlayerInfos;
	}
}
