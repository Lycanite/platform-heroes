﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

[NetworkSettings (channel = 1, sendInterval = 0.25f)]
public class NetSyncMovement : NetworkBehaviour {
	public bool ClientOwned = false;
	protected float CurrentTime = 0;

	// Position:
	[SyncVar]
	public bool SyncPosition = true;
	[SyncVar (hook = "OnSyncPosition")]
	public Vector3 Position = new Vector3 (0, 0, 0);
	[System.Serializable]
	public class PositionSyncEntry {
		public Vector3 Position; // The position of this entry.
		public float Time; // The time that this entry was created.
		public float TimeCompleted; // The time that this entry was completed.
		public PositionSyncEntry (Vector3 position, float time) {
			this.Position = position;
			this.Time = time;
			this.TimeCompleted = time;
		}
	}
	public List<PositionSyncEntry> PositionEntries = new List<PositionSyncEntry> (); // The first entry is the position and time to move to.
	public PositionSyncEntry LastPositionEntry = new PositionSyncEntry (new Vector3 (0, 0, 0), 0); // The position to move from.
	
	// Angles:
	[SyncVar]
	public bool SyncAngles = false;
	[SyncVar]
	public Vector3 Angles;

	// Scale:
	[SyncVar]
	public bool SyncScale = false;
	[SyncVar]
	public Vector3 Scale;

	public bool Active = true;


	// ========== Start ==========
	void Start () {
		this.Position = this.transform.position;
		this.Angles = this.transform.eulerAngles;
		this.Scale = this.transform.localScale;

		Debug.Log ("The NetSyncMovement component is active on: " + this.gameObject + " this is still in development and shouldn't be used.");
	}


	// ========== Update ==========
	void Update () {
		if (!this.Active)
			return;
		this.CurrentTime += Time.deltaTime;

		// In Control: (Clients -> Server)
		if ((NetworkManager.Get ().ServerSide () && !this.ClientOwned)
			|| (NetworkManager.Get ().ClientSide () && this.hasAuthority)) {
			if (this.SyncPosition)
				this.Position = this.transform.position;
			if (this.SyncAngles)
				this.Angles = this.transform.eulerAngles;
			if (this.SyncScale)
				this.Scale = this.transform.localScale;
			this.CmdUpdatePos (this.transform.position);
		}

		// Delayed Movement: (Server -> Clients)
		else {
			if (!this.hasAuthority) {
				if (this.SyncPosition) {
					this.UpdatePositionFromHistory ();
					this.transform.position = this.Position;
				}
				if (this.SyncAngles)
					this.transform.eulerAngles = this.Angles;
				if (this.SyncScale)
					this.transform.localScale = this.Scale;
			}
			else {
				Debug.Log ("Unable to apply movement sync to: " + this.gameObject);
			}
		}
	}


	// ========== Update Position ==========
	[Command]
	void CmdUpdatePos (Vector3 pos) {
		this.UpdatePosition (pos);
	}


	// ========== On Sync ==========
	void OnSyncPosition (Vector3 pos) {
		this.UpdatePosition (pos);
	}


	// ========== Update Values ==========
	void UpdatePosition (Vector3 pos) {
		if (this.hasAuthority)
			return;
		
		this.PositionEntries.Add (new PositionSyncEntry (pos, this.CurrentTime));
		if (this.PositionEntries.Count > 20) {
			this.LastPositionEntry = this.PositionEntries[0];
			this.LastPositionEntry.TimeCompleted = this.CurrentTime;
			this.PositionEntries.RemoveAt (0);
		}
		//this.Position = pos; // TEST
	}


	// ========== Update Position From History ==========
	void UpdatePositionFromHistory () {
		int entryMinimum = 1;
		if (this.PositionEntries.Count < entryMinimum)
			return;

		float lastTime = 0;
		if (this.LastPositionEntry != null)
			lastTime = this.LastPositionEntry.TimeCompleted;
		var targetPositionEntry = this.PositionEntries[0];
		float progress = (this.CurrentTime - lastTime) / (targetPositionEntry.Time - this.LastPositionEntry.Time);
		while (progress >= 1) {
			this.LastPositionEntry = targetPositionEntry;
			this.LastPositionEntry.TimeCompleted = this.CurrentTime;
			this.PositionEntries.RemoveAt (0);
			if (this.PositionEntries.Count < entryMinimum)
				return;
			progress = 0;
		}
		Vector3 distance = this.PositionEntries[0].Position - this.LastPositionEntry.Position;
		this.Position = this.LastPositionEntry.Position + (distance * progress);
	}
}