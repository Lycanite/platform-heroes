﻿using UnityEngine;
using System.Collections;

public class CharacterJonBams : MonoBehaviour {
	public CreatureBase Host;

	// ========== Update ==========
	public void Update () {
		if (this.Host == null)
			this.Host = this.GetComponent<CreatureBase> ();
		if (this.Host == null)
			return;

		// Bams Hat:
		if (this.Host.AttackCooldown > 0)
			this.transform.Find ("JonBamsHatMesh").gameObject.SetActive (false);
		else
			this.transform.Find ("JonBamsHatMesh").gameObject.SetActive (true);
	}
}
