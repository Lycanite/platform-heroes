using UnityEngine;
using System.Collections;

public class DamageSource {
	public enum DamageType {
		Physical,
		Poison,
		Fire,
		Ice,
		Electric,
		Light,
		Shadow,
		Drowning
	};

	public int Amount;
	/** Cause - The GameObject that has caused the damage. (Projectile, Spike, Creature Melee, etc) **/
	public GameObject Cause;
	/** Owner - The Owner of the damage or owner of the Cause, sometime the Cause and Owner are the same. (Creature, Spike, etc) **/
	public GameObject Owner;
	/** Type - The type of damage, different damage types will have different effects in some cases. **/
	public DamageType Type;

	// ========== Constructor ==========
	public DamageSource(int amount, GameObject cause, GameObject owner, DamageType type) {
		this.Cause = cause;
		this.Amount = amount;
		this.Owner = owner;
		this.Type = type;
	}
}

