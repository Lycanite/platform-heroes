using UnityEngine;

public class SpawnPointBase : MonoBehaviour {


	// ========== Spawn ==========
	/** Spawns the provided GameObject at their Spawn Point's postion(s). **/
	public virtual GameObject Spawn (GameObject spawnObject) {
		return (GameObject)GameObject.Instantiate (spawnObject, this.GetSpawnPosition (), spawnObject.transform.rotation);
	}


	// ========== Get Spawn Position ==========
	/** Gets the next postion to spawn at. **/
	public virtual Vector3 GetSpawnPosition () {
		return this.transform.position;
	}


	// ========== Has Nearby Player ==========
	/** Returns true if a player is within the provided distance of this spawn point. **/
	public virtual bool HasNearbyPlayer (float distance = 10) {
		foreach (PlayerStats playerStats in LevelBase.Get ().Players) {
			if (playerStats.PlayerObject == null)
				continue;
			if (Vector3.Distance (this.transform.position, playerStats.transform.position) <= distance)
				return true;
		}
		return false;
	}
}
