using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class SpawnerBase : NetworkBehaviour {

	// ===== Spawned Objects =====
	public List<GameObject> SpawnedObjects = new List<GameObject> ();
	public GameObject SpawnPrefab; // The prefab of the object to spawn.
	public GameObject SpawnParent; // The GameObject to parent the spawned object to.
	public GameObject SpawnEffectPrefab; // The effect prefab to spawn when spawning. (This will override creature spawn effect prefabs).
	
	// ===== Spawner Info =====
	public SpawnPointBase SpawnPoint; // The spawn point to spawn from.
	public int SpawnMax = 1; // The maximum amount of creatures to maintain.
	public bool Continuous = true; // If true, this spawner will keep spawning creatures up to the max count.
	public bool Active = true; // If true, this spawner will function, set to false to disable.
	public int SpawnTime = 3; // The time (seconds) between spawning creatures (since the last death if at max count).
	protected float SpawnTimeCount = 0; // The current time between spawns.

	// ===== Creatures =====
	public List<CreatureAI> AI = new List<CreatureAI> (); // A list of AIs to apply to spawned creatures.
	public int StartingJumpHeight = 0; // TODO Package all stats. The StartingJumpHeight a spawned creature should use.
	public bool ReverseDirection = false; // If true, the spawned creature will spawn moving the opposite way (Patrol AI).

	// ===== Network =====
	public bool LocalOnly = false; // If true, this spawner is a cosmetic client side only spawner.


	// ========== Start ==========
	public virtual void Start () {
		// Network Spawning:
		if (NetworkManager.Get ().ServerSide () && !this.LocalOnly) {
			NetworkServer.Spawn (this.gameObject);
		}

		// Components:
		if (this.SpawnPoint == null)
			this.SpawnPoint = this.GetComponent<SpawnPointBase> ();
		if (this.SpawnPoint == null)
			this.SpawnPoint = this.gameObject.AddComponent<SpawnPointBase> ();
	}
	
	
	// ========== Update ==========
	public virtual void Update () {
		if (!NetworkManager.Get ().ServerSide () && !this.LocalOnly)
			return;

		// Check List for Bad Objects:
		foreach (GameObject spawnedObject in this.SpawnedObjects.ToArray ()) {
		if (spawnedObject == null)
			this.SpawnedObjects.Remove (spawnedObject);
		}

		this.SpawnUpdate ();
	}
	
	
	// ========== Spawn Update ==========
	public virtual void SpawnUpdate () {
		if (!this.Active)
			return;

		// Spawn Check:
		if (this.SpawnedObjects.Count >= this.SpawnMax)
			return;

		// Spawn Timer:
		if (this.SpawnTimeCount > 0) {
			this.SpawnTimeCount -= Time.deltaTime;
			return;
		}

		// Spawn:
		GameObject spawnedObject = this.SpawnPoint.Spawn (this.SpawnPrefab);
		if (this.SpawnEffectPrefab != null)
			this.SpawnPoint.Spawn (this.SpawnEffectPrefab);
		this.SpawnedObjects.Add (spawnedObject);
		this.SpawnTimeCount = this.SpawnTime;

		// Creature:
		CreatureBase spawnedCreature = spawnedObject.GetComponent<CreatureBase> ();
		if (spawnedCreature != null) {
			spawnedCreature.BecomeSpawner = false;
			spawnedCreature.Spawner = this;
			spawnedCreature.Depth = this.transform.position.z;
			spawnedCreature.transform.SetParent (this.GetSpawnParent ());

			// AI:
			if (this.AI.Count > 0) {
				spawnedCreature.AI.Clear ();
				foreach (CreatureAI ai in this.AI)
					spawnedCreature.AI.Add (ai.Clone ());
			}

			// Stats:
			if (this.StartingJumpHeight != 0) {
				spawnedCreature.JumpHeight.ValueBase = this.StartingJumpHeight;
				spawnedCreature.JumpHeight.ValueCurrent = this.StartingJumpHeight;
			}
			if (!this.ReverseDirection)
				spawnedCreature.WallRight = true;
			else
				spawnedCreature.WallLeft = true;

			// Network:
			spawnedCreature.LocalOnly = this.LocalOnly;

			// Creature Spawn Effect:
			if (this.SpawnEffectPrefab == null && spawnedCreature.SpawnPrefab != null)
				this.SpawnPoint.Spawn (spawnedCreature.SpawnPrefab);
		}

		if (!this.Continuous)
			this.Active = false;
		this.OnSpawn (spawnedObject);
	}


	// ========== Get Spawn Parent ==========
	/** Returns the Transform that spawned objects to use as their parent when spawning. If null, returns this parent's transform. **/
	public virtual Transform GetSpawnParent () {
		if (this.SpawnParent == null)
			return this.transform.parent;
		return this.SpawnParent.transform;
	}


	// ========== Add ==========
	/** Adds the GameObject to this spawner's ist of spawned objects. **/
	public virtual void Add (GameObject spawnedObject) {
		if (!this.SpawnedObjects.Contains (spawnedObject))
			this.SpawnedObjects.Add (spawnedObject);
	}


	// ========== Remove ==========
	/** Removes the GameObject from this spawner's ist of spawned objects. **/
	public virtual void Remove (GameObject spawnedObject) {
		if (this.SpawnedObjects.Contains (spawnedObject))
			this.SpawnedObjects.Remove (spawnedObject);
	}
	
	
	// ========== On Spawn ==========
	/** Called when this spawner spawns an object. Useful for extending this class. **/
	public virtual void OnSpawn (GameObject spawnedObject) {

	}
	
	
	// ========== On Death ==========
	/** Called by an object spawned by this spawner when it dies. **/
	public virtual void OnDeath (GameObject spawnedObject) {
		this.Remove (spawnedObject);
	}


	// ========== Copy From Creature ==========
	/** Sets this spawner's prefab, stats, AI and other presets to the same as the provided creature. **/
	public virtual void CopyFromCreature (CreatureBase creature) {
		this.SpawnPrefab = Global.Get ().Creatures[creature.CreatureName.ToLower ()];
		this.SpawnParent = creature.transform.parent.gameObject;

		// AI:
		foreach (CreatureAI ai in creature.AI)
			this.AI.Add (ai.Clone ());

		// Stats:
		this.StartingJumpHeight = creature.StartingJumpHeight; // TODO Package all stats.
		if (creature.WallRight && !creature.WallLeft)
			this.ReverseDirection = true;

		// Network:
		this.LocalOnly = creature.LocalOnly;
	}
}
