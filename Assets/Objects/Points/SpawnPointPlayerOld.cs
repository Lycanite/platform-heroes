﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPointPlayerOld : SpawnerBase {
	
	
	// ========== Start ==========
	public override void Start () {

	}

	// ===== Player Info =====
	public int PlayerNumber = 0; // 0 = Any player.


	// ========== On Spawn ==========
	public override void OnSpawn (GameObject spawnedObject) {
		LevelBase.Get ().Camera.SetFollowTarget (spawnedObject); // TODO TEMP
	}
}
