using UnityEngine;

public class PickupLife : PickupBase {
	public int Lives = 1;


	// ========== Pickup ==========
	public override void OnPickup (GameObject gameObject) {
		// Players:
		CreaturePlayer player = gameObject.GetComponent<CreaturePlayer> ();
		if (player != null) {
			player.AddLives (this.Lives);
			this.PickedUp ();
		}
	}
}
