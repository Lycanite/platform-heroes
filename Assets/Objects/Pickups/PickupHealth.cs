using UnityEngine;

public class PickupHealth : PickupBase {
	public float Health = 0; // The amount of health to heal by, if 0, this will fully recover health.


	// ========== Pickup ==========
	public override void OnPickup (GameObject gameObject) {
		// Players:
		CreaturePlayer player = gameObject.GetComponent<CreaturePlayer> ();
		if (player != null) {
			if (player.Heal (this.Health)) {
				this.PickedUp ();
			}
		}
	}
}
