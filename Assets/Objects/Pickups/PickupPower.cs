using UnityEngine;
using System.Collections;

public class PickupPower : PickupBase {
	public PowerBase Power;
	public PowerManager.PowerID PowerID;
	
	// ========== Start ==========
	public override void Start () {
		base.Start ();
		this.Power = PowerManager.Get ().GetPower (this.PowerID);
	}

	
	// ========== Pickup ==========
	public override void OnPickup (GameObject gameObject) {
		// Creatures and Players with Power Support:
		if (gameObject.GetComponent<CreatureLogicPower> () != null) {
			CreatureLogicPower creaturePower = gameObject.GetComponent<CreatureLogicPower> ();
			if (creaturePower.AddPower (this.Power)) {
				this.PlayPickupSound ();
			}
			this.PickedUp ();
		}
	}


	// ========== Update Pickup ==========
	public override void UpdatePickup (GameObject gameObject) {
		// Top Up Creatures and Players with Power Support:
		CreatureLogicPower creaturePower = gameObject.GetComponent<CreatureLogicPower> ();
		if (creaturePower != null) {
			creaturePower.AddPower (this.Power);
		}
	}
}
