using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

[NetworkSettings (channel = 1, sendInterval = 0.25f)]
public class PickupBase : NetworkBehaviour {
	// Sound:
	protected AudioSource AudioSource;
	protected float PickupSoundPitch = 1;
	
	// Stats:
	[SyncVar (hook="OnSyncActive")] public bool Active = true;
	public bool Consumable = true;
	public bool Respawn = false;
	public float PickedUpDelay = 6;
	public float PickedUpDelayTime = 0;

	// Objects:
	public GameObject EffectObject;
	public List<GameObject> CollidedGameObjects = new List<GameObject> ();

	// Network:
	protected bool LocalPickup = false; // If true, this pickup is independant on each client.

	// ========== Start ==========
	public virtual void Start () {
		if (this.GetComponent<AudioSource> () == null) {
			this.gameObject.AddComponent<AudioSource> ().playOnAwake = false;
		}
		this.AudioSource = this.GetComponent<AudioSource> ();

		if (NetworkManager.Get ().ServerSide ())
			NetworkServer.Spawn (this.gameObject);
	}


	// ========== Update ==========
	public virtual void Update () {
		// Respawning or Destruction:
		if (NetworkManager.Get ().ServerSide () || this.LocalPickup) {
			if (this.PickedUpDelayTime > 0) {
				this.PickedUpDelayTime -= Time.deltaTime;
				if (this.PickedUpDelayTime <= 0) {
					if (this.Respawn)
						this.Activate ();
					else
						GameObject.Destroy (this.gameObject);
				}
			}
		}

		// Collided GameObjects:
		foreach (GameObject gameObject in this.CollidedGameObjects) {
			if (gameObject != null) {
				this.UpdatePickup (gameObject);
			}
		}
	}
	
	
	// ========== Collision ==========
	public virtual void OnGameObjectCollisionEnter (GameObject gameObject) {
		if (!this.Active || (NetworkManager.Get ().ClientSide () && !this.LocalPickup))
			return;
		
		if (!this.CollidedGameObjects.Contains (gameObject)) {
			this.CollidedGameObjects.Add (gameObject);
			this.OnPickup (gameObject);
		}
	}

	public virtual void OnGameObjectCollisionExit (GameObject gameObject) {
		if (this.CollidedGameObjects.Contains (gameObject))
			this.CollidedGameObjects.Remove (gameObject);
	}


	// ========== Pickup ==========
	/** Called when a GameObject first collides with the pickup. Server Side unless LocalPickup is true. **/
	public virtual void OnPickup (GameObject gameObject) {}


	// ========== Update Pickup ==========
	/** Called on Update for all GameObjects within the pickup. Server Side unless LocalPickup is true. **/
	public virtual void UpdatePickup (GameObject gameObject) {}


	// ========== Picked Up ==========
	/** Called when this pickup is picked up, plays sounds, effects, etc. **/
	public virtual void PickedUp () {
		if (!this.Consumable)
			return;

		this.PlayPickupSound ();
		this.PlayPickupEffect ();
		this.Deactivate ();
	}
	

	// ========== Play Sound ==========
	/** Plays the pickup sound effect, will also send to clients. **/
	public void PlayPickupSound () {
		this.OnPlayPickupSound ();
		this.DoPlayPickupSound (this.PickupSoundPitch);
		if (NetworkManager.Get ().ServerSide ()) {
			this.RpcPlayPickupSound (this.PickupSoundPitch);
		}
	}

	[ClientRpc]
	protected void RpcPlayPickupSound (float pitch) {
		this.DoPlayPickupSound (pitch);
	}

	protected void DoPlayPickupSound (float pitch) {
		this.AudioSource.pitch = pitch;
		this.AudioSource.Play ();
	}

	public virtual void OnPlayPickupSound () { }


	// ========== Play Pickup Effect ==========
	/** Plays the pickup partcile effect if one exists, will also send to clients. **/
	public void PlayPickupEffect () {
		if (this.EffectObject == null)
			return;
		this.DoPlayPickupEffect ();
		if (NetworkManager.Get ().ServerSide ()) {
			this.RpcPlayPickupEffect ();
		}
	}

	[ClientRpc]
	protected void RpcPlayPickupEffect () {
		this.DoPlayPickupEffect ();
	}

	protected void DoPlayPickupEffect () {
		if (this.EffectObject == null)
			return;
		GameObject effectObject = GameObject.Instantiate (this.EffectObject);
		effectObject.transform.position = this.transform.position;
	}


	// ========== Activation ==========
	public virtual void Activate () {
		this.Active = true;
		if (this.GetComponent<Collider> () != null)
			this.gameObject.GetComponent<Collider> ().enabled = true;
		if (this.GetComponent<MeshRenderer> () != null)
			this.gameObject.GetComponent<MeshRenderer> ().enabled = true;
		if (this.GetComponent<ParticleSystem> () != null)
			this.gameObject.GetComponent<ParticleSystem> ().Play ();
		if (this.GetComponent ("Halo") != null) {
			Component halo = this.gameObject.GetComponent ("Halo");
			halo.GetType ().GetProperty ("enabled").SetValue (halo, true, null);
		}
		foreach (Transform child in this.transform) {
			child.gameObject.SetActive (true);
		}
	}

	public virtual void Deactivate () {
		this.Active = false;
		this.CollidedGameObjects.Clear ();
		this.PickedUpDelayTime = this.PickedUpDelay;
		if (this.GetComponent<Collider> () != null)
			this.gameObject.GetComponent<Collider> ().enabled = false;
		if (this.GetComponent<MeshRenderer> () != null)
			this.gameObject.GetComponent<MeshRenderer> ().enabled = false;
		if (this.GetComponent<ParticleSystem> () != null)
			this.gameObject.GetComponent<ParticleSystem> ().Stop ();
		if (this.GetComponent ("Halo") != null) {
			Component halo = this.gameObject.GetComponent ("Halo");
			halo.GetType ().GetProperty ("enabled").SetValue (halo, false, null);
		}
		foreach (Transform child in this.transform) {
			child.gameObject.SetActive (false);
		}
	}

	public virtual void OnSyncActive (bool active) {
		if (this.LocalPickup)
			return;
		if (active)
			this.Activate ();
		else
			this.Deactivate ();
	}
}
