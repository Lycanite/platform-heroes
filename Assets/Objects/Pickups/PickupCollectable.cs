using System.Collections.Generic;
using UnityEngine;

public class PickupCollectable : PickupBase {
	// Collectable Info:
	public enum CollectableType {
		Crest,
		Key
	}
	public CollectableType Type;

	// Components:
	public GameObject CrestObject;

	// ========== Start ==========
	public override void Start () {
		base.Start ();
		this.LocalPickup = true;

		// Crest Setup:
		if (this.Type == CollectableType.Crest && this.CrestObject != null) {
			if (LevelBase.Get ().LevelInfo.CrestMesh != null)
				this.CrestObject.GetComponent<MeshFilter> ().mesh = LevelBase.Get ().LevelInfo.CrestMesh;
			if (LevelBase.Get ().LevelInfo.CrestMaterial != null)
				this.CrestObject.GetComponent<MeshRenderer> ().material = LevelBase.Get ().LevelInfo.CrestMaterial;
			if (LevelBase.Get ().LevelInfo.ZoneInfo.CrestSound != null)
				this.GetComponent<AudioSource> ().clip = LevelBase.Get ().LevelInfo.ZoneInfo.CrestSound;
		}
	}


	// ========== Pickup ==========
	public override void OnPickup (GameObject gameObject) {
		// Players:
		CreaturePlayer player = gameObject.GetComponent<CreaturePlayer> ();
		if (player == null)
			return;

		if (this.Type == CollectableType.Crest)
			LevelBase.Get ().Stats.CrestCollected = true;
		else if (this.Type == CollectableType.Key)
			LevelBase.Get ().Stats.KeyCollected = true;

		this.PickedUp ();
	}
}
