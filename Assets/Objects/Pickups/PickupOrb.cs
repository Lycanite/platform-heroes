﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class PickupOrb : PickupBase {
	public int Score = 1;
	public bool Multiply = true;
	public PlayerStats PickupPlayer;
	
	// ========== Pickup ==========
	public override void OnPickup (GameObject gameObject) {
		if (!this.Active)
			return;

		// Players:
		if (gameObject.GetComponent<CreaturePlayer> () != null) {
			CreaturePlayer player = gameObject.GetComponent<CreaturePlayer> ();
			this.PickupPlayer = player.PlayerStats;
			player.AddScore (this.Score, this.Multiply);
			this.PickedUp ();
		}
	}
	
	
	// ========== Play Pickup Sound ==========
	public override void OnPlayPickupSound () {
		this.PickupSoundPitch = 0.99f + (this.PickupPlayer.Multiplier / 50) % 10;
	}
}
