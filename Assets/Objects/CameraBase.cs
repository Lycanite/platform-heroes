using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class CameraBase : MonoBehaviour {

	public enum CameraState {
		Menu,
		Target
	};

	public CameraState State; // The current state that this camera is in.

	public Vector3 FocusPosition; // The position to focus on (follow, etc).

	public Vector3 TargetPosition; // The current position that the camera is moving to this update.
	public Quaternion TargetRotation; // The current rotation that the camera is turning to this update.
	public Vector3 TargetAngle = new Vector3 (0, 1, 0); // The angle to rotate around.

	public Vector3 LastStatePosition; // The position this camera was at in its last state.
	public Quaternion LastStateRotation; // The rotation this camera was at in its last state.

	public float StateSwapTime = 0; // The current time (seconds) spent transitioning from one state position to another.
	public float StateSwapTimeMax = 1; // The time (seconds) it takes to transition from one state position to another.
	public float MoveSpeed; // The speed that this camera moves at.
	public float Distance = 10; // The distance this camera should keep from the focus position.
	public float Height = 5; // The height this camera should have relative to the focus position.
	public float TiltOffset = 35F; // The tilt offset used by this camera.

	public float MinY = -20; // The minimum y position that this camera can go to. TODO Use a min and max Vector3 for more advanced restrictions.

	public GameObject FollowTarget; // The target gameobject that this camera should position the focus on.

	public List<GameObject> MenuFollowTargets; // A list of gameobjects to randomly cycle through when in the menu state.
	public float MenuTargetSwapTime = 5; // The current time (seconds) until this camera should swap menu follow target.
	public float MenuTargetSwapTimeMin = 5; // The max time (seconds) it takes for this camera to randomly swap menu follow target.
	public float MenuTargetSwapTimeMax = 10; // The min time (seconds) it takes for this camera to randomly swap menu follow target.

	public List<AudioSource> GlobalSounds = new List<AudioSource> (); // A list of global sound effects to be played on the camera itself.

	// Water:
	public List<Collider> WaterColliders = new List<Collider> (); // A list of water colliders that camera has collided with (for water effects, deprecated).
	public float WaterSubmerge = 0; // How submerged underwater the camera view is (for water effects, deprecated).

	// Music:
	protected AudioSource MusicSource; // The first AudioSource added to this camera (if none created on start) used for playing music.
	

	// ========== Start ==========
	public void Start () {
		this.LastStatePosition = this.transform.position;
		this.LastStateRotation = this.transform.rotation;
		this.MusicSource = this.GetComponent<AudioSource> ();
		if (this.MusicSource == null)
			this.MusicSource = this.gameObject.AddComponent<AudioSource> ();
	}


	// ========== Update ==========
	public void Update () {
		// Menu State:
		if (this.State == CameraState.Menu) {
			this.MenuTargetSwapTime -= Time.deltaTime;
			if (this.MenuTargetSwapTime <= 0 && this.MenuFollowTargets.Count > 0) {
				GameObject newFollowTarget = this.MenuFollowTargets[UnityEngine.Random.Range (0, this.MenuFollowTargets.Count)];
				this.MenuFollowTargets.Add (this.FollowTarget);
				this.FollowTarget = newFollowTarget;
				this.MenuFollowTargets.Remove (this.FollowTarget);
				this.MenuTargetSwapTime = UnityEngine.Random.Range (this.MenuTargetSwapTimeMin, this.MenuTargetSwapTimeMax);
				this.SetState (CameraState.Menu);
			}
		}

		// Follow Target:
		if (this.FollowTarget != null) {
			this.SetFocusPosition (this.FollowTarget.transform.position);
			CameraTarget cameraTarget = this.FollowTarget.GetComponent<CameraTarget> ();
			if (cameraTarget != null)
				this.Distance = cameraTarget.Distance;
		}

		// Set Target:
		Vector3 rotatedAngle = Quaternion.Euler (this.TargetAngle) * new Vector3 (0, this.Height, -this.Distance);
		this.TargetPosition = rotatedAngle + this.GetFocusPosition ();
		this.ApplyTarget ();

		// Music:
		if (LevelBase.Get () != null) {
			AudioClip music = LevelBase.Get ().GetMusic ();
			if (music != null && this.MusicSource.clip != music) {
				this.MusicSource.Stop ();
				this.MusicSource.clip = music;
				this.MusicSource.loop = true;
				this.MusicSource.Play ();
			}
		}

		// Clean Up Global Sounds:
		if (this.GlobalSounds.Count > 0) {
			foreach (AudioSource audioSource in this.GlobalSounds.ToArray ()) {
				if (!audioSource.isPlaying) {
					this.GlobalSounds.Remove (audioSource);
					GameObject.Destroy (audioSource);
				}
			}
		}
	}
	
	
	// ========== Apply Targets ==========
	/** Called after all update logic where the camera moves to it's target position and rotation. **/
	public void ApplyTarget () {
		// Smoothly Transition Between State Targets:
		if (this.StateSwapTime < this.StateSwapTimeMax) {
			this.StateSwapTime += Time.deltaTime * 1.0F;
			this.transform.position = Vector3.Lerp (this.LastStatePosition, this.TargetPosition, this.StateSwapTime);
			this.transform.rotation = Quaternion.Slerp (this.LastStateRotation, this.TargetRotation, this.StateSwapTime);
			return;
		}
		
		// Clamp:
		this.TargetPosition.y = Mathf.Max (this.MinY, this.TargetPosition.y);

		// Snap To AI Position:
		this.transform.position = Vector3.Lerp(this.transform.position, this.TargetPosition, Time.deltaTime * this.GetFocusDistance () * 0.2f);
		this.transform.rotation = this.TargetRotation;

		// Update DoF Distance:
		DepthOfField dof = this.GetComponent<DepthOfField> ();
		if (dof != null) {
			dof.focalLength = this.Distance;
		}
	}
	
	
	// ========== Set State ==========
	/** Used to swap states and set transition time between state targets. **/
	public void SetState (CameraState state) {
		// Set AI Transition:
		this.StateSwapTime = 0;
		this.LastStatePosition = this.transform.position;
		this.LastStateRotation = this.transform.rotation;
		this.State = state;
	}
	
	
	// ========== Set Focus Position ==========
	/** Sets the focus position. **/
	public void SetFocusPosition (Vector3 focusPosition) {
		this.FocusPosition = focusPosition;
	}
	
	
	// ========== Get Focus Position ==========
	/** Gets the focus position. **/
	public Vector3 GetFocusPosition () {
		return this.FocusPosition;
	}
	
	
	// ========== Get Focus Direction ==========
	/** Gets the focus direction for the camera to look to. **/
	public Vector3 GetFocusDirection () {
		return this.GetFocusPosition () - this.transform.position;
	}
	
	
	// ========== Get Focus Distance ==========
	/** Gets the distance between the camera and the position of the focus. **/
	public float GetFocusDistance () {
		return Vector3.Distance (this.GetFocusPosition (), this.transform.position);
	}


	// ========== Set Follow Target ==========
	/** Sets the GameObject for the Camera to follow. **/
	public void SetFollowTarget (GameObject targetObject) {
		this.FollowTarget = targetObject;
		this.SetState (CameraState.Target);
	}


	// ========== Water Colliders ==========
	public void AddWaterCollider (Collider collider) {
		if (!this.WaterColliders.Contains (collider))
			this.WaterColliders.Add (collider);
	}

	public void RemoveWaterCollider (Collider collider) {
		if (this.WaterColliders.Contains (collider))
			this.WaterColliders.Remove (collider);
	}


	// ========== Play Sound ==========
	public void PlaySound (AudioClip audioClip) {
		AudioSource audioSource = this.gameObject.AddComponent<AudioSource> ();
		this.GlobalSounds.Add (audioSource);
		audioSource.clip = audioClip;
		audioSource.loop = false;
		audioSource.Play ();
	}
}
