using System.Collections.Generic;
using UnityEngine;

public class AIPackage : MonoBehaviour {
	public bool Shared = true; // If true, creatures will clone AIs from this package instead of taking them.
	public List<CreatureAI> AI = new List<CreatureAI> ();

	// ========== Add AI ==========
	/** Adds AI to this Ai Package. **/
	public void AddAI (CreatureAI ai) {
		this.AI.Add (ai);
	}

	// ========== Remove AI ==========
	/** Adds AI to this Ai Package. **/
	public void RemoveAI (CreatureAI ai) {
		if (this.AI.Contains (ai))
			this.AI.Remove (ai);
	}

	// ========== Add To Creatures ==========
	/** Adds this package to all Creatures that are child objects. **/
	public void AddToCreatures () {
		foreach (CreatureBase creature in this.GetComponentsInChildren <CreatureBase> ()) {
			if (!creature.AIPackages.Contains (this))
				creature.AIPackages.Add (this);
		}
	}

	// ========== Remove From Creatures ==========
	/** Removes this package from all Creatures that are child objects. **/
	public void RemoveFromCreatures () {
		foreach (CreatureBase creature in this.GetComponentsInChildren<CreatureBase> ()) {
			if (creature.AIPackages.Contains (this))
				creature.AIPackages.Remove (this);
		}
	}
}
