using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class CreatureAINavigate : CreatureAI {
	public Navigator Navigator;
	public List<GameObject> Nodes = new List<GameObject> ();
	public CreatureAINavigate CopyAINodes;

	public bool StartAtClosestNode = true;
	public bool Loop = false;
	public bool StartInReverse = false;

	// ========== Can Start ==========
	/** Returns true if this AI can start executing. **/
	public override bool CanStart () {
		if (this.Host.AIMovementWait > 0)
			return false;
		if (this.Nodes.Count == 0 && this.CopyAINodes != null)
			this.Nodes = this.CopyAINodes.Nodes;
		return this.Nodes.Count > 0;
	}


	// ========== On Start ==========
	/** Called when this AI starts. **/
	public override void OnStart () {
		this.Navigator = new Navigator ();
		foreach (GameObject node in this.Nodes)
			this.Navigator.AddNode (node);
		if (this.StartAtClosestNode)
			this.Navigator.SetTargetNode (this.Navigator.GetClosestNode (this.Host.transform.position));
		this.Navigator.Loop = this.Loop;
		this.Navigator.Reverse = this.StartInReverse;
	}
	
	
	// ========== On Update ==========
	/** Called when this AI updates. **/
	public override void OnUpdate () {
		this.Navigator.UpdateNavigator (this.Host.transform.position);
		Vector3 direction = this.Navigator.GetTargetNode ().GetDirection (this.Host.transform.position);
		this.Host.Movement.Move (direction.x, direction.y);
	}


	// ========== Clone ==========
	/** Returns a new CreatureAI instance based on this instance. **/
	public override CreatureAI Clone (CreatureBase targetHost = null) {
		CreatureAINavigate aiClone = (CreatureAINavigate) base.Clone (targetHost);
		aiClone.Nodes = this.Nodes;
		aiClone.StartAtClosestNode = this.StartAtClosestNode;
		aiClone.Loop = this.Loop;
		aiClone.StartInReverse = this.StartInReverse;
		return aiClone;
	}


	// ========== Create Instance ==========
	/** Creatures a new instance of this class. **/
	public override CreatureAI CreateInstance (CreatureBase host, string name, int priority, int bitmask) {
		CreatureAI ai = (CreatureAI)ScriptableObject.CreateInstance<CreatureAINavigate> ();
		ai.Init (host, name, priority, bitmask);
		return ai;
	}
}

