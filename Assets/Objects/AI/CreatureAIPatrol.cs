using UnityEngine;

[System.Serializable]
public class CreatureAIPatrol : CreatureAI {
	protected float Movement = 1;

	// ========== Can Start ==========
	/** Returns true if this AI can start executing. **/
	public override bool CanStart () {
		return this.Host.AIMovementWait <= 0;
	}
	
	
	// ========== On Update ==========
	/** Called when this AI updates. **/
	public override void OnUpdate () {
		if (this.Host.WallLeft && this.Host.WallRight)
			this.Host.Movement.Move (0);
		else if (this.Host.WallLeft)
			this.Movement = 1;
		else if (this.Host.WallRight)
			this.Movement = -1;

		this.Host.Movement.Move (this.Movement);
	}


	// ========== Clone ==========
	/** Returns a new CreatureAI instance based on this instance. **/
	public override CreatureAI Clone (CreatureBase targetHost = null) {
		CreatureAIPatrol aiClone = (CreatureAIPatrol)base.Clone (targetHost);
		return aiClone;
	}


	// ========== Create Instance ==========
	/** Creatures a new instance of this class. **/
	public override CreatureAI CreateInstance (CreatureBase host, string name, int priority, int bitmask) {
		CreatureAI ai = (CreatureAI)ScriptableObject.CreateInstance<CreatureAIPatrol> ();
		ai.Init (host, name, priority, bitmask);
		return ai;
	}
}

