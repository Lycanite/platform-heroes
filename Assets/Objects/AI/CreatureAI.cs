using UnityEngine;

[System.Serializable]
public abstract class CreatureAI : ScriptableObject {
	public static int BitmaskMovement = 1; // The default bitmask for AIs that control movement.
	public static int BitmaskTargetAttack = 2; // The default bitmask for AIs that control attack targeting.
	public static int BitmaskAttack = 4; // The default bitmask for AIs that control primary attacks.

	/** Host - The creature that is using this AI. **/
	public CreatureBase Host;
	/** Name - The name of this AI. **/
	public string Name;
	/** Priority - The priority of this AI over other AIs with a conflicting Bitmask. **/
	public int Priority;
	/** Bitmask - Used to dictate which AIs conflict with each other such as random moving conflciting with chasing. **/
	public int Bitmask;

	// ========== Init ==========
	public virtual void Init (CreatureBase host, string name, int priority, int bitmask) {
		this.Host = host;
		this.Name = name;
		this.Priority = priority;
		this.Bitmask = bitmask;
	}

	
	// ========== Bitmask Overlap ==========
	/** Returns true if the provided bitmask overlaps with this AI's bitmask. **/
	public virtual bool BitmaskOverlap (int targetBitmask) {
		return (this.Bitmask & targetBitmask) != 0;
	}
	
	
	// ========== Can Start ==========
	/** Returns true if this AI can start executing. **/
	public virtual bool CanStart () {
		return true;
	}
	
	
	// ========== Can Continue ==========
	/** Called after Execute () and returns true if this AI can continue executing. **/
	public virtual bool CanContinue () {
		return this.CanStart ();
	}
	
	
	// ========== On Start ==========
	/** Called when this AI starts. **/
	public virtual void OnStart () {
		
	}


	// ========== On Stop ==========
	/** Called when this AI stops. **/
	public virtual void OnStop () {

	}
	
	
	// ========== On Update ==========
	/** Called when this AI updates. **/
	public virtual void OnUpdate () {

	}


	// ========== Clone ==========
	/** Returns a new CreatureAI instance based on this instance. **/
	public virtual CreatureAI Clone (CreatureBase targetHost = null) {
		CreatureAI aiClone = this.CreateInstance (targetHost, this.Name, this.Priority, this.Bitmask);
		return aiClone;
	}


	// ========== Create Instance ==========
	/** Creatures a new instance of this class. **/
	public abstract CreatureAI CreateInstance (CreatureBase host, string name, int priority, int bitmask);
}

