using UnityEngine;
using System.Collections;

[System.Serializable]
public class CreatureAIPlayer : CreatureAI {
	protected bool JumpLock = false;
	protected bool AttackLock = false;
	protected bool PowerLock = false;
	protected string gamepadID;
	protected bool gamepad;
	protected bool mouseKeyboard;

	// ========== Can Start ==========
	/** Returns true if this AI can start executing. **/
	public override bool CanStart () {
		return true;
	}
	
	
	// ========== On Update ==========
	/** Called when this AI updates. **/
	public override void OnUpdate () {
		/*if (((CreaturePlayer)this.Host).PlayerStats == null || !((CreaturePlayer)this.Host).PlayerStats.Info.IsLocal ())
			return;*/
		if (!this.Host.LocalOwned ())
			return;

		int gamePadIDNum = ((CreaturePlayer)this.Host).PlayerStats.Info.GamePadID;
		this.gamepad = (gamePadIDNum >= 1 && gamePadIDNum <= 11);
		this.gamepadID = gamePadIDNum.ToString ();
		this.mouseKeyboard = ((CreaturePlayer)this.Host).PlayerStats.Info.MouseKeyboard;
		if (!this.gamepad && !this.mouseKeyboard)
			return;
		
		// Move:
		if (this.GetAxis ("Horizontal") != 0)
			this.Host.Movement.Move (this.GetAxis ("Horizontal"));

		// Jump:
		if (this.GetButton ("Jump") && !this.JumpLock) {
			this.JumpLock = true;
			this.Host.Movement.Jump ();
		}
		else if (!this.GetButton ("Jump"))
			this.JumpLock = false;
		
		// Attack:
		if (this.GetButton ("Fire") && !this.AttackLock) {
			this.AttackLock = true;
			this.Host.Attack ();
		}
		else if (!this.GetButton ("Fire"))
			this.AttackLock = false;

		// Use Power:
		if (this.GetButton ("Power") && !this.PowerLock) {
			this.PowerLock = true;
			this.Host.UsePower ();
		}
		else if (!this.GetButton ("Power"))
			this.PowerLock = false;
	}


	// ========== Get Button ==========
	/** Gets a button based on the gamepad and/or keyboard relative to this player. **/
	public virtual bool GetButton (string button) {
		if (this.mouseKeyboard && Input.GetButton (button + " Keyboard"))
			return true;
		if (this.gamepad && Input.GetButton (button + " " + this.gamepadID))
			return true;
		return false;
	}


	// ========== Get Axis ==========
	/** Gets an axis based on the gamepade and/or keyboard relative to this player. **/
	public virtual float GetAxis (string axis) {
		if (this.gamepad && Input.GetAxis (axis + " " + this.gamepadID) != 0) {
			return Input.GetAxis (axis + " " + this.gamepadID);
		}
		if (this.mouseKeyboard && Input.GetAxis (axis + " Keyboard") != 0)
			return Input.GetAxis (axis + " Keyboard");
		return 0;
	}


	// ========== Clone ==========
	/** Returns a new CreatureAI instance based on this instance. **/
	public override CreatureAI Clone (CreatureBase targetHost = null) {
		CreatureAIPlayer aiClone = (CreatureAIPlayer)base.Clone (targetHost);
		return aiClone;
	}


	// ========== Create Instance ==========
	/** Creatures a new instance of this class. **/
	public override CreatureAI CreateInstance (CreatureBase host, string name, int priority, int bitmask) {
		CreatureAI ai = (CreatureAI)ScriptableObject.CreateInstance<CreatureAIPlayer> ();
		ai.Init (host, name, priority, bitmask);
		return ai;
	}
}

