#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (AIPackage))]
public class AIPackageEditor : Editor {

	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		AIPackage aiPackage = (AIPackage)this.target;

		// Components:
		EditorGUILayout.BeginVertical ("HelpBox");
		GUILayout.Label ("AI Components", "BoldLabel");
		if (GUILayout.Button ("Add Creature AI Navigate Setup")) {
			aiPackage.gameObject.AddComponent<CreatureAINavigateSetup> ();
		}
		EditorGUILayout.EndVertical ();

		// Users:
		EditorGUILayout.BeginVertical ("HelpBox");
		GUILayout.Label ("AI Users", "BoldLabel");
		if (GUILayout.Button ("Add To Creatures")) {
			aiPackage.AddToCreatures ();
		}
		if (GUILayout.Button ("Remove From Creatures")) {
			aiPackage.RemoveFromCreatures ();
		}
		EditorGUILayout.EndVertical ();
	}
}
#endif
