using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Navigator {
	[System.Serializable]
	public class NavigatorNode {
		public int Index = 0;
		public GameObject NodeObject;
		protected Vector3 NodePosition = new Vector3 (0, 0, 0);

		public NavigatorNode (GameObject gameObject) {
			this.NodeObject = gameObject;
			this.NodePosition = gameObject.transform.position;
		}

		public NavigatorNode (Vector3 position) {
			this.NodePosition = position;
		}

		/* Gets the position of this node. */
		public Vector3 GetPosition () {
			if (this.NodeObject != null)
				this.NodePosition = this.NodeObject.transform.position;
			return this.NodePosition;
		}

		/* Gets the direction facing this node from the provided position. */
		public Vector3 GetDirection (Vector3 position) {
			Vector3 heading = this.GetPosition () - position;
			float distance = heading.magnitude;
			if (distance != 0)
				return (heading / distance);
			else
				return new Vector3 (0, 0, 0);
		}
	}

	public interface INavigatorListener {
		void OnNavigatorNodeReached (NavigatorNode node); // Called when a node is reached.
		void OnNavigatorFirstNodeReached (NavigatorNode node); // Called when the node at the first index is reached.
		void OnNavigatorLastNodeReached (NavigatorNode node); // Called when the node at the last index is reached.
	}

	// ========== Components ==========
	/* User - If not null, the user will be called by various node methods. */
	public INavigatorListener Listener;

	// ========== Nodes ==========
	/* Nodes - A list of all nodes to navigate. */
	public List<NavigatorNode> Nodes = new List<NavigatorNode> ();
	/* Target Node - The node we are heading towards. */
	public int TargetNodeIndex;
	/* Previous Node - The last node we were at. */
	public int PreviousNodeIndex;

	// ========== Navigation ==========
	/* Loop - If true, when the last node is reached, the first node will be set as the next target. */
	public bool Loop = false;
	/* Target Distance - When the target node is within this distance it is seen as reached. This should never be less than the movement speed of the user. */
	public float TargetDistance = 0.5f;

	// ========== Condition ==========
	/* Reverse - If true, the nodes are navigated in reverse order. This is automatically set when the last node is reached if not looping. */
	public bool Reverse = false;


	// ========== Update navigator ==========
	/* Updates this navigator, handling if nodes are reached, getting the next node, etc. */
	public void UpdateNavigator (Vector3 position) {
		if (this.NodeReached (position)) {

			// Listener:
			if (this.Listener != null) {
				this.Listener.OnNavigatorNodeReached (this.GetTargetNode ());
				if (this.TargetNodeIndex == 0)
					this.Listener.OnNavigatorFirstNodeReached (this.GetTargetNode ());
				else if (this.TargetNodeIndex == this.Nodes.Count - 1)
					this.Listener.OnNavigatorLastNodeReached (this.GetTargetNode ());
			}

			this.NextNode ();
		}
	}


	// ========== Node Reached ==========
	/* Returns true if the target node is reached from the specified position. */
	public bool NodeReached (Vector3 position) {
		if (Vector3.Distance (this.GetTargetNode ().GetPosition (), position) <= this.TargetDistance)
			return true;
		return false;
	}


	// ========== Next Node ==========
	/* Sets the target node to the next node in the path. Also handles looping and reversing. */
	public void NextNode () {
		int nextNodeIndex = this.TargetNodeIndex;
		if (!this.Reverse) {
			nextNodeIndex++;
			if (nextNodeIndex >= this.Nodes.Count) {
				if (this.Loop)
					nextNodeIndex = 0;
				else {
					nextNodeIndex -= 2;
					this.Reverse = true;
				}
			}
		}
		else {
			nextNodeIndex--;
			if (nextNodeIndex < 0) {
				if (this.Loop)
					nextNodeIndex = this.Nodes.Count - 1;
				else {
					nextNodeIndex += 2;
					this.Reverse = false;
				}
			}
		}
		this.SetTargetNode (this.Nodes[nextNodeIndex]);
	}


	// ========== Get Target Node ==========
	/* Returns the current node the navigator is navigating towards. */
	public NavigatorNode GetTargetNode () {
		return this.Nodes [this.TargetNodeIndex];
	}


	// ========== Get Previous Node ==========
	/* Returns the previous node the navigator is navigating from. */
	public NavigatorNode GetPreviousNode () {
		return this.Nodes[this.PreviousNodeIndex];
	}


	// ========== Set Target Node ==========
	/* Sets the node to navigate towards to the provided node. */
	public void SetTargetNode (NavigatorNode node) {
		this.PreviousNodeIndex = this.TargetNodeIndex;
		this.TargetNodeIndex = node.Index;
	}


	// ========== Get Closest Node ==========
	/* Returns the closest node to the provided position. Useful for automatically detecting a starting node. */
	public NavigatorNode GetClosestNode (Vector3 position) {
		float nearestDistance = 0;
		NavigatorNode nearestNode = null;
		foreach (NavigatorNode node in this.Nodes) {
			float distance = Vector3.Distance (node.GetPosition (), position);
			if (nearestNode == null || distance < nearestDistance) {
				nearestDistance = distance;
				nearestNode = node;
			}
		}
		return nearestNode;
	}


	// ========== Add Node ==========
	/* Adds a GameObject node to this navigator. */
	public void AddNode (GameObject gameObject) {
		NavigatorNode node = new NavigatorNode (gameObject);
		this.AddNode (node);
	}

	/* Adds a Vector3 node to this navigator. */
	public void AddNode (Vector3 position) {
		NavigatorNode node = new NavigatorNode (position);
		this.AddNode (node);
	}

	/* Adds a node to this navigator. */
	public void AddNode (NavigatorNode node) {
		node.Index = this.Nodes.Count;
		this.Nodes.Add (node);
	}


	// ========== Remove Node ==========
	/* Removes the provided node from this navigator. */
	public void RemoveNode (NavigatorNode node) {
		if (!this.Reverse && node.Index >= this.TargetNodeIndex)
			this.TargetNodeIndex = this.PreviousNodeIndex;
		else if (this.Reverse && node.Index <= this.TargetNodeIndex)
			this.TargetNodeIndex = this.PreviousNodeIndex;
		this.TargetNodeIndex = this.PreviousNodeIndex;
		this.Nodes.Remove (node);
		this.NextNode ();
	}

	/* Removes the a node from this navigator by node index. */
	public void RemoveNode (int nodeIndex) {
		this.RemoveNode (this.Nodes[nodeIndex]);
	}
}

