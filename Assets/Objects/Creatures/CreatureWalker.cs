using UnityEngine;

public class CreatureWalker : CreatureBase {

	// ========== Awake ==========
	public override void Awake () {
		base.Awake ();

		// Initialize AI:
		if (this.AI.Count == 0) {
			CreatureAIPatrol patrolAI = ScriptableObject.CreateInstance<CreatureAIPatrol> ();
			patrolAI.Init (this, "Patrol", 0, CreatureAI.BitmaskMovement);
			this.AI.Add (patrolAI);
		}
	}
	
	
	// ========== Update ==========
	public override void Update () {
		base.Update ();
	}
}

