using UnityEngine;
using UnityEngine.UI;

public class CreatureDisplay : MonoBehaviour {

	// Components:
	public RectTransform Display;
	public CreatureBase Creature;
	public RectTransform HealthMax;
	protected Image HealthMaxImage;
	public RectTransform HealthCurrent;
	protected Image HealthCurrentImage;

	protected float FlashInterval = 0.05f;
	protected float FlashTime = 0f;
	protected bool FlashState = true;


	// ========== Start ==========
	void Start () {
		this.Display = this.GetComponent<RectTransform> ();
	}


	// ========== Update ==========
	void Update () {
		if (this.Creature == null || this.Creature.Dead) {
			GameObject.Destroy (this.gameObject);
			return;
		}

		// Follow Creature:
		this.transform.position = new Vector3 (this.Creature.transform.position.x, this.Creature.transform.position.y + this.Creature.ProjectileOffsetY, this.Creature.transform.position.z);


		// Update Health:
		if (this.HealthMax == null && this.HealthCurrent == null)
			return;
		if (this.HealthMaxImage == null) {
			this.HealthMaxImage = this.HealthMax.GetComponent<Image> ();
			if (this.HealthMaxImage == null)
				return;
		}
		if (this.HealthCurrentImage == null) {
			this.HealthCurrentImage = this.HealthCurrent.GetComponent<Image> ();
			if (this.HealthCurrentImage == null)
				return;
		}

		// Size:
		this.HealthMax.sizeDelta = new Vector2 (this.HealthMax.sizeDelta.y * this.Creature.HealthMax.ValueCurrent, this.HealthMax.sizeDelta.y);
		this.HealthCurrent.sizeDelta = new Vector2 (this.HealthMax.sizeDelta.y * this.Creature.Health, this.HealthMax.sizeDelta.y);

		// Alpha:
		if (this.Creature.Hurt > 0) {
			if (this.FlashTime > 0) {
				this.FlashTime -= Time.deltaTime;
			}
			else {
				this.FlashState = !this.FlashState;
				this.FlashTime = this.FlashInterval;
			}
		}
		else
			this.FlashState = true;
		Color healthColor = this.HealthMaxImage.color;
		healthColor.a = this.FlashState ? 1 : 0.1f; ;
		this.HealthMaxImage.color = healthColor;
		healthColor = this.HealthCurrentImage.color;
		healthColor.a = this.FlashState ? 1 : 0.1f; ;
		this.HealthCurrentImage.color = healthColor;

		// Hiding:
		this.HealthMax.gameObject.SetActive (this.Creature.Health < this.Creature.HealthMax.ValueCurrent);
	}
}
