using UnityEngine;
using System.Collections;

public class PowerBase : ScriptableObject {
	/** ID - The ID of this power, this uses the PowerManager power names enum. **/
	public PowerManager.PowerID powerID;
	/** Name - The name of this power. **/
	public string Name;
	/** Bitmask - Used to dictate which powers conflict with each other such as Shot conflciting with Strike. **/
	public int Bitmask;
	/** An ID which can be used for specific animations such as attack animations. See the Creature Animator. **/
	public int AnimationID;
	/* Special effects object spawned when power is granted. */
	public GameObject EffectObject;
	/* The image used for GUIs. */
	public Sprite Icon;
	/* The color to use for the icons in the GUI. */
	public Color Color;

	// ========== Init ==========
	public virtual PowerBase Init (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		this.powerID = powerID;
		this.Name = name;
		this.Bitmask = bitmask;
		return this;
	}

	
	// ========== Bitmask Overlap ==========
	public virtual bool BitmaskOverlap (int targetBitmask) {
		return (this.Bitmask & targetBitmask) != 0;
	}


	// ========== UI Info ==========
	public virtual bool UsesCharge () {
		return false;
	}
	
	public virtual bool UsesEnergy () {
		return false;
	}

	public virtual bool UsesDuration () {
		return false;
	}
}