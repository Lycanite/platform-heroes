using UnityEngine;
using System.Collections;

public class PowerAttack : PowerBase {
	
	// ========== Create Instance ==========
	public static PowerAttack CreateInstance (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		return ScriptableObject.CreateInstance<PowerAttack> ().Init (powerID, name, bitmask);
	}


	// ========== Init ==========
	public new PowerAttack Init (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		base.Init (powerID, name, bitmask);
		this.Bitmask = 1;
		return this;
	}


	// ========== UI Info ==========
	public override bool UsesCharge () {
		return true;
	}
}