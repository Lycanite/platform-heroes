using UnityEngine;
using System.Collections;

public class PowerDuration : PowerBase {
	
	// ========== Create Instance ==========
	public static PowerDuration CreateInstance (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		return ScriptableObject.CreateInstance<PowerDuration> ().Init (powerID, name, bitmask);
	}
	
	
	// ========== Init ==========
	public new PowerDuration Init (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		base.Init (powerID, name, bitmask);
		this.Bitmask = 0;
		return this;
	}


	// ========== UI Info ==========
	public override bool UsesDuration () {
		return true;
	}
}