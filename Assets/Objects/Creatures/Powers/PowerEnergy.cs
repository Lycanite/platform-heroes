using UnityEngine;
using System.Collections;

public class PowerEnergy : PowerBase {
	
	// ========== Create Instance ==========
	public static PowerEnergy CreateInstance (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		return ScriptableObject.CreateInstance<PowerEnergy> ().Init (powerID, name, bitmask);
	}
	
	
	// ========== Init ==========
	public new PowerEnergy Init (PowerManager.PowerID powerID, string name, int bitmask = 0) {
		base.Init (powerID, name, bitmask);
		this.Bitmask = 2;
		return this;
	}


	// ========== UI Info ==========
	public override bool UsesEnergy () {
		return true;
	}
}