using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

[NetworkSettings (channel = 1, sendInterval = 0.25f)]
public class CreatureBase : NetworkBehaviour, IRegisterableObject {

	// ===== Info =====
	public string CreatureName = "creature";
	public Sprite Icon;

	// ===== Network =====
	public bool ClientAuthority = false; // If true, the local client can control this creature.
	public bool LocalOnly = false; // If true, this creature is only spawned on the client and controlled by the client.

	// ===== Spawning and Despawning =====
	public bool BecomeSpawner = true; // If true, this creature well not be setup, instead a spawner will be placed in its spot to spawn creatures of its kind.
	public float JustSpawned = 1; // Time (seconds) that this creature counts as a fresh spawn, this grants immunity to damage but also prevents dealing any damage.
	public float DestroyTimer = 5; // Time (seconds) until this creature is removed completely. This time is used to play a ragdoll and death sounds, etc.
	public GameObject SpawnPrefab; // Spawned when just spawned, this is used by the spawner unless the spawner has an override.
	public GameObject DeathPrefab; // Spawned on death, normally a smoke cloud.

	// ===== Components =====
	/* Controller - The main Character Controller. */
	public CharacterController Controller;
	/* Animator - The main animator component. */
	public Animator Animator;
	/* Movement - The Creature Movement component that handles all movement behaviour. */
	public CreatureLogicMovement Movement;
	/* Physics - The Creature Physics component that handles all physics interaction and trigger collisions. */
	public CreatureLogicPhysics Physics;
	/* Power - The Creature Power component that handles all power settings and behaviour. Must be manually added via prefab. */
	public CreatureLogicPower Power;
	/* Spawner - The spawner that spawed this creature, if any. */
	public SpawnerBase Spawner;
	/* GUI */
	public GameObject Display;
	/* Network Sync */
	public NetworkTransform NetTransform;
	public NetworkAnimator NetAnimator;


	// ===== Stats =====
	/* Health - Death when at 0. */
	public int StartingHealthMax = 3;
	public CreatureStat HealthMax;
	[SyncVar] public int Health;

	/* Damage - How much melee damage attacks do, some attacks may divide or multiply this. Projectiles have their own strength stat that is used instead. */
	public int StartingStrength = 1;
	public CreatureStat Strength;
	[SyncVar] public float AttackCooldown = 0;
	[SyncVar] public float PowerCooldown = 0;

	/* Speed - How fast this creature moves. */
	public int StartingSpeed = 3;
	public CreatureStat Speed;

	/* Jump Height - How high this creature jumps. */
	public int StartingJumpHeight = 3;
	public CreatureStat JumpHeight;

	/* Jump Count - How many times this creature can jump before needing to be grounded. 0 = No jump! */
	public int StartingJumpCount = 2;
	public CreatureStat JumpCount;
	
	/* Weight - How heavy this creature is, this will increase or decrese the base falling rate. */
	public int StartingWeight = 3;
	public CreatureStat Weight;
	
	/* Endurance - How many 0.25 seconds of damage resistance this creature has after taking damage. */
	public int StartingEndurance = 1;
	public CreatureStat Endurance;

	/* Swim - How fast this creature swims (used as a multiplier to speed when in water). */
	public int StartingSwim = 1;
	public CreatureStat Swim;

	/* Phase - The current phase this creature is in, used for controlling AIs with alternating behaviours and also for boss fights. */
	[SyncVar] public int Phase = 0;
	public int PhaseCount = 1; // How many phases this creature has in total. Used to calculate boss health and phase changes.


	// ===== Drops =====
	/* Reward - Dropped on death. */
	public GameObject RewardPrefab;
	public GameObject RewardEffect;
	[SyncVar] public int RewardAmount = 0;


	// ===== Conditions =====
	/* Active - If false, this creature wont move or update animation at all. It will still react to certain things such as damage or collision. */
	[SyncVar] public bool Active = true;

	/* Team ID - The ID of the team that this creature belongs to. Enemies use a negative team number. Team 0 is no team. */
	[SyncVar] public int TeamID = -1;

	/* Is Boss - If true, this creature is given the boss status where it will show a boss health bar during the boss level state, etc. */
	public bool IsBoss = false;
	protected bool LevelStatsStart = false; // Set to true when startup waiting on the LevelStats object to be available is complete.

	/* Grounded - If true, the creature is on solid ground. */
	[SyncVar (hook = "OnSyncGrounded")] public bool Grounded = false;

	/* Wall Left - If true, this create has collided with a wall on it's left. */
	[SyncVar] public bool WallLeft = false;

	/* Wall Right - If true, this create has collided with a wall on it's right. */
	[SyncVar] public bool WallRight = false;

    /* Wall Right - If true, this create has collided with a ceiling above it. */
	[SyncVar] public bool Ceiling = false;

    /* Collision Clear Time - The ticks until WallLeft, WallRight and Ceiling are reset to false. */
	public int CollisionClearTime = 5;

	/* Underwater - If true, this creature is underwater. This is not true when at the water surfacce. */
	[SyncVar] public bool Underwater = false;
	
	/* Moving - If true, this creature is moving on its own accord (where only being pushed doesn't count). */
	[SyncVar (hook = "OnSyncMoving")] public bool Moving = false;
	
	/* Swimming - If true, this creature is swimming. */
	[SyncVar (hook = "OnSyncSwimming")] public bool Swimming = false;
	
	/* Flying - If true, this creature is flying. */
	[SyncVar] public bool Flying = false;

	/* Attack Animation ID - Set to an ID to trigger a specific attack animation via the Creature Animator. 0 = No attack. */
	public int AttackAnimationID = 0; // 0 = None, 1 = Base, 2 = Blast, 3 = Strike, 4 = Bomb
	
	/* Dead - Set to true when this creature dies. */
	[SyncVar (hook="OnSyncDead")] public bool Dead = false;
	
	/* Hurt - When above 0 (decreases to 0 in ticks), this creature has just been hurt, no damage unless fatal can be received at this time. */
	[SyncVar] public float Hurt = 0;

	/* Eye Height - Used for position GUIs, etc. */
	public float EyeHeight = 2f;

	/* Projectile Y Offset - Y offset used when firing projectiles. */
	public float ProjectileOffsetY = 1.5f;


	// ===== Movement =====
	/* Face Movement - If true, this creature will automatically turn to face the direction it is moving. */
	[SyncVar] public bool FaceMovement = true;
	/* Tether Object - If set, this object will resticy how far this creature can move. */
	public GameObject TetherObject;
	/* Tether Object - Sets how far in x distance this creature can move from the Tether object (if set). */
	public float TetherX = 50;
	/* Depth - The depth to lock this Creature to. */
	[SyncVar] public float Depth = 0;
	/* Last Attacker - The last creature that has damaged this creature within the last 5 seconds. */
	public CreatureBase LastAttacker;
	private float LastAttackClearTime = 5;
	private float LastAttackClearTimeMax = 5;


	// ===== AI =====
	public List<AIPackage> AIPackages = new List<AIPackage> ();
	public List<CreatureAI> AI = new List<CreatureAI> ();
	public List<CreatureAI> AIActive = new List<CreatureAI> ();
	/* AI Wait - How long in seconds this creature will stop movement AI for. */
	public float AIMovementWait = 0;
	
	
	// ===== Sounds =====
	public UnityEngine.Audio.AudioMixerGroup AudioMixerGroup;
	public AudioLibrary AudioLibrary;
	public List<AudioEntry> AddSoundEntries = new List<AudioEntry> () {
		new AudioEntry ("taunt", "voice", null),
		new AudioEntry ("win", "voice", null),
		new AudioEntry ("jump", "main", null),
		new AudioEntry ("land", "main", null),
		new AudioEntry ("hurt", "voice", null),
		new AudioEntry ("death", "voice", null),
		new AudioEntry ("deathfall", "voice", null)
	};


	// ========== Registry Name ==========
	public virtual string GetRegistryName () {
		return this.CreatureName;
	}


	// ========== Server Spawn ==========
	public virtual void ServerSpawn () {
		NetworkManager.Get ().Log ("[Server] Spawning: " + this.gameObject);
		NetworkServer.Spawn (this.gameObject);
	}


	// ========== Awake ==========
	public virtual void Awake () {
		// Get Components:
		this.Controller = this.GetComponent<CharacterController> ();
		if (this.Animator == null)
			this.Animator = this.GetComponent<Animator> ();

		if (this.GetComponent<CreatureLogicMovement> () == null)
			this.gameObject.AddComponent<CreatureLogicMovement> ();
		this.Movement = this.GetComponent<CreatureLogicMovement> ();

		if (this.GetComponent<CreatureLogicPhysics> () == null)
			this.gameObject.AddComponent<CreatureLogicPhysics> ();
		this.Physics = this.GetComponent<CreatureLogicPhysics> ();

		this.Power = this.GetComponent<CreatureLogicPower> ();
		
		// Network Transform:
		this.NetTransform = this.gameObject.AddComponent<NetworkTransform> ();
		this.NetTransform.transformSyncMode = NetworkTransform.TransformSyncMode.SyncCharacterController;
		this.NetTransform.interpolateMovement = 1;
		if (this.LocalOnly)
			this.NetTransform.enabled = false;

		// Audio Library:
		this.AudioLibrary = this.gameObject.GetComponent<AudioLibrary> ();
		if (this.AudioLibrary == null)
			this.AudioLibrary = this.gameObject.AddComponent<AudioLibrary> ();
		this.AudioLibrary.AudioMixerGroup = this.AudioMixerGroup;
		foreach (AudioEntry audioEntry in this.AddSoundEntries) {
			this.AudioLibrary.AddSound (audioEntry);
		}

		// Initialize Stats:
		this.HealthMax = new CreatureStat ("Health", this.StartingHealthMax);
		this.Strength = new CreatureStat ("Strength", this.StartingStrength);
		this.Speed = new CreatureStat ("Speed", this.StartingSpeed);
		this.JumpHeight = new CreatureStat ("JumpHeight", this.StartingJumpHeight);
		this.JumpCount = new CreatureStat ("JumpCount", this.StartingJumpCount);
		this.Weight = new CreatureStat ("Weight", this.StartingWeight);
		this.Endurance = new CreatureStat ("Endurance", this.StartingEndurance);
		this.Swim = new CreatureStat ("Swim", this.StartingSwim);

		this.Health = this.StartingHealthMax;
		this.Depth = this.transform.position.z;

		this.CollisionClearTime = 5;
	}


	// ========== Start ==========
	public virtual void Start () {
		// AI:
		AIPackage attachedPackage = this.GetComponent<AIPackage> ();
		if (attachedPackage != null && !this.AIPackages.Contains (attachedPackage))
			this.AIPackages.Add (attachedPackage);
		foreach (AIPackage aiPackage in this.AIPackages) {
			foreach (CreatureAI ai in aiPackage.AI) {
				if (!aiPackage.Shared)
					this.AI.Add (ai);
				else
					this.AI.Add (ai.Clone ());
				ai.Host = this;
			}
		}
		this.SortAI ();

		// Become Spawner:
		if (this.BecomeSpawner && this.Spawner == null) {
			if (NetworkManager.Get ().ServerSide () || this.LocalOnly) {
				GameObject spawner = GameObject.Instantiate (LevelBase.Get ().CreatureSpawnerPrefab);
				spawner.transform.SetParent (this.transform.parent);
				spawner.transform.position = this.transform.position;
				SpawnerBase spawnerBase = spawner.GetComponent<SpawnerBase> ();
				spawnerBase.CopyFromCreature (this);
				this.Death (true, true);
			}
			return;
		}

		// Audio:
		this.AudioLibrary.LocalOnly = this.LocalOnly;
        foreach (AudioEntry audioEntry in this.AddSoundEntries) {
			this.AudioLibrary.AddSound (audioEntry);
		}

		// Network Spawning:
		if (NetworkManager.Get ().ServerSide () && !this.LocalOnly) {
			this.ServerSpawn ();
		}

		// Reward Effect:
		if (this.RewardAmount > 0 && this.RewardPrefab != null && this.RewardEffect != null) {
			GameObject rewardEffect = GameObject.Instantiate (this.RewardEffect);
			rewardEffect.transform.SetParent (this.transform, true);
			rewardEffect.transform.localPosition = new Vector3 (0, this.ProjectileOffsetY, 0);
		}

		// Add GUI:
		this.AddGUI ();
	}
	
	
	// ========== Update ==========
	public virtual void Update () {
		this.NetTransform.enabled = this.Active && !this.Dead && !this.LocalOnly;
		//this.NetAnimator.enabled = this.Active && !this.LocalOnly;

		// Level Stats Start:
		if (!this.LevelStatsStart && LevelBase.Get ().Stats != null) {
			if (this.IsBoss)
				LevelBase.Get ().Stats.AddBoss (this);
			this.LevelStatsStart = true;
		}

		// Death and Decay - Server Side or Local Only:
		if (this.Dead) {
			if (NetworkManager.Get ().ServerSide () || this.LocalOnly) {
				this.DestroyTimer -= Time.deltaTime;
				if (this.DestroyTimer <= 0) {
					// Destroy Effect:
					if (this.DeathPrefab != null) {
						GameObject deathObject = GameObject.Instantiate (this.DeathPrefab);
						deathObject.transform.position = this.transform.position;
						deathObject.transform.position += new Vector3 (0, this.ProjectileOffsetY, 0);
					}
					Destroy (this.gameObject);
				}
			}
			return;
		}

		// Just Spawned:
		if (this.JustSpawned > 0) {
			this.JustSpawned -= Time.deltaTime;
			if (this.JustSpawned < 0)
				this.JustSpawned = 0;
		}

		// Update Local Owned Conditions:
		if (this.LocalOwned ()) {
			if (!this.Grounded && this.Controller.isGrounded) {
				this.PlaySound ("land");
			}
			this.Grounded = this.Controller.isGrounded;
			this.Moving = false;
			this.Swimming = false;
			this.Underwater = false;
		}

		// Server Side or Local Only
		if (NetworkManager.Get ().ServerSide () || this.LocalOnly) {
			// Last Attacker:
			if (this.LastAttacker != null) {
				if (this.LastAttackClearTime - Time.deltaTime <= 0) {
					this.LastAttacker = null;
					this.LastAttackClearTime = this.LastAttackClearTimeMax;
				}
			}

			// Health Check:
			if (this.Health <= 0)
				this.Death (false, true);

			// Update Hurt Time:
			if (this.Hurt > 0)
				this.Hurt -= Time.deltaTime;

			// Update Cooldowns:
			if (this.AttackCooldown > 0)
				this.AttackCooldown -= Time.deltaTime;
			if (this.PowerCooldown > 0)
				this.PowerCooldown -= Time.deltaTime;
		}
		
		// Update Components Before AI:
		this.Movement.CreatureUpdateBeforeAI ();
		this.Physics.CreatureUpdateBeforeAI ();
		if (this.Power != null)
			this.Power.CreatureUpdateBeforeAI ();

		// Update AI:
		if (this.Active) {
			int activeBitmask = 0;
			foreach (CreatureAI creatureAI in this.AI) {
				if (creatureAI.Host != this)
					creatureAI.Host = this;

				// Currently Active:
				if (this.AIActive.Contains (creatureAI)) {
					// Can Update:
					if (!creatureAI.BitmaskOverlap (activeBitmask) && creatureAI.CanContinue ()) {
						activeBitmask |= creatureAI.Bitmask;
						creatureAI.OnUpdate ();
					}
					// Needs Deactivated:
					else {
						creatureAI.OnStop ();
						this.AIActive.Remove (creatureAI);
					}
				}

				// Currently Inactive:
				else {
					// Needs Activated:
					if (!creatureAI.BitmaskOverlap (activeBitmask) && creatureAI.CanStart ()) {
						activeBitmask |= creatureAI.Bitmask;
						creatureAI.OnStart ();
						this.AIActive.Add (creatureAI);
					}
				}
			}
		}
		if (this.AIMovementWait > 0)
			this.AIMovementWait -= Time.deltaTime;

		// Update Components:
		this.Movement.CreatureUpdate ();
		this.Physics.CreatureUpdate ();
		if (this.Power != null)
			this.Power.CreatureUpdate ();

		// Tethering:
		if (this.LocalOwned ()) {
			if (this.TetherObject != null) {
				if (this.transform.position.x > this.TetherObject.transform.position.x + this.TetherX)
					this.transform.position = new Vector3 (
						this.TetherObject.transform.position.x + this.TetherX,
						this.transform.position.y,
						this.transform.position.z
					);
				else if (this.transform.position.x < this.TetherObject.transform.position.x - this.TetherX)
					this.transform.position = new Vector3 (
						this.TetherObject.transform.position.x - this.TetherX,
						this.transform.position.y,
						this.transform.position.z
					);
			}
		}

		// Update Animation:
		if (this.Animator != null) {
			this.Animator.SetBool ("Grounded", this.Grounded);
			this.Animator.SetBool ("Moving", this.Moving);
			this.Animator.SetBool ("Swimming", this.Swimming);
			this.Animator.SetBool ("Hurt", this.Hurt > 0);
			this.Animator.SetInteger ("Attack", this.AttackAnimationID);
		}
		if (this.ClientControl ())
			this.SendStates ();
		this.AttackAnimationID = 0;

		// Clear Physics Conditions:
		if (this.LocalOwned ()) {
			if (this.WallLeft && this.WallRight) {
				this.WallLeft = false;
				this.WallRight = false;
			}
			if (this.CollisionClearTime-- <= 0) {
				this.WallLeft = false;
				this.WallRight = false;
				this.Ceiling = false;
				this.CollisionClearTime = 0;
			}
		}
	}


	// ========== AI ==========
	/** Sorts all AI by priority. **/
	public void SortAI () {
		this.AI.Sort ((a, b) => -(a.Priority.CompareTo (b.Priority)));
	}


	// ========== GUI ==========
	/** Creates any GUI objects for this creature. **/
	public virtual void AddGUI () {
		if (this.Display != null || LevelBase.Get ().CreatureDisplayPrefab == null)
			return;
		this.Display = GameObject.Instantiate (LevelBase.Get ().CreatureDisplayPrefab);
		this.Display.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y + this.EyeHeight, this.transform.position.z);
		this.Display.transform.SetParent (this.transform.parent);
		CreatureDisplay display = this.Display.GetComponent<CreatureDisplay> ();
		if (display != null)
			display.Creature = this;
	}


	// ========== Attack ==========
	/* Makes this creature attempt to attack. */
	public void Attack () {
		if (NetworkManager.Get ().ServerSide ())
			this.DoAttack ();
		else
			this.CmdAttack ();
	}

	[Command]
	public void CmdAttack () {
		this.DoAttack ();
	}

	public void DoAttack () {
		if (this.Dead)
			return;
		if (this.Power != null)
			this.Power.OnCreatureAttack ();
	}

	public void SetAttackAnimation (int animationID) {
		this.DoSetAttackAnimation (animationID);
		if (NetworkManager.Get ().ServerSide ())
			this.RpcSetAttackAnimation (animationID);
	}

	[ClientRpc]
	public void RpcSetAttackAnimation (int animationID) {
		this.DoSetAttackAnimation (animationID);
	}

	public void DoSetAttackAnimation (int animationID) {
		this.AttackAnimationID = animationID;
	}


	// ========== Power ==========
	/* Makes this creature attempt to use its power (players will require charge based powerups). */
	public void UsePower () {
		if (NetworkManager.Get ().ServerSide ())
			this.DoUsePower();
		else
			this.CmdUsePower();
	}

	[Command]
	public void CmdUsePower () {
		this.DoUsePower ();
	}

	public void DoUsePower () {
		if (this.Dead)
			return;
		if (this.Power != null)
			this.Power.OnCreatureUsePower ();
	}
	
	
	// ========== Damage ==========
	/* Damages this creature and returns true if the damage was successful. */
	public bool Damage (DamageSource damageSource) {
		// Check For Immunity:
		if (this.Dead || this.Hurt > 0 || this.JustSpawned > 0 || !this.CanTakeDamageFrom (damageSource))
			return false;

		int damageAmount = damageSource.Amount;

		// Power based damage reduction:
		if (this.Power != null)
			damageAmount = Mathf.RoundToInt ((float)damageAmount * this.Power.OnCreatureDamage ());

		// Check For Any Damage:
		if (damageAmount <= 0)
			return false;

		// Apply Damage:
		this.Hurt = (float)this.Endurance.ValueCurrent * 0.25F;
		this.Health -= damageAmount;
		
		// Damage Sound:
		this.PlaySound ("hurt");

		// Damage Counter:
		this.DisplayDamage (damageAmount);

		// Death Check:
		if (this.Health <= 0)
			this.Death (false, true);

		return true;
	}
	
	/* Returns true if the provided damage can be applied to this creature. */
	public virtual bool CanTakeDamageFrom (DamageSource damageSource) {
		if (damageSource.Owner != null) {
			// Creature Owner:
			CreatureBase ownerCreature = damageSource.Owner.GetComponent<CreatureBase> ();
			if (ownerCreature != null) {
				if (ownerCreature.Dead || ownerCreature.JustSpawned > 0)
					return false; // The damage is from a creature that is dead or just spawned.
				if (this.TeamID != 0 && ownerCreature.TeamID == this.TeamID)
					return false; // The owner creature is an ally.
				this.LastAttacker = ownerCreature;
				return true;
			}
		}
		return true;
	}

	/* Creates a damage counter for the damage amount provided. */
	public void DisplayDamage (int amount) {
		this.DoDisplayDamage (amount);
		if (NetworkManager.Get ().ServerSide ()) {
			this.RpcDisplayDamage (amount);
		}
	}

	[ClientRpc]
	public void RpcDisplayDamage (int amount) {
		this.DoDisplayDamage (amount);
	}

	public void DoDisplayDamage (int amount) {
		GameObject damageCounterPrefab = LevelBase.Get ().DamageCounterPrefab;
		if (damageCounterPrefab != null) {
			GameObject damageCounter = GameObject.Instantiate (damageCounterPrefab, this.transform.position + new Vector3 (0, this.EyeHeight, 0), damageCounterPrefab.transform.rotation);
			TextDisplay damageCounterText = damageCounter.GetComponent<TextDisplay> ();
			if (damageCounterText != null)
				damageCounterText.SetText (amount.ToString ());
		}
	}


	// ========== Heal ==========
	/* Heals this creature by the specified amount. If the amount is 0 or less the creature will be fully healed. Returns true on success. */
	public bool Heal (float amount) {
		// Check For Death ot Full Health:
		if (this.Dead || this.Health >= this.HealthMax.ValueCurrent)
			return false;

		int healAmount = (int) Math.Floor (amount);
		if (amount == 0)
			healAmount = this.HealthMax.ValueCurrent;
		this.Health = Math.Min (this.Health + healAmount, this.HealthMax.ValueCurrent);

		return true;
	}


	// ========== Death ==========
	/* Kills this creature. If true is passed, this creature will not do any death effects. */
	public void Death (bool silent, bool force) {
		if (this.Dead)
			return;
		this.Dead = true;
		if (this.Spawner != null)
			this.Spawner.OnDeath (this.gameObject);

		// Silent Death (Removal):
		if (silent) {
			Destroy (this.gameObject);
			return;
		}

		// Kill Credit:
		if (this.LastAttacker != null) {
			this.LastAttacker.OnKill (this);
		}

		// Drop Reward:
		if (this.RewardPrefab != null) {
			this.DropPickups (this.RewardPrefab, this.RewardAmount);
		}

		// Death Sound:
		this.PlaySound ("death");

		this.OnDeath ();
		this.Ragdoll ();
		this.ScheduleDestroy ();
	}

	/* Called during death. */
	public virtual void OnDeath () {}

	/* Turns this object into a Ragdoll. */
	public virtual void Ragdoll () {
		Rigidbody rigidbody = this.GetComponent<Rigidbody> ();
		if (rigidbody != null)
			rigidbody.isKinematic = true;
		Collider collider = this.GetComponent<Collider> ();
		if (collider != null)
			collider.enabled = false;
		this.NetTransform.enabled = false;
		this.Controller.enabled = false;
		//this.NetAnimator.enabled = false;
		this.GetComponent<Animator> ().enabled = false;
		//this.GetComponent<Animator> ().SetBool ("Dead", true);
		
		Transform root = this.transform.Find ("root");
		if (root == null)
			root = this.transform.Find ("Root");
		if (root == null) {
			foreach (Transform childTransform in this.transform.GetComponentsInChildren<Transform> ()) {
				if (childTransform.name == "root" || childTransform.name == "Root") {
					root = childTransform;
					continue;
				}
			}
		}
		if (root == null)
			return;
		this.RagdollBone (root);
	}

	/* Sets up the components to make the provided transform part of a ragdoll. */
	public void RagdollBone (Transform bone, Transform parentBone = null) {
		bone.gameObject.layer = 11; // Ragdoll Physics Layer
		// Set Rigidbody:
		if (bone.name != "root") {
			Rigidbody boneRigidbody = bone.GetComponent<Rigidbody> ();
			if (boneRigidbody == null)
				boneRigidbody = bone.gameObject.AddComponent<Rigidbody> ();
			boneRigidbody.isKinematic = false;
			boneRigidbody.useGravity = true;
			boneRigidbody.mass = 3.125f;
			Collider boneCollider = bone.GetComponent<Collider> ();
			if (boneCollider == null) {
				boneCollider = bone.gameObject.AddComponent<CapsuleCollider> ();
				((CapsuleCollider)boneCollider).radius = 0.25f;
			}
		}

		// Set Child Bones:
		for (int i = 0; i < bone.childCount; i++) {
			Transform childBone = bone.GetChild (i);
			if ((childBone.name.Length > 3 && childBone.name.Substring (0, 3) == "ik_") ||
				(childBone.name.Length > 5 && childBone.name.Substring (0, 5) == "pole_") ||
				childBone.name.Contains ("_pole_") ||
				childBone.name.Contains ("_ik_"))
				continue;
			this.RagdollBone (childBone, bone);
		}

		// Set Joint:
		if (parentBone == null || parentBone.name == "root")
			return;
		Joint boneJoint = bone.GetComponent<CharacterJoint> ();
		if (boneJoint == null)
			boneJoint = bone.gameObject.AddComponent<CharacterJoint> ();
		boneJoint.connectedBody = parentBone.GetComponent<Rigidbody> ();
	}

	/* Called to destroy this object within the provided amount of seconds, this is usually called on death to allow some sounds to play. */
	public void ScheduleDestroy (float seconds = 5) {
		this.DestroyTimer = seconds;
	}
	

	// ========== Set Depth ==========
	/* Sets the Z Depth for this creature. */
	public void SetDepth (float depth) {
		if (this.LocalOwned ())
			this.Depth = depth;
	}

	
	// ========== Knockback ==========
	/* Knocks this creature back. */
	public void Knockback (float forceX, float forceY) {
		if (!this.LocalOwned ())
			return;
		if (forceX != 0)
			this.Movement.Push (forceX);
		if (forceY != 0)
			this.Movement.StartJump (forceY);
	}


	// ========== On Kill ==========
	/* Called by another creature when it dies if this creature was it's last recent attacker. */
	public virtual void OnKill (CreatureBase killedCreature) {}


	// ========== Drop Pickups ==========
	/* Drops the specified prefab by the specified amount in random locations relative to this creature's position as pickups. */
	public virtual void DropPickups (GameObject dropPrefab, int dropAmount) {
		for (int i = 0; i < dropAmount; i++) {
			GameObject dropObject = GameObject.Instantiate (dropPrefab);
			NetworkServer.Spawn (dropObject);
			dropObject.transform.position = this.transform.position;
			dropObject.transform.position += new Vector3 (UnityEngine.Random.value * 2, this.ProjectileOffsetY + (UnityEngine.Random.value * 2), 0);
		}
	}


	// ========== Play Sound ==========
	/** Plays the provided sound effect (by name) using the AudioLibrary. **/
	public void PlaySound (string soundName) {
        this.AudioLibrary.LocalOnly = this.LocalOnly;
        this.AudioLibrary.PlaySound (soundName);
	}


	// ========== On Sync ==========
	/** Events called when SyncVars are updated, local players can ignore some changes here for smoother gameplay. **/
	protected void OnSyncGrounded (bool grounded) {
		if (!this.hasAuthority)
			this.Grounded = grounded;
	}

	protected void OnSyncMoving (bool moving) {
		if (!this.hasAuthority)
			this.Moving = moving;
	}

	protected void OnSyncSwimming (bool swimming) {
		if (!this.hasAuthority)
			this.Swimming = swimming;
	}

	protected void OnSyncDead (bool dead) {
		if (!this.Dead && dead) {
			this.Ragdoll ();
			this.ScheduleDestroy ();
		}
		this.Dead = dead;
	}


	// ========== Send States ==========
	/** Sends various states that are client controlled to the server. **/
	protected int LastSentStates = -1;
	protected void SendStates () {
		int states = 0;
		if (this.Grounded)
			states |= 1;
		if (this.Moving)
			states |= 2;
		if (this.Swimming)
			states |= 4;
		if (this.LastSentStates != states)
			this.CmdSendStates (this.Grounded, this.Moving, this.Swimming);
		this.LastSentStates = states;
	}

	[Command]
	protected void CmdSendStates (bool grounded, bool moving, bool swimming) {
		this.Grounded = grounded;
		this.Moving = moving;
		this.Swimming = swimming;
	}


	// ========== Server Control ==========
	/** Returns true when server side and not in client authority but not if running locally. **/
	public bool ServerControl () {
		return NetworkManager.Get ().ServerSide () && !this.ClientAuthority && !this.LocalOnly;
	}


	// ========== Client Control ==========
	/** Returns true when client side and in this client's authority but not if running locally. **/
	public bool ClientControl () {
		return NetworkManager.Get ().ClientSide () && this.hasAuthority && !this.LocalOnly;
	}


	// ========== Local Owned ==========
	/** Returns true if the local client/host has specific authority over this creature. This should be false for remote players unless the creature is local only or their player character. **/
	public bool LocalOwned () {
		return this.ServerControl () || this.hasAuthority || this.LocalOnly;
	}
}

