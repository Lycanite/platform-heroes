using UnityEngine;
using UnityEngine.Networking;

public class CreaturePlayer : CreatureBase {

	// Player:
	public PlayerInfo PlayerInfo; // The PlayerInfo object for this player.
	public PlayerStats PlayerStats; // The PlayerStats object for this player.
	[SyncVar (hook= "OnSyncPlayerStatsNetID")] public uint PlayerStatsNetID; // Used for connecting to the stats gameobject on the client side.

	// Score Multiplier:
	public GameObject MultiplierDisplayPrefab; // The prefab to use for show score multipliers.
	protected GameObject MultiplierDisplay; // The current object for displaying score multipliers.
	[SyncVar] public string MultiplierText = ""; // The current text to display on the score multiplier.
	protected float MultiplierLast = 0; // The multiplier amount on the last tick, used for detecting increases for animation.


	// ========== Server Spawn ==========
	public override void ServerSpawn () {
		if (NetworkManager.Get ().ServerSide ()) {
			this.PlayerStatsNetID = this.PlayerStats.GetComponent <NetworkIdentity> ().netId.Value;
			if (this.PlayerStats.Info.IsLocal ())
				NetworkServer.Spawn (this.gameObject);
			else {
				NetworkManager.Get ().SpawnWithClientAuthority (this.gameObject, this.PlayerStats.Info.PlayerManager.Connection);
				this.ClientAuthority = true;
				//this.SyncMovement.ClientOwned = true;
			}
		}
	}


	// ========== Awake ==========
	public override void Awake () {
		base.Awake ();

		// Initialize AI:
		CreatureAIPlayer playerAI = ScriptableObject.CreateInstance<CreatureAIPlayer> ();
		playerAI.Init (this, "Player", 0, CreatureAI.BitmaskMovement);
		this.AI.Add (playerAI);

		this.BecomeSpawner = false;
	}


	// ========== Start ==========
	public override void Start () {
		base.Start ();
	}


	// ========== GUI ==========
	public override void AddGUI () {
		return; // Player GUI are dynamically managed on update.
	}


	// ========== Update ==========
	public override void Update () {
		base.Update ();

		if (this.PlayerStats == null)
			return;

		// Multiplier Display:
		if (this.MultiplierDisplay == null && this.MultiplierDisplayPrefab != null) {
			this.MultiplierDisplay = GameObject.Instantiate (this.MultiplierDisplayPrefab);
			this.MultiplierDisplay.transform.parent = this.gameObject.transform;
		}
		TextMesh multiplierText = this.MultiplierDisplay.GetComponent<TextMesh> ();
		if (this.MultiplierDisplay != null && multiplierText != null) {
			this.MultiplierDisplay.transform.localPosition = new Vector3 (0, 3.5f, 0);
			this.MultiplierDisplay.transform.eulerAngles = new Vector3 (0, 0, 0);
			
			if (NetworkManager.Get ().ServerSide () && Game.Get ().CurrentGameType == Game.GameType.Level) {
				if (Mathf.RoundToInt (this.PlayerStats.Multiplier) > 1)
					this.MultiplierText = "<b>x" + Mathf.RoundToInt (this.PlayerStats.Multiplier) + "</b>";
				else
					this.MultiplierText = "";
			}
			else if (Game.Get ().CurrentGameType == Game.GameType.Menu && this.PlayerInfo != null) {
				this.MultiplierText = this.PlayerInfo.PlayerName;
				this.MultiplierDisplay.transform.localScale = new Vector3 (0.25F, 0.25F, 0.25F);
			}

			multiplierText.text = this.MultiplierText;

			AnimationBase multiplierAnimation = this.MultiplierDisplay.GetComponent<AnimationBase> ();
			if (multiplierAnimation != null) {
				if (this.PlayerStats.Multiplier > this.MultiplierLast)
					multiplierAnimation.Reset ();
			}
			this.MultiplierLast = this.PlayerStats.Multiplier;
		}
	}


	// ========== Death ==========
	/* Called during death. */
	public override void OnDeath () {
		if (this.PlayerStats != null)
			this.PlayerStats.OnDeath ();
	}


	// ========== On Kill ==========
	/* Called by another creature when it dies if this creature was it's last recent attacker. */
	public override void OnKill (CreatureBase killedCreature) {
		this.PlayerStats.AddKill (killedCreature);
	}


	// ========== Client On Pickup ==========
	/* Called when by a client owned player to handle pickups instead of the server. */
	public void ClientOnPickup (GameObject gameObject) {
		if (this.hasAuthority)
			this.CmdClientOnPickup (gameObject);
	}

	[Command]
	protected void CmdClientOnPickup (GameObject gameObject) {
		this.DoClientOnPickup (gameObject);
	}

	protected void DoClientOnPickup (GameObject gameObject) {
		PickupBase pickup = gameObject.GetComponent<PickupBase> ();
		if (pickup != null)
			pickup.OnPickup (this.gameObject);
	}


	// ========== Client Update Pickup ==========
	/* Called when by a client owned player to handle pickup updates instead of the server. */
	public void ClientUpdatePickup (GameObject gameObject) {
		if (this.hasAuthority)
			this.CmdClientUpdatePickup (gameObject);
	}

	[Command]
	protected void CmdClientUpdatePickup (GameObject gameObject) {
		this.DoClientUpdatePickup (gameObject);
	}

	protected void DoClientUpdatePickup (GameObject gameObject) {
		PickupBase pickup = gameObject.GetComponent<PickupBase> ();
		if (pickup != null)
			pickup.UpdatePickup (this.gameObject);
	}


	// ========== Score ==========
	public void AddScore (int amount, bool multiply = true) {
		this.PlayerStats.AddScore (amount, multiply);
	}


	// ========== Lives ==========
	public void AddLives (int amount) {
		this.PlayerStats.AddLives (amount);
	}


	// ========== Level Exit ==========
	public void LevelExit (GameObject exitObject) {
		this.PlayerStats.LevelExit (exitObject);
	}


	// ========== On Sync ==========
	/** Events called when SyncVars are updated, local players can ignore some changes here for smoother gameplay. **/
	protected void OnSyncPlayerStatsNetID (uint netID) {
		this.PlayerStatsNetID = netID;
		GameObject playerStatsObject = ClientScene.FindLocalObject (new NetworkInstanceId (this.PlayerStatsNetID));
		if (playerStatsObject == null) {
			Debug.Log ("[Client] Unable to find a player stats object for: " + this.gameObject);
			return;
		}
		this.PlayerStats = playerStatsObject.GetComponent<PlayerStats> ();
		if (this.PlayerStats != null) {
			this.PlayerInfo = this.PlayerStats.Info;
			this.AssignLocalPlayer ();
		}
	}


	// ========== Assign Local Player ==========
	/** Sends a request from a client to remote host to assign a local player for this player object. **/
	protected bool LocalPlayerAssigned = false;
	public void AssignLocalPlayer () {
		if (this.LocalPlayerAssigned || NetworkManager.Get ().ServerSide ())
			return;

		NetworkManager.Get ().Log ("Attempting to assign a local player for: " + this.gameObject);
		if (this.PlayerStats == null)
			return;
		NetworkManager.Get ().Log ("PlayerStats found!");
		this.PlayerInfo = this.PlayerStats.Info;
		if (this.PlayerInfo == null)
			return;
		if (!this.PlayerInfo.IsLocal ()) {
			NetworkManager.Get ().Log ("PlayerInfo found but not local, skipping!");
			return;
		}
		NetworkManager.Get ().Log ("PlayerInfo found! Sending request to server!");

		ClientScene.AddPlayer ((short)this.PlayerInfo.ID);
		this.LocalPlayerAssigned = true;
	}
}

