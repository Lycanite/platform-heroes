using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

[NetworkSettings (channel = 1, sendInterval = 0.25f)]
public class CreatureLogic : NetworkBehaviour {
	// ===== Components =====
	public CreatureBase Host;


	// ========== Awake ==========
	public virtual void Awake () {
		// Get Components:
		this.Host = this.GetComponent<CreatureBase> ();
	}
	
	
	// ========== Creature Update Before AI ==========
	public virtual void CreatureUpdateBeforeAI () { 

	}
	
	
	// ========== Creature Update ==========
	public virtual void CreatureUpdate () { 
		
	}
	
	
	// ========== Creature Attack ==========
	public virtual bool OnCreatureAttack () { 
		return false;
	}


	// ========== Creature Use Power ==========
	public virtual bool OnCreatureUsePower () { 
		return false;
	}


	// ========== Creature Damage ==========
	public virtual float OnCreatureDamage () { 
		return 1;
	}
}

