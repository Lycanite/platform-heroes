using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System;

public class CreatureLogicPower : CreatureLogic {
	// ===== Active Powers =====
	public PowerBase BasePower; // The base power that this creature uses for standard attacks.
	public List<PowerBase> Powers = new List<PowerBase> (); // A list of picked up powers.

	
	// ===== Power Definitions =====
	public PowerDefinition BasePowerDef;
	public Dictionary<PowerManager.PowerID, PowerDefinition> PowerDefinitions = new Dictionary<PowerManager.PowerID, PowerDefinition> ();
	[System.Serializable]
	public class PowerDefinition {
		public PowerManager.PowerID powerID;
		public string name; // What this creature calls the power such as Acid Blast for Lanky.
		public int energyMax = 3; // Converts to Charges and Duration for those types of powers.
		public int energy = 0; // Current amount of energy, resets to max when the power is added.
		public int strength = 1; // Attack damage, speed multipliers, etc.
		public float cooldown = 1; // Time in seconds until the power can be fired again, used mainly for attacks.
		public GameObject prefab; // Prefab used by power for projectiles, shields, effects, etc.
	}
	public List<PowerDefinition> PowerSetup = new List<PowerDefinition> ();


	// ========== Awake ==========
	public override void Awake () {
		base.Awake ();
		
		// Power Names:
		foreach (PowerDefinition powerDef in this.PowerSetup) {
			this.PowerDefinitions[powerDef.powerID] = powerDef;
		}
	}


	// ========== Start ==========
	public void Start () {
		this.BasePower = PowerManager.Get ().GetPower (PowerManager.PowerID.Base);
	}
	
	
	// ========== Creature Update Before AI ==========
	public override void CreatureUpdateBeforeAI () { 

	}
	
	
	// ========== Creature Update ==========
	public override void CreatureUpdate () { 
		
	}


	// ========== Creature Attack ==========
	public override bool OnCreatureAttack () {
		// Check If Can Attack:
		PowerBase power = this.GetBasePower ();
		if (power == null)
			return false;
		PowerDefinition powerDef = this.BasePowerDef;
		if (this.Host.AttackCooldown > 0)
			return false;

		// Create Projectile Object:
		if (powerDef.prefab != null) {
			this.FireProjectile (powerDef.prefab);
        }
        this.Host.AttackCooldown = powerDef.cooldown;

		// Animation:
		this.Host.SetAttackAnimation (power.AnimationID);

		return true;
	}
	
	
	// ========== Creature Use Power ==========
	public override bool OnCreatureUsePower () { 
		// Check If Can Use Power:
		PowerBase power = this.GetChargePower ();
		if (power == null)
			return false;
		PowerDefinition powerDef = this.PowerDefinitions[power.powerID];
		if (powerDef.energy <= 0 || this.Host.PowerCooldown > 0)
			return false;

		// Create Projectile Object:
		if (powerDef.prefab != null) {
			this.FireProjectile (powerDef.prefab);
        }
        this.Host.PowerCooldown = powerDef.cooldown;

        // Consume Charges:
        powerDef.energy--;
		if (powerDef.energy <= 0)
			this.RemovePower (power);

		// Animation:
		this.Host.SetAttackAnimation (power.AnimationID);

		return true;
	}


	// ========== Fire Projectile ==========
	public void FireProjectile (GameObject projectileObj) {
		GameObject projectileObject = GameObject.Instantiate (projectileObj);
		projectileObject.transform.position = this.transform.position;
		projectileObject.transform.position += new Vector3 (0, this.Host.ProjectileOffsetY, 0);

		// Projectile:
		ProjectileBase projectile = projectileObject.GetComponent<ProjectileBase> ();
		if (projectile != null) {
			projectile.Owner = this.gameObject;
			if (this.Host.Movement.Facing == 0)
				projectile.Direction.x = -projectile.Direction.x;
			if (NetworkManager.Get ().ServerSide ()) {
				NetworkServer.Spawn (projectileObject);
			}
			return;
		}

		// Projectile Launcher:
		ProjectileLauncher projectileLauncher = projectileObject.GetComponent<ProjectileLauncher> ();
		if (projectileLauncher != null) {
			projectileLauncher.Owner = this.gameObject;
			if (this.Host.Movement.Facing == 0)
				projectileLauncher.DirectionScale = new Vector3 (-1, 1, 1);
			return;
		}
	}


	// ========== Creature Damage ==========
	/** Called when this creature is attacked and returns a multiplier for the damage, returning 0 will cancel the damage completely. **/
	public override float OnCreatureDamage () { 
		return 1;
	}
	
	
	// ========== Add Power ==========
	/* Adds an active power to this creature. Returns true if the power is applied fresh or topped up and false if the power is already at full charge. */
	public bool AddPower (PowerBase power) {
		PowerDefinition powerDef = this.PowerDefinitions [power.powerID];

		if (this.Powers.Contains (power) && powerDef.energy >= powerDef.energyMax)
			return false;
		
		powerDef.energy = powerDef.energyMax;

		foreach (PowerBase activePower in this.Powers.ToArray ())
			if (activePower.BitmaskOverlap (power.Bitmask))
				this.Powers.Remove (activePower);
		this.Powers.Add (power);

		// Effect:
		this.PlayAddPowerEffect (power);
		
		this.ActiveChargePowerCached = false;
		this.ActiveEnergyPowerCached = false;
		this.ActiveDurationPowersCached = false;

		return true;
	}


	// ========== Recharge Power ==========
	/* Resets the provided power's charge/energy/duration if it is active. */
	public void RechargePower (PowerBase power) {
		if (!this.Powers.Contains (power))
			return;
		PowerDefinition powerDef = this.PowerDefinitions[power.powerID];
		powerDef.energy = powerDef.energyMax;
	}
	
	
	
	// ========== Network ==========
	public void PlayAddPowerEffect (PowerBase power) {
		if (NetworkManager.Get ().ServerSide ()) {
			this.DoPlayAddPowerEffect (power);
			this.RpcPlayAddPowerEffect (power.powerID.ToString ());
		}
	}

	[ClientRpc]
	public void RpcPlayAddPowerEffect (string powerID) {
		PowerBase power = PowerManager.Get ().GetPower ((PowerManager.PowerID) Enum.Parse (typeof (PowerManager.PowerID), powerID, true));
		if (power != null)
			this.DoPlayAddPowerEffect (power);
	}

	public void DoPlayAddPowerEffect (PowerBase power) {
		if (power.EffectObject != null) {
			GameObject effectObject = GameObject.Instantiate (power.EffectObject);
			effectObject.transform.position = this.transform.position;
			if (effectObject.GetComponent<EffectBase> () != null) {
				EffectBase effect = effectObject.GetComponent<EffectBase> ();
				effect.Owner = this.gameObject;
				effect.FollowOwner = true;
			}
		}
	}
	
	
	// ========== Remove Power ==========
	/* Removes an active power to this creature. */
	public void RemovePower (PowerBase power) {
		if (power == null)
			return;
		
		if (this.Powers.Contains (power))
			this.Powers.Remove (power);
		
		this.ActiveChargePowerCached = false;
		this.ActiveEnergyPowerCached = false;
		this.ActiveDurationPowersCached = false;
	}


	// ========== Base Power ==========
	/* Returns the base power that this creature uses for standard attacks. */
	public PowerBase GetBasePower () {
		return this.BasePower;
	}
	
	
	// ========== Charge Power ==========
	/* Returns the first active charge power this creature has, there shouldn't normally be more than 1. */
	protected PowerBase ActiveChargePower;
	protected bool ActiveChargePowerCached = false;
	public PowerBase GetChargePower () {
		
		if (this.ActiveChargePowerCached)
			return this.ActiveChargePower;
		this.ActiveChargePowerCached = true;
		
		foreach (PowerBase activePower in this.Powers) {
			if (activePower.UsesCharge ()) {
				this.ActiveChargePower = activePower;
				return activePower;
			}
		}
		this.ActiveChargePower = null;
		return null;
	}
	
	
	// ========== Energy Power ==========
	/* Returns the first active energy power this creature has, there shouldn't normally be more than 1. */
	protected PowerBase ActiveEnergyPower;
	protected bool ActiveEnergyPowerCached = false;
	public PowerBase GetEnergyPower () {
		
		if (this.ActiveEnergyPowerCached)
			return this.ActiveEnergyPower;
		this.ActiveEnergyPowerCached = true;
		
		foreach (PowerBase activePower in this.Powers) {
			if (activePower.UsesEnergy ()) {
				this.ActiveEnergyPower = activePower;
				return activePower;
			}
		}
		this.ActiveEnergyPower = null;
		return null;
	}
	
	
	// ========== Duration Powers ==========
	/* Returns a list of all the active duration powers. */
	protected List<PowerBase> ActiveDurationPowers;
	protected bool ActiveDurationPowersCached = false;
	public List<PowerBase> GetDurationPowers () {
		
		if (this.ActiveDurationPowersCached)
			return this.ActiveDurationPowers;
		this.ActiveDurationPowersCached = true;
		
		this.ActiveDurationPowers = new List<PowerBase> ();
		foreach (PowerBase activePower in this.Powers) {
			if (activePower.UsesDuration ())
				this.ActiveDurationPowers.Add (activePower);
		}
		return this.ActiveDurationPowers;
	}


	// ========== Get Power Info ==========
	/* Returns the creature's name variation of the provided power. */
	public string GetPowerName (PowerBase power) {
		if (power == null)
			return "";
		if (!this.PowerDefinitions.ContainsKey (power.powerID))
			return power.name;
		return PowerDefinitions[power.powerID].name;
	}
	
	/* Returns the current amount of energy for the specified power. */
	public int GetPowerEnergy (PowerBase power) {
		if (power == null)
			return 0;
		if (!this.PowerDefinitions.ContainsKey (power.powerID))
			return 0;
		return PowerDefinitions[power.powerID].energy;
	}

	/* Returns the maximum amount of energy for the specified power. */
	public int GetPowerEnergyMax (PowerBase power) {
		if (power == null)
			return 0;
		if (!this.PowerDefinitions.ContainsKey (power.powerID))
			return 0;
		return PowerDefinitions[power.powerID].energyMax;
	}
}

