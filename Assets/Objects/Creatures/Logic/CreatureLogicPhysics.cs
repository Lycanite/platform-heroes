using UnityEngine;
using System.Collections.Generic;

public class CreatureLogicPhysics : CreatureLogic {
	// ===== Phyics Stats =====
	/** Physics ohject pushing related stats, should be based on CreatureBase stats in the future. **/
	public float PushPower = 100.0F;
	public float Weight = 10.0F;

	// ===== Water =====
	public List<Collider> WaterColliders = new List<Collider> ();
	public float WaterSubmerge = 0;


	// ========== Awake ==========
	public override void Awake () {
		base.Awake ();
	}


	// ========== Creature Update Before AI ==========
	public override void CreatureUpdateBeforeAI () { 
		base.CreatureUpdateBeforeAI ();
		if (!NetworkManager.Get ().ServerSide () && !this.Host.LocalOwned ())
			return;

		// Water:
		this.WaterSubmerge = 0;
		if (this.WaterColliders.Count > 0) {
			foreach (Collider waterCollider in this.WaterColliders) {
				float waterSurface = waterCollider.bounds.max.y;
				AreaWater waterZone = waterCollider.GetComponent<AreaWater> ();
				if (waterZone != null)
					waterSurface -= waterZone.WaveHeight;
				float creatureBottom = this.Host.Controller.bounds.min.y;
				float difference = waterSurface - creatureBottom;
				float submerge = difference / this.Host.Controller.height;
				if (submerge > this.WaterSubmerge)
					this.WaterSubmerge = submerge;
			}
		}
		if (this.Host.LocalOwned ()) {
			if (this.WaterSubmerge >= 0.5)
				this.Host.Swimming = true;
			if (this.WaterSubmerge >= 1)
				this.Host.Underwater = true;
		}
	}
	
	
	// ========== Collider Hit ==========
	public void OnControllerColliderHit (ControllerColliderHit hit) {
		if (!NetworkManager.Get ().ServerSide () && !this.Host.LocalOwned ())
			return;
		if (this.Host.Dead)
			return;

		// Wall Collision: (Only Default or Creature Layer and Not Players or Projectiles)
		if (hit.collider.gameObject.layer == 0 || hit.collider.gameObject.layer == 9) {
			if (hit.collider.gameObject.GetComponent<CreaturePlayer> () == null && hit.collider.gameObject.GetComponent<ProjectileBase> () == null) {
				if (hit.normal.y < 0.707) {
					if (hit.normal.x > 0) {
						this.Host.WallLeft = true;
						this.Host.CollisionClearTime = 4;
						if (hit.collider.GetComponent<CreatureLogicPhysics> () != null) {
							CreatureBase creature = hit.collider.GetComponent<CreatureLogicPhysics> ().Host;
							creature.WallRight = true;
							creature.CollisionClearTime = 4;
						}
					}
					else if (hit.normal.x < 0) {
						this.Host.WallRight = true;
						this.Host.CollisionClearTime = 4;
						if (hit.collider.GetComponent<CreatureLogicPhysics> () != null) {
							CreatureBase creature = hit.collider.GetComponent<CreatureLogicPhysics> ().Host;
							creature.WallLeft = true;
							creature.CollisionClearTime = 4;
						}
					}
				}
			}
		}

        // Ceiling Collision:
        if (hit.normal.y <= -0.9)
        {
            this.Host.Ceiling = true;
            this.Host.CollisionClearTime = 4;
        }

		// Push Rigid Bodies:
		if (NetworkManager.Get ().ServerSide ()) {
			Rigidbody body = hit.collider.attachedRigidbody;
			if (body != null && !body.isKinematic && hit.moveDirection.y >= -0.3) {
				Vector3 pushDir = new Vector3 (hit.moveDirection.x, 0, 0);
				body.velocity += pushDir / body.mass;
				float limit = 40 / body.mass;
				float zVelocity = body.velocity.z;
				if (body.velocity.magnitude > limit)
					body.velocity = body.velocity.normalized * limit;
				if (body.velocity.z > zVelocity)
					body.velocity = new Vector3 (body.velocity.x, body.velocity.y, zVelocity);
			}
		}
		
		// Attach To Platforms:
		PlatformBase platform = hit.gameObject.GetComponent<PlatformBase> ();
		if (platform != null) {
			platform.OnGameObjectCollision (this.gameObject);
		}
	}


	// ========== Trigger ==========
	public void OnTriggerEnter (Collider collider) {
		if (!NetworkManager.Get ().ServerSide () && !this.Host.LocalOwned ())
			return;
		if (this.Host.Dead)
			return;

		// Pickup Collision:
		if (NetworkManager.Get ().ServerSide ()) {
			if (collider.gameObject.GetComponent<PickupBase> () != null) {
				collider.gameObject.GetComponent<PickupBase> ().OnGameObjectCollisionEnter (this.gameObject);
			}
		}

		// Wall Trigger Collision:
		if (collider.tag == "AIWall") {
			if (collider.transform.position.x < this.transform.position.x) {
				this.Host.WallLeft = true;
				this.Host.CollisionClearTime = 4;
			}
			else if (collider.transform.position.x > this.transform.position.x) {
				this.Host.WallRight = true;
				this.Host.CollisionClearTime = 4;
			}
		}
	}

	public void OnTriggerExit (Collider collider) {
		if (!NetworkManager.Get ().ServerSide () && !this.Host.LocalOwned ())
			return;
		if (this.Host.Dead)
			return;

		// Pickup Collision:
		if (NetworkManager.Get ().ServerSide ()) {
			if (collider.gameObject.GetComponent<PickupBase> () != null) {
				collider.gameObject.GetComponent<PickupBase> ().OnGameObjectCollisionExit (this.gameObject);
			}
		}
	}


	// ========== Water Colliders ==========
	public void AddWaterCollider (Collider collider) {
		if (!this.WaterColliders.Contains (collider))
			this.WaterColliders.Add (collider);
	}

	public void RemoveWaterCollider (Collider collider) {
		if (this.WaterColliders.Contains (collider))
			this.WaterColliders.Remove (collider);
	}
}
