using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;

public class CreatureLogicMovement : CreatureLogic, AnimationBase.IAnimationListener {

	// ===== Components =====
	public CharacterController Controller;


	// ===== Movement =====
	/** The movement to be applied at the end of the update. **/
	public Vector3 MoveDirection;

	/** The current force of movement, . **/
	public Vector3 MovementCurrent;

	/** The target force of movement. **/
	public Vector3 MovementTarget;

	/** The current movement speed, used for animation. **/
	[SyncVar (hook="OnSyncMovementSpeed")] private float MovementSpeed = 1;

	/** The current force of push (external movement). **/
	protected float PushCurrent = 0;

	/** The target force of push (external movement). **/
	protected float PushTarget = 0;

	/** The rotation to be applied at the end of the update. **/
	protected Vector3 RotateDirection;

	/** The current rotation of this creature. **/
	protected Quaternion RotationLast;

	/** The target rotation of this creature. **/
	protected Quaternion RotationTarget;

	/** The current speed rate between rotation states, resets to 0 on rotation completion. **/
	protected float RotationRate = 0;

	/** Current Jump - The number of jumps before being reset (usually by being grounded or underwater). **/
	[SyncVar (hook="OnSyncCurrentJump")] public int CurrentJump = 0;
	
	/** Falling Rate - Decreases by weight when not grounded or flying and is increased by jumping, etc. **/
	[SyncVar (hook="OnSyncFallingRate")] public float FallingRate = 0;
	
	/** Falling Max - The maximum falling speed. This will likely be moved to WorldBase. **/
	public float FallingMax = 100;
	
	/** Jump Force - Decreases by weight and is set by jumping. **/
	[SyncVar (hook="OnSyncJumpForce")] public float JumpForce = 0;

	/** Facing - The direction that this creature is facing. **/
	[SyncVar] public int Facing = 1;

	/** Turning Force - The amount of additive force gained from input for a direction (used to stop analogues from spinning players around when flicking on release). **/
	private float TurningForce = 0;

	/** Turning Force Limit - The amount of force a creature needs before forcing a change of facing direction (used to stop analogues from spinning players around when flicking on release). **/
	private float TurningForceLimit = 0.3f;

	/** Facing Clockwise - Whether this creature rotates clockwise or anticlockwise when changing facing direction. **/
	protected bool FacingClockwise = true;


	// ========== Awake ==========
	public override void Awake () {
		base.Awake ();

		// Get Components:
		this.Controller = this.GetComponent<CharacterController> ();
	}
	
	
	// ========== Creature Update ==========
	/** Called by the host CreatureBase after all Conditions and CreatureAI has updated. **/
	public override void CreatureUpdate () {
		// Reset Jumps:
		if (this.Host.Grounded || this.Host.Swimming)
			this.CurrentJump = 0;

		if (this.Host.LocalOwned ()) {
			// Apply Movement:
			if (!this.Host.Flying)
				this.ApplyGravity ();
			this.ApplyMovement ();
			this.ApplyPush ();
			this.Controller.Move (this.MoveDirection);
		}

		// Apply Rotation:
		this.ApplyFacing ();
		if (this.Host.FaceMovement) {
			Quaternion rotation = new Quaternion ();
			rotation.SetLookRotation (this.RotateDirection);
			this.RotationTarget = rotation;
			this.RotationRate += 5 * Time.deltaTime;
			this.transform.rotation = Quaternion.Slerp (this.RotationLast, this.RotationTarget, this.RotationRate);
			if (Quaternion.Angle (this.RotationTarget, this.transform.rotation) <= 0.1F) {
				this.RotationLast = this.transform.rotation;
				this.RotationRate = 0;
			}
		}


		if (this.Host.LocalOwned ()) {
			// Lock Z:
			this.transform.position = new Vector3 (this.transform.position.x, this.transform.position.y, this.Host.Depth);

			// Update Movement Speed:
			this.MovementSpeed = 1 + (Mathf.Max (this.MoveDirection.x, -this.MoveDirection.x) * 10);
			if (!this.Host.FaceMovement)
				this.MovementSpeed = -this.MovementSpeed;

			// Send Local Owned States To Server:
			if (!this.Host.LocalOnly)
				this.SendStates ();
		}
		if (this.Host.Animator != null)
			this.Host.Animator.SetFloat ("MovementSpeed", this.MovementSpeed);

		this.Cleanup ();
	}


	// ========== Cleanup ==========
	/** Resets various variables used for the next update of movement, etc. **/
	public void Cleanup () {
		// Cleanup:
		this.MoveDirection = new Vector3 (0, 0, 0);
		this.MovementTarget = new Vector3 (0, 0, 0);
		float grip = 40;
		if (this.Host.Grounded)
			grip = 80;
		if (this.PushTarget > 0)
			this.PushTarget = Math.Max (this.PushTarget - (Time.deltaTime * this.Host.Weight.ValueCurrent * grip), 0);
		else if (this.PushTarget < 0)
			this.PushTarget = Math.Min (this.PushTarget + (Time.deltaTime * this.Host.Weight.ValueCurrent * grip), 0);
		this.RotateDirection = new Vector3 (0, 0, 0);
	}
	
	
	// ========== Add Movement and Rotaion ==========
	/** Add to the movement vector of the next update. **/
	public void AddMovement (float x, float y, float z) {
		this.MoveDirection.x += x * Time.deltaTime;
		this.MoveDirection.y += y * Time.deltaTime;
		this.MoveDirection.z += z * Time.deltaTime;
	}

	/** Add to the rotation vector of the next update. **/
	public void AddRotation (float x, float y, float z) {
		this.RotateDirection.x += x * Time.deltaTime;
		this.RotateDirection.y += y * Time.deltaTime;
		this.RotateDirection.z += z * Time.deltaTime;
	}
	
	
	// ========== Command Movement and Rotaion ==========
	/** Makes this creature move in the provided directions using the creature's speed by executing DoMove (). **/
	public void Move (float directionX, float directionY = 0) {
		this.DoMove (directionX, directionY);
		if (NetworkManager.Get ().ClientSide () && this.hasAuthority)
			this.CmdMove (directionX, directionY);
	}

	[Command]
	public void CmdMove (float directionX, float directionY) {
		this.SetFacing (directionX);
	}

	public void DoMove (float directionX, float directionY) {
		float moveSpeed = (float)this.Host.Speed.ValueCurrent / 2;
		if (this.Host.Swimming)
			moveSpeed *= (float)this.Host.Swim.ValueCurrent;
		this.StartMove (directionX * moveSpeed, directionY * moveSpeed);
		this.SetFacing (directionX);
	}

	/** Applies movement for this creature to do. Use Move () for movement based on the speed stat. **/
	public void StartMove (float movementX, float movementY) {
		this.MovementTarget.x = movementX * 4;
		this.MovementTarget.y = movementY * 4;
		this.Host.Moving = true;
	}
	
	/** Adds external X movement for this creature to do. Use Move () and DoMove () for walking/running, etc. **/
	public void Push (float push) {
		if (this.PushTarget < push * 4)
		this.PushTarget = push * 4;
	}

	/** Updates the direction that this creature should face based on direction (if facing is enabled). **/
	public void SetFacing (float direction) {
		if (direction == 0 || (direction > 0 && this.TurningForce < 0) || (direction < 0 && this.TurningForce > 0))
			this.TurningForce = 0;
		else
			this.TurningForce += direction;
		if (this.Facing != 1 && this.TurningForce >= this.TurningForceLimit) {
			this.Facing = 1;
			this.FacingClockwise = UnityEngine.Random.value >= 0.5D;
		}
		else if (this.Facing != 0 && this.TurningForce <= -this.TurningForceLimit) {
			this.Facing = 0;
			this.FacingClockwise = UnityEngine.Random.value >= 0.5D;
		}
	}


	// ========== Command Jumping ==========
	/** Performs a jump test to see if this creature can jump, if it can DoJump () is then executed. Use AIJump() or StartJump(force) to force a jump **/
	public void Jump () {
		this.DoJump ();
		if (NetworkManager.Get ().ClientSide () && this.hasAuthority)
			this.CmdJump ();
	}

	[Command]
	public void CmdJump () {
		this.CurrentJump++;
	}

	public void DoJump () {
		if (this.CurrentJump >= this.Host.JumpCount.ValueCurrent)
			return;
		this.CurrentJump++;

		this.AIJump ();
	}

	/** Makes the creature jump using it's own jump stat and playing a sound. **/
	public void AIJump () {
		this.Host.PlaySound ("jump");
		this.StartJump (this.Host.JumpHeight.ValueCurrent);
	}
	
	/** Makes the creature start a jump. Does not increase the jump count, see Jump (). **/
	public void StartJump (float force) {
		this.JumpForce = 15F + (force * 5F);
		if (this.Host.Swimming)
			this.JumpForce *= 0.5f;
		this.FallingRate = 0;
		this.Host.Grounded = false;
	}
	

	// ========== Apply Movement and Rotaion ==========
	/** Applies main movement, usually walking and running or flying, not external forces. **/
	public void ApplyMovement () {
		float movementForce = 5 * Time.deltaTime; // TODO: Add Friction.
		if (!this.Host.Grounded) {
			movementForce /= 2; // TODO: Add Air Control stat.
		}

		/*if (this.MovementTarget.x > 0)
			this.MovementCurrent.x = Mathf.Min (this.MovementCurrent.x + movementForce, this.MovementTarget.x);
		else
			this.MovementCurrent.x = Mathf.Max (this.MovementCurrent.x - movementForce, this.MovementTarget.x);*/
		float change = this.MovementTarget.x - this.MovementCurrent.x;
		this.MovementCurrent.x += (change / movementForce) * Time.deltaTime;

		if (!this.Host.Flying)
			this.MovementTarget.y = 0;
		else {
			if (this.MovementTarget.y > 0)
				this.MovementCurrent.y = Mathf.Min (this.MovementCurrent.y + movementForce, this.MovementTarget.y);
			else
				this.MovementCurrent.y = Mathf.Max (this.MovementCurrent.y - movementForce, this.MovementTarget.y);
		}
		
		this.AddMovement (this.MovementCurrent.x, this.MovementCurrent.y, this.MovementCurrent.z);
	}
	
	/** Applies external horizontal movement, usually knockbacks and wind, etc. **/
	public void ApplyPush () {
		float pushForce = 10 * Time.deltaTime; // TODO: Add Friction.
		
		if (this.PushTarget > 0)
			this.PushCurrent = Mathf.Min (this.PushCurrent + pushForce, this.PushTarget);
		else
			this.PushCurrent = Mathf.Max (this.PushCurrent - pushForce, this.PushTarget);
		this.AddMovement (this.PushCurrent, 0, 0);
	}
	
	/** Applies main vertical movement using gravity, weight and jump force. This is not called if the host is flying. **/
	public void ApplyGravity () {
		// Weight:
		float weight = (float)this.Host.Weight.ValueCurrent * 0.1F;
		if (this.Host.Swimming)
			weight *= 0.5F;

		// Falling:
		float fallingMaxCalc = this.FallingMax * 60F * Time.deltaTime;
		if (this.Host.Swimming)
			fallingMaxCalc *= 0.1F;
		if (!this.Host.Grounded)
			this.FallingRate = Mathf.Min (this.FallingRate + weight * 60F * Time.deltaTime, fallingMaxCalc);
		else
			this.FallingRate = 10;

        // Jumping:
        if (this.Host.Ceiling)
            this.JumpForce = 0;
		if (this.JumpForce > 0) {
			this.JumpForce = Mathf.Max (this.JumpForce - (weight * 180F * Time.deltaTime), 0);
		}

        // Y Movement and Delta Time:
		float movementY = (this.JumpForce - this.FallingRate);

        this.AddMovement (0, movementY, 0);
	}
	
	/** Updates the direction that this creature is facing. **/
	public void ApplyFacing () {
		float zAmount = 0.001F;
		if (this.FacingClockwise)
			zAmount = -0.001F;
		if (this.Facing == 1) {
			this.AddRotation (1, 0, zAmount);
		}
		else if (this.Facing == 0) {
			this.AddRotation (-1, 0, zAmount);
		}
	}


	// ========== On Sync ==========
	/** Events called when SyncVars are updated, local players can ignore some changes here for smoother gameplay. **/
	protected void OnSyncCurrentJump (int currentJump) {
		if (!this.hasAuthority)
			this.CurrentJump = currentJump;
	}

	protected void OnSyncMovementSpeed (float movementSpeed) {
		if (!this.hasAuthority) {
			this.MovementSpeed = movementSpeed;
			if (this.Host.Animator != null)
				this.Host.Animator.SetFloat ("MovementSpeed", this.MovementSpeed);
		}
	}

	protected void OnSyncFallingRate (float fallingRate) {
		if (!this.hasAuthority)
			this.FallingRate = fallingRate;
	}

	protected void OnSyncJumpForce (float jumpForce) {
		if (!this.hasAuthority)
			this.JumpForce = jumpForce;
	}


	// ========== Send States ==========
	/** Sends various states that are client controlled to the server. **/
	protected int StateUpdateTime = 0;
	protected int StateUpdateInterval = 20;
	protected void SendStates () {
		if (this.StateUpdateTime++ % this.StateUpdateInterval == 0)
			this.CmdSendStates (this.MovementSpeed, this.FallingRate, this.JumpForce);
	}

	[Command]
	protected void CmdSendStates (float movementSpeed, float fallingRate, float jumpForce) {
		this.MovementSpeed = movementSpeed;
		if (this.Host.Animator != null)
			this.Host.Animator.SetFloat ("MovementSpeed", this.MovementSpeed);
		this.FallingRate = fallingRate;
		this.JumpForce = jumpForce;
	}


	// ========== On Animation Move ==========
	/* Called by animation components when they move the transform. */
	public void OnAnimationMove (Vector3 movement) {
		
	}


	// ========== On Animation Rotate ==========
	/* Called by animation components when they rotate the transform. */
	public void OnAnimationRotate (Quaternion rotation) {
		
	}


	// ========== On Animation Scale ==========
	/* Called by animation components when they scale the transform. */
	public void OnAnimationScale (Vector3 movement) {
		
	}


	// ========== Get Animation Facing ==========
	/* Returns true if facing x+ (right) and false if facing x- (left). Used to flip some animations. */
	public bool GetAnimationFacing () {
		return this.Facing > 0;
	}
}

