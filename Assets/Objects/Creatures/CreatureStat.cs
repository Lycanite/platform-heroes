using UnityEngine;
using System.Collections;

public class CreatureStat {
	public string Name;
	public int ValueBase;
	public int ValueCurrent;

	// ========== Constructor ==========
	public CreatureStat(string name, int valueBase) {
		this.Name = name;
		this.ValueBase = valueBase;
		this.ValueCurrent = valueBase;
	}


	// ========== Info ==========
	public double GetNormal () {
		return this.ValueCurrent / this.ValueBase;
	}
}

