using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Networking;

public class RulesManager : NetworkBehaviour {
	public static RulesManager Instance;
	public enum MatchType {
		Deathmatch,
		Elmination,
		Points
	};
	[SyncVar] public MatchType CurrentMatchType = MatchType.Deathmatch;

	// Rules:
	[SyncVar] public double TimeLimit = 5;
	[SyncVar] public int Lives = 5;
	[SyncVar] public int Kills = 10;
	[SyncVar] public int Score = 5000;
	[SyncVar] public bool Enemies = true;


	// ========== Get ==========
	public static RulesManager Get () {
		return Instance;
	}

	
	// ========== Awake ==========
	public void Awake () {
		if (Instance == null)
			Instance = this;
		else
			GameObject.Destroy (this.gameObject);
	}


	// ========== Set Match Type ==========
	public void SetMatchType (string matchTypeString) {
		MatchType matchType = (MatchType) Enum.Parse (typeof (MatchType), matchTypeString, true);
		this.CurrentMatchType = matchType;
	}
}
