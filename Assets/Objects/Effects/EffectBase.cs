using UnityEngine;
using System.Collections;

public class EffectBase : MonoBehaviour {
	// Objects:
	public GameObject Owner;

	// Stats:
	public bool FollowOwner = false;
	public Vector3 OwnerOffset;
	public float Life = 3;

	
	// ========== Start ==========
	public void Start () {
		if (this.Owner != null)
			this.OwnerOffset = this.Owner.transform.position - this.transform.position;
	}


	// ========== Update ==========
	public void Update () {
		// Life:
		this.Life -= Time.deltaTime;
		if (this.Life <= 0) {
			GameObject.Destroy (this.gameObject);
			return;
		}

		// Follow Owner:
		if (this.FollowOwner && this.Owner != null)
			this.transform.position = this.Owner.transform.position + this.OwnerOffset;
	}
}