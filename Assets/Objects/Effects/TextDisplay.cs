using UnityEngine;

public class TextDisplay : MonoBehaviour {

	// Text Display:
	public string DisplayText = ""; // The text to display.
	public float Duration = 3; // How long (seconds) until this text is removed.
	protected float TimeActive = 0; // The current amount of time (seconds) that this text has been active for.
	public Vector3 Movement = new Vector3 (0, 0, 0); // How far to move over time (seconds).

	public float GrowTime = 0.25f; // Time (seconds) to animate the text growth.
	public AnimationCurve GrowCurve; // The curve to use when growing the text.

	public float FadeTime = 0.5f; // Time (seconds) that it takes for the text to fade out.

	// Components:
	public TextMesh TextMesh;


	// ========== Start ==========
	public void Start () {
		if (this.TextMesh == null)
			this.TextMesh = this.GetComponent<TextMesh> ();
	}


	// ========== Update ==========
	public void Update () {
		if (this.TimeActive > this.Duration) {
			GameObject.Destroy (this.gameObject);
			return;
		}

		this.transform.position += this.Movement * Time.deltaTime;

		// Grow:
		float growProgress = Mathf.Min (this.TimeActive / this.GrowTime, 1);
		growProgress *= this.GrowCurve.Evaluate (growProgress);
		this.transform.localScale = (new Vector3 (growProgress, growProgress, growProgress));

		// Fade:
		float fadelessTime = this.Duration - this.FadeTime;
		if (this.TimeActive >= fadelessTime) {
			float fadeProgress = Mathf.Min ((this.TimeActive - fadelessTime) / (this.Duration - fadelessTime), 1);
			Color textColor = this.TextMesh.color;
			textColor.a = 1 - fadeProgress;
			this.TextMesh.color = textColor;
		}

		this.TimeActive += Time.deltaTime;
	}


	// ========== Pickup ==========
	/** Sets the text to display. **/
	public void SetText (string text) {
		if (this.TextMesh == null)
			this.TextMesh = this.GetComponent<TextMesh> ();
		this.TextMesh.text = text;
	}
}
