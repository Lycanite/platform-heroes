using System.Collections.Generic;
using UnityEngine;

public class AnimationNavigate : AnimationBase, Navigator.INavigatorListener {

	// Navigation:
	public Navigator Navigator;
	public GameObject NodesParent; // If set, all child objects will be added as nodes on start.
	public List<GameObject> Nodes = new List<GameObject> ();
	public bool StartAtClosestNode = true;

	// Settings:
	public float NodePause = 0;
	protected float TimeNodePaused = 0;
	protected bool AtNode = false;
	public bool UseDuration = false;


	// ========== Start Animation ==========
	public override void StartAnimation () {
		this.Navigator = new Navigator ();
		foreach (GameObject node in this.Nodes)
			this.Navigator.AddNode (node);
		if (this.NodesParent != null) {
			foreach (Transform child in this.NodesParent.transform)
				this.Navigator.AddNode (child.gameObject);
		}
		if (this.StartAtClosestNode)
			this.Navigator.SetTargetNode (this.Navigator.GetClosestNode (this.transform.position));
		this.Navigator.Listener = this;
		this.Navigator.Reverse = this.Reverse;
	}


	// ========== Can Update ==========
	public override bool CanUpdate (float progress) {
		this.Completed = 0;
		if (this.AtNode && this.NodePause > 0) {
			if (this.TimeNodePaused < this.NodePause) {
				this.TimeNodePaused += Time.deltaTime;
				return false;
			}
		}
		return true;
	}


	// ========== Update Animation ==========
	public override void UpdateAnimation (float progress) {
		this.AtNode = false;
		this.Navigator.Loop = this.Loop;
		this.Navigator.Reverse = this.Reverse;
		
		if (!this.UseDuration) {
			Vector3 direction = this.Navigator.GetTargetNode ().GetPosition () - this.transform.position;
			direction.Normalize ();
			float speed = this.Speed * Time.deltaTime;
			this.MoveProgress = Vector3.zero; // Movement is not additive when using speed.
			this.Move (direction * speed);
		}
		else {
			Vector3 direction = this.Navigator.GetTargetNode ().GetPosition () - this.Navigator.GetPreviousNode ().GetPosition ();
			direction.Normalize ();
			float distance = Vector3.Distance (this.Navigator.GetPreviousNode ().GetPosition (), this.Navigator.GetTargetNode ().GetPosition ());
			Vector3 offset = this.Navigator.GetPreviousNode ().GetPosition () - this.InitPosition;
			this.Move (offset + (direction * (distance * progress)));
		}

		this.Navigator.UpdateNavigator (this.transform.position);
	}


	// ========== Navigator First Node Reached ==========
	public void OnNavigatorFirstNodeReached (Navigator.NavigatorNode node) {
		if (this.Reverse) {
			this.Completed = 1;
			if (this.Alternate)
				this.Navigator.Reverse = !this.Reverse;
		}
	}


	// ========== Navigator Last Node Reached ==========
	public void OnNavigatorLastNodeReached (Navigator.NavigatorNode node) {
		if (!this.Reverse) {
			this.Completed = 1;
			if (this.Alternate)
				this.Navigator.Reverse = !this.Reverse;
		}
	}


	// ========== Navigator Node Reached ==========
	public void OnNavigatorNodeReached (Navigator.NavigatorNode node) {
		this.TimeActive = 0;
		this.AtNode = true;
		this.TimeNodePaused = 0;
	}
}
