using UnityEngine;

public class AnimationConstant : AnimationBase {

	// Animation:
	public Vector3 MoveAmount = new Vector3 (0, 0, 0); // The amount to move by over the duration.
	public AnimationCurve MoveCurve; // The curve to use when moving.
	public Vector3 SpinAmount = new Vector3 (0, 0, 0); // The amount to spin by (degrees) over the duration.
	public AnimationCurve SpinCurve; // The curve to use when spinning.
	public Vector3 GrowAmount = new Vector3 (0, 0, 0); // The amount to grow by over the duration.
	public AnimationCurve GrowCurve; // The curve to use when growing.


	// ========== Update Animation ==========
	public override void UpdateAnimation (float progress) {
		this.UpdateMovement (progress);
		this.UpdateSpin (progress);
		this.UpdateGrow (progress);
	}


	// ========== Update Movement ==========
	public void UpdateMovement (float progress) {
		Vector3 movement = this.MoveAmount * this.MoveCurve.Evaluate (progress);

		// Reverse:
		if (this.Reverse)
			movement = -movement;

		// Facing:
		if (this.AnimationListener != null && !this.AnimationListener.GetAnimationFacing ())
			movement.x = -movement.x;

		// Apply:
		this.Move (movement);
	}


	// ========== Update Spin ==========
	public void UpdateSpin (float progress) {
		Vector3 spin = -this.SpinAmount * this.SpinCurve.Evaluate (progress);

		// Reverse:
		if (this.Reverse)
			spin *= -1;

		// Facing:
		if (this.AnimationListener != null && !this.AnimationListener.GetAnimationFacing ())
			spin.z = -spin.z;

		// Apply:
		this.Spin (spin);
	}


	// ========== Update Grow ==========
	public void UpdateGrow (float progress) {
		Vector3 growth = this.GrowAmount * this.GrowCurve.Evaluate (progress);

		// Reverse:
		if (this.Reverse)
			growth = -growth;

		// Apply:
		this.Grow (growth);
	}
}
