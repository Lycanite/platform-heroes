using UnityEngine;

public class AnimationRandom : AnimationBase {

	// Animation:
	public Vector3 MoveAmount = new Vector3 (0.25f, 0.25f, 0.25f); // The amount to move by relative to initial position.
	public AnimationCurve MoveCurve; // The curve to use when moving.
	public Vector3 SpinAmount = new Vector3 (0.5f, 0.5f, 0.5f); // The amount to tilt by relative to the initial angles.
	public AnimationCurve SpinCurve; // The curve to use when spinning.
	public Vector3 GrowAmount = new Vector3 (0, 0, 0); // The amount to grow by relative to initial scale.
	public AnimationCurve GrowCurve; // The curve to use when growing.

	// Target State:
	protected Vector3 TargetPosition;
	protected Vector3 TargetAngles;
	protected Vector3 TargetScale;

	// Last Target State:
	protected Vector3 LastTargetPosition;
	protected Vector3 LastTargetAngles;
	protected Vector3 LastTargetScale;


	// ========== Start Animation ==========
	public override void StartAnimation () {
		this.TargetPosition = this.InitPosition;
		this.TargetAngles = this.InitAngles;
		this.TargetScale = this.InitScale;
	}


	// ========== Update Animation ==========
	public override void UpdateAnimation (float progress) {
		// Move:
		Vector3 movement = this.TargetPosition - this.LastTargetPosition;
		this.Move (movement * this.MoveCurve.Evaluate (progress));

		// Spin:
		Vector3 spin = this.TargetAngles - this.LastTargetAngles;
		this.Spin (spin * this.SpinCurve.Evaluate (progress));

		// Grow:
		Vector3 growth = this.TargetScale - this.LastTargetScale;
		this.Grow (growth * this.GrowCurve.Evaluate (progress));
	}


	// ========== Reset ==========
	/** Sets the last position, etc and randomly generates new ones based on the amounts set. **/
	public override void Reset () {
		base.Reset ();
		
		this.LastTargetPosition = this.TargetPosition;
		this.LastTargetAngles = this.TargetAngles;
		this.LastTargetScale = this.TargetScale;

		float targetPositionX = this.MoveAmount.x != 0 ? Random.Range (-this.MoveAmount.x, this.MoveAmount.x) : 0;
		float targetPositionY = this.MoveAmount.y != 0 ? Random.Range (-this.MoveAmount.y, this.MoveAmount.y) : 0;
		float targetPositionZ = this.MoveAmount.z != 0 ? Random.Range (-this.MoveAmount.z, this.MoveAmount.z) : 0;
		this.TargetPosition = this.FromPosition + new Vector3 (targetPositionX, targetPositionY, targetPositionZ);
		if (this.Reverse)
			this.TargetPosition = this.FromPosition + new Vector3 (-targetPositionX, -targetPositionY, -targetPositionZ);
		
		float targetAnglesX = this.SpinAmount.x != 0 ? Random.Range (-this.SpinAmount.x, this.SpinAmount.x) : 0;
		float targetAnglesY = this.SpinAmount.y != 0 ? Random.Range (-this.SpinAmount.y, this.SpinAmount.y) : 0;
		float targetAnglesZ = this.SpinAmount.z != 0 ? Random.Range (-this.SpinAmount.z, this.SpinAmount.z) : 0;
		this.TargetAngles = this.FromAngles + new Vector3 (targetAnglesX, targetAnglesY, targetAnglesZ);
		if (this.Reverse)
			this.TargetAngles = this.FromAngles + new Vector3 (-targetAnglesX, -targetAnglesY, -targetAnglesZ);
		
		float targetScaleX = this.GrowAmount.x != 0 ? Random.Range (-this.GrowAmount.x, this.GrowAmount.x) : 0;
		float targetScaleY = this.GrowAmount.y != 0 ? Random.Range (-this.GrowAmount.y, this.GrowAmount.y) : 0;
		float targetScaleZ = this.GrowAmount.z != 0 ? Random.Range (-this.GrowAmount.z, this.GrowAmount.z) : 0;
		this.TargetScale = this.FromScale + new Vector3 (targetScaleX, targetScaleY, targetScaleZ);
		if (this.Reverse)
			this.TargetScale = this.FromScale + new Vector3 (-targetScaleX, -targetScaleY, -targetScaleZ);
	}
}
