using UnityEngine;

public class AnimationBase : MonoBehaviour {

	// Animation:
	public float Duration = 0.5f; // How long it takes to complete this animation.
	public float TimeActive = 0; // Time (seconds) that this animation has played.
	protected float Completed = 0; // The leftover progress after completing an animation. Also determines when to stop or reset.

	public float Pause = 0; // The time (seconds) to pause for between loops.
	protected float TimePaused = 0; // The time 9seconds) that this animation has currently paused for.

	public float Speed = 1; // How fast the animation plays.
	public bool Loop = true; // If true, this animation will loop.
	public bool Alternate = false; // If true, this animation will reverse after each loop.
	public bool Reverse = false; // If true, this animation will play in reverse.
	public bool LocalSpace = false; // If true, local position and angles are used. Scaling is always local.
	public bool Relative = false; // If true, each time the animation resets, it starts again based on its initial state, if false, the last state.
	public bool DestroyOnStop = false; // If true, the animated GameObject will be destroyed when the animation completes, this will never happen if looping.

	// Initial State: The state of the transform on start.
	protected Vector3 InitPosition;
	protected Vector3 InitAngles;
	protected Vector3 InitScale;

	// From State: The last state of the transform to animate from.
	protected Vector3 FromPosition;
	protected Vector3 FromAngles;
	protected Vector3 FromScale;

	// Progress: How much animation has been apllied to each state.
	protected Vector3 MoveProgress;
	protected Vector3 SpinProgress;
	protected Vector3 GrowProgress;

	// Components:
	public IAnimationListener AnimationListener; // If set, this animation notify the listener of movement, etc and will check if for x flips.

	// Animation Listener Interface:
	public interface IAnimationListener {
		void OnAnimationMove (Vector3 movement); // Called when this animation moves the transform.
		void OnAnimationRotate (Quaternion rotation); // Called when this animation rotates the transform.
		void OnAnimationScale (Vector3 movement); // Called when this animation scales the transform.
		bool GetAnimationFacing (); // Returns true if facing x+ (right) and false if facing x- (left).
	}


	// ========== Start ==========
	public virtual void Start () {
		// Set Initial State:
		this.InitPosition = this.LocalSpace ? this.transform.localPosition : this.transform.position;
		this.InitAngles = this.LocalSpace ? this.transform.localEulerAngles : this.transform.eulerAngles;
		this.InitScale = this.transform.localScale;

		// Check Components:
		if (this.AnimationListener == null)
			this.AnimationListener = this.GetComponent<IAnimationListener> ();

		this.StartAnimation ();
		if (this.Alternate)
			this.Reverse = !this.Reverse;
		this.Reset ();
	}


	// ========== Start Animation ==========
	public virtual void StartAnimation () {
		// Do initial animation setup here.
	}


	// ========== Update ==========
	public virtual void Update () {
		// Progress
		float progress = this.TimeActive / this.Duration;

		// Finish:
		if (this.Completed > 0) {
			// Looping:
			if (this.Loop) {

				// Pausing:
				if (this.Pause > 0) {
					if (this.TimePaused < this.Pause) {
						this.TimePaused += Time.deltaTime;
						return;
					}
				}

				// Reset:
				this.Reset ();
				progress = this.TimeActive / this.Duration;
			}

			// Not Looping:
			else {
				if (this.DestroyOnStop)
					GameObject.Destroy (this.gameObject);
				return;
			}
		}

		// Completion:
		if (progress >= 1) {
			this.Completed = progress % 1;
			progress = 1;
		}

		if (this.CanUpdate (progress)) {
			// Update Animation:
			this.UpdateAnimation (progress);

			// Increase Time Active:
			this.TimeActive += Time.deltaTime * this.Speed;
		}
	}


	// ========== Can Update ==========
	/* Returns true if UpdateAnimation should be called, if false it will be skipped and TimeActive will not increase, good for additinoal pauses.  */
	public virtual bool CanUpdate (float progress) {
		return true;
	}


	// ========== Update Animation ==========
	public virtual void UpdateAnimation (float progress) {
		// Apply Animation Here.
	}


	// ========== Reset ==========
	public virtual void Reset () {
		this.TimeActive = this.Completed;
		this.Completed = 0;
		this.TimePaused = 0;
		if (this.Alternate)
			this.Reverse = !this.Reverse;

		// Update Last State:
		if (!this.Relative) {
			if (this.LocalSpace) {
				this.FromPosition = this.transform.localPosition;
				this.FromAngles = this.transform.localEulerAngles;
			}
			else {
				this.FromPosition = this.transform.position;
				this.FromAngles = this.transform.eulerAngles;
			}
			this.FromScale = this.transform.localScale;
		}
		else {
			this.FromPosition = this.InitPosition;
			this.FromAngles = this.InitAngles;
			this.FromScale = this.InitScale;
		}

		// Reset Progress:
		this.MoveProgress = Vector3.zero;
		this.Move (-this.MoveProgress);
		this.MoveProgress = Vector3.zero;

		this.SpinProgress = Vector3.zero;
		this.Spin (-this.SpinProgress);
		this.SpinProgress = Vector3.zero;

		this.GrowProgress = Vector3.zero;
		this.Grow (-this.GrowProgress);
		this.GrowProgress = Vector3.zero;
	}


	// ========== Move ==========
	public virtual void Move (Vector3 movement) {
		movement -= this.MoveProgress;
		if (this.LocalSpace)
			this.transform.localPosition += movement;
		else
			this.transform.position += movement;
		if (this.AnimationListener != null)
			this.AnimationListener.OnAnimationMove (movement);
		this.MoveProgress += movement;
	}


	// ========== Spin ==========
	public virtual void Spin (Vector3 spin) {
		Quaternion baseRotation = this.transform.localRotation;
		this.transform.localEulerAngles = Vector3.zero;
		spin -= this.SpinProgress;
		this.transform.Rotate (Vector3.forward, spin.z, Space.Self);
		this.transform.Rotate (Vector3.up, spin.y, Space.Self);
		this.transform.Rotate (Vector3.right, spin.x, Space.Self);
		if (this.AnimationListener != null)
			this.AnimationListener.OnAnimationRotate (this.transform.localRotation);
		this.transform.localRotation *= baseRotation;
		this.SpinProgress += spin;
	}


	// ========== Grow ==========
	public virtual void Grow (Vector3 growth) {
		growth -= this.GrowProgress;
		this.transform.localScale += growth;
		if (this.AnimationListener != null)
			this.AnimationListener.OnAnimationScale (growth);
		this.GrowProgress += growth;
	}
}
