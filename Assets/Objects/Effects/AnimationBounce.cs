using UnityEngine;

public class AnimationBounce : AnimationBase {

	public Vector3 BounceMovement;
	public AnimationCurve MovementCurve;

	protected Vector3 BounceAngles;
	protected AnimationCurve AnglesCurve;

	public Vector3 BounceGrowth = new Vector3 (1, 1, 1);
	public AnimationCurve GrowthCurve;


	// ========== Update Animation ==========
	public override void UpdateAnimation (float progress) {
		if (this.Reverse)
			progress = 1 - progress;

		// Movement:
		this.transform.localPosition = this.InitPosition + (this.BounceMovement * this.MovementCurve.Evaluate (progress));

		// Rotation:
		// TODO Animate bounce rotation.

		// Scale:
		this.transform.localScale = this.InitScale + (this.BounceGrowth * this.GrowthCurve.Evaluate (progress));
	}
}
