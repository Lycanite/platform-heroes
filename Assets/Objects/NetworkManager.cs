﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using System.Collections.Generic;

public class NetworkManager : UnityEngine.Networking.NetworkManager {
	public static NetworkManager Instance;

	public enum NetworkState {
		LocalHost, // Hosting a game internally (singleplayer/multiple instances).
		RemoteHost, // Hosting a game online/LAN.
		LocalJoin, // Joined an internal host (another game instance on the same machine).
		RemoteJoin // Joined a remote host (multiplayer).
	}

	public enum HostState {
		Starting, // Starting up a host.
		Accepting, // Hosting and accepting new clients.
		Locked, // Hosting but not accepting any new clients.
		Stopping, // Closing down a host.
		Stopped // Not hosting.
	}

	public enum JoinState {
		Connecting, // Connecting to a host.
		Connected, // Connected to a host.
		Disconnecting, // Disconnecting from a host.
		Disconnected // Not connected to any host.
	}

	public NetworkState CurrentState = NetworkState.LocalHost;
	public HostState CurrentHostState = HostState.Starting;
	public JoinState CurrentJoinState = JoinState.Connecting;

	private NetworkClient HostClient;

	// Hosting Settings:
	private int LocalPort = 5555;
	public int RemotePort = 7777;
	public string Password = "";
	public bool Hidden = false;

	// Matches:
	public UnityEngine.Networking.Match.MatchInfoSnapshot SelectedMatch;
	public bool PollingMatchList = true;

	private Dictionary<NetworkConnection, List<GameObject>> ClientAuthorityObjectQueue = new Dictionary<NetworkConnection, List<GameObject>> ();
	
	// Debug:
	public bool TestMessages = false;


	// ========== Get ==========
	public static NetworkManager Get () {
		return Instance;
	}

	
	// ========== Awake ==========
	public void Awake () {
		if (Instance == null)
			Instance = this;
		else
			GameObject.Destroy (this.gameObject);
	}


	// ========== Start ==========
	public void Start () {
		this.HostLocal ();
	}


	// ========== Host Local ==========
	public void HostLocal () {
		this.CurrentState = NetworkState.LocalHost;
		this.CurrentHostState = HostState.Starting;
		this.networkPort = this.LocalPort;
		NetworkClient hostClient = this.StartHost ();
		if (hostClient == null)
			this.HostFail ();
	}


	// ========== Join Local ==========
	public void JoinLocal () {
		this.CurrentState = NetworkState.LocalJoin;
		this.CurrentJoinState = JoinState.Connecting;
		this.networkPort = this.LocalPort;
		if (this.Active ())
			this.Stop ();
		NetworkClient joinClient = this.StartClient ();
		if (joinClient == null)
			this.JoinFail ();
	}


	// ========== Host Remote ==========
	public void Host () {
		if (this.Active ())
			this.Stop ();
		this.CurrentState = NetworkState.RemoteHost;
		this.CurrentHostState = HostState.Starting;
		this.networkPort = this.RemotePort;

		// Hidden Match:
		if (this.Hidden) {
			NetworkClient hostClient = this.StartHost ();
			if (hostClient == null)
				this.HostFail ();
			return;
		}

		// Match Maker:
		if (this.matchMaker == null)
			this.StartMatchMaker ();
		this.matchMaker.CreateMatch(this.matchName, this.matchSize, true, "", "", "", 0, 0, this.OnMatchCreate);
	}


	// ========== Join Remote ==========
	public void Join (bool direct = false) {
		// Direct:
		if (direct) {
			if (this.Active ())
				this.Stop ();
			this.CurrentState = NetworkState.RemoteJoin;
			this.CurrentJoinState = JoinState.Connecting;
			this.networkPort = this.RemotePort;
			NetworkClient joinClient = this.StartClient ();
			if (joinClient == null)
				this.JoinFail ();
			return;
		}

		// Match Maker:
		if (this.SelectedMatch == null) {
			string info = "Please choose a match from the Match Maker list to join.";
			LevelBase.Get ().Interface.OpenPopup ("servers", "No Match Selected!", info, null, true, "Ok");
			return;
		}
		if (this.Active ())
			this.Stop ();
		this.CurrentState = NetworkState.RemoteJoin;
		this.CurrentJoinState = JoinState.Connecting;
		if (this.matchMaker == null)
			this.StartMatchMaker ();
		this.matchMaker.JoinMatch(this.SelectedMatch.networkId, "" , "", "", 0, 0, OnMatchJoined);
	}


	// ========== Start Host ==========
	public override NetworkClient StartHost () {
		this.HostClient = base.StartHost ();
		return this.HostClient;
	}


	// ========== Stop Host/Client ==========
	public void Stop () {
		this.StopHost ();
		if (!this.Local ())
			this.HostLocal ();
	}


	// ========== Host/Join Fail ==========
	/** Called when starting up a host failed. Unless the failure was during a local host startup the local host is started back up. **/
	public void HostFail () {
		if (!this.Local ())
			this.HostLocal ();
		else
			this.JoinLocal ();
	}

	/** Called when joining a host failed. Unless the failure was during a local client startup the local host is started back up. **/
	public void JoinFail () {
		if (!this.Local ()) {
			this.HostLocal ();
			return;
		}
		string info = "Unable to startup the local host or join it, please restart the game.";
		LevelBase.Get ().Interface.OpenPopup ("servers", "Can't Host or Join Locally", info, null, true, "Ok");
	}


	// ========== Refresh Host List ==========
	public void RefreshMatchList () {
		this.PollingMatchList = true;
		if (this.matchMaker == null)
			this.StartMatchMaker ();
		this.matchMaker.ListMatches (0, 100, "", true, 0, 0, OnMatchList);
	}


	// ========== Select Match ==========
	public void SelectMatch (int matchListID) {
		if (this.matches == null || matchListID >= this.matches.Count)
			return;
		this.SelectedMatch = this.matches[matchListID];
		this.Log ("[Client] Selected Match: " + this.SelectedMatch);
	}


	// ========== On Server ==========
	// When the server is started.
	public override void OnStartServer () {
		this.CurrentHostState = HostState.Accepting;
		this.Log ("[Server] Started" + (this.Local () ? " (local)" : ""));
	}

	// When the server is stopped.
	public override void OnStopServer () {
		this.CurrentHostState = HostState.Stopped;
		this.Log ("[Server] Stopped" + (this.Local () ? " (local)" : ""));
		ClientManager.Get ().Reset ();
		this.HostClient = null;
	}
	
	// When a client connects to the hosted server:
	public override void OnServerConnect (NetworkConnection conn) {
		this.Log ("[Server] Client joined from: " + conn);
	}

	// When the server receives a ready message from a client:
	public override void OnServerReady (NetworkConnection conn) {
		NetworkServer.SetClientReady (conn);
		this.Log ("[Server] Client ready: " + conn);
		ClientManager.Get ().AddClient (conn);

		// Spawn Queued Authority Objects:
		if (this.ClientAuthorityObjectQueue.ContainsKey (conn)) {
			foreach (GameObject gameObject in this.ClientAuthorityObjectQueue[conn]) {
				NetworkServer.SpawnWithClientAuthority (gameObject, conn);
			}
			this.ClientAuthorityObjectQueue [conn].Clear ();
		}
	}

	// When the server changes scene:
	public override void OnServerSceneChanged (string sceneName) {
		this.Log ("[Server] Scene changed: " + sceneName);
	}

	// When a client disconnects from the hosted server:
	public override void OnServerDisconnect (NetworkConnection conn) {
		this.Log ("[Server] Client left from: " + conn);
		ClientManager.Get ().RemoveClient (conn);
	}

	// When the server receives an error from a client:
	public override void OnServerError (NetworkConnection conn, int errorCode) {
		this.Log ("[Server] Client error: " + errorCode + " - " + conn);
		ClientManager.Get ().RemoveClient (conn);
	}

	// When a client requests a new player (this is changed to instead assign an existing game object as a player):
	public override void OnServerAddPlayer (NetworkConnection conn, short playerControllerId) {
		this.Log ("[Server] Client requested player for connection: " + conn + " and controller ID: " + playerControllerId);
		PlayerManager playerManager = ClientManager.Get ().PlayerManagers[conn.connectionId];
		PlayerInfo playerInfo = playerManager.PlayerInfos[playerControllerId];
		NetworkServer.AddPlayerForConnection (playerManager.Connection, playerInfo.CurrentPlayerStats.PlayerObject, playerControllerId);
	}


	// ========== On Client ==========
	// When the client is started:
	public override void OnStartClient (NetworkClient client) {
		this.CurrentJoinState = JoinState.Connecting;
		if (NetworkServer.active) {
			this.Log ("[Client] Started as self connected client: " + client.connection + (this.Local () ? " (local)" : ""));
			return;
		}
		this.Log ("[Client] Started: " + client.connection + (this.Local () ? " (local)" : ""));
	}

	// When the client is stopped:
	public override void OnStopClient () {
		this.CurrentJoinState = JoinState.Disconnected;
		this.Log ("[Client] Stopped" + (this.Local () ? " (local)" : ""));
		ClientManager.Get ().Reset ();
	}
	
	// When the client connects to a server:
	public override void OnClientConnect (NetworkConnection conn) {
		this.CurrentJoinState = JoinState.Connected;
		this.Log ("[Client] Joined server: " + conn);
		ClientManager.Get ().Reset ();
		ClientScene.Ready (this.client.connection);
	}

	// When the client is not ready:
	public override void OnClientNotReady (NetworkConnection conn) {
		this.Log ("[Client] Not ready: " + conn);
		//ClientScene.Ready (conn);
	}

	// When the client changes scene:
	public override void OnClientSceneChanged (NetworkConnection conn) {
		this.Log ("[Client] Scene changed on server: " + conn);
		ClientScene.Ready (conn);
	}

	// When the client disconnects:
	public override void OnClientDisconnect (NetworkConnection conn) {
		if (this.CurrentJoinState != JoinState.Disconnecting) {
			string info = "Lost connection from the host.";
			LevelBase.Get ().Interface.OpenPopup ("servers", "Lost Connection", info, null, true, "Ok");
		}
		this.CurrentJoinState = JoinState.Disconnected;
		this.Log ("[Client] Left server: " + conn);
		this.HostLocal ();
	}

	// When the client receives an error:
	public override void OnClientError (NetworkConnection conn, int errorCode) {
		this.CurrentJoinState = JoinState.Disconnected;
		this.Log ("[Client] Error: " + errorCode + " o- " + conn);
		string info = "A problem occured when connecting to the host, if connecting directly please ensure that the correct address and ports were used and that the host has port forwarded, also check your firewalls and that you aren't confusing a local IP with a remote IP.";
		LevelBase.Get ().Interface.OpenPopup ("servers", "Error Connecting To Host", info, null, true, "Ok");
		if (!this.Local ())
			this.HostLocal ();
	}


	// ========== On Match ==========
	// When the match is created:
	public override void OnMatchCreate (bool success, string extendedInfo, MatchInfo matchInfo) {
		if (!success) {
			this.Log ("[Server] Match Maker Error");
			return;
		}
		if (this.CurrentState != NetworkState.RemoteHost) {
			this.CurrentState = NetworkState.RemoteHost;
			this.CurrentHostState = HostState.Starting;
		}
		if (this.Active ())
			this.Stop ();
		this.networkPort = this.RemotePort;
		NetworkClient hostClient = this.StartHost (matchInfo);
		if (hostClient == null) {
			this.HostFail ();
			return;
		}
		this.Log ("[Server] Listed in Match Maker");
	}

	// When a list of polled matches is received:
	public override void OnMatchList (bool success, string extendedInfo, List<MatchInfoSnapshot> matches) {
		this.matches = matches;
		this.PollingMatchList = false;
	}

	// When a match is joined:
	public override void OnMatchJoined (bool success, string extendedInfo, MatchInfo matchInfo) {
		if (!success) {
			this.Log ("[Client] Match Maker Error");
			return;
		}
		if (this.CurrentState != NetworkState.RemoteJoin) {
			this.CurrentState = NetworkState.RemoteJoin;
			this.CurrentJoinState = JoinState.Connecting;
		}
		if (this.Active ())
			this.Stop ();
		this.networkPort = this.RemotePort;
		NetworkClient joinClient = this.StartClient (matchInfo);
		if (joinClient == null) {
			this.JoinFail ();
			return;
		}
		this.Log ("[Client] Joined server: " + matchInfo.address + " using Match Maker.");
	}


	// ========== Network Checks ==========
	/** Server Side - Returns true if things should run from the server side. **/
	public bool ServerSide () {
		if (!NetworkServer.active)
			return false;
		return this.CurrentState == NetworkState.LocalHost || this.CurrentState == NetworkState.RemoteHost;
	}

	/** Client Side - Returns true if things should run from the client side. **/
	public bool ClientSide () {
		if (!NetworkClient.active)
			return false;
		return !this.ServerSide ();
	}

	/** Active - Returns true when hosting or joined as a client for both local and remote clients, when false client management should be suspended. **/
	public bool Active () {
		return NetworkServer.active || NetworkClient.active;
	}

	/** Local - Returns true if the server is in a LocalHost or LocalJoin state **/
	public bool Local () {
		return this.CurrentState == NetworkState.LocalHost || this.CurrentState == NetworkState.LocalJoin;
	}


	// ========== Get Network Properties ==========
	public string GetMatchName () {
		return this.matchName;
	}

	public string GetAddress () {
		return this.networkAddress;
	}

	public int GetPort () {
		return this.RemotePort;
	}

	public int GetMaxConnections () {
		return this.maxConnections;
	}

	public string GetPassword () {
		return this.Password;
	}


	// ========== Set Network Properties ==========
	public void SetMatchName (string name) {
		this.matchName = name;
	}

	public void SetAddress (string address) {
		this.networkAddress = address;
	}

	public void SetPort (int port) {
		this.RemotePort = port;
	}

	public void SetMaxConnections (int max) {
		this.maxConnections = max;
	}

	public void SetPassword (string password) {
		this.Password = password;
	}


	// ========== Spawn With Client Authority ==========
	public void SpawnWithClientAuthority (GameObject gameObject, NetworkConnection conn) {
		// If Ready:
		if (conn.isReady) {
			NetworkServer.SpawnWithClientAuthority (gameObject, conn);
			return;
		}

		// Not Ready:
		if (!this.ClientAuthorityObjectQueue.ContainsKey (conn))
			this.ClientAuthorityObjectQueue.Add (conn, new List<GameObject> ());
		this.ClientAuthorityObjectQueue[conn].Add (gameObject);
	}


	// ========== Test Log ==========
	public void Log (object message) {
		if (!this.TestMessages)
			return;
		Debug.Log (message);
	}
}
