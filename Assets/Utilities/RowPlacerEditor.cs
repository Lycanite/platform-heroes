#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (RowPlacer))]
public class RowPlacerEditor : Editor {

	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		RowPlacer placer = (RowPlacer)target;
		if (GUILayout.Button ("Update Placement"))
			placer.UpdatePlacement ();
	}
}
#endif
