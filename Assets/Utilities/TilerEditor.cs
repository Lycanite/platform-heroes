#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof (Tiler))]
public class TilerEditor : Editor {

	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		Tiler tiler = (Tiler)target;
		if (GUILayout.Button ("Update Tiles"))
			tiler.UpdateTiles ();
	}
}
#endif
