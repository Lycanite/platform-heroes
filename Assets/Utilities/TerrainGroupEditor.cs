#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TerrainGroup))]
public class TerrainGroupEditor : Editor {
	
	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		TerrainGroup terrains = (TerrainGroup)target;

		// General:
		EditorGUILayout.BeginVertical ("HelpBox");
			GUILayout.Label ("Management", "BoldLabel");
			if (GUILayout.Button ("Load Terrain Objects"))
				terrains.LoadTerrains ();
			if (GUILayout.Button ("Fix Non Saving Terrains"))
				terrains.FixNonSavingTerrains ();
			if (GUILayout.Button ("Set Terrain Material"))
				terrains.SetTerrainMaterial ();
		EditorGUILayout.EndVertical ();

		// Template:
		EditorGUILayout.BeginVertical ("HelpBox");
			GUILayout.Label ("Template", "BoldLabel");
			if (GUILayout.Button ("Copy All Prototypes"))
				terrains.CopyPrototypesFromTemplate (true, true, true);
			if (GUILayout.Button ("Copy Textures"))
				terrains.CopyPrototypesFromTemplate (true, false, false);
			if (GUILayout.Button ("Copy Trees"))
				terrains.CopyPrototypesFromTemplate (false, true, false);
			if (GUILayout.Button ("Copy Details"))
				terrains.CopyPrototypesFromTemplate (false, false, true);
		EditorGUILayout.EndVertical ();

		// Connecting:
		EditorGUILayout.BeginVertical ("HelpBox");
			GUILayout.Label ("Connections", "BoldLabel");
			if (GUILayout.Button ("Slope Stitch Terrains"))
				terrains.SlopeAllTerrains ();
			if (GUILayout.Button ("Hard Stitch Terrains"))
				terrains.StitchAllTerrains ();
		EditorGUILayout.EndVertical ();

		// Generation:
		EditorGUILayout.BeginVertical ("HelpBox");
			GUILayout.Label ("Generation", "BoldLabel");
			if (GUILayout.Button ("Generate New Terrain"))
				terrains.GenerateTerrain ();
		EditorGUILayout.EndVertical ();
	}
}
#endif
