#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (CreatureAINavigateSetup))]
public class CreatureAINavigateSetupEditor : Editor {

	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		CreatureAINavigateSetup aiSetup = (CreatureAINavigateSetup)this.target;

		// Nodes:
		EditorGUILayout.BeginVertical ("HelpBox");
		GUILayout.Label ("Nodes", "BoldLabel");
		if (GUILayout.Button ("Update Nodes")) {
			aiSetup.AddChildNodes ();
		}
		EditorGUILayout.EndVertical ();
		
		EditorGUILayout.BeginVertical ("HelpBox");
		GUILayout.Label ("AI", "BoldLabel");
		string buttonText = "Add Creature AI Navigate";
		if (aiSetup.AI != null)
			buttonText = "Update Creature AI Navigate";
		if (GUILayout.Button (buttonText)) {
			aiSetup.AddAI ();
		}
		if (GUILayout.Button ("Remove AI Navigate")) {
			if (aiSetup.AI != null) {
				aiSetup.RemoveAI ();
			}
			DestroyImmediate (aiSetup);
		}
		EditorGUILayout.EndVertical ();
	}
}
#endif
