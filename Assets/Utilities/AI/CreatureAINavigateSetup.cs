using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (AIPackage))]
public class CreatureAINavigateSetup : MonoBehaviour {
	public CreatureAINavigate AI;
	public string Name = "Navigate";
	public int Priority = 1;
	public int Bitmask = CreatureAI.BitmaskMovement;

	public GameObject NodesParent; // Used by the editor for auto adding nodes that are a child to this object.
	public List<GameObject> Nodes = new List<GameObject> ();

	public bool StartAtClosestNode = true;
	public bool Loop = false;
	public bool StartInReverse = false;

	// ========== Add AI ==========
	/** Adds AI to the AI Package. **/
	public void AddAI () {
		if (this.AI == null) {
			this.AI = ScriptableObject.CreateInstance<CreatureAINavigate> ();
			this.GetComponent<AIPackage> ().AddAI (this.AI);
		}
		this.AI.Init (null, this.Name, this.Priority, this.Bitmask);
		this.AI.Nodes = this.Nodes;
		this.AI.StartAtClosestNode = this.StartAtClosestNode;
		this.AI.Loop = this.Loop;
		this.AI.StartInReverse = this.StartInReverse;
	}

	// ========== Remove AI ==========
	/** Removes AI from the AI Package. **/
	public void RemoveAI () {
		if (this.AI != null)
			this.GetComponent<AIPackage> ().RemoveAI (this.AI);
	}

	// ========== Add Child Nodes ==========
	/** Adds all child objects as nodes. **/
	public void AddChildNodes () {
		this.Nodes = new List<GameObject> ();
		Transform parent = this.transform;
		if (this.NodesParent != null)
			parent = NodesParent.transform;
		foreach (Transform child in parent) {
			this.Nodes.Add (child.gameObject);
		}
		if (this.AI != null)
			this.AI.Nodes = this.Nodes;
	}
}
