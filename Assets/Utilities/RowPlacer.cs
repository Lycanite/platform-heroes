#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

public class RowPlacer : MonoBehaviour {
	public GameObject PlacePrefab;
	public GameObject PlaceParent;

	public int PlaceAmount = 5;
	public float PlaceLength = 10;
	public Vector3 PlaceDirection = new Vector3 (1, 0, 0);
	public AnimationCurve PlaceCurve;
	public float CurveSize = 5;
	public Vector3 CurveDirection = new Vector3 (0, 1, 0);


	// ========== Update Placement ==========
	/** Place all of the selected objects. This will remove any existing ones. **/
	public void UpdatePlacement () {
		// Set object parent:
		GameObject parent = this.gameObject;
		if (this.PlaceParent != null)
			parent = this.PlaceParent;

		// Remove existing objects:
		var oldObjects = new List<GameObject> ();
		foreach (Transform child in parent.transform) {
			oldObjects.Add (child.gameObject);
		}
		foreach (GameObject oldGameObject in oldObjects) {
			DestroyImmediate (oldGameObject);
		}

		// Create objects along the row:
		for (int i = 0; i < this.PlaceAmount; i++) {
			float progress = (float)i / (this.PlaceAmount - 1);
			GameObject gameObject = GameObject.Instantiate (this.PlacePrefab);
			gameObject.transform.parent = parent.transform;
			gameObject.transform.localPosition = this.PlaceDirection * (progress * this.PlaceLength);
			if (this.PlaceCurve != null)
				gameObject.transform.localPosition += this.CurveDirection * (this.PlaceCurve.Evaluate (progress) * this.CurveSize);
		}
	}
}
#endif
