#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class Tiler : MonoBehaviour {
	public GameObject TiledPrefab;
	public GameObject TileParent;

	public Vector3 Tiling = new Vector3 (1, 1, 1);
	public Vector3 Spacing;
	public Vector3 Offset;
	public Vector3 Centering;

	public Tiler[] SlaveTilers;
	public bool UseMasterLayout = false;
	public Vector3 ScaleMasterTiling = new Vector3 (1, 1, 1);

	// ========== Update Tiles ==========
	/** Regenerates all the tiled prefabs. **/
	public void UpdateTiles () {
		// Set tile parent:
		GameObject tileParent = this.gameObject;
		if (this.TileParent != null)
			tileParent = this.TileParent;

		// Remove existing tiles:
		var oldTiles = new List<GameObject> ();
		foreach (Transform child in tileParent.transform) {
			oldTiles.Add (child.gameObject);
		}
		foreach (GameObject oldTile in oldTiles) {
			DestroyImmediate (oldTile);
		}

		// Get offset:
		Vector3 offset = this.Offset;
		offset.x += this.Centering.x * -this.Spacing.x * (this.Tiling.x / 2);
		offset.y += this.Centering.y * -this.Spacing.y * (this.Tiling.y / 2);
		offset.z += this.Centering.z * -this.Spacing.z * (this.Tiling.z / 2);

		// Create tiles:
		int maxLimit = 1000;
		int i = 0;
		for (int x = 0; x < this.Tiling.x; x++) {
			for (int y = 0; y < this.Tiling.y; y++) {
				for (int z = 0; z < this.Tiling.z; z++) {
					if (i++ >= maxLimit)
						break;
					Object newObjectInstance = PrefabUtility.InstantiatePrefab (this.TiledPrefab);
					if (newObjectInstance is GameObject) {
						GameObject gameObject = (GameObject)newObjectInstance;
						gameObject.transform.parent = tileParent.transform;
						gameObject.transform.localPosition = new Vector3 (offset.x + (this.Spacing.x * x), offset.y + (this.Spacing.y * y), offset.z + (this.Spacing.z * z));
						gameObject.transform.name = gameObject.transform.name + "X" + x + "Y" + y + "Z" + z;
					}
				}
			}
		}

		// Start tiling on slave Tilers:
		if (this.SlaveTilers != null)
			foreach (Tiler slaveTiler in this.SlaveTilers) {
				if (slaveTiler.UseMasterLayout) {
					if (slaveTiler.ScaleMasterTiling.x != 0f)
						slaveTiler.Tiling.x = this.Tiling.x * slaveTiler.ScaleMasterTiling.x;
					if (slaveTiler.ScaleMasterTiling.y != 0f)
						slaveTiler.Tiling.y = this.Tiling.y * slaveTiler.ScaleMasterTiling.y;
					if (slaveTiler.ScaleMasterTiling.z != 0f)
						slaveTiler.Tiling.z = this.Tiling.z * slaveTiler.ScaleMasterTiling.z;
					slaveTiler.Spacing = this.Spacing;
					slaveTiler.Offset = this.Offset;
					slaveTiler.Centering = this.Centering;
				}
				slaveTiler.UpdateTiles ();
			}
	}
}
#endif
