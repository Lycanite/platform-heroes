#if UNITY_EDITOR
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[ExecuteInEditMode]
public class TerrainGroup : MonoBehaviour {

	// ========== Terrains ==========
	public Terrain[] terrains;
	public Terrain TemplateTerrain;
	public TerrainData TemplateTerrainData;
	public string GroupName = "G01";
	public int SeamWidth = 5;
	public int TerrainSize = 100;
	public Material TerrainMaterial;
	public GameObject TerrainPrefab;
	public string TerrainAssetPath = "Assets/Terrain";


	// ========== Get Template Terrain Data ==========
	/** Returns the template terrain data to use, if TemplateTerrain is set TemplateTerrainData will be set to its terrain data. **/
	public TerrainData GetTemplateTerrainData () {
		if (this.TemplateTerrain != null)
			this.TemplateTerrainData = this.TemplateTerrain.terrainData;
		return this.TemplateTerrainData;
	}


	// ========== Generate Terrain ==========
	/** Generates a new terrain based on the last terrain in the terrains array. **/
	public void GenerateTerrain () {
		this.LoadTerrains ();
		Terrain lastTerrain = this.terrains[this.terrains.Length - 1];
		Object newTerrainInstance = PrefabUtility.InstantiatePrefab (this.TerrainPrefab);
		Terrain newTerrain = ((GameObject)newTerrainInstance).GetComponent<Terrain> ();

		// Position New Terrain:
		newTerrain.transform.parent = lastTerrain.transform.parent;
		newTerrain.transform.localPosition = lastTerrain.transform.localPosition + new Vector3 (this.TerrainSize, 0, 0);

		// Label New Terrain:
		string lastNumber = "";
		string newNumber = "";
		if (this.terrains.Length + 1 < 10) { // Terrain names from 01-09
			lastNumber += "0";
			newNumber += "0";
		}
		lastNumber += this.terrains.Length;
		newNumber += (this.terrains.Length + 1);
		string newName = "Terrain" + newNumber;
		newTerrain.transform.name = newName;

		// Copy Last/Template Terrain Asset:
		string sceneName = EditorSceneManager.GetActiveScene ().name;
		string sceneNameBase = Regex.Replace (sceneName, @"[\d-]", string.Empty);
		string assetPath = this.TerrainAssetPath + "/" + sceneNameBase;
		string lastAssetPath = assetPath + "/" + sceneName + this.GroupName + "T" + lastNumber + ".asset";
		if (this.GetTemplateTerrainData () != null)
			lastAssetPath = AssetDatabase.GetAssetPath (this.GetTemplateTerrainData ());
		string newAssetPath = assetPath + "/" + sceneName + this.GroupName + "T" + newNumber + ".asset";
		AssetDatabase.CopyAsset (lastAssetPath, newAssetPath);

		// Assign Copied Terrain Asset To New:
		TerrainData terrainData = AssetDatabase.LoadAssetAtPath<TerrainData> (newAssetPath);
		newTerrain.terrainData = terrainData;
		newTerrain.materialType = Terrain.MaterialType.Custom;
		newTerrain.materialTemplate = this.TerrainMaterial;
		newTerrain.GetComponent<TerrainCollider> ().terrainData = terrainData;
		PrefabUtility.RecordPrefabInstancePropertyModifications (newTerrain);

		// Reload Terrains:
		this.LoadTerrains ();
	}


	// ========== Set Terrain Material ==========
	/** Applies the selected material to all terrains, if null the Unity default is used. **/
	public void SetTerrainMaterial () {
		this.LoadTerrains ();
		foreach (Terrain terrain in this.terrains) {
			if (this.TerrainMaterial != null) {
				terrain.materialType = Terrain.MaterialType.Custom;
				terrain.materialTemplate = this.TerrainMaterial;
			}
			else {
				terrain.materialType = Terrain.MaterialType.BuiltInStandard;
			}
		}
	}


	// ========== Load Terrains ==========
	/** Loads all children of this Terrains object with Terrain components into the terrains array. **/
	public void LoadTerrains (bool updateNeighbors = true) {
		this.terrains = this.gameObject.GetComponentsInChildren<Terrain> ();
		if (updateNeighbors)
			this.UpdateNeighbors ();
	}


	// ========== Update Neighbors ==========
	/** Sets the neighbors for each terrain. **/
	public void UpdateNeighbors () {
		for (int i = 0; i < terrains.Length; i++) {
			Terrain left = null;
			if (i - 1 > 0)
				left = terrains[i - 1];
			Terrain right = null;
			if (i + 1 < terrains.Length)
				right = terrains[i + 1];
			terrains[i].SetNeighbors (left, null, right, null);
		}
	}


	// ========== Fix Non Saving Terrains ==========
	/** Fixes terrains that wont save their assigned terrain data. **/
	public void FixNonSavingTerrains () {
		this.LoadTerrains ();
		foreach (Terrain terrain in this.terrains) {
			PrefabUtility.RecordPrefabInstancePropertyModifications (terrain);
		}
	}


	// ========== Copy Textures From Template ==========
	/** Copies all the texture, foliage and tree data from the template terrain to all listed terrains. **/
	public void CopyPrototypesFromTemplate (bool copyTextures, bool copyTrees, bool copyDetails) {
		if (this.GetTemplateTerrainData () == null) {
			Debug.LogError ("You have not set a Terrain Template. Please use either a terrain object from the heirarchy, a terrain prefab or set the terrain data directly.");
			return;
		}
		foreach (Terrain terrain in this.terrains) {
			if (copyTextures)
				terrain.terrainData.splatPrototypes = this.TemplateTerrain.terrainData.splatPrototypes;
			if (copyTrees)
				terrain.terrainData.treePrototypes = this.TemplateTerrain.terrainData.treePrototypes;
			if (copyDetails)
				terrain.terrainData.detailPrototypes = this.TemplateTerrain.terrainData.detailPrototypes;
		}
	}


	// ========== Stitch Terrains ==========
	/** Stitches all terrains added to the main terrain array together. This currently only works from left to right. **/
	public void StitchAllTerrains() {
		for (int i = 0; i < terrains.Length; i++) {
			if (i + 1 < terrains.Length)
				this.StitchTerrains (this.terrains [i], this.terrains [i + 1]);
		}
	}

	/** Stitches the two provided terrains together. **/
	public void StitchTerrains(Terrain terrainA, Terrain terrainB) {
		if (this.SeamWidth <= 0)
			this.SeamWidth = 1;

		if (terrainA.terrainData.heightmapWidth != terrainB.terrainData.heightmapWidth) {
			Debug.LogError ("Both Terrains must have matching resolutions.");
			return;
		}

		int resolution = terrainA.terrainData.heightmapResolution;
		float[,] heightsA = terrainA.terrainData.GetHeights(0, 0, resolution, resolution);
		float[,] heightsB = terrainB.terrainData.GetHeights(0, 0, resolution, resolution);

		for (int i = 0; i < resolution; i++) {
			for (int j = 0; j < this.SeamWidth; j++) {
				
				float differenceA = (heightsB[i, j] - heightsA[i, resolution - 1 - j]) / 2;
				float differenceB = differenceA;

				if (j > 0) {
					differenceA = (heightsA [i, resolution - 1 - j + 1] - heightsA [i, resolution - 1 - j]) / 2;
				}

				if (j > 0) {
					differenceB = (heightsB [i, j] - heightsB [i, j - 1]) / 2;
				}

				heightsA[i, resolution - 1 - j] += differenceA;
				heightsB[i, j] -= differenceB;
			}
		}

		terrainA.terrainData.SetHeights(0, 0, heightsA);
		terrainB.terrainData.SetHeights(0, 0, heightsB);
	}


	// ========== Slope Terrains ==========
	/** Stitches all terrains added to the main terrain array together creating an average slope, this may alter the landscape a bit. This currently only works from left to right. **/
	public void SlopeAllTerrains() {
		for (int i = 0; i < terrains.Length; i++) {
			if (i + 1 < terrains.Length)
				this.SlopeTerrains (this.terrains [i], this.terrains [i + 1]);
		}
	}

	/** Stitches the two provided terrains together. **/
	public void SlopeTerrains(Terrain terrainA, Terrain terrainB) {
		if (this.SeamWidth <= 0)
			this.SeamWidth = 1;

		if (terrainA.terrainData.heightmapWidth != terrainB.terrainData.heightmapWidth) {
			Debug.LogError ("Both Terrains must have matching resolutions.");
			return;
		}

		int resolution = terrainA.terrainData.heightmapResolution;
		float[,] heightsA = terrainA.terrainData.GetHeights(0, 0, resolution, resolution);
		float[,] heightsB = terrainB.terrainData.GetHeights(0, 0, resolution, resolution);

		for (int i = 0; i < resolution; i++) {
			for (int j = 0; j < this.SeamWidth; j++) {
				float strengthB = (float)j / this.SeamWidth;
				float strengthA = 1 - strengthB;

				float differenceA = (heightsB[i, 0] - heightsA[i, resolution - 1]);
				float differenceB = differenceA;

				if (j > 0) {
					differenceA = (heightsA [i, resolution - 1 - j + 1] - heightsA [i, resolution - 1 - j]);
					differenceB = (heightsB [i, j] - heightsB [i, j - 1]);
				}

				heightsA[i, resolution - 1 - j] += differenceA * strengthA;
				heightsB[i, j] -= differenceB * strengthB;
			}
		}

		terrainA.terrainData.SetHeights(0, 0, heightsA);
		terrainB.terrainData.SetHeights(0, 0, heightsB);
	}
}
#endif
