using UnityEngine;

public class PlayerInfoLocal : MonoBehaviour {
	// Info:
	public int ID = 0;
	public string PlayerName = "Player 1";
	public string Character;

	// Input:
	public int GamePadID = -1;
	public bool MouseKeyboard = true;


	// ========== Start ==========
	public void Start () {
		this.transform.SetParent (ClientManager.Get ().transform);
	}

	
	// ========== Copy From Player Info ==========
	/** Copies all local properties from the provided Player Info, this can then be used when changing hosts or newly hosting. **/
	public void CopyFromPlayerInfo (PlayerInfo playerInfo) {
		this.ID = playerInfo.ID;
		this.PlayerName = playerInfo.PlayerName;
		this.Character = playerInfo.Character;

		this.GamePadID = playerInfo.GamePadID;
		this.MouseKeyboard = playerInfo.MouseKeyboard;
	}


	// ========== Apply To Player Info ==========
	/** Sends all local properties to the provided Player Info, this can then be used when newly hosting or joining. **/
	public void ApplyToPlayerInfo (PlayerInfo playerInfo) {
		playerInfo.SetID (this.ID);
		playerInfo.SetPlayerName (this.PlayerName);
		playerInfo.SetPlayerCharacter (this.Character);

		playerInfo.GamePadID = this.GamePadID;
		playerInfo.MouseKeyboard = this.MouseKeyboard;
	}


	// ========== Create From Player Info ==========
	/** Populates and instantiates a new PlayerInfoLocal from the provided PlayerInfo. **/
	public static PlayerInfoLocal CreateFrom (PlayerInfo playerInfo) {
		GameObject playerInfoLocalObject = new GameObject ("PlayerInfoLocal");
		PlayerInfoLocal playerInfoLocal = playerInfoLocalObject.AddComponent<PlayerInfoLocal> ();
		playerInfoLocal.CopyFromPlayerInfo (playerInfo);
		return playerInfoLocal;
	}
}
