using UnityEngine;
using UnityEngine.Networking;

public class PlayerInfo : NetworkBehaviour {
	[SyncVar] private uint PlayerManagerNetID;
	public PlayerManager PlayerManager;
	public PlayerStats CurrentPlayerStats;

	// Info:
	[SyncVar] public int ID = 0;
	[SyncVar] public string PlayerName = "Player 1";
	[SyncVar] public string Character = "lanky";
	[SyncVar] public int Team = 0;

	// Input:
	[SyncVar] public int GamePadID = -1;
	public bool MouseKeyboard = false;


	// ========== Start ==========
	public void Start () {
		// Server:
		if (NetworkManager.Get ().ServerSide ()) {
			this.PlayerManagerNetID = this.PlayerManager.GetComponent <NetworkIdentity> ().netId.Value;
			if (this.PlayerManager.IsLocal ())
				NetworkServer.Spawn (this.gameObject);
			else
				NetworkServer.SpawnWithClientAuthority (this.gameObject, this.PlayerManager.Connection);
		}

		// Client:
		else {
			GameObject playerManagerObject = ClientScene.FindLocalObject (new NetworkInstanceId (this.PlayerManagerNetID));
			this.PlayerManager = playerManagerObject.GetComponent<PlayerManager> ();
			this.PlayerManager.AddPlayerInfo (this);
		}

		this.transform.SetParent (this.PlayerManager.transform, false);
		this.MouseKeyboard = this.ID == 0;
		ClientManager.Get ().Version++;
	}

	
	// ========== Is Local ==========
	/** Is Local - Returns true if this Player Info belongs to this client/host. **/
	public bool IsLocal () {
		return this.PlayerManager != null && this.PlayerManager.IsLocal ();
	}


	// ========== Get Character prefab ==========
	public GameObject GetCharacterPrefab () {
		return Global.Get ().GetCharacter (this.Character);
	}


	// ========== Set ID ==========
	public void SetID (int id) {
		if (NetworkManager.Get ().ServerSide ()) {
			this.ID = id;
			return;
		}
		this.CmdSetID (id);
	}

	[Command]
	public void CmdSetID (int id) {
		this.ID = id;
	}


	// ========== Set Player Name ==========
	public void SetPlayerName (string playerName) {
		if (NetworkManager.Get ().ServerSide ()) {
			this.PlayerName = playerName;
			return;
		}
		this.CmdSetPlayerName (playerName);
	}

	[Command]
	public void CmdSetPlayerName (string playerName) {
		this.PlayerName = playerName;
	}


	// ========== Set Player Character ==========
	public void SetPlayerCharacter (string character) {
		if (NetworkManager.Get ().ServerSide ()) {
			this.Character = character;
			return;
		}
		this.CmdSetPlayerCharacter (character);
	}

	[Command]
	public void CmdSetPlayerCharacter (string character) {
		this.Character = character;
	}


	// ========== Set Player Team ==========
	public void SetPlayerTeam (int team) {
		if (NetworkManager.Get ().ServerSide ()) {
			this.Team = team;
			return;
		}
		this.CmdSetPlayerTeam (team);
	}

	[Command]
	public void CmdSetPlayerTeam (int team) {
		this.Team = team;
	}


	// ========== Destroy ==========
	/** Destroys this PlayerInfo. **/
	public void Destroy () {
		if (this != null)
			GameObject.Destroy (this.gameObject);
	}
}
