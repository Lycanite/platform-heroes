﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerDisplays : MonoBehaviour {
	public int PlayerInfoVersion = 0;
	public GameObject PlayerDisplayPrefab;
	List<PlayerInfo> PlayerInfos = new List<PlayerInfo> ();
	List<GameObject> PlayerDisplayObjects = new List<GameObject> ();


	// ========== Update ==========
	public void Update () {
		// Instantiate Menu Player Displays:
		ClientManager clientManager = ClientManager.Get ();
		if (this.PlayerInfoVersion != clientManager.Version) {
			foreach (GameObject playerDisplayObject in this.PlayerDisplayObjects)
				GameObject.Destroy (playerDisplayObject);

			this.PlayerInfos = clientManager.GetAllPlayerInfos ();
			this.PlayerInfoVersion = clientManager.Version;

			int i = 0;
			foreach (PlayerInfo playerInfo in this.PlayerInfos) {
				GameObject playerDisplayObject = GameObject.Instantiate (this.PlayerDisplayPrefab);
				playerDisplayObject.transform.SetParent (this.transform, false);
				PlayerDisplay playerDisplay = playerDisplayObject.GetComponent<PlayerDisplay> ();
				playerDisplay.SetPlayer (playerInfo, i);
				this.PlayerDisplayObjects.Add (playerDisplayObject);
				i++;
			}
		}

		// Update Menu Player Displays Position:
		this.transform.localPosition = new Vector3 (-1.6f * (this.PlayerInfos.Count - 1), this.transform.localPosition.y, this.transform.localPosition.z);
	}
}
