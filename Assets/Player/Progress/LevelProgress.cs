
[System.Serializable]
public class LevelProgress {
	public bool Unlocked = false;

	public bool Completed = false;
	public bool Aced = false;
	public bool Crest = false;
	public int ScoreBest = 0;
	public float TimeBest = 0;

	public bool Loaded = false;

	// ========== Constructor ==========
	public LevelProgress (LevelInfo levelInfo) {
		this.Setup (levelInfo);
	}


	// ========== Setup ==========
	/* Sets up this progress based on the provided info. */
	public void Setup (LevelInfo levelInfo) {
		if (!this.Unlocked)
			this.Unlocked = levelInfo.UnlockedByDefault;

		this.Loaded = true;
	}
}
