using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelResults {
	public static string[] Ranks = new string[] { "Bronze", "Silver", "Gold", "Platinum" }; // A static list of rank names.

	// Components:
	LevelInfo LevelInfo; // The LevelInfo that this results applies to.

	// Results:
	public int Score = 0; // The total score of all players.
	public float Time = 0; // The time it took for all players to get to the level exit.
	public bool Aced = false; // True if no players died.
	public bool CrestCollected = false; // True if the crest was collected by a local player (not online remote players).
	public bool KeyCollected = false; // True if the key was collected by a local player (not online remote players).
	public bool SecretExit = false; // True if all players exited via the secret exit. Levels finished this way wont save Score, Time or Aced.

	// ========== Constructor ==========
	public LevelResults (LevelInfo levelInfo) {
		this.LevelInfo = levelInfo;
	}

	
	// ========== From Level Stats ==========
	/* Populates results from the provided LevelInfo. */
	public void FromLevelStats (LevelStats levelStats) {
		this.Score = 0;
		this.SecretExit = this.LevelInfo.HasDoor;
		foreach (PlayerStats playerStats in levelStats.Winners) {
			this.Score += playerStats.Score;
			if (this.LevelInfo.HasDoor && !levelStats.SecretExits.Contains (playerStats))
				this.SecretExit = false;
		}
		this.Time = levelStats.CurrentTime;
		this.Aced = levelStats.Deaths <= 0;
		this.CrestCollected = levelStats.CrestCollected;
		this.KeyCollected = levelStats.KeyCollected && this.LevelInfo.HasKey;
	}


	// ========== Save ==========
	/* Updates ZoneProgress and LevelProgress using the results and saves player data. */
	public void Save () {
		// Get Progress:
		LevelProgress levelProgress = this.LevelInfo.GetProgress ();
		ZoneProgress zoneProgress = this.LevelInfo.ZoneInfo.GetProgress ();

		// Any Exit:
		levelProgress.Completed = true;
		if (this.CrestCollected)
			levelProgress.Crest = true;
		if (this.KeyCollected)
			zoneProgress.Key = true;
		if (this.SecretExit)
			zoneProgress.Door = true;

		// Normal Exit Only:
		if (!this.SecretExit) {
			levelProgress.ScoreBest = Mathf.Max (levelProgress.ScoreBest, this.Score);
			if (levelProgress.TimeBest <= 0)
				levelProgress.TimeBest = this.Time;
			else
				levelProgress.TimeBest = Mathf.Min (levelProgress.TimeBest, this.Time);
			if (this.Aced)
				levelProgress.Aced = true;
		}

		// Update and Save:
		zoneProgress.ReloadLevels ();
		ClientManager.Get ().GetLocalPlayerManager ().PlayerProgress.SaveAll ();
	}
}
