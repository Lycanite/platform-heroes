
using System.Collections.Generic;

[System.Serializable]
public class CharacterProgress {
	public bool Unlocked = false;
	public List<string> SkinsUnlocked = new List<string> ();

	public bool Loaded = false; // Set to true if already setup and taking updates only.

	// ========== Constructor ==========
	public CharacterProgress (CharacterInfo characterInfo) {
		this.Setup (characterInfo);
	}


	// ========== Setup ==========
	/* Sets up this progress based on the provided info. */
	public void Setup (CharacterInfo characterInfo) {
		// Already Loaded:
		if (this.Loaded) {
			if (!this.Unlocked)
				this.Unlocked = characterInfo.UnlockedByDefault;
			return;
		}
	}
}
