#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (PlayerProgress))]
public class PlayerProgressEditor : Editor {

	// ========== On Inspector GUI ==========
	public override void OnInspectorGUI () {
		this.DrawDefaultInspector ();
		PlayerProgress playerProgress = (PlayerProgress)this.target;
		if (GUILayout.Button ("Save Player Data"))
			playerProgress.SaveAll ();
		if (GUILayout.Button ("Clear Player Data"))
			playerProgress.ClearData ();
	}
}
#endif
