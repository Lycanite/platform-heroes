using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class PlayerProgress : NetworkBehaviour {
	public Dictionary<string, ZoneProgress> ZoneProgress;
	public Dictionary<string, CharacterProgress> CharacterProgress;

	public List<ZoneProgress> ZoneProgressEntries;
	public List<CharacterProgress> CharacterProgressEntries;

	// ========== Start ==========
	public void Start () {
		// Load Zone and Level Progress:
		this.ZoneProgress = this.Load<Dictionary<string, ZoneProgress>> ("ZoneProgress.sav");
		this.ZoneProgressEntries = new List<ZoneProgress> ();
		if (this.ZoneProgress == null)
			this.ZoneProgress = new Dictionary<string, ZoneProgress> ();
		ZoneManager zoneManager = ZoneManager.Get ();
		if (zoneManager != null) {
			foreach (KeyValuePair<string, ZoneInfo> entry in zoneManager.Zones) {
				// Loaded Entry:
				if (this.ZoneProgress.ContainsKey (entry.Key))
					this.ZoneProgress[entry.Key].Setup (entry.Value);
				// New Entry:
				else
					this.ZoneProgress.Add (entry.Key, new ZoneProgress (entry.Value));
				// Editor Reference:
				this.ZoneProgressEntries.Add (this.ZoneProgress[entry.Key]);
			}
		}
		this.Save<Dictionary<string, ZoneProgress>> ("ZoneProgress.sav", this.ZoneProgress);

		// Setup Character Progress:
		this.CharacterProgress = this.Load<Dictionary<string, CharacterProgress>> ("CharacterProgress.sav");
		this.CharacterProgressEntries = new List<CharacterProgress> ();
		if (this.CharacterProgress == null)
			this.CharacterProgress = new Dictionary<string, CharacterProgress> ();
		CharacterManager characterManager = CharacterManager.Get ();
		if (characterManager != null) {
			foreach (KeyValuePair<string, CharacterInfo> entry in characterManager.Characters) {
				// Loaded Entry:
				if (this.CharacterProgress.ContainsKey (entry.Key))
					this.CharacterProgress[entry.Key].Setup (entry.Value);
				// New Entry:
				else
					this.CharacterProgress.Add (entry.Key, new CharacterProgress (entry.Value));
				// Editor Reference:
				this.CharacterProgressEntries.Add (this.CharacterProgress[entry.Key]);
			}
		}
		this.Save<Dictionary<string, CharacterProgress>> ("CharacterProgress.sav", this.CharacterProgress);
	}


	// ========== Load ==========
	public T Load<T> (string filename) {
		if (!File.Exists (Path.Combine (Application.persistentDataPath, filename)))
			return default(T);
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Open (Path.Combine (Application.persistentDataPath, filename), FileMode.Open);
		T loadedProgress = (T)bf.Deserialize (file);
		file.Close ();
		return loadedProgress;
	}


	// ========== Save ==========
	public void Save<T> (string filename, T data) {
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Path.Combine (Application.persistentDataPath, filename));
		bf.Serialize (file, data);
		file.Close ();
	}


	// ========== Delete ==========
	public void Delete (string filename) {
		if (!File.Exists (Path.Combine (Application.persistentDataPath, filename)))
			return;
		File.Delete (Path.Combine (Application.persistentDataPath, filename));
	}


	// ========== Save All ==========
	public void SaveAll () {
		this.Save<Dictionary<string, ZoneProgress>> ("ZoneProgress.sav", this.ZoneProgress);
		this.Save<Dictionary<string, CharacterProgress>> ("CharacterProgress.sav", this.CharacterProgress);
	}


	// ========== Clear Data ==========
	public void ClearData () {
		this.Delete ("ZoneProgress.sav");
		this.Delete ("CharacterProgress.sav");
		this.Start ();
	}


	// ========== Get Zone Progress ==========
	public ZoneProgress GetZoneProgress (string name) {
		if (!this.ZoneProgress.ContainsKey (name))
			return null;
		return this.ZoneProgress[name];
	}
}
