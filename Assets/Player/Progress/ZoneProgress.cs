using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ZoneProgress {
	public Dictionary<int, LevelProgress> LevelProgress = new Dictionary<int, LevelProgress> ();
	public List<LevelProgress> LevelProgressEntries;

	public bool Unlocked = false;

	public bool Key = false;
	public bool Door = false;

	public int Complete = 0; // A cached count of how many levels are completed in this zone.
	public int Crests = 0; // A cached count of how many crests are collected in this zone.

	public bool Loaded = false; // Set to true if already setup and taking updates only.

	// ========== Constructor ==========
	public ZoneProgress (ZoneInfo zoneInfo) {
		this.Setup (zoneInfo);
	}


	// ========== Setup ==========
	/* Sets up this progress based on the provided info. */
	public void Setup (ZoneInfo zoneInfo) {
		if (!this.Unlocked)
			this.Unlocked = zoneInfo.UnlockedByDefault;

		this.LevelProgressEntries = new List<LevelProgress> ();
		foreach (KeyValuePair<int, LevelInfo> entry in zoneInfo.Levels) {
			// Loaded Entry:
			if (this.LevelProgress.ContainsKey (entry.Key))
				this.LevelProgress[entry.Key].Setup (entry.Value);
			// New Entry:
			else
				this.LevelProgress.Add (entry.Key, new LevelProgress (entry.Value));
			// Editor Reference:
			this.LevelProgressEntries.Add (this.LevelProgress[entry.Key]);
		}

		this.Loaded = true;
		this.ReloadLevels ();
	}


	// ========== Reload Levels ==========
	/* Iterates through each level to cache the number of completed levels and crests collected and unlocks levels/zones that need to be unlocked. */
	public void ReloadLevels () {
		// Count Completed Levels, Collected Crests and Unlock Levels:
		this.Complete = 0;
		this.Crests = 0;
		bool lastLevelCompleted = false;
		foreach (LevelProgress levelProgress in this.LevelProgress.Values) {
			if (lastLevelCompleted)
				levelProgress.Unlocked = true;
			lastLevelCompleted = levelProgress.Completed;
			if (levelProgress.Completed)
				this.Complete++;
			if (levelProgress.Crest)
				this.Crests++;
		}

		// Unlock Next Zone:
		if (this.Complete >= 3) {
			bool foundSelf = false;
			foreach (ZoneProgress zone in ClientManager.Get ().GetLocalPlayerManager ().PlayerProgress.ZoneProgress.Values) {
				if (foundSelf) {
					zone.Unlocked = true;
					break;
				}
				if (zone == this)
					foundSelf = true;
			}

		}
	}
}
