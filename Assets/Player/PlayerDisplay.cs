﻿using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class PlayerDisplay : MonoBehaviour {
	public PlayerInfo PlayerInfo;
	public GameObject PlayerObject;
	public SpawnPointBase SpawnPoint;
	public string LastCharacter;

	public float YSpeed = 2;
	public float TargetHeight = 0;
	public float MenuHeight = 0;
	public float CharacterSelectionHeight = 5;

	private bool FirstSpawn = true;


	// ========== Update ==========
	public void Update () {
		if (this.PlayerInfo == null)
			GameObject.Destroy (this.gameObject);

		// Character Change:
		if (this.PlayerInfo.Character != this.LastCharacter)
			this.SpawnPlayer ();

		// Update Player Object Position:
		if (this.PlayerObject != null) {
			CreatureBase creature = this.PlayerObject.GetComponent<CreatureBase> ();
			creature.transform.localScale = this.transform.localScale;
			creature.transform.localEulerAngles = new Vector3 (0, 0, 0);
			creature.Depth = this.transform.position.z;
			if (creature.gameObject.layer != this.gameObject.layer) {
				creature.gameObject.layer = this.gameObject.layer;
				for (int i = 0; i < creature.transform.childCount; i++) {
					GameObject childObject = creature.transform.GetChild (i).gameObject;
					childObject.layer = this.gameObject.layer;
				}
			}
		}

		// Change Height:
		this.TargetHeight = this.MenuHeight;
		if (LevelBase.Get ().Interface.CurrentMenuName == "characterselection")
			this.TargetHeight = this.CharacterSelectionHeight;
		float y = this.transform.localPosition.y;
		if (y != this.TargetHeight) {
			if (y > this.TargetHeight)
				y = Math.Max (y - this.YSpeed * Time.deltaTime, this.TargetHeight);
			if (y < this.TargetHeight)
				y = Math.Min (y + this.YSpeed * Time.deltaTime, this.TargetHeight);
			this.transform.localPosition = new Vector3 (this.transform.localPosition.x, y, this.transform.localPosition.z);
		}
	}


	// ========== Set Player ==========
	public void SetPlayer (PlayerInfo playerInfo, int order) {
		if (this.PlayerObject != null)
			GameObject.Destroy (this.PlayerObject);
		this.PlayerInfo = playerInfo;
		this.SpawnPlayer ();
		this.transform.localPosition = new Vector3 (3.2f * order, 0, 0);
	}


	// ========== Spawn Player ==========
	public void SpawnPlayer () {
		if (this.PlayerObject != null)
			GameObject.Destroy (this.PlayerObject);
		this.LastCharacter = this.PlayerInfo.Character;
		this.PlayerObject = GameObject.Instantiate (this.PlayerInfo.GetCharacterPrefab ());
		this.PlayerObject.transform.position = this.SpawnPoint.transform.position;
		this.PlayerObject.transform.SetParent (this.transform, true);
		CreaturePlayer creature = this.PlayerObject.GetComponent<CreaturePlayer> ();
		creature.BecomeSpawner = false;
		creature.FaceMovement = false;
		creature.Active = false;
		creature.LocalOnly = true;
		creature.PlayerInfo = this.PlayerInfo;

		// Play Sound:
		if (!this.FirstSpawn) {
			creature.PlaySound ("win");
		}

		this.FirstSpawn = false;
	}
}
