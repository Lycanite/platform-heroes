using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PlayerManager : NetworkBehaviour {

	// Progress:
	public PlayerProgress PlayerProgress; // Stores all progress for this local machine.
	
	// Players:
	public Dictionary<int, PlayerInfo> PlayerInfos = new Dictionary<int, PlayerInfo> (); // All players managed by this player manager.
	public GameObject PlayerInfoPrefab; // The prefab to use when creating a new Player Info.
	private int NextPlayerID = 0; // The ID of the next player to be managed.
	public string DefaultCharacter = "lanky";

	// Network:
	public NetworkConnection Connection; // The current network connection used by this player manager.
	[SyncVar] public int ConnectionID; // The connection ID that this player manager is using.
	private bool Local = false; // If true, this PlayerManager is the local player manager for managing players on the local machine.


	// ========== Start Authority ==========
	public override void OnStartAuthority () {
		this.Local = true;
	}

	
	// ========== Start ==========
	public void Start () {
		this.PlayerProgress = this.gameObject.AddComponent<PlayerProgress> ();

		// Server:
		if (NetworkManager.Get ().ServerSide ()) {
			if (this.IsLocal ())
				NetworkServer.Spawn (this.gameObject);
			else {
				NetworkServer.SpawnWithClientAuthority (this.gameObject, this.Connection);
			}
		}

		// Client:
		else {
			ClientManager.Get ().AddPlayerManager (this);
		}

		this.transform.SetParent (ClientManager.Get ().transform, false);
	}
	
	
	// ========== Update ==========
	public void Update () {
		
	}


	// ========== Create Player Info ==========
	/** Creates and instantiates a new PlayerInfo and adds it to the PlayerInfos. Will also copy from PlayerInfoLocal if provided. **/
	public void CreatePlayerInfo (int gamepadID = -1, PlayerInfoLocal playerInfoLocal = null) {
		string playerName = "";
		string character = this.DefaultCharacter;
		if (playerInfoLocal != null) {
			playerName = playerInfoLocal.PlayerName;
			character = playerInfoLocal.Character;
			gamepadID = playerInfoLocal.GamePadID;
		}
		if (NetworkManager.Get ().ServerSide ())
			this.DoCreatePlayerInfo (playerName, character, gamepadID);
		else
			this.CmdCreatePlayerInfo (playerName, character, gamepadID);
	}

	[Command]
	public void CmdCreatePlayerInfo (string playerName, string character, int gamepadID) {
		this.DoCreatePlayerInfo (playerName, character, gamepadID);
	}

	public void DoCreatePlayerInfo (string playerName, string character, int gamepadID) {
		GameObject playerInfoObject = GameObject.Instantiate (this.PlayerInfoPrefab);
		PlayerInfo playerInfo = playerInfoObject.GetComponent<PlayerInfo> ();
		playerInfo.PlayerManager = this;
		playerInfo.SetID (this.NextPlayerID++);
		playerInfo.PlayerName = "Player " + (playerInfo.ID + 1).ToString ();
		playerInfo.Character = character;
		playerInfo.GamePadID = gamepadID;
		this.AddPlayerInfo (playerInfo);
		if (playerInfo.GamePadID >= 0 && playerInfo.IsLocal ())
			ClientManager.Get ().ReserveGamepadID (playerInfo.GamePadID);
	}


	// ========== Add Player Info ==========
	public void AddPlayerInfo (PlayerInfo playerInfo) {
		this.PlayerInfos.Add (playerInfo.ID, playerInfo);
	}


	// ========== Remove Player Info ==========
	/** Removes an active PlayerInfo by ID. **/
	public void RemovePlayerInfo (int playerID) {
		if (playerID >= this.PlayerInfos.Count)
			return;
		PlayerInfo playerInfo = this.PlayerInfos [playerID];
		playerInfo.Destroy ();
		this.PlayerInfos.Remove (playerID);
		this.ReallocatePlayerIDs ();
	}


	// ========== Reallocate Player IDs ==========
	/** Typically called when the list of PlayerInfos is altered, this reassigns each player ID. **/
	public void ReallocatePlayerIDs () {
		this.NextPlayerID = 0;
		Dictionary<int, PlayerInfo> newPlayerInfos = new Dictionary<int, PlayerInfo> ();
		foreach (PlayerInfo playerInfo in this.PlayerInfos.Values) {
			playerInfo.SetID (this.NextPlayerID);
			playerInfo.PlayerName = "Player " + (playerInfo.ID + 1).ToString ();
			this.NextPlayerID++;
			newPlayerInfos.Add (playerInfo.ID, playerInfo);
		}
		this.PlayerInfos = newPlayerInfos;
	}


	// ========== First Player Check ==========
	/** Checks to make sure that there is at least 1 player, if not one is created. **/
	public void FirstPlayerCheck () {
		if (NetworkManager.Get ().ServerSide ())
			this.DoFirstPlayerCheck ();
		else
			this.CmdFirstPlayerCheck ();
	}

	[Command]
	public void CmdFirstPlayerCheck () {
		this.DoFirstPlayerCheck ();
	}

	public void DoFirstPlayerCheck () {
		if (this.PlayerInfos.Count >= 1)
			return;
		this.CreatePlayerInfo ();
	}


	// ========== Empty ==========
	/** Empties this PlayerManager by removing and destroying all PlayerInfos. **/
	public void Empty () {
		foreach (PlayerInfo playerInfo in this.PlayerInfos.Values) {
			if (playerInfo != null)
				playerInfo.Destroy ();
		}
		this.PlayerInfos.Clear ();
	}


	// ========== Destroy ==========
	/** Empties the destroys this PlayerManager. **/
	public void Destroy () {
		this.Empty ();
		if (this != null)
			GameObject.Destroy (this.gameObject);
	}


	// ========== Is Local ==========
	/** Is Local - Returns true if this Player Info belongs to the local client (includes the host client). **/
	public bool IsLocal () {
		if (NetworkManager.Get ().ServerSide ()) {
			return this.ConnectionID == NetworkManager.Get ().client.connection.connectionId;
		}
		return this.Local;
	}
}
