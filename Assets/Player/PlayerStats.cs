using UnityEngine;
using UnityEngine.Networking;

public class PlayerStats : NetworkBehaviour {
	
	// ========== Main Objects ==========
	public PlayerInfo Info;
	[SyncVar] public uint PlayerInfoNetID;
	public GameObject PlayerObject;
	public CreaturePlayer PlayerCreature;
	public SpawnPointBase SpawnPoint;
	public LevelBase Level;
	
	// ========== UI Objects ==========
	public GameObject PlayerHUDPrefab;
	public GameObject PlayerHUD;
	
	// ========== Main Stats ==========
	[SyncVar] public int Lives = 3;
	[SyncVar] public int Score = 0;
	private int ScorePowerupGoal = 100;
	[SyncVar] public float Multiplier = 1;
	[SyncVar] public int Kills = 0;
	[SyncVar] public int Deaths = 0;
	[SyncVar] public bool LevelExited = false;
	[SyncVar] public float RespawnTime = 0;
	[SyncVar] public float RespawnTimeMax = 3;
	[SyncVar] protected uint PlayerObjectIDSync = 0;
	protected uint PlayerObjectID = 0;
	
	
	// ========== Start ==========
	public virtual void Start () {
		this.Level = LevelBase.Get ();
		this.transform.SetParent (this.Level.transform, false);

		// Init UI:
		if (Game.Get ().CurrentGameType == Game.GameType.Level) {
			this.PlayerHUD = GameObject.Instantiate (this.PlayerHUDPrefab);
			InterfaceHUDPlayer playerHUD = this.PlayerHUD.GetComponent<InterfaceHUDPlayer> ();
			if (playerHUD != null) {
				playerHUD.playerStats = this;
			}
		}

		// Starting Spawn Point:
		if (this.SpawnPoint == null)
			this.SpawnPoint = this.Level.GetSpawnPoint (this);

		// Server:
		if (NetworkManager.Get ().ServerSide ()) {
			this.PlayerInfoNetID = this.Info.GetComponent <NetworkIdentity> ().netId.Value;
			if (this.Info.PlayerManager.IsLocal ())
				NetworkServer.Spawn (this.gameObject);
			else
				NetworkServer.SpawnWithClientAuthority (this.gameObject, this.Info.PlayerManager.Connection);
		}

		// Client:
		else {
			GameObject playerInfoObject = ClientScene.FindLocalObject (new NetworkInstanceId (this.PlayerInfoNetID));
			this.Info = playerInfoObject.GetComponent<PlayerInfo> ();
			if (this.Info != null) {
				this.Info.CurrentPlayerStats = this;
				if (this.Info.IsLocal ()) {
					LevelBase.Get ().LocalPlayers.Add (this);
					if (this.PlayerCreature != null)
						this.PlayerCreature.AssignLocalPlayer ();
				}
			}
		}
	}
	
	
	// ========== Update ==========
	public virtual void Update () {
		if (NetworkManager.Get ().ServerSide () && Game.Get ().CurrentGameType == Game.GameType.Level) {
			// Player Character Check:
			if (this.PlayerObject != null) {
				if (NetworkServer.active)
					this.PlayerObjectIDSync = this.PlayerObject.GetComponent<NetworkIdentity> ().netId.Value;
				CreatureBase playerPrefabCreature = this.Info.GetCharacterPrefab ().GetComponent<CreatureBase> ();
				if (this.PlayerCreature != null && playerPrefabCreature != null && this.PlayerCreature.CreatureName != playerPrefabCreature.CreatureName) {
					this.PlayerCreature.Death (true, true);
					this.PlayerObject = null;
					this.PlayerCreature = null;
				}
			}
			else {
				if (NetworkServer.active)
					this.PlayerObjectIDSync = 0;
			}

			// Player Spawning:
			if ((this.PlayerObject == null || this.PlayerCreature.Dead) && !this.LevelExited)
				this.RespawnUpdate ();

			// Score Multiplier:
			if (this.Multiplier > 1)
				this.Multiplier = Mathf.Max (this.Multiplier - (Time.deltaTime * this.Multiplier), 1);
		}

		// Network Client:
		if (!NetworkServer.active && NetworkClient.active) {
			if (this.PlayerObjectID != this.PlayerObjectIDSync) {
				if (this.PlayerObjectIDSync == 0) {
					this.PlayerObject = null;
					this.PlayerCreature = null;
				}
				else {
					this.PlayerObject = ClientScene.FindLocalObject (new NetworkInstanceId (this.PlayerObjectIDSync));
					this.PlayerCreature = this.PlayerObject.GetComponent<CreaturePlayer> ();
					this.PlayerObjectID = this.PlayerObjectIDSync;
				}
			}
		}
	}


	// ========== Respawn Update ==========
	/** Called on update if the PlayerObject is null (player has yet to spawn in a new game or has died and is waiting to respawn). **/
	public virtual void RespawnUpdate () {
		if (Game.Get ().CurrentGameType != Game.GameType.Level)
			return;

		if (this.RespawnTime > 0) {
			this.RespawnTime -= Time.deltaTime;
			return;
		}

		// Arena Checks:
		if (this.Level.CurrentLevelType == LevelBase.LevelType.Arena) {
			if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Elmination && this.Lives <= 0)
				return;
		}

		// Adventure Checks:
		else {
			if (this.Lives <= 0)
				return;
			if (!this.Level.AllPlayersDead) {
				if (this.Level.Stats.CurrentLevelState != LevelStats.LevelState.Normal)
					return;
				this.SpawnPoint = this.Level.UpdateSpawnPoint (this.SpawnPoint);
				if (!this.SpawnPoint.HasNearbyPlayer ())
					return;
			}
		}

		this.Spawn ();
		this.RespawnTime = this.RespawnTimeMax;
	}
	
	
	// ========== Spawn ==========
	public virtual void Spawn () {
		this.SpawnPoint = this.Level.UpdateSpawnPoint (this.SpawnPoint);
		if ((this.PlayerObject != null && !this.PlayerCreature.Dead) || this.SpawnPoint == null)
			return;
		this.PlayerObject = this.SpawnPoint.Spawn (this.Info.GetCharacterPrefab ());
		this.PlayerCreature = this.PlayerObject.GetComponent<CreaturePlayer> ();
		if (this.PlayerCreature == null)
			return;
		this.Level.OnPlayerSpawn ();
		this.PlayerCreature.PlayerStats = this;
		this.PlayerCreature.PlayerInfo = this.Info;
		this.PlayerCreature.TeamID = 1;
		if (this.Level.CurrentLevelType != LevelBase.LevelType.Adventure)
			this.PlayerCreature.TeamID = this.Info.Team;
		if (this.Level.Camera.FollowTarget != null) {
			CameraTarget cameraTarget = this.Level.Camera.FollowTarget.GetComponent<CameraTarget> ();
			if (cameraTarget != null) {
				this.PlayerCreature.TetherObject = cameraTarget.gameObject;
				this.PlayerCreature.TetherX = 50;
			}
		}

		if (this.PlayerHUD != null)
			this.PlayerHUD.GetComponent<InterfaceHUDPlayer> ().OnPlayerUpdate ();
	}
	
	
	// ========== Score ==========
	/* Increases the player's score by the specified amount. If multiply is true, the score multiplier will increase. */
	public void AddScore (int amount, bool multiply = true) {
		if (multiply)
			amount = Mathf.RoundToInt ((float)amount * this.Multiplier);
		this.Score += amount;
		if (multiply)
			this.Multiplier += 2;
		if (this.Score >= this.ScorePowerupGoal && this.Level.CurrentLevelType == LevelBase.LevelType.Arena) {
			PowerBase chargePower = this.PlayerCreature.Power.GetChargePower ();
			if (chargePower != null)
				this.PlayerCreature.Power.RechargePower (chargePower);
			else
				this.PlayerCreature.Power.AddPower (PowerManager.Get ().GetPower (PowerManager.PowerID.Blast));
			if (RulesManager.Get ().CurrentMatchType == RulesManager.MatchType.Points)
				this.ScorePowerupGoal += 100;
			else
				this.Score -= this.ScorePowerupGoal;
		}
	}


	// ========== Lives ==========
	/* Increases the player's lives by the specified amount. */
	public void AddLives (int amount) {
		this.Lives += amount;
	}


	// ========== Kills ==========
	/* Called when the player kills a creature, depending on the match type, rules and teams, the kill count increases. */
	public void AddKill (CreatureBase killedCreature) {
		CreaturePlayer killedPlayer = killedCreature.GetComponent<CreaturePlayer> ();
		if (killedPlayer != null && (killedPlayer.TeamID == 0 || killedPlayer.TeamID != this.Info.Team)) {
			this.Kills++;
		}
	}


	// ========== Death ==========
	/* Called when a player dies, reduces their lives and increases their deaths. Lives cannot go below 0. */
	public void OnDeath () {
		this.Lives = Mathf.Max (this.Lives - 1, 0);
		this.Deaths++;
		this.Level.OnPlayerDeath ();

		// Drop Blood Score Orbs (No Multiplier) If Arena:
		if (this.Score > 0 && this.Level.CurrentLevelType == LevelBase.LevelType.Arena) {
			int dropScore = Mathf.RoundToInt ((float)this.Score / 2);
			this.Score -= dropScore;
			this.PlayerCreature.DropPickups (this.Level.SingleScorePickupPrefab, dropScore);
		}
	}


	// ========== On Checkpoint ==========
	/** Called when a player activates a new checkpoint. **/
	public virtual void Checkpoint (SpawnPointBase spawnPoint) {
		this.SpawnPoint = spawnPoint;
		if (!Game.Get ().RuleSharedPlayerSpawn)
			return;
		foreach (PlayerStats player in this.Level.Players) {
			if (player == null)
				continue;
			player.SpawnPoint = spawnPoint;
		}
	}


	// ========== Level Exit ==========
	public void LevelExit (GameObject exitObject) {
		if (NetworkManager.Get ().ClientSide () || this.LevelExited)
			return;
		this.LevelExited = true;
		Destroy (this.PlayerObject);
		if (this.Deaths <= 0)
			this.Score *= 2;
	}


	// ========== Remove ==========
	public void Remove () {
		if (this.PlayerObject != null && this.PlayerCreature != null)
			this.PlayerCreature.Death (true, true);
		if (this.PlayerHUD != null)
			GameObject.Destroy (this.PlayerHUD);
		GameObject.Destroy (this.gameObject);
	}
}
